/*---------------------------------------------------------------------------------------------

[AS3] sysout
=======================================================================================

Copyright (c) 2015 UpCode Tecnologia

e   contato@upcode.net.br
w   http://upcode.net.br

Please retain this info when redistributed.

VERSION HISTORY:
v0.1    12/05/2015   Initial concept

USAGE:

// Display the out and save to log file.
sysout(...);

---------------------------------------------------------------------------------------------*/

package 
{
	import flash.filesystem.File;
	import flash.globalization.DateTimeFormatter;
	import flash.system.Capabilities;
	
	import mx.core.FlexGlobals;
	
	import br.com.desenv.framework.util.DsvObjectUtil;

	/**
	 * Display the out using trace(...) and save to log file on ApplicationStorageDirectory/sysout...
	 */
	public function sysout(... arguments):void 
	{
		var output:String = "";
		
		for each(var obj:* in arguments)
		{
			if(obj is String)
			{
				var message:* = (obj as String).split("\n");
				
				for(var lineIndex:int = 0x00; lineIndex < message.length; lineIndex++)
				{
					output += "[NPL] [" + new Date().toUTCString() + "] = L." + (lineIndex + 0x01) + " => " + message[lineIndex] + ((lineIndex + 0x01) < message.length ? " \n" : "");	
				}
			}
			else
				output += "[NPL] [" + new Date().toUTCString() + "] => " + obj;
		}

		if (Capabilities.isDebugger || !Capabilities.isDebugger && (FlexGlobals.topLevelApplication.parametrosSistema.getParametro("LOG_OUT") != null && (FlexGlobals.topLevelApplication.parametrosSistema.getParametro("LOG_OUT") as String) == "1"))
		{
			var dtf:DateTimeFormatter = new DateTimeFormatter("en-US");
			dtf.setDateTimePattern("yyyyMMdd");
			
			var file:File = File.documentsDirectory; 
			file = File.applicationStorageDirectory.resolvePath("out/sysout " + (dtf.format(FlexGlobals.topLevelApplication.applicationStartUpTime as Date)) + ".log");
			
			DsvObjectUtil.writeTextFile(file, output);	
		}
		
		trace(output);
	}
}