/*---------------------------------------------------------------------------------------------
	
	[AS3] syserr
	=======================================================================================
	
	Copyright (c) 2015 UpCode Tecnologia
	
	e   contato@upcode.net.br
	w   http://upcode.net.br
	
	Please retain this info when redistributed.
	
	VERSION HISTORY:
	v0.1    11/05/2015   Initial concept
	
	USAGE:
	
	// Display the error and save to log file.
	syserr(...);

---------------------------------------------------------------------------------------------*/


package 
{
	import flash.filesystem.File;
	import flash.globalization.DateTimeFormatter;
	import flash.system.Capabilities;
	
	import mx.core.FlexGlobals;
	import mx.rpc.events.FaultEvent;
	
	import br.com.desenv.framework.util.DsvObjectUtil;
	
	/**
	 * Display the error using trace(...) and save to log file on ApplicationStorageDirectory/syserr...
	 */
	public function syserr(... arguments):void 
	{
		var output:String = "\n";
		
		output += "[NPL] ====SYSERR====\n";
		output += "[NPL] [ERROR TIME] [" + new Date().toUTCString() + "] \n";
		
		for each(var obj:* in arguments)
		{
			if(obj is String)
			{
				output += "[NPL] [ERROR DESCRIPTION]\n";
				
				var error:* = (obj as String).split("\n");
				
				for(var lineIndex:int = 0x00; lineIndex < error.length; lineIndex++)
				{
					output += "[NPL] = L." + (lineIndex + 0x01) + " => " + error[lineIndex] + " \n";	
				}
				
				output += "[NPL] [END ERROR DESCRIPTION]\n";
			}
			else if(obj is Error) 
			{
				output += "[NPL] [ERROR DESCRIPTION]\n";
				
				output += "[NPL] =Error Code => " + (obj as Error).errorID;
				output += "\n[NPL] =Error Name => " + (obj as Error).name;
				output += "\n[NPL] =Error Message => " + (obj as Error).message;
				
				output += "\n[NPL] =Error Stack Trace => \n";
				
				var stacktraceError:* = (obj as Error).getStackTrace().split("\n");
				
				for(var traceLineError:String in stacktraceError)
				{
					output += "[NPL]			" + stacktraceError[traceLineError] + "\n"; 	
				}
			
				output += "\n[NPL] [END ERROR DESCRIPTION]\n";
			}
			else if(obj is FaultEvent)
			{
				output += "[NPL] [MESSAGE FAULT DESCRIPTION]\n";
				
				output += "[NPL] =Fault Code => " + (obj as FaultEvent).fault.faultCode;
				output += "\n[NPL] =Fault Name => " + (obj as FaultEvent).fault.name;
				output += "\n[NPL] =Fault Message => " + (obj as FaultEvent).fault.faultString;

				output += "\n[NPL] =Fault Stack Trace => \n";
				
				var stacktraceFault:* = (obj as FaultEvent).fault.getStackTrace().split("\n");
				
				for(var traceLineFault:String in stacktraceFault)
				{
					output += "[NPL]			" + stacktraceFault[traceLineFault] + "\n"; 	
				}
				
				output += "[NPL] [END FAULT DESCRIPTION]\n";				
			}
		}
		
		output += "[NPL] ======================\n";
		
		if (Capabilities.isDebugger || !Capabilities.isDebugger && (FlexGlobals.topLevelApplication.parametrosSistema.getParametro("LOG_ERR") != null && (FlexGlobals.topLevelApplication.parametrosSistema.getParametro("LOG_ERR") as String) == "1"))
		{
			var dtf:DateTimeFormatter = new DateTimeFormatter("en-US");
			dtf.setDateTimePattern("yyyyMMdd");
			
			var file:File = File.documentsDirectory; 
			file = File.applicationStorageDirectory.resolvePath("err/syserr " + (dtf.format(FlexGlobals.topLevelApplication.applicationStartUpTime as Date)) + ".log");
			
			DsvObjectUtil.writeTextFile(file, output);
		}

		trace(output);
	}
}