package br.com.desenv.nepalign.cockpit.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.service.cockpit.CockPitRetornoClassificacaoVendedor")]
	public class CockPitRetornoClassificacaoVendedor
	{
		public var idVendedor:Number;
		public var codigoVendedor:Number;
		public var nomeVendedor:String;
		public var valorVenda:Number;
		public var diferencaAcima:Number;
		public var diferencaAbaixo:Number;
		public var colocacao:Number;
		
		public function CockPitRetornoClassificacaoVendedor()
		{
			
		}
		
		public function get nomeVendedorCodigoConcatenado():String
		{
			return codigoVendedor.toString() + " - " + nomeVendedor;
		}
	}	
}