package br.com.desenv.nepalign.cockpit.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.service.cockpit.CockPitParametrosClassificacaoVendedor")]	
	public class CockPitParametrosClassificacaoVendedor
	{
		public var listaEmpresasFisicas:String;
		public var dataInicial:Date;
		public var dataFinal:Date;
		public var idVendedor:Number;
		
		public function CockPitParametrosClassificacaoVendedor()
		{
			
		}
	}	
}