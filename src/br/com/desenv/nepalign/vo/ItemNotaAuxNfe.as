package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemNotaAuxNfe")]
	public class ItemNotaAuxNfe 
	{
		public var id: Number = NaN;
		public var itemNotaFiscal: ItemNotaFiscal;
		public var flgItemImportado: Number = NaN;
		public var flgItemCombustivel: Number = NaN;
		public var flgGrupoCombIncCIDE: Number = NaN;
		public var flgItemMedicamento: Number = NaN;
		public var flgItemVeiculo: Number = NaN;
		public var flgICMSIncide: Number = NaN;
		public var flgIPIIncide: Number = NaN;
		public var flgIIincide: Number = NaN;
		public var flgPISIncide: Number = NaN;
		public var flgPISSTIncide: Number = NaN;
		public var flgCOFINSIncide: Number = NaN;
		public var flgCOFINSSTIncide: Number = NaN;
		public var flgISSQNIncide: Number = NaN;
		public var observacao: String;
		public function ItemNotaAuxNfe()
		{
		}
		
		public function get flgItemImportadoDescricaoCompleta(): String
		{
			if (this.flgItemImportado == 0)
			{
				return "Nao";
			}
			else if (this.flgItemImportado == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgItemCombustivelDescricaoCompleta(): String
		{
			if (this.flgItemCombustivel == 0)
			{
				return "Nao";
			}
			else if (this.flgItemCombustivel == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgGrupoCombIncCIDEDescricaoCompleta(): String
		{
			if (this.flgGrupoCombIncCIDE == 0)
			{
				return "Nao";
			}
			else if (this.flgGrupoCombIncCIDE == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgItemMedicamentoDescricaoCompleta(): String
		{
			if (this.flgItemMedicamento == 0)
			{
				return "Nao";
			}
			else if (this.flgItemMedicamento == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgItemVeiculoDescricaoCompleta(): String
		{
			if (this.flgItemVeiculo == 0)
			{
				return "Nao";
			}
			else if (this.flgItemVeiculo == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgICMSIncideDescricaoCompleta(): String
		{
			if (this.flgICMSIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgICMSIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgIPIIncideDescricaoCompleta(): String
		{
			if (this.flgIPIIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgIPIIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgIIincideDescricaoCompleta(): String
		{
			if (this.flgIIincide == 0)
			{
				return "Nao";
			}
			else if (this.flgIIincide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgPISIncideDescricaoCompleta(): String
		{
			if (this.flgPISIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgPISIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgPISSTIncideDescricaoCompleta(): String
		{
			if (this.flgPISSTIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgPISSTIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgCOFINSIncideDescricaoCompleta(): String
		{
			if (this.flgCOFINSIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgCOFINSIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgCOFINSSTIncideDescricaoCompleta(): String
		{
			if (this.flgCOFINSSTIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgCOFINSSTIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgISSQNIncideDescricaoCompleta(): String
		{
			if (this.flgISSQNIncide == 0)
			{
				return "Nao";
			}
			else if (this.flgISSQNIncide == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

