package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Departamento")]
	public class Departamento 
	{
		public var id: int;
		public var descricao: String;
		public var departamentoPai: Departamento;
		public var situacaoEnvioLV: SituacaoEnvioLV;
		public var ativo: String;
		public var codigo: String;
		public var ordemDisposicao: int;
		public var descricaoCompleta: String;
		public var idDepCache: int;
		public var idSubDepCache: int;
		public function Departamento()
		{
		}
		
		public function get ativoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "S")
			{
				return "SIM";	
			}
			else if (this.ativo == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

