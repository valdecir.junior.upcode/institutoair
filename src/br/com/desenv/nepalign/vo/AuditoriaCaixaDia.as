package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AuditoriaCaixaDia")]

	public var caixadia: String;
	public var formapagamento: String;
	public var valorParcial: Number = NaN;
	public var valorInformado: Number = NaN;
	public var valorDivergencia: Number = NaN;
	
	public class AuditoriaCaixaDia
	{
		public function AuditoriaCaixaDia()
		{
		}
	}
}