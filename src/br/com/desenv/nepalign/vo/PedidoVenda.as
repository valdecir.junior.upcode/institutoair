package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PedidoVenda")]
	public class PedidoVenda 
	{
		public var id: int;
		public var tipoMovimentacaoEstoque: TipoMovimentacaoEstoque;
		public var empresaFisica: EmpresaFisica;
		public var vendedor: Vendedor;
		public var cliente: Cliente;
		public var usuario: Usuario;
		public var situacaoPedidoVenda: SituacaoPedidoVenda;
		public var planoPagamento: PlanoPagamento;
		public var metodoPagamento: MetodoPagamento;
		public var cidadeEntrega: Cidade;
		public var ufEntrega: Uf;
		public var paisEntrega: Pais;
		public var formaEntrega: FormaEntrega;
		public var statusPedido: StatusPedido;
		public var tipoMeioPagamento: MeioPagamento;
		public var tipoLogradouroEnderecoEntrega: TipoLogradouro;
		public var dataVenda: Date;
		public var descontoTotal: Number = 0.0;
		public var numeroControle: String;
		public var valorTotalPedido: Number = NaN;
		public var codigoPedidoEnviadoCliente: String;
		public var valorTotalProdutosPedidos: Number = 0.0;
		public var valorTotalCustoFrete: Number = 0.0;
		public var valorTotalDescProdutos: Number = 0.0;
		public var valorTotalDescFrete: Number = 0.0;
		public var numParcelas: int;
		public var valorParcela: Number = 0.0;
		public var totalDescPedido: Number = 0.0;
		public var dataExpiracaoPedido: Date;
		public var logradouroEnderecoEntrega: String;
		public var bairroEnderecoEntrega: String;
		public var numEnderecoEntrega: String;
		public var complementoEnderecoEntrega: String;
		public var cepEnderecoEntrega: String;
		public var numTelefone: String;
		public var dddTelefone: String;
		public var dataEntregaPedido: Date;
		public var valorEmbalagemPresente: Number = 0.0;
		public var valorJuros: Number = 0.0;
		public var valorCupomDesconto: Number = 0.0;
		public var numCupomDesconto: int;
		public var referenciaEnderecoEntrega: String;
		public var observacao: String;
		public var usuarioAutorizador: Usuario;
		public var selecionado:Boolean;
		public var codigoExterno:int;
		public var destinatarioPedido:String;
		public var codigoPlano:String;
		
		public function PedidoVenda()
		{
		}
		
		public function get dataFormatada() :String
		{
			const d = '.';
			const e = '';
			const s = ':';
			const b = '/';
			
			var date:Date = this.dataVenda;
			
			var resultDate:String = "";
			
			resultDate +=  e.concat(pad(date.date, 2), b, pad(date.month, 2), b,  pad(date.fullYear, 2));
			resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));
			
			
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}	
		public function get cepEnderecoEntregaFormatado(): String
		{
			if (this.cepEnderecoEntrega == null)
			{
				return this.cepEnderecoEntrega;
			}	
			else
			{
				return Formatador.formatarCep(this.cepEnderecoEntrega);
			}		
		}
		public function get numTelefoneFormatado(): String
		{
			if (this.numTelefone == null)
			{
				return this.numTelefone;
			}	
			else
			{
				return Formatador.formatarFone(this.numTelefone);
			}		
		}
		
		public function get pedidoVertis():Boolean
		{
			return (this.empresaFisica.id == 10 && !isNaN(this.codigoExterno));
		}
		
		public function get recuperarValorDesconto():String
		{
			var valorDesconto: Number;

			if(this.valorTotalPedido < this.valorTotalProdutosPedidos)
			{
				var valorPlano:Number = (this.valorTotalProdutosPedidos)*(this.planoPagamento.percentual/100)
				if(this.planoPagamento.tipoPercentual=="-")
				{
					valorDesconto = (this.valorTotalProdutosPedidos - valorPlano) - this.valorTotalPedido;
				}
				else if(this.planoPagamento.tipoPercentual=="+")
				{
				  valorDesconto = (this.valorTotalProdutosPedidos + valorPlano) - this.valorTotalPedido;
				}
				
			}
			return Formatador.formatarDouble(valorDesconto,2);
		}
		public function get recuperarValorAcrecimo():String
		{
			var valor: Number;
			
			if(this.valorTotalPedido > this.valorTotalProdutosPedidos)
			{
				var valorPlano:Number = (this.valorTotalProdutosPedidos)*(this.planoPagamento.percentual/100)
				if(this.planoPagamento.tipoPercentual=="-")
				{
					valor = this.valorTotalPedido -(this.valorTotalProdutosPedidos - valorPlano)  ;
				}
				else if(this.planoPagamento.tipoPercentual=="+")
				{
					valor = this.valorTotalPedido -(this.valorTotalProdutosPedidos + valorPlano) ;
				}
				
			}
			return Formatador.formatarDouble(valor,2);
		}
		public function get valorTotalPedidoFormatado():String
		{
			return Formatador.formatarDouble(valorTotalPedido,2);
		}
		
	}
}

