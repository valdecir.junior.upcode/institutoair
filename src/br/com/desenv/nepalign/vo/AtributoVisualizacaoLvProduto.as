package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AtributoVisualizacaoLvProduto")]
	public class AtributoVisualizacaoLvProduto 
	{
		public var id: Number = NaN;
		public var atributoVisualizacaoLv: AtributoVisualizacaoLv;
		public var estoqueProduto: EstoqueProduto;
		public var situacaoEnvioLv: SituacaoEnvioLV;
		public function AtributoVisualizacaoLvProduto()
		{
		}
		
		
	}
}

