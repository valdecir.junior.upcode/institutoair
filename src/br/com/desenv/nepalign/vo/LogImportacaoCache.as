package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LogImportacaoCache")]
	public class LogImportacaoCache 
	{
		public var id: int;
		public var dataHora: Date;
		public var tipoOperacaoLV: TipoOperacaoLV;
		public var descricaoErro: String;
		public function LogImportacaoCache()
		{
		}
		
		
	}
}

