package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemNotaFiscal")]
	public class ItemNotaFiscal 
	{
		public var id: Number = NaN;
		public var notaFiscal: NotaFiscal;
		public var produto: Produto;
		public var quantidade: Number = NaN;
		public var unidadeMedida: UnidadeMedida;
		public var valorUnitario: Number = NaN;
		public var valorTotal: Number = NaN;
		public var aliquotaICMS: Number = NaN;
		public var aliquotaIPI: Number = NaN;
		public var valorIPI: Number = NaN;
		public var lote: String;
		public var validade: Date;
		public var itemMovimentacaoEstoque: ItemMovimentacaoEstoque;
		public var percPis: Number = NaN;
		public var percCofins: Number = NaN;
		public var percCsll: Number = NaN;
		public var percIrrf: Number = NaN;
		public var percReducaoBaseCalc: Number = NaN;
		public var margemLucro: Number = NaN;
		public var codigoSituacaoTributaria: CodigoSituacaoTributaria;
		public var origemMercadoria: OrigemMercadoria;
		public var valorFrete: Number = NaN;
		public var valorSeguro: Number = NaN;
		public var dataFabricacao: Date;
		public var valorBaseCalcICMS: Number = NaN;
		public var valorBaseCalcICMSST: Number = NaN;
		public var valorICMS: Number = NaN;
		public var valorICMSST: Number = NaN;
		public var pMC: Number = NaN;
		public var percReducaoBaseCalcST: Number = NaN;
		public var codigoCFOP: String;
		public var sequenciaItem: Number = NaN;
		public var cstPis: Number = NaN;
		public var cstCofins: Number = NaN;
		public var valorDesconto: Number = NaN;
		public var cfop: Cfop;
		public var codigoSituacaoTributariaICMS: CodigoSituacaoTributaria;
		public var codigoSituacaoTributariaCOFINS: Cstcofins;
		public var codigoSituacaoTributariaPIS: Cstpis;
		public var aliquotaCOFINS: Number = NaN;
		public var fabricacao: Date;
		public var aliquotaPIS: Number = NaN;
		public function ItemNotaFiscal()
		{
		}
		public function get produtoCompleto():String
		{
			return this.produto.id + "-" + this.produto.nomeProduto;
				
		}
		
		public function get cfopCompleto():String
		{
			return this.cfop.codigo + "-" + this.cfop.descricao;
		}
		
		public function get icmsCompleto():String
		{
			return this.codigoSituacaoTributariaICMS.codigo + "-" + this.codigoSituacaoTributariaICMS.descricao;
		}
		
		public function get pisCompleto():String
		{
			return this.codigoSituacaoTributariaPIS.codigo + "-" + this.codigoSituacaoTributariaPIS.descricao;
		}
		
		public function get cofinsCompleto():String
		{
			return this.codigoSituacaoTributariaCOFINS.codigo + "-" + this.codigoSituacaoTributariaCOFINS.descricao;
		}
	}
}

