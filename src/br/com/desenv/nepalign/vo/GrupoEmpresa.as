package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.GrupoEmpresa")]
	public class GrupoEmpresa 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public function GrupoEmpresa()
		{
		}
		
		
	}
}

