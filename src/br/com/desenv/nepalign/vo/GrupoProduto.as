package br.com.desenv.nepalign.vo
{
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.GrupoProduto")]
	public class GrupoProduto 
	{
		public var id: int;
		public var descricao: String;
		public var ultimaAlteracao: Date;
		public var codigo: int;
		public var ncm:String;
		
		public function GrupoProduto()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return codigo + " - " + descricao;
		}
		
		
	}
}

