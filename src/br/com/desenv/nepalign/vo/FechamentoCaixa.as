package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.FechamentoCaixa")]
	public class FechamentoCaixa 
	{
		public var id: Number = NaN;
		public var caixadia: CaixaDia;
		public var dataFechamento: Date;
		public var valorCalculado: Number = NaN;
		public var valorInformado: Number = NaN;
		public function FechamentoCaixa()
		{
		}
		
		
	}
}

