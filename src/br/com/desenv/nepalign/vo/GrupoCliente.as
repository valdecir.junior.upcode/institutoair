package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.GrupoCliente")]
	public class GrupoCliente 
	{
		public var id: Number = NaN;
		public var tabelaPreco: TabelaPreco;
		public var descricao: String;
		public function GrupoCliente()
		{
		}
		
		
	}
}

