package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Impressora")]
	public class Impressora 
	{
		public var id: int;
		public var nome: String;
		public var caminho: String;
		public var descricao: String;
		public var caixa: String;
		public var pdv: String;
		public var fiscal: String;
		public var crediario: String;
		public var ip:String;
		public var matricial:String;
		
		public function Impressora()
		{
		}
		
		public function get caixaDescricaoCompleta(): String
		{
			if (this.caixa == null)
			{
				return this.caixa;
			}
			else if (this.caixa == "S")
			{
				return "SIM";	
			}
			else if (this.caixa == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get pdvDescricaoCompleta(): String
		{
			if (this.pdv == null)
			{
				return this.pdv;
			}
			else if (this.pdv == "S")
			{
				return "SIM";	
			}
			else if (this.pdv == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get fiscalDescricaoCompleta(): String
		{
			if (this.fiscal == null)
			{
				return this.fiscal;
			}
			else if (this.fiscal == "S")
			{
				return "SIM";	
			}
			else if (this.fiscal == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get crediarioDescricaoCompleta(): String
		{
			if (this.crediario == null)
			{
				return this.crediario;
			}
			else if (this.crediario == "S")
			{
				return "SIM";	
			}
			else if (this.crediario == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get matricialDescricaoCompleta(): String
		{
			if (this.matricial == null)
			{
				return "NÃO";
			}
			else if (this.matricial == "S")
			{
				return "SIM";	
			}
			else if (this.caixa == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "NÃO";
			}
		}
		
	}
}

