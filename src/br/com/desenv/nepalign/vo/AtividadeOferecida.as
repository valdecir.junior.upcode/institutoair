package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AtividadeOferecida")]
	public class AtividadeOferecida 
	{
		public var id: int;
		public var descricao: String;
		public var manha: String;
		public var tarde: String;
		public var noite: String;
		public var observacao: String;
		public var duracao: int;
		public function AtividadeOferecida()
		{
		}
		
		public function get manhaDescricaoCompleta(): String
		{
			if (this.manha == null)
			{
				return this.manha;
			}
			else if (this.manha == "N")
			{
				return "Não";	
			}
			else if (this.manha == "S")
			{
				return "Sim";	
			}
			else
			{
				return "";
			}
		}
		public function get tardeDescricaoCompleta(): String
		{
			if (this.tarde == null)
			{
				return this.tarde;
			}
			else if (this.tarde == "N")
			{
				return "Não";	
			}
			else if (this.tarde == "S")
			{
				return "Sim";	
			}
			else
			{
				return "";
			}
		}
		public function get noiteDescricaoCompleta(): String
		{
			if (this.noite == null)
			{
				return this.noite;
			}
			else if (this.noite == "N")
			{
				return "Não";	
			}
			else if (this.noite == "S")
			{
				return "Sim";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

