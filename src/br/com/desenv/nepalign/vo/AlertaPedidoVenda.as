package br.com.desenv.nepalign.vo
{

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AlertaPedidoVenda")]
	public class AlertaPedidoVenda 
	{
		public var id: int;
		public var itemPedidoVenda: ItemPedidoVenda;
		public var empresaOrigemAlerta: EmpresaFisica;
		public var statusAlerta: StatusAlerta;
		public var empresaVencedora: EmpresaFisica;
		public var dataAlerta: Date;
		public var dataResposta: Date;
		public var usuario: Usuario;
		public var observacao: String;
		public var selecionado:Boolean;
		public var flag:String;
		public function AlertaPedidoVenda()
		{
		}
		
		public function get dataFormatada() :String
		{
			const d:String = '.';
			const e:String = '';
			const s:String = ':';
			const b:String = '/';
			
			var date:Date = this.dataAlerta;
			var resultDate:String = "";
			
			resultDate +=  e.concat(pad(date.date, 2), b, pad(date.month + 1, 2), b,  pad(date.fullYear, 2));
			resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));
					
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero:String = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}	
	}
}