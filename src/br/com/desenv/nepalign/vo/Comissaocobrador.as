package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ComissaoCobrador")]
	public class ComissaoCobrador 
	{
		public var id: Number = NaN;
		public var duplicataParcela: DuplicataParcela;
		public var chequeRecebido: ChequeRecebido;
		public var cobrador: Cobrador;
		public var percentual: Number = NaN;
		public var dataPagamento: Date;
		public var dataPagamentoComissao: Date;
		public var valorRecebido: Number = NaN;
		public var valorComissao: Number = NaN;
		public function ComissaoCobrador()
		{
		}
		
		
	}
}

