package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Professor")]
	public class Professor 
	{
		public var id: int;
		public var nome: String;
		public var foneContato: String;
		public var email: String;
		public function Professor()
		{
		}
		
		
	}
}

