package br.com.desenv.nepalign.vo
{
	import mx.formatters.DateFormatter;
	import mx.formatters.Formatter;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PagamentoDocumentoEntradaSaida")]
	public class PagamentoDocumentoEntradaSaida 
	{
		public var id: Number;
		public var empresa: EmpresaFisica;
		public var documentoEntradaSaida: DocumentoEntradaSaida;
		public var formaPagamento: FormaPagamento;
		public var entradaSaida: String;
		public var numeroFatura: String;
		public var dataVencimento: Date;
		public var valor: Number;
		public var situacao: String;
		public var observacao: String;
		public function PagamentoDocumentoEntradaSaida()
		{
		}
		
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (this.entradaSaida == null)
			{
				return this.entradaSaida;
			}
			else if (this.entradaSaida == "E")
			{
				return "Entrada";	
			}
			else if (this.entradaSaida == "S")
			{
				return "Saída";	
			}
			else
			{
				return "";
			}
		}
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "1")
			{
				return "Digitado";	
			}
			else if (this.situacao == "2")
			{
				return "Revisado";	
			}
			else
			{
				return "";
			}
		}
		
		public function get dataVencimentoFormatado()
		{
			return Formatador.formatarDate(this.dataVencimento);
		}
		
		public function get valorFormatado()
		{
			return Formatador.formatarDouble(this.valor, 2);
		}
		
		
	}
}

