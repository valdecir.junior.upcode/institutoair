package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EstoqueProduto")]
	public class EstoqueProduto 
	{
		public var id: Number = NaN;
		public var tamanho: Tamanho;
		public var cor: Cor;
		public var produto: Produto;
		public var ordemProduto: OrdemProduto;
		public var situacaoEnvioLV: SituacaoEnvioLV;
		public var codigoBarras:String;
		public var observacao: String;
		public function EstoqueProduto()
		{
		}
		public function get estoqueProdutoFormatado() :String
		{
			var descricao:String = new String();
			descricao =""+ produto.nomeProduto + " - " + cor.descricaoCorEmpresa + " - " + tamanho.descricao + " - " + ordemProduto.descricao + " - " + produto.codigoProduto;
			
			return descricao;
		}
		
	}
}

