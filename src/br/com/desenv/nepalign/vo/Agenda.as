package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	


	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Agenda")]
	public class Agenda 
	{
		public var id: int;
		public var nome: String;
		public var operadoraTelefoneComercial: Operadora;
		public var telefoneComecial: String;
		public var operadoraTelefoneResidencial: Operadora;
		public var telefoneResidencial: String;
		public var operadoraCelular1: Operadora;
		public var celular1: String;
		public var operadoraCelular2: Operadora;
		public var celular2: String;
		public var emailComercial: String;
		public var emailPessoal: String;
		public var cidade: String;
		public var foto: ByteArray;
		public function Agenda()
		{
		}
		
		public function get telefoneComecialFormatado(): String
		{
			if (this.telefoneComecial == null)
			{
				return this.telefoneComecial;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneComecial);
			}		
		}
		public function get telefoneResidencialFormatado(): String
		{
			if (this.telefoneResidencial == null)
			{
				return this.telefoneResidencial;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneResidencial);
			}		
		}
		public function get celular1Formatado(): String
		{
			if (this.celular1 == null)
			{
				return this.celular1;
			}	
			else
			{
				return Formatador.formatarFone(this.celular1);
			}		
		}
		public function get celular2Formatado(): String
		{
			if (this.celular2 == null)
			{
				return this.celular2;
			}	
			else
			{
				return Formatador.formatarFone(this.celular2);
			}		
		}
		public function get operadoraCelularFormatado(): String
		{
			if (this.celular1 == null)
			{
				return this.celular1;
			}	
			else
			{
				return "("+operadoraCelular1.codigo+")"+Formatador.formatarFone(this.celular1);
			}		
		}
		public function get operadoraTelefoneComercialFormatado(): String
		{
			if (this.telefoneComecial == null)
			{
				return this.telefoneComecial;
			}	
			else
			{
				return "("+operadoraTelefoneComercial.codigo+")"+Formatador.formatarFone(this.telefoneComecial);
			}		
		}
		public function get operadoraTelefoneResidencialFormatado(): String
		{
			if (this.telefoneResidencial == null)
			{
				return this.telefoneResidencial;
			}	
			else
			{
				return "("+operadoraTelefoneResidencial.codigo+")"+Formatador.formatarFone(this.telefoneResidencial);
			}		
		}
		
		
		
	}
}

