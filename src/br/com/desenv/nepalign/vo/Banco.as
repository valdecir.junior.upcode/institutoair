package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Banco")]
	public class Banco 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public function Banco()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return this.codigo + "-" + this.descricao;
		}
	}
}

