package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Creditocliente")]
	public class Creditocliente 
	{
		public var id: Number = NaN;
		public var cliente: Cliente;
		public var idDocumentoOrigem: String;
		public var valor: Number = NaN;
		public var dataDocumento: Date;
		public var numeroDocumento: Number = NaN;
		public var valorUtilizado: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var observacao: String;
		public var origem: String;
		public function Creditocliente()
		{
		}
		
		
	}
}

