package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.VitrineLvProduto")]
	public class VitrineLvProduto 
	{
		public var id: Number = NaN;
		public var vitrineLv: VitrineLv;
		public var estoqueProduto: EstoqueProduto;
		public var produtoDestaque:String;
		public var ordemExibicao:Number = NaN;
		public var situacaoEnvioLv: SituacaoEnvioLV;
		
		public function VitrineLvProduto()
		{
		}
		
		public function get produtoDestaqueDescricaoCompleta():String
		{
			return (this.produtoDestaque == "0" ? "Não" : "Sim");
		}
		
		public function get estoqueProdutoDescricaoCompleta():String
		{
			return this.estoqueProduto.codigoBarras + " - " + this.estoqueProduto.produto.nomeProduto;
		}
	}
}

