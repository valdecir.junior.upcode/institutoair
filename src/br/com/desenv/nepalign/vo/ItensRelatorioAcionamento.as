package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItensRelatorioAcionamento")]
	public class ItensRelatorioAcionamento 
	{
		public var id: Number = NaN;
		public var duplicataParcela: DuplicataParcela;
		public var cliente: Cliente;
		public var dataAcionamento: Date;
		public var empresaFisica: EmpresaFisica;
		public function ItensRelatorioAcionamento()
		{
		}
		
		
	}
}

