package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ChequeEmitidoLancamento")]
	public class ChequeEmitidoLancamento 
	{
		public var id: Number = NaN;
		public var lancamentoContaPagar: LancamentoContaPagar;
		public var chequeEmitido: ChequeEmitido;
		public function ChequeEmitidoLancamento()
		{
		}
		
		
	}
}

