package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.util.DsvObjectUtil;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemMovimentacaoEstoque")]
	public class ItemMovimentacaoEstoque 
	{
		public var id: Number = NaN;
		public var estoqueProduto: EstoqueProduto;
		public var movimentacaoEstoque: MovimentacaoEstoque;
		public var unidadeMedida: UnidadeMedida;
		public var produto: Produto;
		public var documentoOrigem: String;
		public var sequencialItem: int;
		public var quantidade: Number = 0.0;
		public var valor: Number = 0.0;
		public var custo: Number = 0.0;
		public var venda: Number = 0.0;
		public var numeroLote: String;
		public var dataValidade: Date;
		public var dataFabricacao: Date;
		
		public function ItemMovimentacaoEstoque()
		{
		}
		
		public function get valorTotalCusto():Number
		{
			var valorTotalCusto:Number = this.quantidade * this.custo;
			return DsvObjectUtil.round(valorTotalCusto, 2);
		}
		
		public function get valorTotalVenda():Number
		{
			var valorTotalVenda:Number = this.quantidade * this.venda;
			return DsvObjectUtil.round(valorTotalVenda, 2);
		}		
	}
}