package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CartaCorrecao")]
	public class CartaCorrecao 
	{
		public var id: Number = NaN;
		public var cnpjEmitente: String;
		public var serieNotaFiscal: String;
		public var numeroNotaFiscal: Number = NaN;
		public var textoCarta: String;
		public var identificador: Number = NaN;
		public var dataHoraRec: Date;
		public var protocolo: String;
		public var motivoStatus: String;
		public var status: Number = NaN;
		public var codEvento:Number = NaN;
		public function CartaCorrecao()
		{
		}
		
		public function get cnpjEmitenteFormatado(): String
		{
			if (this.cnpjEmitente == null)
			{
				return this.cnpjEmitente;
			}	
			else
			{
				return Formatador.formatarCnpj(this.cnpjEmitente);
			}		
		}
		public function get identificadorDescricaoCompleta(): String
		{
			if (isNaN(this.identificador))
			{
				return this.identificador.toString();
			}
			else if (this.identificador == 0)
			{
				return "Sefaz";	
			}
			else if (this.identificador == 1)
			{
				return "Receita Federal";	
			}
			else
			{
				return "";
			}
		}
		
		public function get motivoStatusFormatado():String
		{
			return motivoStatus.substring(0,99);
		}
		
	}
}

