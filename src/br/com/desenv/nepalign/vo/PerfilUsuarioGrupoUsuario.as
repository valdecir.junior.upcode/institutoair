package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PerfilUsuarioGrupoUsuario")]
	public class PerfilUsuarioGrupoUsuario 
	{
		public var id: Number = NaN;
		public var perfilUsuario: PerfilUsuario;
		public var grupo: Grupo;
		public function PerfilUsuarioGrupoUsuario()
		{
		}
		
		
	}
}

