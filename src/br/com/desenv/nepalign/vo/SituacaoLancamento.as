package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SituacaoLancamento")]
	public class SituacaoLancamento 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var considerarBaixado: Number = NaN;
		public function SituacaoLancamento()
		{
		}
		
		
	}
}

