package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SaldoCaixa")]
	public class SaldoCaixa
	{
		public var id: Number = NaN;
		public var empresaCaixa:EmpresaCaixa = null;
		public var caixa:Caixa = null;
		public var mes:Number = NaN;
		public var ano:Number = NaN;
		public var saldo:Number = NaN;
		
		public function SaldoBancos()
		{
		}
	}
}