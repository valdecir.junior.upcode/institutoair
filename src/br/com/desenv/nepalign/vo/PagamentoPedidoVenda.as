package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PagamentoPedidoVenda")]
	public class PagamentoPedidoVenda 
	{
		public var id: Number = NaN;
		public var pedidoVenda: PedidoVenda;
		public var formaPagamento: FormaPagamento;
		public var lancamentoContaReceber: LancamentoContaReceber;
		public var dataPagamento: Date;
		public var chequeDataEmetido: Date;
		public var valor: Number = NaN;
		public var numeroDocumento: String;
		public var numeroCheque: String;
		public var cnpjCpf: String;
		public var numeroDuplicata: String;
		public var situacaoLancamento: Number = NaN;
		public var idNotaFiscal: String;
		public var cpfCheque: String;
		public var nomeCheque: String;
		public var bancoCheque: String;
		public var agenciaCheque: String;
		public var controlePagamentoCartao: String;
		public var itemSequencial:String;
		public var usuario:Usuario;
		public var chequeRecebido:ChequeRecebido;
		public var caixaDia:CaixaDia;
		public var numeroParcelas:Number = NaN;
		public var pagamentoCartao:PagamentoCartao;
		public var operadoraCartao:OperadoraCartao;
		public var entrada:String;
		public var destaque:Boolean = false;
		public function PagamentoPedidoVenda()
		{
		}
		public function get entradaDescricaoCompleta():String
		{
			if(entrada=="S")
			{
				return "SIM"
			}
			else if(entrada=="N")
			{
				return "NÃO"
			}
			else
			{
				return "NÃO"
			}
		}
		public function get cpfChequeFormatado(): String
		{
			if (this.cpfCheque == null)
			{
				return this.cpfCheque;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cpfCheque);
			}		
		}
		public function get dataFormatada() :String
		{
			const d = '.';
			const e = '';
			const s = ':';
			const b = '/';
			
			var date:Date = this.dataPagamento;
			
			var resultDate:String = "";
			
			resultDate +=  e.concat(pad(date.date, 2), b, pad(date.month, 2), b,  pad(date.fullYear, 2));
			//resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));
			
			
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}
		public function get dataEmissaoFormatada() :String
		{
			const d = '.';
			const e = '';
			const s = ':';
			const b = '/';
			
			var date:Date = this.chequeDataEmetido;
			
			var resultDate:String = "";
			
			resultDate +=  e.concat(pad(date.date, 2), b, pad(date.month, 2), b,  pad(date.fullYear, 2));
			//resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));
			
			
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}	
		
	}
}

