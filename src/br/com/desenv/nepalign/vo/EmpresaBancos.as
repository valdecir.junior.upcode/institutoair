package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EmpresaBancos")]
	public class EmpresaBancos
	{
		public var id:Number = NaN;
		public var nomeEmpresa:String;
		
		public function EmpresaBancos()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return id.toString() + " - " + nomeEmpresa;
		}
	}	
}