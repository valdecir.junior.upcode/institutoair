package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Usuario")]
	public class Usuario 
	{
		public var id: int;
		public var nome: String;
		public var senha: String;
		public var login: String;
		public var usuarioGlobal: String;
		public var usuarioMaster: String;
		public var usuarioCaixa: String;
		public var foto: ByteArray;
		public var empresaPadrao:Empresa;
		public var operadora:Operadora;
		public var telefone:String;
		public var usuarioDesenv:String;
		public var perfilUsuario:PerfilUsuario;
		public var ativo:String;
		public function Usuario()
		{
		}
		
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		
		public function get usuarioGlobalDescricaoCompleta(): String
		{
			if (this.usuarioGlobal == null)
			{
				return this.usuarioGlobal;
			}
			else if (this.usuarioGlobal == "S")
			{
				return "SIM";	
			}
			else if (this.usuarioGlobal == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get usuarioMasterDescricaoCompleta(): String
		{
			if (this.usuarioMaster == null)
			{
				return this.usuarioMaster;
			}
			else if (this.usuarioMaster == "S")
			{
				return "SIM";	
			}
			else if (this.usuarioMaster == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get usuarioCaixaDescricaoCompleta(): String
		{
			if (this.usuarioCaixa == null)
			{
				return this.usuarioCaixa;
			}
			else if (this.usuarioCaixa == "S")
			{
				return "SIM";	
			}
			else if (this.usuarioCaixa == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get usuarioAtivoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "S")
			{
				return "SIM";	
			}
			else if (this.ativo == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
	}
}

