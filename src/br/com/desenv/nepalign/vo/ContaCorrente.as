package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ContaCorrente")]
	public class ContaCorrente 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var banco: Banco;
		public var dataAberturaConta: Date;
		public var descricao: String;
		public var numeroAgencia: String;
		public var numeroConta: String;
		public var telefone: String;
		public var fax: String;
		public var limite: Number = NaN;
		public var vencimentoLimite: Date;
		public var anoMesAberto: String;
		public var saldoInicial: Number = NaN;
		public var gerente: String;
		public var instrucaoBoleto: String;
		public var carteiraCobranca: String;
		public var sequenciaNossoNumero: Number = NaN;
		public var empresaFisica2: Number = NaN;
		public var dvContaCorrente: Number = NaN;
		public var postoAgencia: Number = NaN;
		
		public function ContaCorrente()
		{
		}
		
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		
		public function get faxFormatado(): String
		{
			if (this.fax == null)
			{
				return this.fax;
			}	
			else
			{
				return Formatador.formatarFone(this.fax);
			}		
		}
		
		public function get descricaoCompleta():String
		{
			return id + "-" + descricao;
		}
		
	}
	
}
