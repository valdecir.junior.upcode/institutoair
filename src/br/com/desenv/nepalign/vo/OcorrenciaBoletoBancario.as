package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.OcorrenciaBoletoBancario")]
	public class OcorrenciaBoletoBancario 
	{
		public var id: Number = NaN;
		public var codigoOcorrencia: String;
		public var descricao: String;
		public function OcorrenciaBoletoBancario()
		{
		}
		public function get ocorrenciaDescricaoCompleta():String
		{
			return this.codigoOcorrencia+" - "+this.descricao;
		}
		
	}
}

