package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PerfilUsuario")]
	public class PerfilUsuario 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var supervisor: String;
		public function PerfilUsuario()
		{
		}
		
		public function get supervisorDescricaoCompleta(): String
		{
			if (this.supervisor == null)
			{
				return this.supervisor;
			}
			else if (this.supervisor == "S")
			{
				return "Sim";	
			}
			else if (this.supervisor == "N")
			{
				return "Nao";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

