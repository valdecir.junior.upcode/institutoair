package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoMovimentoCaixaDia")]
	public class TipoMovimentoCaixaDia 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var tipoConta: TipoConta;
		public var origem: Number = NaN;
		public var geraDespesa:String;

		public function TipoMovimentoCaixaDia()
		{
		}
		
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (isNaN(this.origem))
			{
				return "";
			}
			else if (this.origem == 1)
			{
				return "VENDA";	
			}
			else if (this.origem == 2)
			{
				return "ENTRADA";	
			}
			else if (this.origem == 3)
			{
				return "SAÍDA";	
			}
			else
			{
				return "";
			}
		}		
	}
}