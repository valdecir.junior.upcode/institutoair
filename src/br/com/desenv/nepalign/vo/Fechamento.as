package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Fechamento")]
	public class Fechamento 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var formaFechamento: FormaFechamento;
		public var movimento: String;
		public var descricao: String;
		public function Fechamento()
		{
		}
		
		
	}
}

