package br.com.desenv.nepalign.vo
{
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cor")]
	public class Cor 
	{
		public var id: Number = NaN;
		public var descricaoCorEmpresa: String;
		public var descricaoCorFabrica: String;
		public var ultimaAlteracao: Date;
		public var codigo: Number = NaN;
		public var corBasica1: CorBasica;
		public var corBasica2: CorBasica;
		public var corBasica3:CorBasica;
		
		public function Cor()
		{
		}
		
		public function get descricaoCompleta() :String
		{
			return id.toString() + " - " + descricaoCorEmpresa;
		}
	}
}

