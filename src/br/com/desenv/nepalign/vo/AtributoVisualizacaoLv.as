package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AtributoVisualizacaoLv")]
	public class AtributoVisualizacaoLv 
	{
		public var id: Number = NaN;
		public var nome: String;
		public var ordemExibicao: Number = NaN;
		public var imagemListaProduto: String;
		public var imagemDetalheProduto: String;
		public var url: String;
		public var situacaoEnvioLv: SituacaoEnvioLV;
		public function AtributoVisualizacaoLv()
		{
		}
		
		
	}
}

