package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SituacaoPedidoVenda")]
	public class SituacaoPedidoVenda 
	{
		public var id: int;
		public var descricao: String;
		public function SituacaoPedidoVenda()
		{
		}
		
		
	}
}

