package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import flash.utils.getQualifiedClassName;
	
	import mx.controls.Alert;
	import mx.utils.object_proxy;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Operacao")]
	public class Operacao 
	{
		public var id: int;
		public var usuario: Usuario;
		public var nomeClasse: String;
		public var dataOperacao: Date;
		public var objeto: ByteArray;
		public var operacao: String;
		public function Operacao()
		{
		}
		
		public function get dataOperacaoFormatado(): String
		{
			if (this.dataOperacao == null)
			{
				return "";
			}	
			else
			{
				return Formatador.formatarDate(this.dataOperacao);
			}		
		}
		
		public function get objetoFormatado():String
		{
			this.objeto.position = 0;
			var object:Object = this.objeto.readObject() as Object;
			
			return getQualifiedClassName(object);
		}
	}
}
