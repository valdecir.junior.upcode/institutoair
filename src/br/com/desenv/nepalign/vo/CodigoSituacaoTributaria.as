package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CodigoSituacaoTributaria")]
	public class CodigoSituacaoTributaria 
	{
		public var id: Number = NaN;
		public var impostosTaxasTarifas: ImpostosTaxasTarifas;
		public var codigo: String;
		public var descricao: String;
		public function CodigoSituacaoTributaria()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return codigo + "-" + descricao;
		}
		
		
	}
}

