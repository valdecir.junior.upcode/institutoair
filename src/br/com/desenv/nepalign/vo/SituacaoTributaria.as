package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SituacaoTributaria")]
	public class SituacaoTributaria 
	{
		public var id: int;
		public var codigo: String;
		public var descricao: String;
		public function SituacaoTributaria()
		{
		}
		
		
	}
}

