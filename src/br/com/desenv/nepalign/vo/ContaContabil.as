package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ContaContabil")]
	public class ContaContabil
	{
		public var id: Number = NaN;
		public var idEmpresa: Number = NaN;
		public var descricao:String = null;
		public var codigoContabil:String = null;
		public var codigoContabilNivel1:String = null;
		public var codigoContabilNivel2:String = null;
		public var codigoContabilNivel3:String = null;
		public var creditoDebito:Number = NaN;
		
		public function ContaContabil()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return codigoContabil + " - " + descricao;
		}
	}
}