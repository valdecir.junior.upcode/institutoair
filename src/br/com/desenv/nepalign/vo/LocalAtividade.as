package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LocalAtividade")]
	public class LocalAtividade 
	{
		public var id: int;
		public var nomeLocal: String;
		public var descricao: String;
		public function LocalAtividade()
		{
		}
		
		
	}
}

