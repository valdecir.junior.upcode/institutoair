package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Funcionalidade")]
	public class Funcionalidade 
	{
		public var id: int;
		public var descricao: String;
		public var situacao: String;
		public var modulo: Modulo;
		public var nomeServico: String;
		public function Funcionalidade()
		{
		}
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "A")
			{
				return "Ativa";	
			}
			else if (this.situacao == "I")
			{
				return "Inativa";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

