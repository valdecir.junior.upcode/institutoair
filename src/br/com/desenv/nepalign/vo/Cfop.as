package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cfop")]
	public class Cfop 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public var naturezaOperacao: String;
		public var possuiTributacaoICMS: Number = NaN;
		public var possuiTributacaoIPI: Number = NaN;
		public function Cfop()
		{
		}
		
		public function get possuiTributacaoICMSDescricaoCompleta(): String
		{
			if (this.possuiTributacaoICMS == 0)
			{
				return "Nao";
			}
			else if (this.possuiTributacaoICMS == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get possuiTributacaoIPIDescricaoCompleta(): String
		{
			if (this.possuiTributacaoIPI == 0)
			{
				return "Nao";
			}
			else if (this.possuiTributacaoIPI == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
		public function get descricaoCompleta():String
		{
			return codigo+"-"+descricao;
		}
	}
}

