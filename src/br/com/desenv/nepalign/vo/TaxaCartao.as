package br.com.desenv.nepalign.vo
{
	import mx.controls.DateField;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TaxaCartao")]
	public class TaxaCartao
	{
		public var id:Number = NaN;
		public var empresaFinanceiro:EmpresaFinanceiro; 
		public var formaPagamentoCartao:FormaPagamentoCartao;
		public var parcela:Number = NaN;
		public var taxa:Number = NaN;
		public var dataVigencia:Date;
		public var vigente:Boolean;
		
		public function TaxaCartao()
		{
			
		}
		
		public function get taxaFormatada():String
		{
			if(isNaN(taxa))
				return "";
			
			return Formatador.formatarDouble(taxa, 0x02);
		}
		
		public function set taxaFormatada(value:String):void
		{
			if(value == "")
			{
				taxa = NaN;
				return;
			}
			
			taxa = Number(value.replace(",", "."));
		}
		
		public function get dataVigenciaFormatada():String
		{
			return DateField.dateToString(dataVigencia, "DD/MM/YYYY");
		}
		
		public function get vigenteFormatado():String
		{
			if(vigente)
				return "SIM";
			else
				return "NÃO";
		}
	}
}