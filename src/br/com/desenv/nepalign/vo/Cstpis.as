package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cstpis")]
	public class Cstpis 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public function Cstpis()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return codigo + "-" + descricao;
		}
		
		
	}
}

