package br.com.desenv.nepalign.vo
{
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ImpostoEmpresa")]
	public class ImpostoEmpresa 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var impostoMunicipal: Number = NaN;
		public var impostoEstadual: Number = NaN;
		public var impostoFederal: Number = NaN;
		public function ImpostoEmpresa()
		{
		}
		
		
	}
}

