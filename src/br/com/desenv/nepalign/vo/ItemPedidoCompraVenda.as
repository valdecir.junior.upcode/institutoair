package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemPedidoCompraVenda")]
	public class ItemPedidoCompraVenda 
	{
		public var id: Number;
		public var pedidoCompraVenda: PedidoCompraVenda;
		public var cor: Cor;
		public var ordemProduto: OrdemProduto;
		public var grupoProduto: GrupoProduto;
		
		public var produto: Produto;
		public var sequencial:Number;
		public var descricaoProduto: String;
		public var grade: Grade;
		public var quantidade1:Number;
		public var quantidade2:Number;
		public var quantidade3:Number;
		public var quantidade4:Number;
		public var quantidade5:Number;
		public var quantidade6:Number;
		public var quantidade7:Number;
		public var quantidade8:Number;
		public var quantidade9:Number;
		public var quantidade10:Number;
		public var quantidade11:Number;
		public var quantidade12:Number;
		public var quantidade13:Number;
		public var quantidade14:Number;
		public var quantidade15:Number;
		public var quantidade16:Number;
		public var quantidade17:Number;
		public var quantidadeTotal:Number;
		public var valorUnitarioCusto:Number;
		public var margem:Number;
		public var valorUnitarioVenda: Number;
		public var valorTotalVenda: Number;
		public var valorTotalCusto: Number;
		public var situacao: String;
		public var observacao: String;
		public var baixadoCancelado: String; // B = Sim ou C = Nao
		public var custoBruto:Number;
		public var destaque:Boolean = false;
		
		private var _letraGrade: String;
		
		public function ItemPedidoCompraVenda()
		{
		}

		public function set letraGrade(letraGrade: String): void
		{
			this._letraGrade = letraGrade;
		}

		public function get letraGrade(): String
		{
			return this._letraGrade;
		}
		
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			
			else if (this.situacao == "1")
			{
				return "Item do Pedido Efetivado";	
			}
			else if (this.situacao == "2")
			{
				return "Item Atendido Parcialmente";	
			}
			else if (this.situacao == "3")
			{
				return "Item Atendido Integralmente";	
			}
			else if (this.situacao == "4")
			{
				return "Item Cancelado";	
			}			
			else if (this.situacao == "5")
			{
				return "Item do Pré Cadastro";	
			}	
			else if(this.situacao == "6")
			{
				return "Baixa Autorizada";
			}
			else
			{
				return "";
			}
		}
		
		public function get situacaoDescricaoAbreviada(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
				
			else if (this.situacao == "1")
			{
				return "E";	
			}
			else if (this.situacao == "2")
			{
				return "P";	
			}
			else if (this.situacao == "3")
			{
				return "B";	
			}
			else if (this.situacao == "4")
			{
				return "C";	
			}			
			else if (this.situacao == "5")
			{
				return "Pre";	
			}	
			else if(this.situacao == "6")
			{
				return "BA";
			}
			else
			{
				return "";
			}
		}
	
		public function get produtoCompleto(): String
		{
			return this.produto.id + " - " + this.produto.nomeProduto;
		}
		
		public function get ordemProdutoCompleto(): String
		{
			return this.ordemProduto.id + "-" + this.ordemProduto.descricao;
		}
		
		public function get corCompleto(): String
		{
			return this.cor.id + "-" + this.cor.descricaoCorEmpresa;
		}
		
		public function get valorUnitarioVendaFormatado(): String
		{
			return Formatador.formatarDouble(this.valorUnitarioVenda, 2);
		}

		public function get valorUnitarioCustoFormatado(): String
		{
			return Formatador.formatarDouble(this.valorUnitarioCusto, 2);
		}

		public function get valorTotalVendaFormatado(): String
		{
			return Formatador.formatarDouble(this.valorTotalVenda, 2);
		}

		public function get valorTotalCustoFormatado(): String
		{
			return Formatador.formatarDouble(this.valorTotalCusto, 2);
		}
		public function get custoBrutoFormatado(): String
		{
			return Formatador.formatarDouble(this.custoBruto, 2);
		}		
		
		public function get valorDescricaoBaixaCompleto(): String
		{
			if (this.id == -1)
			{
				return "Baixado";
			}
			else if (this.id == -2)
			{
				return "Saldo";
			}
			else
			{
				return "Original";
			}
			
		}
		
		public function get sequencialFormatado():String
		{
			if(isNaN(this.sequencial) || this.sequencial== 0 || this.sequencial == -1 || this.sequencial == -2)
				return "";
			else
				return this.sequencial.toString();
		}
		
	}
}

