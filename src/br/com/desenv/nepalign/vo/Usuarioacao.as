package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Usuarioacao")]
	public class Usuarioacao 
	{
		public var id: int;
		public var acao: Acao;
		public var usuario: Usuario;
		public var empresa: Empresa;
		public function Usuarioacao()
		{
		}
		
		
	}
}

