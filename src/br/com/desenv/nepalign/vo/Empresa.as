package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Empresa")]
	public class Empresa 
	{
		public var id: Number;
		public var razao: String;
		public var email:String;
		public var telefone:String;
		
		public function Empresa()
		{
		}
		
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		
		public function get idFormatado():String
		{
			return this.id.toString().length == 1 ? "F-0" + this.id.toString() : "F-" + this.id.toString();
		}
		
		public function get nomeConcatenado():String
		{
			return this.idFormatado + " " + this.razao;
		}
	}
}