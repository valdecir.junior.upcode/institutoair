package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cstcofins")]
	public class Cstcofins 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public function Cstcofins()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return codigo + "-" + descricao;
		}
		
		
	}
}

