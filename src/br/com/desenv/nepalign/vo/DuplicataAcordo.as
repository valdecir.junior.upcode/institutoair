package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.DuplicataAcordo")]
	public class DuplicataAcordo 
	{
		public var id: Number = NaN;
		public var duplicataNova: Duplicata;
		public var duplicataAntigaParcela: DuplicataParcela;
		
		public var empresaFisica: EmpresaFisica;
		public var cliente: Cliente;
		public var numeroNovaDuplicata: Number;
		public var numeroDuplicataAntiga: Number;
		public var numeroParcelaDuplicataAntiga: Number = NaN;
		
		public function DuplicataAcordo()
		{
		}
		
		public function get valorDuplicataNova():Number
		{
			return duplicataNova.valorTotalCompra;
		}
		
		
	}
}

