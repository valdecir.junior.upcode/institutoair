package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MotivoMovimentoBoleto")]
	public class MotivoMovimentoBoleto 
	{
		public var id: Number = NaN;
		public var motivoOcorrenciaBoletoBancario: MotivoOcorrenciaBoletoBancario;
		public var movimentoBoletoBancario: MovimentoBoletoBancario;
		public var ocorrenciaBoletoBancario:OcorrenciaBoletoBancario;
		public function MotivoMovimentoBoleto()
		{
		}
		
		
	}
}

