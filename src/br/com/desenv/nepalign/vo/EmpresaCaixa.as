package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EmpresaCaixa")]
	public class EmpresaCaixa
	{
		public var id:Number = NaN;
		public var nomeEmpresa:String;
		
		public function EmpresaCaixa()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return id.toString() + " - " + nomeEmpresa;
		}
	}	
}