package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SaldoEstoqueProduto")]
	public class SaldoEstoqueProduto 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var estoqueProduto: EstoqueProduto;
		public var situacaoEnvioLV: SituacaoEnvioLV;
		public var mes: Number;
		public var ano: Number;
		public var saldo: Number = 0.0;
		public var venda: Number = 0.0;
		public var custo: Number = 0.0;
		public var dataUltimaAlteracao: Date;
		public var destaque: Boolean = false;
		public var custoMedio: Number  = 0.0;
		public var ultimoCusto: Number = 0.0;
		
		public function SaldoEstoqueProduto()
		{
		}
		
	}
}