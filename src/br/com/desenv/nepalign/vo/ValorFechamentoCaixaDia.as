package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.controls.DateField;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ValorFechamentoCaixaDia")]
	public class ValorFechamentoCaixaDia 
	{
		public var id: Number = NaN;
		public var caixadia: CaixaDia;
		public var formaPagamento: FormaPagamento;
		public var dataFechamento: Date;
		public var valorCalculado: Number = NaN;
		public var valorInformado: Number = NaN;
		
		public function ValorFechamentoCaixaDia()
		{
		}
		
		public function get valorCalculadoFormatado(): String 
		{
			if (isNaN(this.valorCalculado))
			{
				return "0";
			}	
			else
			{
				return ""+Formatador.formatarDouble(this.valorCalculado,2);
			}		
		}
		
		public function get valorInformadoFormatado(): String
		{
			if (isNaN(this.valorInformado))
			{
				return "0";
			}	
			else
			{
				return Formatador.formatarDouble(this.valorInformado,2);
			}		
		}
		public function get diferenca():Number
		{
			var valor:Number= (parseFloat(this.valorCalculado.toFixed(2))- parseFloat(this.valorInformado.toFixed(2)));
			return parseFloat(valor.toFixed(2));
		}
		public function get destaque():Boolean
		{
			if(this.diferenca!=0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}

