package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Ncm")]
	public class Ncm 
	{
		public var id: Number = NaN;
		public var ncm: String;
		public var nome: String;
		public function Ncm()
		{
		}
		
		
	}
}

