package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Uf")]
	public class Uf 
	{
		public var id: Number = NaN;
		public var sigla: String;
		public var nome: String;
		public var codEstadoIBGE: String;
		public function Uf()
		{
		}
		
		
	}
}

