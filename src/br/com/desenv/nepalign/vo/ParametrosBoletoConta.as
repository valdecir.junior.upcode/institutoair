package br.com.desenv.nepalign.vo
{
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ParametrosBoletoConta")]
	public class ParametrosBoletoConta 
	{
		public var id: Number = NaN;
		public var contaCorrente: ContaCorrente;
		public var ano: Number = NaN;
		public var codigoCedente: Number = NaN;
		public var posto: Number = NaN;
		public var sequencial: Number = NaN;
		public var instrucaoPadrao: Number = NaN;
		public var carteiraCobranca: Number = NaN;
		public var ativoInativo: Number = NaN;
		public var geracaoNossoNumero: Number = NaN;
		
		public function ParametrosBoletoConta()
		{
		}
		
	}
	
}

