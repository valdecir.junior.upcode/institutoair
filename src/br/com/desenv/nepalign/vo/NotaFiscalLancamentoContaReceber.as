package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.NotaFiscalLancamentoContaReceber")]
	public class NotaFiscalLancamentoContaReceber 
	{
		public var id: Number = NaN;
		public var lancamentoContaReceber: LancamentoContaReceber;
		public var notaFiscal: NotaFiscal;
		public function NotaFiscalLancamentoContaReceber()
		{
		}
		
		
	}
}

