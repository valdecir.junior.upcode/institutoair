package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Tamanho")]
	public class Tamanho 
	{
		public var id: Number;
		public var descricao: String;
		public var ultimaAlteracao: Date;
		public var codigo:Number;
		
		public function Tamanho()
		{
		}
		
	}
	
}
