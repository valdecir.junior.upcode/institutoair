package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	
	
	
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Avalista")]
	public class Avalista 
	{
		public var id: Number;
		public var foto: ByteArray;
		public var nome: String;
		public var logradouro: String;
		public var numeroEndereco: String;
		public var complementoEndereco: String;
		public var itinerario: String;
		public var cidadeEndereco: Cidade;
		public var cep: String;
		public var cidadeCache: String;
		public var estadoCache: String;
		public var residenciaPropria: String;
		public var valorAluguelPrestacao: Number;
		public var cpfCnpj: String;
		public var bairro: String;
		public var nomeMae: String;
		public var nomePai: String;
		public var nomeConjuge: String;
		public var dataHoraCadastro: Date;
		public var dataNascimento: Date;
		public var operadoraTelefoneResidencial: Operadora;
		public var telefoneResidencial: String;
		public var operadoraCelular01: Operadora;
		public var telefoneCelular01: String;
		public var operadoraCelular02: Operadora;
		public var telefoneCelular02: String;
		public var mediaAtraso: Number;
		public var dataEntradaScpc: Date;
		public var dataBaixaScpc: Date;
		public var salario: Number;
		public var outraRenda: Number;
		public var estadoCivil: String;
		public var rg: String;
		public var tempoResidencia: Date;
		public var empresaTrabalha: String;
		public var profissao: String;
		public var tempoTrabalho: Date;
		public var logradouroComercial: String;
		public var numeroComercial: String;
		public var cidadeComercial: Cidade;
		public var cidadeComercialCache: String;
		public var estadoComercialCache: String;
		public var cepComercial: String;
		public var operadoraComercial: Operadora;
		public var telefoneComercial: String;
		public var informacaoComercial: String;
		public var numeroCalcado: String;
		public var time: String;
		public var observacao: String;
		public var historico: String;
		public var fotoRg: ByteArray;
		public var fotoCpf: ByteArray;
		public function Avalista()
		{
		}
		
		public function get cepFormatado(): String
		{
			if (this.cep == null)
			{
				return this.cep;
			}	
			else
			{
				return Formatador.formatarCep(this.cep);
			}		
		}
		public function get residenciaPropriaDescricaoCompleta(): String
		{
			if (this.residenciaPropria == null)
			{
				return this.residenciaPropria;
			}
			else if (this.residenciaPropria == "S")
			{
				return "Sim";	
			}
			else if (this.residenciaPropria == "N")
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		public function get cpfCnpjFormatado(): String
		{
			if (this.cpfCnpj == null)
			{
				return this.cpfCnpj;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cpfCnpj);
			}		
		}
		public function get telefoneResidencialFormatado(): String
		{
			if (this.telefoneResidencial == null)
			{
				return this.telefoneResidencial;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneResidencial);
			}		
		}
		public function get telefoneCelular01Formatado(): String
		{
			if (this.telefoneCelular01 == null)
			{
				return this.telefoneCelular01;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneCelular01);
			}		
		}
		public function get telefoneCelular02Formatado(): String
		{
			if (this.telefoneCelular02 == null)
			{
				return this.telefoneCelular02;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneCelular02);
			}		
		}
		public function get estadoCivilDescricaoCompleta(): String
		{
			if (this.estadoCivil == null)
			{
				return this.estadoCivil;
			}
			else if (this.estadoCivil == "S")
			{
				return "Solteiro";	
			}
			else if (this.estadoCivil == "C")
			{
				return "Casado";	
			}
			else if (this.estadoCivil == "V")
			{
				return "Viuvo";	
			}
			else if (this.estadoCivil == "A")
			{
				return "Amasiado";	
			}
			else
			{
				return "";
			}
		}
		public function get cepComercialFormatado(): String
		{
			if (this.cepComercial == null)
			{
				return this.cepComercial;
			}	
			else
			{
				return Formatador.formatarCep(this.cepComercial);
			}		
		}
		public function get telefoneComercialFormatado(): String
		{
			if (this.telefoneComercial == null)
			{
				return this.telefoneComercial;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneComercial);
			}		
		}
		
		public function get valorAluguelPrestacaoFormatado(): String
		{
			return Formatador.formatarDouble(this.valorAluguelPrestacao, 2);
		}
		
		public function get salarioFormatado(): String
		{
			return Formatador.formatarDouble(this.salario, 2);
		}
		
		public function get outraRendaFormatado(): String
		{
			return Formatador.formatarDouble(this.outraRenda, 2);
		}
		
		public function get tempoResidenciaAnoMesDia(): String
		{
			if (this.tempoResidencia != null)
			{
				var anoMesDia:AnoMesDia = AnoMesDia.calcularDiferencaDataAtual(this.tempoResidencia);
				
				return anoMesDia.imprimir();
			}
			
			return "";
		}
		
		public function get tempoTrabalhoAnoMesDia(): String
		{
			if (this.tempoTrabalho != null)
			{
				var anoMesDia:AnoMesDia = AnoMesDia.calcularDiferencaDataAtual(this.tempoTrabalho);
				
				return anoMesDia.imprimir();
			}
			
			return "";
		}
		
		public function get dataNascimentoFormatado():String
		{
			if (this.dataNascimento != null)
			{
				var anoMesDia:AnoMesDia = AnoMesDia.calcularDiferencaDataAtual(this.dataNascimento);
				
				return Formatador.formatarDate(this.dataNascimento) + " (idade: " + anoMesDia.imprimir() + ")";
			}
			
			return "";
		}
	}		
		
	
}

