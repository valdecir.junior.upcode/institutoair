package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LogOperacaoNepal")]
	public class LogOperacaoNepal 
	{
		public var id: int;
		public var desricao: String;
		public var dataHora: Date;
		public var tipoLog: String;
		public var tipoOperacaoLV: int;
		public var strException: String;
		public function LogOperacaoNepal()
		{
		}
		
		public function get tipoLogDescricaoCompleta(): String
		{
			if (this.tipoLog == null)
			{
				return this.tipoLog;
			}
			else if (this.tipoLog == "1")
			{
				return "Erro de Sistema";	
			}
			else if (this.tipoLog == "2")
			{
				return "Alerta de Problema";	
			}
			else if (this.tipoLog == "3")
			{
				return "Importação LV";	
			}
			else if (this.tipoLog == "4")
			{
				return "Outros";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

