package br.com.desenv.nepalign.vo
{
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ChequeRecebido")]
	public class ChequeRecebido 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var cliente: Cliente;
		public var banco: Banco;
		public var contaCorrente: ContaCorrente;
		public var statusCheque:StatusCheque;
		public var dataEmissao:Date;
		public var numeroConta: String;
		public var numeroCheque: String;
		public var cpfCnpjPortador: String;
		public var valor: Number = NaN;
		public var preDatado: Number = NaN;
		public var bomPara: Date;
		public var observacao: String;
		public var selecionado:Boolean = false;
		public var cobrador:Cobrador;
		public var dataCobranca:Date;
		public var numeroAgencia:String;
		public var emitente:String;
		public var saldo:Number =NaN;
		public var consistencia:String;
		public var promisoria:String;
		public var dataHoraInclusao:Date;
		public var destaque:Boolean = false;
		public function ChequeRecebido()
		{
		}
		
		public function get preDatadoDescricaoCompleta(): String
		{
			if (this.preDatado == 0)
			{
				return "Nao";
			}
			else if (this.preDatado == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
		}
		
		public function get descricao():String
		{
			return ""+this.id+" - "+this.numeroCheque;
		}
		
		public function get valorSaldoRestante():Number
		{
		return (isNaN(this.saldo)?0.0:this.saldo);
		}
		
		public function get promisoriaDescricao():String
		{
			if (this.promisoria == "N")
			{
				return "Não";
			}
			else if (this.promisoria == "S")
			{
				return "Sim";
			}
			else
			{
				return "Não";
			}
		}
		
		public function get cpfCnpjFormatado(): String
		{
			if (this.cpfCnpjPortador == null)
			{
				return this.cpfCnpjPortador;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cpfCnpjPortador);
			}		
		}
	}
}

