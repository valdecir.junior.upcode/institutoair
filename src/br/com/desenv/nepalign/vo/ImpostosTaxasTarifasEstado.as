package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ImpostosTaxasTarifasEstado")]
	public class ImpostosTaxasTarifasEstado 
	{
		public var id: Number = NaN;
		public var uf: Uf;
		public var impostostaxastarifas: ImpostosTaxasTarifas;
		public var valorPercentual: Number = NaN;
		public var valorPercentualInter: Number = NaN;
		public function ImpostosTaxasTarifasEstado()
		{
		}
		
		
	}
}

