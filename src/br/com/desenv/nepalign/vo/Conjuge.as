package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Conjuge")]
	public class Conjuge 
	{
		public var id: Number;
		public var foto: ByteArray;
		public var nome: String;
		public var cpfCnpj: String;
		public var rg: String;
		public var dataNascimento: Date;
		public var operadoraCelular01: Operadora;
		public var telefoneCelular01: String;
		public var operadoraCelular02: Operadora;
		public var telefoneCelular02: String;
		public var salario: Number;
		public var outraRenda: Number;
		public var empresaTrabalha: String;
		public var profissao: String;
		public var tempoTrabalho: Date;
		public var logradouroComercial: String;
		public var numeroComercial: String;
		public var cidadeComercial: Cidade;
		public var cidadeComercialCache: String;
		public var estadoComercialCache: String;
		public var cepComercial: String;
		public var operadoraComercial: Operadora;
		public var telefoneComercial: String;
		public var informacaoComercial: String;
		public var numeroCalcado: String;
		public var time: String;
		public var fotoRg: ByteArray;
		public var fotoCpf: ByteArray;
		public function Conjuge()
		{
		}
		
		public function get cpfCnpjFormatado(): String
		{
			if (this.cpfCnpj == null)
			{
				return this.cpfCnpj;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cpfCnpj);
			}		
		}
		public function get telefoneCelular01Formatado(): String
		{
			if (this.telefoneCelular01 == null)
			{
				return this.telefoneCelular01;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneCelular01);
			}		
		}
		public function get telefoneCelular02Formatado(): String
		{
			if (this.telefoneCelular02 == null)
			{
				return this.telefoneCelular02;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneCelular02);
			}		
		}
		public function get cepComercialFormatado(): String
		{
			if (this.cepComercial == null)
			{
				return this.cepComercial;
			}	
			else
			{
				return Formatador.formatarCep(this.cepComercial);
			}		
		}
		public function get telefoneComercialFormatado(): String
		{
			if (this.telefoneComercial == null)
			{
				return this.telefoneComercial;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneComercial);
			}		
		}
		
		
		
		
		
		public function get salarioFormatado(): String
		{
			return Formatador.formatarDouble(this.salario, 2);
		}
		
		public function get outraRendaFormatado(): String
		{
			return Formatador.formatarDouble(this.outraRenda, 2);
		}
		
		
		
		public function get tempoTrabalhoAnoMesDia(): String
		{
			if (this.tempoTrabalho != null)
			{
				var anoMesDia:AnoMesDia = AnoMesDia.calcularDiferencaDataAtual(this.tempoTrabalho);
				
				return anoMesDia.imprimir();
			}
			
			return "";
		}
		
		public function get dataNascimentoFormatado():String
		{
			if (this.dataNascimento != null)
			{
				var anoMesDia:AnoMesDia = AnoMesDia.calcularDiferencaDataAtual(this.dataNascimento);
				
				return Formatador.formatarDate(this.dataNascimento) + " (idade: " + anoMesDia.imprimir() + ")";
			}
			
			return "";
		}
		
		
	}
}

