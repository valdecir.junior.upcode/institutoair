package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.FormaPagamentoDuplicataParcela")]
	public class FormaPagamentoDuplicataParcela
	{
		public var itemSequencial:int;
		public var forma:FormaPagamento;
		public var valor:Number;
		public var sobra:Number;
		public var chequeRecebido:ChequeRecebido;
		public var numeroParcelas:int;
		public var operadoraCartao:OperadoraCartao;
		public function FormaPagamentoDuplicataParcela()
		{
		}
		public function get chequeDescicao():String
		{
			return ""+this.chequeRecebido.id+" - "+this.chequeRecebido.numeroCheque;
		}
	}
}