package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.core.FlexGlobals;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AlertaLoja")]
	public class AlertaLoja
	{
		public var id: int;
		public var alertaPedidoVenda: AlertaPedidoVenda;
		public var empresaFisica: EmpresaFisica;
		public var respostaAlerta: String;
		public var selecionado:Boolean;
		public function AlertaLoja()
		{
		} 
		
		public function get respostaAlertaDescricaoCompleta(): String
		{
			if (this.respostaAlerta == null)
			{
				return this.respostaAlerta;
			}
			else if (this.respostaAlerta == "S")
			{
				return "SIM";	
			}
			else if (this.respostaAlerta == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		
		public function get visualizacaoRespostaAlerta():String
		{
			if(this.alertaPedidoVenda.empresaVencedora != null)
				return "Respondido";
			else if(this.alertaPedidoVenda.statusAlerta.id == FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idStatusAlertaAlertaCancelado"))
				return "Cancelado";
			else if(this.alertaPedidoVenda.empresaVencedora == null && 
				this.alertaPedidoVenda.statusAlerta.id != FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idStatusAlertaAlertaAtivo") && 
				this.alertaPedidoVenda.statusAlerta.id != FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idStatusAlertaAlertaApropriadoParaLoja") && 
				this.alertaPedidoVenda.statusAlerta.id != FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idStatusAlertaAlertaFinalizado"))
				return "Não encontrado";
			else 
				return "Não respondido";
		}
	}
}

