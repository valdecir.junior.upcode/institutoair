package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoMovimentacaoEstoque")]
	public class TipoMovimentacaoEstoque 
	{
		public var id: int;
		public var descricao: String;
		public var emprestimo: String;
		public var exigePedido: String;
		public var exigeCliente: String;
		public var estoque: Estoque;
		public var tipoNotaFiscal: TipoNotaFiscal;
		public var observacao: String;
		public var exigePagamento: String;
		public var exibirNumPedidoVenda: String;
		public var entradaSaida: String;
		public function TipoMovimentacaoEstoque()
		{
		}
		
		public function get emprestimoDescricaoCompleta(): String
		{
			if (this.emprestimo == null)
			{
				return this.emprestimo;
			}
			else if (this.emprestimo == "S")
			{
				return "SIM";	
			}
			else if (this.emprestimo == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get exigePedidoDescricaoCompleta(): String
		{
			if (this.exigePedido == null)
			{
				return this.exigePedido;
			}
			else if (this.exigePedido == "S")
			{
				return "SIM";	
			}
			else if (this.exigePedido == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get exigeClienteDescricaoCompleta(): String
		{
			if (this.exigeCliente == null)
			{
				return this.exigeCliente;
			}
			else if (this.exigeCliente == "S")
			{
				return "SIM";	
			}
			else if (this.exigeCliente == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get exigePagamentoDescricaoCompleta(): String
		{
			if (this.exigePagamento == null)
			{
				return this.exigePagamento;
			}
			else if (this.exigePagamento == "S")
			{
				return "SIM";	
			}
			else if (this.exigePagamento == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get exibirNumPedidoVendaDescricaoCompleta(): String
		{
			if (this.exibirNumPedidoVenda == null)
			{
				return this.exibirNumPedidoVenda;
			}
			else if (this.exibirNumPedidoVenda == "S")
			{
				return "SIM";	
			}
			else if (this.exibirNumPedidoVenda == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (this.entradaSaida == null)
			{
				return this.entradaSaida;
			}
			else if (this.entradaSaida == "E")
			{
				return "ENTRADA";	
			}
			else if (this.entradaSaida == "S")
			{
				return "SAÍDA";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

