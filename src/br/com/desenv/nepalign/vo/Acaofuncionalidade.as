package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Acaofuncionalidade")]
	public class Acaofuncionalidade 
	{
		public var id: int;
		public var acao: Acao;
		public var funcionalidade: Funcionalidade;
		public function Acaofuncionalidade()
		{
		}
		
		
	}
}

