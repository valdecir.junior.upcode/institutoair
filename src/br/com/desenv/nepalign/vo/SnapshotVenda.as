package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SnapshotVenda")]
	public class SnapshotVenda 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var valorVenda: Number;
		public var valorCusto: Number;
		public var margem: Number;
		public var quantidade:int;
		
		public function SnapshotVenda()
		{
		}
	}
}