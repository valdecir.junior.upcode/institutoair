package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ImpostosTaxasTarifas")]
	public class ImpostosTaxasTarifas 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public var tipoImposto: Number = NaN;
		public var valorPercentualPadrao: Number = NaN;
		public function ImpostosTaxasTarifas()
		{
		}
		
		
	}
}

