package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Acao")]
	public class Acao 
	{
		public var id: int;
		public var descricao: String;
		public var funcionalidade: Funcionalidade;
		public var nomeMetodo: String;
		public var logarAcao: String;
		public var situacao: String;
		public function Acao()
		{
		}
		
		public function get logarAcaoDescricaoCompleta(): String
		{
			if (this.logarAcao == null)
			{
				return this.logarAcao;
			}
			else if (this.logarAcao == "S")
			{
				return "Sim";	
			}
			else if (this.logarAcao == "N")
			{
				return "Nao";	
			}
			else
			{
				return "";
			}
		}
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "A")
			{
				return "Ativa";	
			}
			else if (this.situacao == "I")
			{
				return "Inativa";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

