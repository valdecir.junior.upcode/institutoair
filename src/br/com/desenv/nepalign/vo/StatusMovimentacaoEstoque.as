package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.StatusMovimentacaoEstoque")]
	public class StatusMovimentacaoEstoque 
	{
		public var id: int;
		public var descricao: String;
		public function StatusMovimentacaoEstoque()
		{
		}
		
		
	}
}

