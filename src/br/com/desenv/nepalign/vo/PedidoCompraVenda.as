package br.com.desenv.nepalign.vo
{
	import mx.collections.ArrayCollection;
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PedidoCompraVenda")]
	public class PedidoCompraVenda 
	{
		public var id: Number;
		public var tipo: String;
		public var empresaFisica: EmpresaFisica;
		public var fornecedorCliente: Fornecedor;
		public var representante: Representante;
		public var dataPedido: Date;
		public var numeroPedido: Number;
		public var dataQuinzena: Date;
		public var condicaoPagamento: String;
		public var percentualDesconto: String;
		public var percentualFinanceiro: String;
		public var percentualFornecedor: Number;
		public var cifFob: String;
		public var observacao: String;
		public var situacao: String;
		public var dataHoraCadastro: Date;
		public var usuarioCadastro: Usuario;
		public var dataHoraUltimaAlteracao: Date;
		public var usuarioUltimaAlteracao:Usuario;
		public var pedidoCompraFornecedor:String;
		public var selecionado:Boolean = false;
		
		public var listaItemPedidoCompraVenda: ArrayCollection = new ArrayCollection();
		
		
		public function PedidoCompraVenda()
		{
		}
		
		public function get tipoDescricaoCompleta(): String
		{
			if (this.tipo == null)
			{
				return this.tipo;
			}
			else if (this.tipo == "C")
			{
				return "Compra";	
			}
			else if (this.tipo == "V")
			{
				return "Venda";	
			}
			else
			{
				return "";
			}
		}
		public function get cifFobDescricaoCompleta(): String
		{
			if (this.cifFob == null)
			{
				return this.cifFob;
			}
			else if (this.cifFob == "C")
			{
				return "CIF";	
			}
			else if (this.cifFob == "F")
			{
				return "FOB";	
			}
			else
			{
				return "";
			}
		}
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "1")
			{
				return "Efetivado";	
			}
			else if (this.situacao == "2")
			{
				return "Atendido Parcialmente";	
			}
			else if (this.situacao == "3")
			{
				return "Atendido Integralmente";	
			}
			else if (this.situacao == "4")
			{
				return "Cancelado";	
			}
			else if (this.situacao == "5")
			{
				return "Pré Cadastro";	
			}
			else if (this.situacao == "6")
			{
				return "Baixa Autorizada";	
			}
			else if (this.situacao == "7")
			{
				return "Cancelamento Autorizado";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

