package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cobrador")]
	public class Cobrador 
	{
		public var id: Number = NaN;
		public var nome: String;
		public var endereco: String;
		public var cidade: Cidade;
		public var ativo: String;
		public var bairro: String;
		public var cep: String;
		public var telefone: String;
		public var celular: String;
		public var internoExterno:String
		public function Cobrador()
		{
		}
		
		public function get ativoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "S")
			{
				return "Sim";	
			}
			else if (this.ativo == "N")
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		public function get internoExternoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "I")
			{
				return "Interno";	
			}
			else if (this.ativo == "E")
			{
				return "Externo";	
			}
			else
			{
				return "";
			}
		}
		public function get cepFormatado(): String
		{
			if (this.cep == null)
			{
				return this.cep;
			}	
			else
			{
				return Formatador.formatarCep(this.cep);
			}		
		}
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		public function get celularFormatado(): String
		{
			if (this.celular == null)
			{
				return this.celular;
			}	
			else
			{
				return Formatador.formatarFone(this.celular);
			}		
		}
		
	}
}

