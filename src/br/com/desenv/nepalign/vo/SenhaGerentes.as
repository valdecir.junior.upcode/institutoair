package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SenhaGerentes")]
	public class SenhaGerentes 
	{
		public var id: Number = NaN;
		public var usuario: Usuario;
		public var senha: String;
		public var empresaFisica: EmpresaFisica;
		public var modulo:ModuloSenhaGerentes;
		public var mostrarSenha:Boolean = false;
		public function SenhaGerentes()
		{
		}
		public function  get senhaEscondida():String
		{
			if(this.mostrarSenha)
			{
				return this.senha;
			}
			else
			{
				return "*******************"
			}
		}
		public function  set mostrarSenhaEscondida(flag:Boolean):void
		{
			this.mostrarSenha = flag;
		}
	}
}

