package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoConta")]
	public class TipoConta 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var incideCpmf: Number = NaN;
		public var credito: Number = NaN;
		public function TipoConta()
		{
		}
		
		public function get incideCpmfDescricaoCompleta(): String
		{
			if (this.incideCpmf == 0)
			{
				return "Nao";
			}
			else if (this.incideCpmf == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
		}
		
		public function get creditoDescricaoCompleta(): String
		{
			if (this.credito == 0)
			{
				return "Débito";
			}
			else if (this.credito == 1)
			{
				return "Crédito";
			}
			else
			{
				return "";
			}
		}
	}
}