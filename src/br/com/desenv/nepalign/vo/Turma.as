package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Turma")]
	public class Turma 
	{
		public var id: int;
		public var descricao: String;
		public var horarioInicial: Date;
		public var horarioFinal: Date;
		public var diaSemana: String;
		public var professor: Professor;
		public var localAtividade: LocalAtividade;
		public var capacidadeAtendidos: int;
		public var atividadeOferecida: AtividadeOferecida;
		public function Turma()
		{
		}
		
		public function get diaSemanaDescricaoCompleta(): String
		{
			if (this.diaSemana == null)
			{
				return this.diaSemana;
			}
			else if (this.diaSemana == "1")
			{
				return "Domingo";	
			}
			else if (this.diaSemana == "2")
			{
				return "Segunda";	
			}
			else if (this.diaSemana == "3")
			{
				return "Terça";	
			}
			else if (this.diaSemana == "4")
			{
				return "Quarta";	
			}
			else if (this.diaSemana == "5")
			{
				return "Quinta";	
			}
			else if (this.diaSemana == "6")
			{
				return "Sexta";	
			}
			else if (this.diaSemana == "7")
			{
				return "Sábado";	
			}
			else
			{
				return "";
			}
		}
		public function get horarioInicialFormatado() :String
		{
			const d:String = '.';
			const e:String = '';
			const s:String = ':';
			const b:String = '/';
			
			var date:Date = this.horarioInicial;
			var resultDate:String = "";
			
			//resultDate +=  e.concat(pad(date.date, 2), b, pad(date.month + 1, 2), b,  pad(date.fullYear, 2));
			resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));
			
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero:String = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}			
		public function get horarioFinalFormatado() :String
		{
			const d:String = '.';
			const e:String = '';
			const s:String = ':';
			const b:String = '/';
			
			var date:Date = this.horarioFinal;
			var resultDate:String = "";
			
			//resultDate +=  e.concat(pad(date.date, 2), b, pad(date.month + 1, 2), b,  pad(date.fullYear, 2));
			resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));
			
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero:String = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}		
	}
}

