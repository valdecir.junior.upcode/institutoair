package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MotivoOcorrenciaBoletoBancario")]
	public class MotivoOcorrenciaBoletoBancario 
	{
		public var id: Number = NaN;
		public var codigoMotivo: String;
		public var descricao: String;
		public function MotivoOcorrenciaBoletoBancario()
		{
		}
		public function get descricaoCompleta():String
		{
			return this.codigoMotivo +" - "+this.descricao;
		}
		
	}
}

