package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ParametroSistema")]
	public class ParametroSistema 
	{
		public var id: Number = NaN;
		public var chave: String;
		public var valor: String;
		public var descricao: String;
		public var empresaFisica: EmpresaFisica;
		public function ParametroSistema()
		{
		}
		
		
	}
}

