package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.FormaPagamento")]
	public class FormaPagamento 
	{
		public var id: Number = NaN;
		public var operadoraCartao: OperadoraCartao;
		public var descricao: String;
		public var codigoReduzido: Number = NaN;
		public var cartaoCredito: Number = NaN;
		public var tipo: Number = NaN;
		public function FormaPagamento()
		{
		}
		
		public function get cartaoCreditoDescricaoCompleta(): String
		{
			if (this.cartaoCredito == 0)
			{
				return "Nao";
			}
			else if (this.cartaoCredito == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get tipoDescricaoCompleta(): String
		{
			if (this.tipo == 0)
			{
				return "Nao";
			}
			else if (this.tipo == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

