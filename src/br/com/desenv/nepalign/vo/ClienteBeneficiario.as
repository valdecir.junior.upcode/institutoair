package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ClienteBeneficiario")]
	public class ClienteBeneficiario 
	{
		public var id: Number = NaN;
		public var cliente: Cliente;
		public var localTrabalho: String;
		public var nivelFixo: String;
		public var nome: String;
		public var salario: Number = NaN;
		public var telefone: String;
		public var temposervico: Date;
		public function ClienteBeneficiario()
		{
		}
		
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		
	}
}

