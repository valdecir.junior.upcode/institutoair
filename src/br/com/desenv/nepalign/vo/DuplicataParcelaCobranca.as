package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.DuplicataParcelaCobranca")]
	public class DuplicataParcelaCobranca 
	{
		public var id: Number = NaN;
		public var ocorrencia: String;
		public var data: Date;
		public var duplicataParcela:DuplicataParcela;
		public var dataImpressao: Date;
		public function DuplicataParcelaCobranca()
		{
		}
		
		public function get ocorrenciaDescricaoCompleta(): String
		{
			if (this.ocorrencia == null)
			{
				return this.ocorrencia;
			}
			else if (this.ocorrencia == "P")
			{
				return "Pagamento";	
			}
			else if (this.ocorrencia == "C")
			{
				return "Cobrança";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

