package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Atendido")]
	public class Atendido 
	{
		public var id: int;
		public var nomeCompleto: String;
		public var dataNascimento: Date;
		public var cpf: String;
		public var rg: String;
		public var dataEmissaoRg: Date;
		public var orgaoEmissorRg: String;
		public var ufRg: String;
		public var logradouro: String;
		public var numero: String;
		public var complemento: String;
		public var idUf: Uf;
		public var cidadeo: Cidade;
		public var nacionalidade: String;
		public var idCidadeAtendido: Cidade;
		public var idFormaLocomocao: FormaLocomocao;
		public var idResidenciaAtendido: ResidenciaAtendido;
		public var situacaoResidencia: String;
		public var informacoesMedicas: String;
		public var foneFixo: String;
		public var foneCelular: String;
		public var email: String;
		public var bairro: String;
		public var cep: String;
		public var situacaoCadasrtral: String;
		public var fotoCaminho: String;
		public var dataVencimentoAtestado:Date;
		public var motivoUltimoAfastamento:String;
		public var foto: ByteArray;
		public var foto2: ByteArray;
		
		
		public function Atendido()
		{
		}
		
		public function get situacaoResidenciaDescricaoCompleta(): String
		{
			if (this.situacaoResidencia == null)
			{
				return this.situacaoResidencia;
			}
			else if (this.situacaoResidencia == "1")
			{
				return "Alugada";	
			}
			else if (this.situacaoResidencia == "2")
			{
				return "Propria";	
			}
			else if (this.situacaoResidencia == "3")
			{
				return "Outros";	
			}
			else
			{
				return "";
			}
		}
		public function get situacaoCadasrtralDescricaoCompleta(): String
		{
			if (this.situacaoCadasrtral == null)
			{
				return this.situacaoCadasrtral;
			}
			else if (this.situacaoCadasrtral == "1")
			{
				return "PRE-CADASTRO";	
			}
			else if (this.situacaoCadasrtral == "2")
			{
				return "PENDENTE DE ATESTADO";	
			}			
			else if (this.situacaoCadasrtral == "3")
			{
				return "ATIVO";	
			}
			else if (this.situacaoCadasrtral == "4")
			{
				return "INATIVO";	
			}
			else if (this.situacaoCadasrtral == "5")
			{
				return "AGUARDANDO VAGA";	
			}			
			else if (this.situacaoCadasrtral == "6")
			{
				return "SUSPENSO";	
			}
			else if (this.situacaoCadasrtral == "7")
			{
				return "AFASTADO";	
			}
			else if (this.situacaoCadasrtral == "8")
			{
				return "FALESCIDO";	
			}			
			else
			{
				return "";
			}
		}
		
	}
}

