package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Duplicata")]
	public class Duplicata 
	{
		public var id: int;
		public var empresaFisica: EmpresaFisica;
		public var cliente: Cliente;
		public var usuario: Usuario;
		public var vendedor: Vendedor;
		public var numeroDuplicata: int;
		public var dataHoraInclusao: Date;
		public var numeroFatura: int;
		public var dataCompra: Date;
		public var numeroNotaFiscal: String;
		public var valorTotalCompra: Number = 0.0;
		public var dataVencimentoPrimeiraPrestacao: Date;
		public var valorEntrada: Number = 0.0;
		public var numeroTotalParcela: String;
		public var pedidoVenda:PedidoVenda;
		public var semPedidoVenda:String;
		public var observacao:String;
		public var jorrovi:Boolean;
		public function Duplicata()
		{
		}
		
		
	}
}

