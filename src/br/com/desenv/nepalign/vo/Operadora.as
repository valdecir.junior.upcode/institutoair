package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Operadora")]
	public class Operadora 
	{
		public var id: int;
		public var nome: String;
		public var codigo: int;
		public function Operadora()
		{
		}
	}
}

