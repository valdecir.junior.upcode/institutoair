package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PedidoCompraVendaGrade")]
	public class PedidoCompraVendaGrade 
	{
		public var id: int;
		public var pedidoCompraVenda: PedidoCompraVenda;
		public var grade: Grade;
		public function PedidoCompraVendaGrade()
		{
		}
		
		
	}
}

