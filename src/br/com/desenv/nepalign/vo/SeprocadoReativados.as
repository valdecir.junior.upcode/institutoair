package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SeprocadoReativados")]
	public class SeprocadoReativados 
	{
		public var id: Number = NaN;
		public var cliente: Cliente;
		public var seprocadoReativado: String;
		public var dataAntiga: Date;
		public var dataNova: Date;
		public var duplicataParcela: DuplicataParcela;
		public var empresaFisica: EmpresaFisica;
		public var observacao:String;
		public var usuario:Usuario;
		public var dataImpressao:Date;
		public var impresso:String;
		public function SeprocadoReativados()
		{
		}
		
		public function get seprocadoReativadoDescricaoCompleta(): String
		{
			if (this.seprocadoReativado == null)
			{
				return this.seprocadoReativado;
			}
			else if (this.seprocadoReativado == "S")
			{
				return "SEPROCADO";	
			}
			else if (this.seprocadoReativado == "R")
			{
				return "REATIVADO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

