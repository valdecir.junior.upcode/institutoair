package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cidade")]
	public class Cidade 
	{
		public var id: int;
		public var uf: Uf;
		public var nome: String;
		public var nomeSemAcento: String;
		public var codigoIbge: int;
		public var siglaUf: String;
		public function Cidade()
		{
		}
		public function get nomeComUf():String
		{
			return nome+" - "+siglaUf;
		}
		public function get nomeSemAcentoComUf():String
		{
			return nomeSemAcento+" - "+siglaUf;
		}	
		
	}
}

