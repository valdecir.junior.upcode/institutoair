package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LiberacaoGerente")]
	public class LiberacaoGerente 
	{
		public var id: Number = NaN;
		public var usuario: Usuario;
		public var duplicata: Duplicata;
		public var nomeUsuario: String;
		public var parcelasLiberadas:ArrayCollection;
		public var dataHora:Date;
		public function LiberacaoGerente()
		{
		}
		
		
	}
}

