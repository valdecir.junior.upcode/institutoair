package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoDocumentoMovimentoEstoque")]
	public class TipoDocumentoMovimentoEstoque 
	{
		public var id: int;
		public var descricao: String;
		public var ativo: String;
		public var atualizaPreco: String;
		public var entradaSaida: String;
		public function TipoDocumentoMovimentoEstoque()
		{
		}
		
		public function get ativoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "S")
			{
				return "SIM";	
			}
			else if (this.ativo == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get atualizaPrecoDescricaoCompleta(): String
		{
			if (this.atualizaPreco == null)
			{
				return this.atualizaPreco;
			}
			else if (this.atualizaPreco == "S")
			{
				return "SIM";	
			}
			else if (this.atualizaPreco == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (this.entradaSaida == null)
			{
				return this.entradaSaida;
			}
			else if (this.entradaSaida == "E")
			{
				return "ENTRADA";	
			}
			else if (this.entradaSaida == "S")
			{
				return "SAÍDA";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

