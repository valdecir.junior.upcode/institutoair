package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.DepartamentoProduto")]
	public class DepartamentoProduto 
	{
		public var id: int;
		public var departamento: Departamento;
		public var produto: Produto;
		public var situacaoEnvioLV: SituacaoEnvioLV;
		public function DepartamentoProduto()
		{
		}
		
		
	}
}

