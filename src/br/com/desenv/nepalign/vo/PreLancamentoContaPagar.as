package br.com.desenv.nepalign.vo
{
	import mx.controls.DateField;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PreLancamentoContaPagar")]
	public class PreLancamentoContaPagar
	{
		public var id: Number = NaN;
		public var empresaFisica:EmpresaFisica;
		public var empresaFisicaOrigem:EmpresaFisica;
		public var fornecedor:Fornecedor;
		public var contaGerencial:ContaGerencial;
		public var tipoMovimentoCaixaDia:TipoMovimentoCaixaDia;
		public var usuario:Usuario;
		public var lancamentoContaPagar:LancamentoContaPagar;
		public var idDocumentoOrigem:Number = NaN;
		public var dataLancamento:Date;
		public var valor:Number = NaN;
		public var numeroDocumento:String;
		public var situacao:String;
		public var observacao:String;
		
		public function PreLancamentoContaPagar()
		{
		}
		
		public function get dataLancamentoFormatada():String
		{
			return DateField.dateToString(dataLancamento, "DD/MM/YYYY");
		}
		
		public function get descricaoSituacaoCompleta():String
		{
			if(situacao == "P")
				return "Pendente";	
			else if(situacao == "E")
				return "Efetuado";
			else if(situacao == "C")
				return "Cancelado";
			else
			{
				return situacao;
			}
		}
	}
}