package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Mva")]
	public class Mva 
	{
		public var id: Number = NaN;
		public var tipoLista: TipoLista;
		public var aliqInterEstadual: Number = NaN;
		public var uf: Uf;
		public var percMva: Number = NaN;
		public var icmsDestino: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public function Mva()
		{
		}
		
		
	}
}

