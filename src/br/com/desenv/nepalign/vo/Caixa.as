package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Caixa")]
	public class Caixa
	{
		public var id: Number = NaN;
		public var empresaCaixa:EmpresaCaixa = null;
		public var nomeCaixa:String = null;
		public var mesAberto:Number = NaN;
		public var anoAberto:Number = NaN;
		
		public function Caixa()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return this.id + "-" + this.nomeCaixa;
		}	
	}
}

