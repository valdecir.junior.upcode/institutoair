package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.NaturezaFornecedor")]
	public class NaturezaFornecedor 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var codigo: String;
		public function NaturezaFornecedor()
		{
		}
		
		
	}
}

