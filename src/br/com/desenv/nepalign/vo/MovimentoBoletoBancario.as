package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MovimentoBoletoBancario")]
	public class MovimentoBoletoBancario 
	{
		public var id: Number = NaN;
		public var boletoBancario: BoletoBancario;
		public var ocorrenciaBoleto: OcorrenciaBoletoBancario;
		public var usuario: Usuario;
		public var nossoNumero: Number = NaN;
		public var valorTitulo: Number = NaN;
		public var data: Date;
		public var tipoBoleto: String;
		public var nomeArquivo: String;
		public var valorPago:Number =  NaN;
		public function MovimentoBoletoBancario()
		{
		}
		
		public function get tipoBoletoDescricaoCompleta(): String
		{
			if (this.tipoBoleto == null)
			{
				return this.tipoBoleto;
			}
			else if (this.tipoBoleto == "A")
			{
				return "Com Registro";	
			}
			else if (this.tipoBoleto == "C")
			{
				return "Sem Registro";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

