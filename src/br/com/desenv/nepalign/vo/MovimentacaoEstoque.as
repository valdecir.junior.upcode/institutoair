package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.core.FlexGlobals;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MovimentacaoEstoque")]
	public class MovimentacaoEstoque 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var estoque: Estoque;
		public var tipoMovimentacaoEstoque: TipoMovimentacaoEstoque;
		public var tipoDocumentoMovimento: TipoDocumentoMovimentoEstoque;
		public var cliente: Cliente;
		public var usuario: Usuario;
		public var entradaSaida: String;
		public var numeroDocumento: String;
		public var dataMovimentacao: Date;
		public var idDocumentoOrigem: Number;
		public var observacao: String;
		public var faturado: String;
		public var manual: String;
		public var numeroVolume: Number = NaN;
		public var peso: Number = 0.0;
		public var valorFrete: Number = 0.0;
		public var forncedor: Fornecedor;
		public var statusMovimentacaoEstoque:StatusMovimentacaoEstoque;
		public var empresaDestino:EmpresaFisica;

		public function MovimentacaoEstoque()
		{
		}
		
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (this.entradaSaida == null)
			{
				return this.entradaSaida;
			}
			else if (this.entradaSaida == "E")
			{
				return "ENTRADA";	
			}
			else if (this.entradaSaida == "S")
			{
				return "SAIDA";	
			}
			else
			{
				return "";
			}
		}
		public function get faturadoDescricaoCompleta(): String
		{
			if (this.faturado == null)
			{
				return this.faturado;
			}
			else if (this.faturado == "S")
			{
				return "SIM";	
			}
			else if (this.faturado == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		public function get manualDescricaoCompleta(): String
		{
			if (this.manual == null)
			{
				return this.manual;
			}
			else if (this.manual == "S")
			{
				return "SIM";	
			}
			else if (this.manual == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		
		public function get destaque():Boolean
		{
			if(this.statusMovimentacaoEstoque.id == parseInt(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idStatusMovimentacaoEstoquePendente")))
				return true;
			
			return false;
		}
	}
}