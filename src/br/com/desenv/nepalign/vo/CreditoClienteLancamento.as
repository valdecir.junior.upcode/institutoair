package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CreditoClienteLancamento")]
	public class CreditoClienteLancamento 
	{
		public var id: Number = NaN;
		public var creditoCliente: Creditocliente;
		public var lancamentoContaReceber: LancamentoContaReceber;
		public var valor: Number = NaN;
		public function CreditoClienteLancamento()
		{
		}
		
		
	}
}

