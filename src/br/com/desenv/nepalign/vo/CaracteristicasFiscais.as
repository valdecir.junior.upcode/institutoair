package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CaracteristicasFiscais")]
	public class CaracteristicasFiscais 
	{
		public var id: Number = NaN;
		public var uf: Uf;
		public var cfop: Cfop;
		public var codigoSituacaoTributaria: CodigoSituacaoTributaria;
		public var subCodigoSituacaoTributaria: SubCodigoSituacaoTributaria;
		public var cstpis: Cstpis;
		public var cstcofins: Cstcofins;
		public var cstipi: Cstipi;
		public var cfopForaUf: Cfop;
		public var descricao: String;
		public var aplicIdCfop: Number = NaN;
		public var icms: Number = NaN;
		public var aplicIcms: Number = NaN;
		public var ipi: Number = NaN;
		public var aplicIpi: Number = NaN;
		public var pis: Number = NaN;
		public var aplicPis: Number = NaN;
		public var cofins: Number = NaN;
		public var aplicCofins: Number = NaN;
		public var csll: Number = NaN;
		public var aplicCsll: Number = NaN;
		public var irrf: Number = NaN;
		public var aplicIRRF: Number = NaN;
		public var reducaoBCIcms: Number = NaN;
		public var aplicReducaoBCIcms: Number = NaN;
		public var mva: Number = NaN;
		public var aplicMva: Number = NaN;
		public var aplicCodigoSitTrib: Number = NaN;
		public var exibirPisNota: Number = NaN;
		public var exibirCofinsNota: Number = NaN;
		public var exibirCsllNota: Number = NaN;
		public var exibirIrrfNota: Number = NaN;
		public var observacao: String;
		public var sobreescreverobs: Number = NaN;
		public var localObs: Number = NaN;
		public var ativo: Number = NaN;
		public var descontoImpostos: Number = NaN;
		public var origemNota: Number = NaN;
		public var suspCalcSt: Number = NaN;
		public var reducaoBCIcmsST: Number = NaN;
		public var aplicReducaoBCIcmsST: Number = NaN;
		public var aplicCfopForaUf: Number = NaN;
		public function CaracteristicasFiscais()
		{
		}
		
		public function get origemNotaDescricaoCompleta():String
		{
			if (this.origemNota == 1)
			{
				return "Empresa";
			}
			else if(this.origemNota == 2)
			{
				return "Fornecedor";
			}
			else
			{
				return "";
			}
		}
		
		public function get aplicIdCfopDescricaoCompleta(): String
		{
			if (this.aplicIdCfop == 0)
			{
				return "Nao";
			}
			else if (this.aplicIdCfop == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
		public function get aplicIcmsDescricaoCompleta(): String
		{
			if (this.aplicIcms == 0)
			{
				return "Nao";
			}
			else if (this.aplicIcms == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicIpiDescricaoCompleta(): String
		{
			if (this.aplicIpi == 0)
			{
				return "Nao";
			}
			else if (this.aplicIpi == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicPisDescricaoCompleta(): String
		{
			if (this.aplicPis == 0)
			{
				return "Nao";
			}
			else if (this.aplicPis == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicCofinsDescricaoCompleta(): String
		{
			if (this.aplicCofins == 0)
			{
				return "Nao";
			}
			else if (this.aplicCofins == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicCsllDescricaoCompleta(): String
		{
			if (this.aplicCsll == 0)
			{
				return "Nao";
			}
			else if (this.aplicCsll == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicIRRFDescricaoCompleta(): String
		{
			if (this.aplicIRRF == 0)
			{
				return "Nao";
			}
			else if (this.aplicIRRF == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicReducaoBCIcmsDescricaoCompleta(): String
		{
			if (this.aplicReducaoBCIcms == 0)
			{
				return "Nao";
			}
			else if (this.aplicReducaoBCIcms == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicMvaDescricaoCompleta(): String
		{
			if (this.aplicMva == 0)
			{
				return "Nao";
			}
			else if (this.aplicMva == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicCodigoSitTribDescricaoCompleta(): String
		{
			if (this.aplicCodigoSitTrib == 0)
			{
				return "Nao";
			}
			else if (this.aplicCodigoSitTrib == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get exibirPisNotaDescricaoCompleta(): String
		{
			if (this.exibirPisNota == 0)
			{
				return "Nao";
			}
			else if (this.exibirPisNota == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get exibirCofinsNotaDescricaoCompleta(): String
		{
			if (this.exibirCofinsNota == 0)
			{
				return "Nao";
			}
			else if (this.exibirCofinsNota == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get exibirCsllNotaDescricaoCompleta(): String
		{
			if (this.exibirCsllNota == 0)
			{
				return "Nao";
			}
			else if (this.exibirCsllNota == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get exibirIrrfNotaDescricaoCompleta():String
		{
			if(this.exibirIrrfNota == 0)
			{
				return "Nao";
			}
			else if(this.exibirIrrfNota == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
		}
		public function get sobreescreverobsDescricaoCompleta(): String
		{
			if (this.sobreescreverobs == 0)
			{
				return "Nao";
			}
			else if (this.sobreescreverobs == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get localObsDescricaoCompleta(): String
		{
			if (this.localObs == 0)
			{
				return "Nao";
			}
			else if (this.localObs == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get ativoDescricaoCompleta(): String
		{
			if (this.ativo == 0)
			{
				return "Nao";
			}
			else if (this.ativo == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get descontoImpostosDescricaoCompleta(): String
		{
			if (this.descontoImpostos == 0)
			{
				return "Nao";
			}
			else if (this.descontoImpostos == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get suspCalcStDescricaoCompleta(): String
		{
			if (this.suspCalcSt == 0)
			{
				return "Nao";
			}
			else if (this.suspCalcSt == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicReducaoBCIcmsSTDescricaoCompleta(): String
		{
			if (this.aplicReducaoBCIcmsST == 0)
			{
				return "Nao";
			}
			else if (this.aplicReducaoBCIcmsST == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get aplicCfopForaUfDescricaoCompleta(): String
		{
			if (this.aplicCfopForaUf == 0)
			{
				return "Nao";
			}
			else if (this.aplicCfopForaUf == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

