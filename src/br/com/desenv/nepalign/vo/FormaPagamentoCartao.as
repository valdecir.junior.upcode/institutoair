package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.FormaPagamentoCartao")]
	public class FormaPagamentoCartao
	{	
		public var id:Number = NaN;
		public var descricao:String;
		public var operadoraCartao:OperadoraCartao;
		public var quantidadeParcelas:Number = NaN;
		
		public function FormaPagamentoCartao()
		{
			
		}
		
		[Bindable]
		public function get descricaoCompleta():String
		{
			return id + " - " + descricao + " - " + operadoraCartao.descricao;
		}
	}	
}