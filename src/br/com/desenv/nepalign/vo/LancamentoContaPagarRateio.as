package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LancamentoContaPagarRateio")]
	public class LancamentoContaPagarRateio
	{
		public var id: Number = NaN;
		public var empresaFisica:EmpresaFisica;
		public var lancamentoContaPagar:LancamentoContaPagar;
		public var porcentagemRateio:Number = NaN;
	}
}