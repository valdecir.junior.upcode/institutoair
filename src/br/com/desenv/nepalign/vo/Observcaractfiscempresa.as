package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Observcaractfiscempresa")]
	public class Observcaractfiscempresa 
	{
		public var id: Number = NaN;
		public var empresa: EmpresaFisica;
		public var caracteristicasFiscais: CaracteristicasFiscais;
		public var observacao: String;
		public function Observcaractfiscempresa()
		{
		}
		
		
	}
}

