package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.HistoricoCobranca")]
	public class HistoricoCobranca 
	{
		public var id: Number = NaN;
		public var dataCobranca: Date;
		public var observacao: String;
		public var tipoContato: TipoContato;
		public var cliente: Cliente;
		public var alerta:Number = NaN;
		public function HistoricoCobranca()
		{
		}
		
		public function get alertaFormatado():String
		{
			if (isNaN(this.alerta))
			{
				return "";
			}
			else if (this.alerta == 1)
			{
				return "Sim";	
			}
			else if (this.alerta == 0)
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		
		
	}
}

