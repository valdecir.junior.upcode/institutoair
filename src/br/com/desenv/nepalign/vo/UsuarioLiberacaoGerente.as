package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.UsuarioLiberacaoGerente")]
	public class UsuarioLiberacaoGerente 
	{
		public var id: Number = NaN;
		public var usuario: Usuario;
		public function UsuarioLiberacaoGerente()
		{
		}
		
		
	}
}

