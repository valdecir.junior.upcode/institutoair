package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Parametrocomissaocobranca")]
	public class Parametrocomissaocobranca 
	{
		public var id: Number = NaN;
		public var formaPagamento: FormaPagamento;
		public var empresaFisica: EmpresaFisica;
		public var cobrador: Cobrador;
		public var qtdDiasAte: Number = NaN;
		public var percentual: Number = NaN;
		public var internoExterno: String;
		public function Parametrocomissaocobranca()
		{
		}
		
		public function get internoExternoDescricaoCompleta(): String
		{
			if (this.internoExterno == null)
			{
				return this.internoExterno;
			}
			else if (this.internoExterno == "I")
			{
				return "Interno";	
			}
			else if (this.internoExterno == "E")
			{
				return "Externo";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

