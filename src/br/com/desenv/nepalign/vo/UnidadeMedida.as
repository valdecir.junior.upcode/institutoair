package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.UnidadeMedida")]
	public class UnidadeMedida 
	{
		public var id: int;
		public var sigla: String;
		public var descricao: String;
		public function UnidadeMedida()
		{
		}
		
		
	}
}

