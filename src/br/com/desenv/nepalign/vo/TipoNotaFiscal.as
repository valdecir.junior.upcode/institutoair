package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoNotaFiscal")]
	public class TipoNotaFiscal 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var cfopDentroEstado: int;
		public var cfopForaEstado: int;
		public var cfopForaPais: int;
		public var observacao: String;
		public function TipoNotaFiscal()
		{
		}
		
	}
}

