package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.NumeroParcelasPromocional")]
	public class NumeroParcelasPromocional 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var dataInicial: Date;
		public var dataFinal: Date;
		public var quantidadeParcelas: Number = NaN;
		public function NumeroParcelasPromocional()
		{
		}
		
		
	}
}

