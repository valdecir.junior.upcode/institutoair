package br.com.desenv.nepalign.vo {
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoEtiquetaVitrine")]
	public class TipoEtiquetaVitrine {
		public var id: Number = NaN;
		public var descricao: String;
		public var empresaFisica: EmpresaFisica;
		public var jasper: String;
		public var precoVista: Number;
		public var precoPrazo: Number;
		public var precoXY: Number;
		public var parcelado: Number;
		
		public function TipoEtiquetaVitrine() {
			//
		}
		
	}
	
}
