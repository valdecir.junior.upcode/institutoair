package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.controls.DateField;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CaixaDia")]
	public class CaixaDia 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var usuario: Usuario;
		public var valorInicial: Number = NaN;
		public var situacao: Number = NaN;
		public var dataAbertura: Date;
		public var horaAbertura: Date;
		public var dataFechamento: Date;
		public var horaFechamento: Date;
		public var observacao: String;
		public function CaixaDia()
		{
		}
		public function get situacaoDescricaoCompleta(): String
		{
			if (isNaN( this.situacao))
			{
				return ""+this.situacao;
			}
			else if (this.situacao == 0)
			{
				return "Fechado";	
			}
			else if (this.situacao == 1)
			{
				return "aberto";	
			}else if(this.situacao == 2)
			{
				return "Aceito";
			}
			else if (this.situacao == 3)
			{
				return "Aceito com Divergencia";	
			}
			else if (this.situacao == 4)
			{
				return "Finalizado";	
			}
			else
			{
				return "";
			}
		}
		public function get descricaoCompleta():String
		{
			return DateField.dateToString(dataAbertura, "DD/MM/YYYY") + " - " + this.usuario.nome;
		}
		public function get dataAberturaFormatada():String
		{
			return DateField.dateToString(dataAbertura, "DD/MM/YYYY");
		}
	}
}

