package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	import br.com.desenv.nepalign.vo.EstoqueProduto;
	import br.com.desenv.nepalign.vo.Produto;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemPedidoVenda")]
	public class ItemPedidoVenda 
	{
		public var id: Number = NaN;
		public var pedidoVenda: PedidoVenda;
		public var itemSequencial: int;
		public var entradaSaida: String;
		public var quantidade: Number = NaN;
		public var precoTabela: Number = NaN;
		public var precoVenda: Number = NaN;
		public var custoProduto: Number = NaN;
		public var codigoRastreamento: String;
	
		public var estoqueProduto: EstoqueProduto;
		
		public var produto: Produto;
		public var statusItemPedido:StatusItemPedido;
		public function ItemPedidoVenda()
		{
		}
		
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (this.entradaSaida == null)
			{
				return this.entradaSaida;
			}
			else if (this.entradaSaida == "E")
			{
				return "Entrada";	
			}
			else if (this.entradaSaida == "S")
			{
				return "Saída";	
			}
			else
			{
				return "";
			}
		}
		
		public function get estoqueProdutoFormatado() :String
		{
			var descricao:String = new String();
			descricao =""+ produto.nomeProduto + " - " + estoqueProduto.cor.descricaoCorEmpresa + " - " + estoqueProduto.tamanho.descricao + " - " + estoqueProduto.ordemProduto.descricao + " - " + produto.codigoProduto;
			
			return descricao;
		}
		
		public function get precoVendaFormatado():String
		{
			var valorFormatado:String = new String();
			valorFormatado = ""+Formatador.formatarDouble(this.precoVenda,2)+"";
			return valorFormatado;
		}
		
	}
}