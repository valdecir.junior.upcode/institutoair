package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TaxaPagamento")]
	public class TaxaPagamento 
	{
		public var id: Number = NaN;
		public var percentual: Number = NaN;
		public var parcelaAte: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var empresaFinanceiro:EmpresaFinanceiro;		
		public var formaPagamento: FormaPagamento;
		
		public function TaxaPagamento()
		{
		}
	}
}