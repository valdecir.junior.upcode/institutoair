package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Modulo")]
	public class Modulo 
	{
		public var id: int;
		public var descricao: String;
		public var situacao: String;
		public function Modulo()
		{
		}
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "A")
			{
				return "Ativo";	
			}
			else if (this.situacao == "I")
			{
				return "Inativo";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

