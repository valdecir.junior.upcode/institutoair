package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.GrupoProdutoTotalizador")]
	public class GrupoProdutoTotalizador
	{
		public var id: int;
		public var grupoTotalizador:GrupoTotalizador;
		public var grupoProduto:GrupoProduto;
		
		public function GrupoProdutoTotalizador()
		{
		}
	}
}