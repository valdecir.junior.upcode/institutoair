package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EmpresaFisica")]
	public class EmpresaFisica 
	{
		public var id: Number = NaN;
		public var razaoSocial: String;
		public var nomeFantasia: String;
		public var cnpj: String;
		public var anoAberto: Number = NaN;
		public var mesAberto: Number = NaN;
		public var taxaJuros: Number = NaN;
		public var endereco: String;
		public var numero: String;
		public var cidade: Cidade;
		public var cidadeCache: String;
		public var ufCache: String;
		public var inscricaoEstadual: String;
		public var operadora: Operadora;
		public var telefone: String;
		public var grupoEmpresa:GrupoEmpresa;
		public var atividade:Atividade;
		public var cep:String;
		public var mesAbertoFin:Number = NaN;
		public var anoAbertoFin:Number = NaN;
		public var percImpostoConsumidor:Number = NaN;
		
		public function EmpresaFisica()
		{
		}
		
		public function get cnpjFormatado(): String
		{
			if (this.cnpj == null)
			{
				return this.cnpj;
			}	
			else
			{
				return Formatador.formatarCnpj(this.cnpj);
			}		
		}
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		public function get dataFormatada(): String
		{
			return this.mesAberto + "/" + this.anoAberto;
		}
		
		public function get cepFormatado():String
		{
			if(this.cep == null)
			{
				return this.cep;
			}
			else
			{
				return Formatador.formatarCep(this.cep);
			}
		}
		
		public function get idFormatado():String
		{
			return this.id.toString().length == 1 ? "F-0" + this.id.toString() : "F-" + this.id.toString();
		}
		
		public function get nomeConcatenado():String
		{
			return this.idFormatado + " " + this.razaoSocial;
		}
		
		public function get nomeFantasiaConcatenado():String 
		{
			return this.idFormatado + " " + this.nomeFantasia;
		}
	}
}

