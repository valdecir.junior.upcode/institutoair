package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LogDuplicata")]
	public class LogDuplicata 
	{
		public var id: Number = NaN;
		public var motivo: String;
		public var data: Date;
		public var valorOriginal: String;
		public var valorAlterado: String;
		public var nomeUsuario: String;
		public var nomeClasse:String;
		public var idDocumentoOrigem:String;
		public var idCliente:int;
		public var idEmpresaFisica:int;
		public function LogDuplicata()
		{
		}
		
		
	}
}

