package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Vendedor")]
	public class Vendedor 
	{
		public var id: int;
		public var empresaFisica: EmpresaFisica;
		public var nome: String;
		public var foto: ByteArray;
		public var codigoVendedorCache: int;
		public var usuario: Usuario;
		public function Vendedor()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return id + " - " + nome;
		}
		
		public function get nomeCodigoCache():String
		{
			return ""+this.codigoVendedorCache+" - "+this.nome;
		}
	}
}

