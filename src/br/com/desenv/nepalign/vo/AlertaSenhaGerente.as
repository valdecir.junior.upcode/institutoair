package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AlertaSenhaGerente")]
	public class AlertaSenhaGerente 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var situacao: String;
		public var usuarioAutorizador:Usuario;
		public function AlertaSenhaGerente()
		{
		}
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "S")
			{
				return "SIM";	
			}
			else if (this.situacao == "N")
			{
				return "NÃO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

