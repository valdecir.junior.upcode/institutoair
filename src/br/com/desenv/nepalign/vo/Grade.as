package br.com.desenv.nepalign.vo
{
	

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Grade")]
	public class Grade 
	{
		public var id: Number;
		public var descricao: String;
		public var tamanho1: Tamanho;
		public var tamanho2: Tamanho;
		public var tamanho3: Tamanho;
		public var tamanho4: Tamanho;
		public var tamanho5: Tamanho;
		public var tamanho6: Tamanho;
		public var tamanho7: Tamanho;
		public var tamanho8: Tamanho;
		public var tamanho9: Tamanho;
		public var tamanho10: Tamanho;
		public var tamanho11: Tamanho;
		public var tamanho12: Tamanho;
		public var tamanho13: Tamanho;
		public var tamanho14: Tamanho;
		public var tamanho15: Tamanho;
		public var tamanho16: Tamanho;
		public var tamanho17: Tamanho;
		public function Grade()
		{
		}
		
		
	}
}

