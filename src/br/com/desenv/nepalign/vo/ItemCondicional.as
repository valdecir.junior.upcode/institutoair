package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemCondicional")]
	public class ItemCondicional 
	{
		public var id: Number = NaN;
		public var condicional: Condicional;
		public var estoqueProduto: EstoqueProduto;
		public var itemSequencial: Number = NaN;
		public var quantidade: Number = NaN;
		public var precoTabela: Number = NaN;
		public var precoVenda: Number = NaN;
		public var situacao: String;
		public function ItemCondicional()
		{
		}
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "A")
			{
				return "ABERTO";	
			}
			else if (this.situacao == "D")
			{
				return "DEVOLVIDO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

