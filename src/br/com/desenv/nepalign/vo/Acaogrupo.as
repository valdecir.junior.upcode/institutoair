package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Acaogrupo")]
	public class Acaogrupo 
	{
		public var id: int;
		public var grupo: Grupo;
		public var acao: Acao;
		public function Acaogrupo()
		{
		}
		
		
	}
}

