package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.GrupoTotalizador")]
	public class GrupoTotalizador
	{
		public var id: int;
		public var descricao: String;
		
		public function GrupoTotalizador()
		{
		}
	}
}