package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CentroFinanceiro")]
	public class CentroFinanceiro 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var descricao: String;
		public var tipoCentroFinanceiro: TipoCentroFinanceiro;
		public function CentroFinanceiro()
		{
		}
	}
}