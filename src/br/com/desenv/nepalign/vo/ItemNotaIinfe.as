package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemNotaIinfe")]
	public class ItemNotaIinfe 
	{
		public var id: Number = NaN;
		public var itemNota: ItemNotaFiscal;
		public var identificadorDI: String;
		public var dataRegistroDI: Date;
		public var siglaUFDesembaraco: String;
		public var numeroAdicao: String;
		public var numSeqItemAdicao: String;
		public var codFabricanteEstrangeiro: String;
		public var numeroDSI: String;
		public var localDesembaraco: String;
		public var dataDesembaraco: Date;
		public var valorBaseCalcII: Number = NaN;
		public var valorDespAduaneiras: Number = NaN;
		public var valorImpostoImportacao: Number = NaN;
		public var valorImpostoOperFinanceiras: Number = NaN;
		public var codigoExportador: String;
		public function ItemNotaIinfe()
		{
		}
		
		
	}
}

