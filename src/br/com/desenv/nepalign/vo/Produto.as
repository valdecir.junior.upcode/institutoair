package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Produto")]
	public class Produto 
	{
		public var id: Number = NaN;
		public var codigoProduto: String;
		public var codigoEan: String;
		public var nomeProduto: String;
		public var codigoNcm: String;
		public var exptipi: String;
		public var unidadeMedida: UnidadeMedida;
		public var tipoItem: TipoItem;
		public var regimeTributario: RegimeTributario;
		public var situacaoTributaria: SituacaoTributaria;
		public var origemMercadoria: OrigemMercadoria;
		public var mva: Mva;
		public var aliquotaIcms: Number;
		public var listaServico: ListaServico;
		public var tipoLista:TipoLista;
		public var grupo: GrupoProduto;
		public var substitutoTributario: String;
		public var aplicacao: String;
		public var ativoInativo: String;
		public var marca: Marca;
		public var fornecedor: Fornecedor;
		public var pesoProduto: Number;
		public var codigoCatalogo: String;
		public var codigoFabricante: String;
		public var nome1Produto: String;
		public var nome2Produto: String;
		public var nome3Produto: String;
		public var nome4Produto: String;
		public var nome5Produto: String;
		public var valoresDescritivo1: String;
		public var valoresDescritivo2: String;
		public var valoresDescritivo3: String;
		public var garantia: Number;
		public var altura: Number;
		public var largura: Number;
		public var profundidade: Number;
		public var genero: Genero;
		public var ocasiaoUso: OcasiaoUso;
		public var material: Material;
		public var localfoto: String;
		public var exemploFoto: ByteArray;
		public var ultimaAlteracao: Date;
		public var produtoLV: String;
		public var ultAlteracaoPreco: Date;
		public var situacaoEnvioLV: SituacaoEnvioLV;
		public var precoPromocao: Number;
		public function Produto()
		{
		}
		
		public function get substitutoTributarioDescricaoCompleta(): String
		{
			if (this.substitutoTributario == null)
			{
				return this.substitutoTributario;
			}
			else if (this.substitutoTributario == "S")
			{
				return "Sim";	
			}
			else if (this.substitutoTributario == "N")
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		public function get ativoInativoDescricaoCompleta(): String
		{
			if (this.ativoInativo == null)
			{
				return this.ativoInativo;
			}
			else if (this.ativoInativo == "A")
			{
				return "Ativo";	
			}
			else if (this.ativoInativo == "I")
			{
				return "Inativo";	
			}
			else
			{
				return "";
			}
		}
		public function get produtoLVDescricaoCompleta(): String
		{
			if (this.produtoLV == null)
			{
				return this.produtoLV;
			}
			else if (this.produtoLV == "S")
			{
				return "Sim";	
			}
			else if (this.produtoLV == "N")
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		
		public function get descricaoProduto():String
		{
			return this.id +" - "+ this.nomeProduto;
		}
		
	}
}

