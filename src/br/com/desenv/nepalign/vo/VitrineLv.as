package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.VitrineLv")]
	public class VitrineLv 
	{
		public var id: Number = NaN;
		public var nome: String;
		public var descricao: String;
		public var imagem: String;
		public var dataInicio: Date;
		public var dataFinal: Date;
		public var ativo: String;
		public var numeroColunas: Number = NaN;
		public var paginacao: String;
		public var randomizar: String;
		public var htmlCustomizado: String;
		public var orderExibicao: Number = NaN;
		public var tipoExibicao: String;
		public var marca: Marca;
		public var situacaoEnvioLV:SituacaoEnvioLV;
		public function VitrineLv()
		{
		}
		
		public function get ativoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "0")
			{
				return "Inativo";	
			}
			else if (this.ativo == "1")
			{
				return "Ativo";	
			}
			else
			{
				return "";
			}
		}
		public function get paginacaoDescricaoCompleta(): String
		{
			if (this.paginacao == null)
			{
				return this.paginacao;
			}
			else if (this.paginacao == "0")
			{
				return "Sim";	
			}
			else if (this.paginacao == "1")
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		public function get randomizarDescricaoCompleta(): String
		{
			if (this.randomizar == null)
			{
				return this.randomizar;
			}
			else if (this.randomizar == "0")
			{
				return "Sim";	
			}
			else if (this.randomizar == "1")
			{
				return "Não";	
			}
			else
			{
				return "";
			}
		}
		public function get tipoExibicaoDescricaoCompleta(): String
		{
			if (this.tipoExibicao == null)
			{
				return this.tipoExibicao;
			}
			else if (this.tipoExibicao == "0")
			{
				return "Vitrine";	
			}
			else if (this.tipoExibicao == "1")
			{
				return "Vitrine Home";	
			}
			else if (this.tipoExibicao == "2")
			{
				return "HotSite";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

