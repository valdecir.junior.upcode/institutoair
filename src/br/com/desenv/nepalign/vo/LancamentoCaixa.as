package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LancamentoCaixa")]
	public class LancamentoCaixa 
	{
		public var id:Number = NaN;
		public var empresaCaixa:EmpresaCaixa = null;
		public var caixa:Caixa = null;
		public var contaContabil:ContaContabil = null;
		public var tipoMovimentoCaixa:TipoMovimentoCaixa = null;
		public var dataLancamento:Date;
		public var dataCompensacao:Date;
		public var dataRecebimento:Date;
		public var valor:Number = NaN;
		public var observacao:String = null;
		
		public function get historico():String
		{
			return tipoMovimentoCaixa.descricao + " - " + observacao;
		}
		
		public function get valorFormatado():String
		{
			return Formatador.formatarDouble(valor, 0x02);
		}
	}
}