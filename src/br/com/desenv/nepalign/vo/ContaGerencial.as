package br.com.desenv.nepalign.vo
{

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ContaGerencial")]
	public class ContaGerencial 
	{
		public var id: Number = NaN;
		public var codigoContaNivel1: Number = NaN;
		public var codigoContaNivel2: Number = NaN;
		public var codigoContaNivel3: Number = NaN;
		public var codigoContaNivel4: Number = NaN;
		public var descricao: String;
		public var contaContabil: String;
		public var codigoReduzido: Number = NaN;
		public var creditoDebito:String;
		public var exibeDespesas:String;
		public var ativa:Number = NaN;
		
		public function ContaGerencial()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return this.contaContabil + "-" + descricao;
		}
	}
}