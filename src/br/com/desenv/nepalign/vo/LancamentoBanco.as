package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LancamentoBanco")]
	public class LancamentoBanco 
	{
		public var id:Number = NaN;
		public var empresaBancos:EmpresaBancos = null;
		public var banco:Bancos = null;
		public var contaContabil:ContaContabil = null;
		public var tipoMovimentoBancos:TipoMovimentoBancos = null;
		public var dataLancamento:Date;
		public var dataCompensacao:Date;
		public var dataRecebimento:Date;
		public var valor:Number = NaN;
		public var observacao:String = null;
		
		public function get historico():String
		{
			return tipoMovimentoBancos.descricao + " - " + observacao;
		}
		
		public function get valorFormatado():String
		{
			return Formatador.formatarDouble(valor, 0x02);
		}
	}
}