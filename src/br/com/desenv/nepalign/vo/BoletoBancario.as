package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.BoletoBancario")]
	public class BoletoBancario 
	{
		public var id: Number = NaN;
		public var notaFiscal: NotaFiscal;
		public var contaCorrente: ContaCorrente;
		public var dataGeracao: Date;
		public var dataVencimento: Date;
		public var dataPagamento: Date;
		public var valorOriginal: Number = NaN;
		public var valorPago: Number = NaN;
		public var nossoNumero: Number = NaN;
		public var numeroDocumento: Number = NaN;
		public var cupomFiscal: Number = NaN;
		public var linhaDigitavel: String;
		public var codigoBarras: String;
		public var codigoPosto: String;
		public var codigoCedente: String;
		public var cliente: Cliente;
		public var duplicataParcela: DuplicataParcela;
		public var clienteCache: String;
		public var duplicataCache: Number = NaN;
		public var parcelaDuplicataCache: Number = NaN;
		public var sequencialBoleto: Number = NaN;
		public var nomeSacado: String;
		public var cpfCnpjSacado: String;
		public var logradouroSacado: String;
		public var numeroLogradouroSacado: Number = NaN;;
		public var complementoLogradoruSacado: String;
		public var cepSacado: Number = NaN;
		public var observacao1: String;
		public var observacao2: String;
		public var observacao3: String;
		public var valorDesconto: Number = NaN;;
		public var valorJurosDiario: Number = NaN;;
		public var cidadeSacado:String;
		public var estadoSacado:String;
		public var bairroSacado:String;
		public var situacaoEnvio: Number = NaN;
		public var selecionado:Boolean = false;
		public var valorAcrescimos: Number = NaN;
		public var ocorrencia:OcorrenciaBoletoBancario;
		public function BoletoBancario()
		{
		}
		public function get situacaoDescricaoCompleta():String
		{
			if(this.situacaoEnvio == 0)
				return "Arquivo Não Gerado";
			if(this.situacaoEnvio == 1)
				return "Arquivo Gerado";
			return "";
		}
		public function get clienteCacheCompleto():String
		{
			if(this.clienteCache != null && this.clienteCache != "")
				return this.clienteCache + " - "+this.nomeSacado;
			else
				return this.cliente.nomeCompleto;
		}
		public function get duplicataCacheCompleto():String
		{
			if(!isNaN(this.duplicataCache))
				return this.duplicataCache + "\\"+this.parcelaDuplicataCache;
			else
				return this.duplicataParcela.numeroDuplicata + "\\"+this.duplicataParcela.numeroParcela;
		}
	}
	
}
