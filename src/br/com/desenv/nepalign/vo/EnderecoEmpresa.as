package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EnderecoEmpresa")]
	public class EnderecoEmpresa 
	{
		public var id: Number = NaN;
		public var empresa: Empresa;
		public var tipoLogradouro: TipoLogradouro;
		public var cidade: Cidade;
		public var tipoEndereco: TipoEndereco;
		public var endereco: String;
		public var complemento: String;
		public var bairro: String;
		public var cep: String;
		public var correspondencia: Number = NaN;
		public var faturamento: Number = NaN;
		public var entrega: Number = NaN;
		public var cobranca: Number = NaN;
		public var numero: Number = NaN;
		public var pais: Pais;
		public function EnderecoEmpresa()
		{
		}
		
		public function get cepFormatado(): String
		{
			if (this.cep == null)
			{
				return this.cep;
			}	
			else
			{
				return Formatador.formatarCep(this.cep);
			}		
		}
		public function get correspondenciaDescricaoCompleta(): String
		{
			if (this.correspondencia == 0)
			{
				return "Nao";
			}
			else if (this.correspondencia == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get faturamentoDescricaoCompleta(): String
		{
			if (this.faturamento == 0)
			{
				return "Nao";
			}
			else if (this.faturamento == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get entregaDescricaoCompleta(): String
		{
			if (this.entrega == 0)
			{
				return "Nao";
			}
			else if (this.entrega == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get cobrancaDescricaoCompleta(): String
		{
			if (this.cobranca == 0)
			{
				return "Nao";
			}
			else if (this.cobranca == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

