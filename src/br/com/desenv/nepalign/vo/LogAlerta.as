package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.charts.chartClasses.DataDescription;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LogAlerta")]
	public class LogAlerta 
	{
		public var id: int;
		public var alertaLoja: AlertaLoja;
		public var tipoLogAlerta: TipoLogAlerta;
		public var data: Date;
		public var usuario: Usuario;
		
		public function get dataFormatada() :String
		{
			const d = '.';
			const e = '';
			const s = ':';
			const b = '/';
			
			var date:Date = this.data;
			
			var resultDate:String = "";
			
			resultDate +=  e.concat(pad(date.day, 2), b, pad(date.month, 2), b,  pad(date.fullYear, 2));
			resultDate +=  " " + e.concat(pad(date.hours, 2), s, pad(date.minutes, 2));

			
			return resultDate;
			
			function pad(value:String, length:int):String
			{
				const zero = '0';
				var result:String = value;
				while (result.length < length)
				{
					result = zero.concat(result);
				}
				return result;
			}
		}
		
				
		public function LogAlerta()
		{
		}
		
		
	}
}

