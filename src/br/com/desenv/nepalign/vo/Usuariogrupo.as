package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Usuariogrupo")]
	public class Usuariogrupo 
	{
		public var id: int;
		public var usuario: Usuario;
		public var grupo: Grupo;
		public var empresa: Empresa;
		public function Usuariogrupo()
		{
		}
		
		
	}
}

