package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Logprodutossemncm")]
	public class Logprodutossemncm 
	{
		public var id: Number = NaN;
		public var produto: Number = NaN;
		public var ncmAntigo: String;
		public var ncmNovo: String;
		public var dataLog: Date;
		public var empresa: Number = NaN;
		public var numeroNotaFiscal: Number = NaN;
		public var observacao: String;
		public function Logprodutossemncm()
		{
		}
		
		
	}
}

