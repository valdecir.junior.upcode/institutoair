package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LancamentoContaPagarFixo")]
	public class LancamentoContaPagarFixo 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var fornecedor: Fornecedor;
		public var centroFinanceiro: CentroFinanceiro;
		public var contaGerencial: ContaGerencial;
		public var tipoConta: TipoConta;
		public var contaCorrente: ContaCorrente;
		public var formaPagamento: FormaPagamento;
		public var valor: Number = NaN;
		public var diaLancamento: Number = NaN;
		public var observacao: String;
		public function LancamentoContaPagarFixo()
		{
		}
		
		
	}
}

