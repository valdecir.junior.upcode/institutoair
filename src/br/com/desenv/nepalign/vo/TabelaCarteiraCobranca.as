package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import mx.controls.DateField;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TabelaCarteiraCobranca")]
	public class TabelaCarteiraCobranca 
	{
		public var id: Number = NaN;
		public var data: Date;
		public var empresaFisica: EmpresaFisica;
		public var percentual: Number = NaN;
		public var sequencial:int;
		public var tipo:String;
		public var empresaFinanceiro:EmpresaFinanceiro;
		public function TabelaCarteiraCobranca()
		{
		}
	public function get dataMesAno():String
	{
		
		var mes:String =""+ (data.month+1);
		var ano:String =""+ data.fullYear;
		mes = mes.length>=2?mes:"0"+mes;
		var s:String = mes+"/"+ano;
		return s;
	}
	public function get percentualCompleto():String
	{
		var s:String = Formatador.formatarDouble(percentual,2);
		s+"%";
		return s;
	}
		
	}
}

