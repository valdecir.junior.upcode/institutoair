package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.OperadoraCartao")]
	public class OperadoraCartao 
	{
		public var id: Number = NaN;
		public var descricao: String;
		
		public function OperadoraCartao()
		{
		}
		
		[Bindable]
		public function get descricaoCompleta():String
		{
			return id + " - " + descricao;
		}
	}
}