package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.NotaFiscal")]
	public class NotaFiscal 
	{
		public var id: Number = NaN;
		public var empresa: EmpresaFisica;
		public var movimentacaoEstoque: MovimentacaoEstoque;
		public var pedidoVenda: PedidoVenda;
		public var tipoNotaFiscal: TipoNotaFiscal;
		public var cfop: Cfop;
		public var transportadora: Transportadora;
		public var situacaoNotaFiscal: SituacaoNotaFiscal;
		public var cliente: Cliente;
		public var numeroFormulario: Number = NaN;
		public var dataEmissao: Date;
		public var numero: Number = NaN;
		public var baseCalculoIss: Number = NaN;
		public var valorIss: Number = NaN;
		public var baseCalculoIcms: Number = NaN;
		public var valorIcms: Number = NaN;
		public var baseCalculoIcmsSubstituicao: Number = NaN;
		public var valorIcmsSubstituicao: Number = NaN;
		public var valorFrete: Number = NaN;
		public var valorSeguro: Number = NaN;
		public var valorTotalIpi: Number = NaN;
		public var valorOutrasDespesas: Number = NaN;
		public var valorTotalServico: Number = NaN;
		public var valorTotalProduto: Number = NaN;
		public var valorTotalNota: Number = NaN;
		public var observacao: String;
		public var cifFob: String;
		public var placaVeiculo: String;
		public var horaSaidaEntrada: Date;
		public var dataSaidaEntrada: Date;
		public var valorTotalContaReceber: Number = NaN;
		public var situacao: Number = NaN;
		public var inscrSubsTrib: String;
		public var quantidadeVolumes: Number = NaN;
		public var peso: Number = NaN;
		public var dataFinalizacao: Date;
		public var entradaSaida: String;
		public var idNotaFiscalVendaConsignacao: String;
		public var idNotaFiscalDevolucaoConsignacao: String;
		public var cfop2: Cfop;
		public var cfop3: Cfop;
		public var valorTotalPis: Number = NaN;
		public var valorTotalCofins: Number = NaN;
		public var valorTotalCsll: Number = NaN;
		public var valorTotalIrrf: Number = NaN;
		public var formaPagamentoNfe: FormaPagamento;
		public var observacoesComplementares: String;
		public var statusNfe: Number = NaN;
		public var dataRetorno: Date;
		public var numProtocoloSefaz: String;
		public var chaveAcesso: String;
		public var motivoStatus: String;
		public var codigoInutilizacao: String;
		public var statusInutilizacao: String;
		public var statusImpressao: Number = NaN;
		public var serieContingencia: String;
		public var serie: String;
		public var statusCancelamento: Number = NaN;
		public var statusContingencia: Number = NaN;
		public var motivoCancelamento: String;
		public var retornoStatusCancelamento: String;
		public var retornoStatusImpressao: String;
		public var motivoInutilizacao: String;
		public var retornoStatusContingencia: String;
		public var valorRetencaoPis: Number = NaN;
		public var valorRetencaoCofins: Number = NaN;
		public var valorRetencaoIrrf: Number = NaN;
		public var valorRetencaoCsll: Number = NaN;
		public var valorDescontoTotal: Number = NaN;
		public var valorIITotal: Number = NaN;
		public var nfeReferenciada: String;
		public var codigoUFNFeReferenciada: Number = NaN;
		public var anoMesEmissaoNFeRef: String;
		public var cnpjEmitente: String;
		public var modeloNfeRef: String;
		public var serieNFeRef: String;
		public var numeroNFeRef: String;
		public var uf: Uf;
		public var finalidade: Number = NaN;
		public var modelo: Number = NaN;
		public var cpfReceptor:String;
		public var tpAmb:Number = NaN;
		public var email:String;
		public var enviouEmail:Number = NaN;
		public var selecionado:Boolean;
		public var valorImpostoMunicipal: Number = NaN;
		public var valorImpostoEstadual: Number = NaN;
		public var valorImpostoFederal: Number = NaN;
		
		public function NotaFiscal()
		{
		}
		
		public function get cifFobDescricaoCompleta(): String
		{
			if (this.cifFob == null)
			{
				return this.cifFob;
			}
			else if (this.cifFob == "C")
			{
				return "CIF";	
			}
			else if (this.cifFob == "F")
			{
				return "FOB";	
			}
			else
			{
				return "";
			}
		}
		public function get entradaSaidaDescricaoCompleta(): String
		{
			if (this.entradaSaida == null)
			{
				return this.entradaSaida;
			}
			else if (this.entradaSaida == "E")
			{
				return "ENTRADA";	
			}
			else if (this.entradaSaida == "S")
			{
				return "SAIDA";	
			}
			else
			{
				return "";
			}
		}
		public function get statusImpressaoDescricaoCompleta(): String
		{
			if (this.statusImpressao == 0)
			{
				return "Nao";
			}
			else if (this.statusImpressao == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
		public function get enviouEmailDescricaoCompleta(): String
		{
			if (this.enviouEmail == 0)
			{
				return "Nao";
			}
			else if (this.enviouEmail == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get finalidadeDescricaoCompleta(): String
		{
			if (this.finalidade == 0)
			{
				return "Nao";
			}
			else if (this.finalidade == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
	
		public function get motivoFormatado():String
		{
			return motivoStatus.split("\n")[0];
			//return motivoStatus.substring(0,99).replace("\n","").replace("\r","");
		}
		
		public function get notaFiscalConsumidorDescricao():String
		{
			if (isNaN(this.modelo))
			{
				return this.modelo + "";
			}
			else if (this.modelo == 55 || isNaN(this.modelo))
			{
				return "NF-e";	
			}
			else if (this.modelo == 65)
			{
				return "NFC-e";	
			}
			else
			{
				return "";
			}
		}
		public function get cpfFormatado(): String
		{
			if (this.cpfReceptor == null)
			{
				return this.cpfReceptor;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cpfReceptor);
			}		
		}
		
		public function get tpAmbFormatado():String
		{
			if(isNaN(this.tpAmb))
				return this.tpAmb + "";
			else if(this.tpAmb == 1)
				return "Produção";
			else if(this.tpAmb == 2)
				return "Homologação";
			else
				return "";
		}
		
	}
}

