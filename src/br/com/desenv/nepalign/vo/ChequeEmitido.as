package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ChequeEmitido")]
	public class ChequeEmitido 
	{
		public var id: Number = NaN;
		public var talaoCheque: TalaoCheque;
		public var valor: Number = NaN;
		public var numeroCheque: Number = NaN;
		public var preDatado: Number = NaN;
		public var dataEmissao: Date;
		public var dataPrevistaDeposito: Date;
		public var dataDeposito: Date;
		public var responsavelEmissao: String;
		public var observacao: String;
		public var situacao: Number = NaN;
		public function ChequeEmitido()
		{
		}
		
		public function get preDatadoDescricaoCompleta(): String
		{
			if (this.preDatado == 0)
			{
				return "Nao";
			}
			else if (this.preDatado == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

