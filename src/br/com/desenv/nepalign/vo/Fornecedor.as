package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Fornecedor")]
	public class Fornecedor 
	{
		public var id: Number;
		public var tipoPessoa: TipoPessoa;
		public var dataCadastro: Date;
		public var nomeFantasia: String;
		public var razaoSocial: String;
		public var cnpjCpf: String;
		public var inscricaoEstadualRg: String;
		public var atividade: Atividade;
		public var statusEntidade: StatusEntidade;
		public var nomeEtiqueta: String;
		public var enderecoFornecedor: String;
		public var complementoEndereco: String;
		public var bairro: String;
		public var cep: String;
		public var cidade: Cidade;
		public var contato: String;
		public var operadoraTelefone: Operadora;
		public var telefone: String;
		public var fax: String;
		public var observacao: String;
		public var comentario: String;
		public var email: String;
		public var inscricaoMunicipal: String;
		public var classificacaoFornecedor: ClassificacaoFornecedor;
		public var categoriaFornecedor: CategoriaFornecedor;
		public var naturezaFornecedor: NaturezaFornecedor;
		public var numeroEndereco: String;
		public var operadoraFax: Operadora;
		public var grupoFornecedor:GrupoFornecedor;
		public var tipoFornecedor: String;
		public function Fornecedor()
		{
		}
		
		public function get cnpjCpfFormatado(): String
		{
			if (this.cnpjCpf == null)
			{
				return this.cnpjCpf;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cnpjCpf);
			}		
		}
		public function get cepFormatado(): String
		{
			if (this.cep == null)
			{
				return this.cep;
			}	
			else
			{
				return Formatador.formatarCep(this.cep);
			}		
		}
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		public function get faxFormatado(): String
		{
			if (this.fax == null)
			{
				return this.fax;
			}	
			else
			{
				return Formatador.formatarFone(this.fax);
			}		
		}
		public function get tipoFornecedorDescricaoCompleta(): String
		{
			if (this.tipoFornecedor == null)
			{
				return this.tipoFornecedor;
			}
			else if (this.tipoFornecedor == "F")
			{
				return "Fornecedor";	
			}
			else if (this.tipoFornecedor == "C")
			{
				return "Cliente";	
			}
			
			else if (this.tipoFornecedor == "A")
			{
				return "Ambos";	
			}
			else
			{
				return "";
			}
		}
		
		public function get nomeConcatenado():String
		{
			return this.id.toString() + "-" + this.razaoSocial;
		}
	}
}