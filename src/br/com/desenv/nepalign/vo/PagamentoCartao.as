package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PagamentoCartao")]
	public class PagamentoCartao 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var dataEmissao: Date;
		public var emitente: String;
		public var formaPagamento: FormaPagamento;
		public var valorOriginal: Number = NaN;
		public var valorTaxado: Number = NaN;
		public var numeroParcelas: Number = NaN;
		public var valorParcela: Number = NaN;
		public var taxaPagamento: TaxaPagamento;
		public var caixaDia: CaixaDia;
		public var operadoraCartao:OperadoraCartao;
		public function PagamentoCartao()
		{
		}
		
		
	}
}

