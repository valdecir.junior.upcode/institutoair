package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemDocumentoEntradaSaida")]
	public class ItemDocumentoEntradaSaida 
	{
		public var id: Number;
		public var documentoEntradaSaida: DocumentoEntradaSaida;
		public var produto: Produto;
		public var cor: Cor;
		public var ordemProduto: OrdemProduto;
		public var sequencial: Number;
		public var descricaoProduto: String;
		public var grade: Grade;
		public var quantidade1: Number;
		public var quantidade2: Number;
		public var quantidade3: Number;
		public var quantidade4: Number;
		public var quantidade5: Number;
		public var quantidade6: Number;
		public var quantidade7: Number;
		public var quantidade8: Number;
		public var quantidade9: Number;
		public var quantidade10: Number;
		public var quantidade11: Number;
		public var quantidade12: Number;
		public var quantidade13: Number;
		public var quantidade14: Number;
		public var quantidade15: Number;
		public var quantidade16: Number;
		public var quantidade17: Number;
		public var quantidadeTotal: Number;
		public var valorUnitarioCompra: Number;
		public var valorUnitarioVenda: Number;
		public var valorTotalCompra: Number;
		public var valorTotalVenda: Number;
		public var situacao: String;
		public var observacao: String;
		public var itemPedidoCompraVenda:ItemPedidoCompraVenda;
		
		public function ItemDocumentoEntradaSaida()
		{
		}
		
		private var _letraGrade: String;
		
		
		public function set letraGrade(letraGrade: String): void
		{
			this._letraGrade = letraGrade;
		}
		
		public function get letraGrade(): String
		{
			return this._letraGrade;
		}		
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "1")
			{
				return "Entregue";	
			}
			else if (this.situacao == "2")
			{
				return "Não Entregue";	
			}
			else if (this.situacao == "3")
			{
				return "Baixa Autorizada";	
			}
			else
			{
				return "";
			}
		}
		
		public function get produtoCompleto(): String
		{
			return this.produto.id + "-" + this.produto.nomeProduto;
		}
		
		public function get ordemProdutoCompleto(): String
		{
			return this.ordemProduto.id + "-" + this.ordemProduto.descricao;
		}
		
		public function get corCompleto(): String
		{
			return this.cor.id + "-" + this.cor.descricaoCorEmpresa;
		}
		
		public function get valorUnitarioCustoFormatado(): String
		{
			return Formatador.formatarDouble(this.valorUnitarioCompra, 2);
		}
		
		public function get valorTotalCustoFormatado(): String
		{
			return Formatador.formatarDouble(this.valorTotalCompra, 2);
		}
		
		public function get valorUnitarioVendaFormatado(): String
		{
			return Formatador.formatarDouble(this.valorUnitarioVenda, 2);
		}
		
		public function get valorTotalVendaFormatado(): String
		{
			return Formatador.formatarDouble(this.valorTotalVenda, 2);
		}
		
		public function get valorDescricaoBaixaCompleto(): String
		{
			if (this.id == -1)
			{
				return "Baixado";
			}
			else if (this.id == -2)
			{
				return "Saldo";
			}
			else
			{
				return "Original";
			}
			
		}
		
	}
}

