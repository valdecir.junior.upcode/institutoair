package br.com.desenv.nepalign.vo
{
	import mx.controls.DateField;
	import mx.core.FlexGlobals;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.DuplicataParcela")]
	public class DuplicataParcela 
	{
		public var id: Number = NaN;
		public var duplicata: Duplicata;
		public var empresaFisica: EmpresaFisica;
		public var cliente: Cliente;
		public var usuario: Usuario;
		public var numeroDuplicata: Number = NaN;
		public var numeroParcela:Number = NaN  ;
		public var dataVencimento: Date;
		public var valorParcela: Number = NaN;
		public var situacao: String;
		public var valorPago: Number = NaN;
		public var dataBaixa: Date;
		public var tipoPagamento: String;
		public var dataVencimentoAcordado: Date;
		public var selecionado:Boolean;
		public var formaPagamento:FormaPagamento;
		public var chequeRecebido:ChequeRecebido;
		public var cobrador:Cobrador;
		public var acordo:String;
		public var jurosInformado:Number= NaN;
		public var jurosOriginal:Number = NaN;
		public var valorDesconto:Number = NaN;
		public var dataCobranca:Date;
		public var destaque:Boolean = false;
		public var jurosPago:Number = NaN;
		public var valorAPagarOriginal:Number =NaN;
		public var valorAPagarInformado:Number = NaN;
		public var seprocadaReativada:String;
		public var cobranca:String;
		private var dataAtual:Date = FlexGlobals.topLevelApplication.dataAtual == null?new Date():FlexGlobals.topLevelApplication.dataAtual;
		
		public function DuplicataParcela() 
		{ 
	
		}
		public function get acordoDescricao():String
		{
			if (this.acordo == "S")
			{
				return "SIM"
			}
			else if (this.acordo == "N")
			{
				return "NÃO";	
			}else
			{
				return "";
			}
		}
		public function get seprocadaReativadaDescricao():String
		{
			if (this.seprocadaReativada == "R")
			{
				return "REATIVADA"
			}
			else if (this.seprocadaReativada == "S")
			{
				return "SEPROCADA";	
			}else
			{
				return "";
			}
		}
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "0")
			{
				return "Aberto";	
			}
			else if (this.situacao == "1")
			{
				return "Baixado";	
			}
			else if (this.situacao =="2")
			{
				return "Parcial";
			}
			else if (this.situacao == "3")
			{
				return "Baixado por Acordo";
			}
			else
			{
				return "";
			}
		}
		public function get tipoPagamentoDescricaoCompleta(): String
		{
			if (this.tipoPagamento == null)
			{
				return this.tipoPagamento;
			}
			else if (this.tipoPagamento == "1")
			{
				return "Total";	
			}
			else if (this.tipoPagamento == "2")
			{
				return "Parcial";	
			}
			else if (this.tipoPagamento == "5")
			{
				return "Acordo";	
			}
			else
			{
				return "";
			}
		}
		public function get valorApagarSemJuros():Number
		{
			var apagar:Number = this.valorParcela;
			var juros:Number  = 0.0;
			if(this.situacao == "0")
			{
				apagar = apagar - (isNaN( this.valorPago)?0.0:this.valorPago);
			}
			else if (this.situacao == "2")
			{
				var valor:Number = 0.0;
				if(this.valorPago<this.jurosPago)
				{
					valor = (this.jurosPago - valorPago)+this.valorParcela
				}
				else 
				{
					valor = this.valorParcela - (valorPago -this.jurosPago )
				}
				apagar = valor;
			}			
			else
			{
				apagar = 0;
			}
			if(apagar<0)
			{
				apagar = 0;
			}
			return parseFloat(apagar.toFixed(2));
		}
		public function get valorApagar():Number
		{
			if(this.situacao == "0" )
			{		
				var apagar:Number = valorPagarBruto;
				var juros:Number  = 0.0;
				if(!isNaN(this.jurosInformado) )
				{
					juros = this.jurosInformado;
				}
				else 
				{
					juros = this.jurosCalculado;
				}
				apagar = apagar + juros;
				return parseFloat(apagar.toFixed(2));
			}
			else if(this.situacao == "2")
			{
				var valor:Number = 0.0;
				if(this.valorPago<this.valorJuros)
				{
					valor = (this.jurosPago - valorPago)+this.valorParcela;
				}
				else 
				{
					valor = this.valorParcela - (valorPago -this.jurosPago );
				}
				valor+=this.jurosCalculado
				return parseFloat(valor.toFixed(2));;
			}
			else
			{
				return 0
			}
		}
		public function get diasVencidos():String
		{
			if(this.dataBaixa!=null && this.situacao == "2")
			{
				var dt:Date = null;
				if(this.dataBaixa.getTime()<=this.dataVencimento.getTime())
				{
					dt = this.dataVencimento;
				}
				else
				{
					dt = this.dataBaixa;
				}
				var diasBaixa:int = parseInt(((dt.getTime()- dataAtual.getTime())/(1000 * 60 * 60 * 24)).toString());
				if(dias<0)
				{
					diasBaixa = diasBaixa*(-1);
				}else
				{
					diasBaixa = diasBaixa*(-1);
				}
				return ""+diasBaixa;
				
			}
			else if(this.situacao == "0")
			{
				var dias:int = parseInt(((dataVencimento.getTime()-dataAtual.getTime())/(1000 * 60 * 60 * 24)).toString());
				if(dias<0){
					dias = dias*(-1);
				}else
				{
					dias = dias*(-1);
				}
				return ""+dias;
			}
			else
			{
				var dias:int = parseInt(((dataVencimento.getTime()-this.dataBaixa.getTime())/(1000 * 60 * 60 * 24)).toString());
				if(dias<0){
					dias = dias*(-1);
				}else
				{
					dias = dias*(-1);
				}
				return ""+dias;
			}
		}
		
		public function get diasAtraso():int
		{
			var dias:int =(dataVencimento.getTime()-dataAtual.getTime())/(1000 * 60 * 60 * 24);
			if(situacao == "0" || situacao == "2")
			{
				if(dias<0){
					dias = dias*(-1);
				}else
				{
					dias = 0;
				}
			}
			else
				dias = 0;
			return dias;
		}
		public function get valorJuros():Number
		{
			var dias:String = this.diasVencidos;
			var d:int = parseInt(dias);
			var taxa:Number = empresaFisica.taxaJuros;
			var retorno:Number = 0.0;
			
			
			var adicionalMenor:Number =0;
			var adicionalMaior:Number =0;
			if(d>FlexGlobals.topLevelApplication.parametrosSistema.getParametroPorEmpresa(FlexGlobals.topLevelApplication.usuarioLogado.empresaPadrao.id,"numeroMinimoDiasJuros"))
			{
				taxa = empresaFisica.taxaJuros;
				taxa = taxa /30;
				taxa = taxa * d;
				taxa = taxa /100;
				adicionalMenor = this.valorApagarSemJuros * taxa;
			}
			if(d>10)
			{
				adicionalMaior = this.valorApagarSemJuros * 0.02; 
			}
			//var decimal:Number = Math.pow(10, 2);
			
			var num:Number = 0.0;
			
			if(this.situacao =='0' || this.situacao =='2')
			{
				if(this.numeroParcela == 0)
				{
					return 0.0
				}
				if(!isNaN(jurosInformado))
				{
					retorno = jurosInformado;
					
				}
				else
				{
					
					retorno = (adicionalMaior+adicionalMenor);
				}
				
			}
			else 
			{
				if(!isNaN( jurosInformado))
				{
					retorno = jurosInformado;
				}
				else 
				{
					retorno = (adicionalMaior+adicionalMenor);
				}
			}
			
			return parseFloat(retorno.toFixed(2));
		}
		public function get jurosCalculado():Number
		{
			if(this.numeroParcela == 0)
			{
				return 0.0
			}
			var dias:String = this.diasVencidos;
			var d:int = parseInt(dias);
			var taxa:Number = empresaFisica.taxaJuros;
			var retorno:Number = 0.0;
			if(d==0)
			{
				return 0;	
			}
			var adicionalMenor:Number =0;
			var adicionalMaior:Number =0;
			if(d>FlexGlobals.topLevelApplication.parametrosSistema.getParametroPorEmpresa(FlexGlobals.topLevelApplication.usuarioLogado.empresaPadrao.id,"numeroMinimoDiasJuros"))
			{
				taxa = empresaFisica.taxaJuros;
				taxa = taxa /30;
				taxa = taxa * d;
				taxa = taxa /100;
				adicionalMenor = this.valorApagarSemJuros * taxa;
			}
			if(d>10)
			{
				adicionalMaior = this.valorApagarSemJuros * 0.02; 
			}
			retorno = (adicionalMaior+adicionalMenor);
			
			return parseFloat(retorno.toFixed(2));
		}
		public function get valorPagarBruto():Number
		{
			var retorno:Number = this.valorParcela;
			retorno = retorno - (isNaN(this.valorDesconto)==true?0:this.valorDesconto);
			//retorno = retorno + (isNaN(this.jurosPago)==true?0:this.jurosPago);
			//var retorno:Number = (isNaN(this.valorPago)==true?0:this.valorPago);
			if(this.situacao == "2")
			{
				var valor:Number = 0.0;
				if(this.valorPago<this.valorJuros)
				{
					valor = (this.jurosPago - valorPago)+this.valorParcela;
				}
				else 
				{
					valor = this.valorParcela - (valorPago -this.jurosPago );
				}
				return parseFloat(valor.toFixed(2));
				
			}
			else if(this.situacao == "0")
			{
				retorno = this.valorParcela - (isNaN(this.valorDesconto)==true?0:this.valorDesconto);;
			}
			return parseFloat(retorno.toFixed(2));
		}
		public function get valorParcelaArrendondado():Number
		{
			return parseFloat(this.valorParcela.toFixed(2));
		}
		public function get faturaCompleta():String
		{
			return this.numeroDuplicata+"/"+this.numeroParcela;
		}
		public function get valorJurosSituacao():String
		{
			if(this.situacao=="1" || this.situacao=="3")
			{
				return ""+this.jurosPago;
			}
			else if(this.situacao=="0" || this.situacao=="2")
			{
				return ""+this.jurosCalculado;
			}
			else
			{
				return "";
			}
		}
		public function get isJorrovi():Boolean
		{
			if(this.acordo=="S")
			{
				return this.duplicata.jorrovi;
			}
			else
			{
				if(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("dataSeralle")!=null)
				{
					var dataParametro:Date = DateField.stringToDate(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("dataSeralle"),"DD/MM/YYYY");
					var dataDuplicata:Date = this.duplicata.dataCompra;
					return (dataDuplicata.getTime() < dataParametro.getTime())
				}
				else
				{
					return true;
				}
			}
		}
		public function get descricaoCompletaIsJorrovi():String
		{
			return this.isJorrovi?"JOR":"SER";
		}
		public function get isValeCalcado():Boolean
		{
			var idPlano:Number = FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idPlanoPagamentoValeCalcadoCrediario")==null?NaN:parseFloat(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("idPlanoPagamentoValeCalcadoCrediario"));
			
			if(!isNaN(idPlano) && this.duplicata.pedidoVenda != null)
				return this.duplicata.pedidoVenda.planoPagamento.id == idPlano;
			else
				return false;
		}
		public function get descricaoCompletaIsValeCalcado():String
		{
			return this.isValeCalcado?"SIM":"NÃO";		
		}
	}
	
}