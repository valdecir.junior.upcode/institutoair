package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MetodoPagamento")]
	public class MetodoPagamento 
	{
		public var id: int;
		public var codigoVertico: String;
		public var descricao: String;
		public function MetodoPagamento()
		{
		}
		
		
	}
}

