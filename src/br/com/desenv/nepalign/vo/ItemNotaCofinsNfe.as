package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemNotaCofinsNfe")]
	public class ItemNotaCofinsNfe 
	{
		public var id: Number = NaN;
		public var itemNotaFiscal: ItemNotaFiscal;
		public var codigoCSTCOFINS: Number = NaN;
		public var valorBaseCalcCOFINS: Number = NaN;
		public var aliquotaCOFINS: Number = NaN;
		public var quantVendida: Number = NaN;
		public var aliquotaCOFINSValor: Number = NaN;
		public var valorCOFINS: Number = NaN;
		public var valorBaseCalcCOFINSST: Number = NaN;
		public var aliquotaCOFINSST: Number = NaN;
		public var quanVendidaCOFINSST: Number = NaN;
		public var aliquotaCONFINSValorST: Number = NaN;
		public var valorCOFINSST: Number = NaN;
		public function ItemNotaCofinsNfe()
		{
		}
		
		
	}
}

