package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TabelaPreco")]
	public class TabelaPreco 
	{
		public var id: Number = NaN;
		public var empresa: Empresa;
		public var descricao: String;
		public var restrita: Number = NaN;
		public var percDescontoDefault: Number = NaN;
		public var percLucroBrutoDefault: Number = NaN;
		public function TabelaPreco()
		{
		}
		
		public function get restritaDescricaoCompleta(): String
		{
			if (this.restrita == 0)
			{
				return "Nao";
			}
			else if (this.restrita == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

