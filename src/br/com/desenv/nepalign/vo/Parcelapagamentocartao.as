package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Parcelapagamentocartao")]
	public class Parcelapagamentocartao 
	{
		public var id: Number = NaN;
		public var pagamentoCartao: PagamentoCartao;
		public var valor: Number = NaN;
		public var dataVencimento: Date;
		public var numero: Number = NaN;
		public function Parcelapagamentocartao()
		{
		}
		
		
	}
}

