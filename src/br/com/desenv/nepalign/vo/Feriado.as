package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Feriado")]
	public class Feriado 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var dataFeriado: Date;
		public var diaSemana: String;
		public function Feriado()
		{
		}
		
		
	}
}

