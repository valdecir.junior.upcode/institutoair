package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ReferenciaBancaria")]
	public class ReferenciaBancaria 
	{
		public var id: int;
		public var cliente: Cliente;
		public var tipoConta: String;
		public var numeroBanco: int;
		public var nomeBanco: String;
		public var agencia: String;
		public var digitoVerificadorAgencia: String;
		public var conta: String;
		public var digitoVerificadorConta: String;
		public function ReferenciaBancaria()
		{
		}
		
		public function get tipoContaDescricaoCompleta(): String
		{
			if (this.tipoConta == null)
			{
				return this.tipoConta;
			}
			else if (this.tipoConta == "C")
			{
				return "Conta Corrente";	
			}
			else if (this.tipoConta == "P")
			{
				return "Poupanca";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

