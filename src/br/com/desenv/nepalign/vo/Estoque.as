package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Estoque")]
	public class Estoque 
	{
		public var id: int;
		public var descricao: String;
		public var ativo: String;
		public function Estoque()
		{
		}
		
		public function get ativoDescricaoCompleta(): String
		{
			if (this.ativo == null)
			{
				return this.ativo;
			}
			else if (this.ativo == "S")
			{
				return "SIM";	
			}
			else if (this.ativo == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

