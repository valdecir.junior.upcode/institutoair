package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EtiquetaVitrine")]
	public class EtiquetaVitrine
	{
		public var codigoBarras:String;
		public var idMarca:Number = 0;
		public var descricaoMarca:String;
		public var nomeProduto:String;
		public var numeroParcelas:int = 0;
		public var porcentagemAVista:String;
		public var precoParcela:Number = 0.0;
		public var precoPrazo:Number = 0.0;
		public var precoVista:Number = 0.0;
		public var quantidade:int = 0 ;
		public var tamanho:String;
		
		public function EtiquetaVitrine()
		{
		}
		
		public function get precoAVistaDescricao():String
		{
			
			if(isNaN(this.precoVista))
			{
				return "0.00";
			}
			else
			{
			  return "" + this.precoVista;
			}
			
		}
		
		public function get precoPrazoDescricao():String
		{
			
			if(isNaN(this.precoPrazo))
			{
				return "0.00";
			}
			else
			{
				return "" + this.precoPrazo;
			}
			
		}
		
		public function get precoParcelaDescricao():String
		{
			
			if(isNaN(this.precoParcela))
			{
				return "0.00";
			}
			else
			{
				return "" + this.precoParcela;
			}
			
		}
		
		public function stringAlizeMe():String
		{
			var str:String = this.codigoBarras + ";" + this.idMarca + ";" + this.descricaoMarca + ";" + this.nomeProduto + ";" + this.numeroParcelas + ";" + this.porcentagemAVista + ";" + this.precoParcela + ";" + this.precoPrazo + ";" + this.precoVista + ";" + this.quantidade + ";" + this.tamanho;
			return str;
		}
		
	}
	
}
