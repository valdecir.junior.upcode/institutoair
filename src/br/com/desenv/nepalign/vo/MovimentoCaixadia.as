package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MovimentoCaixadia")]
	public class MovimentoCaixadia 
	{
		public var id: Number = NaN;
		public var lancamentoContaReceber: LancamentoContaReceber;
		public var formaPagamento: FormaPagamento;
		public var fornecedor: Fornecedor;
		public var cliente: Cliente;
		public var tipoMovimentoCaixaDia:TipoMovimentoCaixaDia; 
		public var tipoConta: TipoConta;
		public var caixaDia: CaixaDia;
		public var pagamentoPedidoVenda: PagamentoPedidoVenda;
		public var usuario: Usuario;
		public var creditoDebito: String;
		public var numeroDocumento: String;
		public var dataVencimento: Date;
		public var cpfCnpjPortador: String;
		public var valor: Number = NaN;
		public var situacao: Number = NaN;
		public var observacao: String;
		public var movimentoParcelaDuplicata:MovimentoParcelaDuplicata;
		public var operacaoRecebimentoDuplicata:OperacaoRecebimentoDuplicata;
		public var diversos:String;
		public var mostrarBoletim:String;
		public var contaGerencial:ContaGerencial;
		public var operadoraCartao:OperadoraCartao;
		public var pagamentoCartao:PagamentoCartao;
		public function MovimentoCaixadia()
		{
		}
		public function get tipoMovimentoDescricao():String
		{
			if(tipoMovimentoCaixaDia!=null)
				return	tipoMovimentoCaixaDia.descricao;
			else
				return	"";
		}
		public function get contaGerencialDescricao():String
		{
			if(tipoMovimentoCaixaDia!=null)
				return	contaGerencial.descricaoCompleta;
			else
				return	"";
		}
		public function get creditoDebitoDescricaoCompleta(): String
		{
			if (this.creditoDebito == null)
			{
				return this.creditoDebito;
			}
			else if (this.creditoDebito == "C")
			{
				return "Entrada";	
			}
			else if (this.creditoDebito == "D")
			{
				return "Saída";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

