package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ArquivoBoletoBancario")]
	public class ArquivoBoletoBancario 
	{
		public var id: Number = NaN;
		public var nomeArquivo: String;
		public var tipoArquivo: String;
		public var dataHoraCriacao: Date;
		public var usuario:Usuario;
		public var caminhoSalvo:String;
		public function ArquivoBoletoBancario()
		{
		}
		
		public function get tipoArquivoDescricaoCompleta(): String
		{
			if (this.tipoArquivo == null)
			{
				return this.tipoArquivo;
			}
			else if (this.tipoArquivo == "E")
			{
				return "ENVIO";	
			}
			else if (this.tipoArquivo == "R")
			{
				return "RETORNO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

