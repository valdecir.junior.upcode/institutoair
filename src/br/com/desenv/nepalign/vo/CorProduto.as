package br.com.desenv.nepalign.vo
{
	
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CorProduto")]
	
	public class CorProduto{
		public var id:int;
		public var cor:Cor;
		public var produto:Produto;
		public var ordemProduto:OrdemProduto;
		
		public function CorProduto(){
			
		}
	
	}
}