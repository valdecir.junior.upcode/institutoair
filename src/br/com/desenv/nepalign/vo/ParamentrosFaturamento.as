package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ParamentrosFaturamento")]
	public class ParamentrosFaturamento 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var transportadora: Transportadora;
		public var aliquotaICMSPadrao: Number = NaN;
		public var aliquotaIPIPadrao: Number = NaN;
		public var observacaoPadraoNotaSaida: String;
		public var observacaoPadraoNotaEntrada: String;
		public var descricaoEspeciePadrao: String;
		public var observacaoIsentoICMS: String;
		public var observacaoDiferidoICMS: String;
		public var observacaoReducaoBaseICMS: String;
		public var substitutoTributario: Number = NaN;
		public var observacaoSubTrib: String;
		public var inscricaoSubstitutoTributario: String;
		public var percReducaoICMSEmpresa: Number = NaN;
		public var percReducaoIPIEmpresa: Number = NaN;
		public var percReducaoISSEmpresa: Number = NaN;
		public var numeroUltimaNotaImpressa: Number = NaN;
		public var observacaoProdutoSemICMS: String;
		public function ParamentrosFaturamento()
		{
		}
		
		public function get substitutoTributarioDescricaoCompleta(): String
		{
			if (this.substitutoTributario == 0)
			{
				return "Nao";
			}
			else if (this.substitutoTributario == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
	}
}

