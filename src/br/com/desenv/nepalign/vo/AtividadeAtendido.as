package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AtividadeAtendido")]
	public class AtividadeAtendido 
	{
		public var id: int;
		public var atendido: Atendido;
		public var atividadeOferecida: AtividadeOferecida;
		public var dataInicio: Date;
		public var dataFim: Date;
		public var periodoPreferencial: String;
		public function AtividadeAtendido()
		{
		}
		
		public function get periodoPreferencialDescricaoCompleta(): String
		{
			if (this.periodoPreferencial == null)
			{
				return this.periodoPreferencial;
			}
			else if (this.periodoPreferencial == "M")
			{
				return "Manhã";	
			}
			else if (this.periodoPreferencial == "T")
			{
				return "Tarde";	
			}
			else if (this.periodoPreferencial == "N")
			{
				return "Noite";	
			}
			else if (this.periodoPreferencial == "I")
			{
				return "Indiferente";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

