package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Endereco")]
	public class Endereco 
	{
		public var id: int;
		public var codigoBairro: int;
		public var enderecoCep: String;
		public var logradouroEndereco: String;
		public var complementoEndereco: String;
		public function Endereco()
		{
		}
		
		
	}
}

