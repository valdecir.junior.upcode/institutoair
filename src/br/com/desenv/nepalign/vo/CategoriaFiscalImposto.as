package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.CategoriaFiscalImposto")]
	public class CategoriaFiscalImposto 
	{
		public var id: Number = NaN;
		public var categoriaFiscal: CategoriaFiscal;
		public var impostosTaxasTarifasEstado: ImpostosTaxasTarifasEstado;
		public var valorPercentual: Number = NaN;
		public var reducaoImposto: Number = NaN;
		public var observacao: String;
		public function CategoriaFiscalImposto()
		{
		}
		
		
	}
}

