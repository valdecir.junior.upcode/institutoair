package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemNotaPisNfe")]
	public class ItemNotaPisNfe 
	{
		public var id: Number = NaN;
		public var itemNota: ItemNotaFiscal;
		public var codigoCSTPIS: Number = NaN;
		public var valorBseCalcPIS: Number = NaN;
		public var aliquotaPIS: Number = NaN;
		public var quatVendPIS: Number = NaN;
		public var aliquotaPISValor: Number = NaN;
		public var valorPIS: Number = NaN;
		public var valorBaseCalcPISST: Number = NaN;
		public var aliquotaPISST: Number = NaN;
		public var quantVendPISST: Number = NaN;
		public var aliquotaPISValorST: Number = NaN;
		public var valorPISST: Number = NaN;
		public function ItemNotaPisNfe()
		{
		}
		
		
	}
}

