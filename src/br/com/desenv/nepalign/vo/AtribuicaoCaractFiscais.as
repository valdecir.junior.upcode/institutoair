package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AtribuicaoCaractFiscais")]
	public class AtribuicaoCaractFiscais 
	{
		public var id: Number = NaN;
		public var caracteristicasFiscais: CaracteristicasFiscais;
		public var cliente: Cliente;
		public var produto: Produto;
		public var atividade: Atividade;
		public var codigoFiscal: String;
		public var tipo: Number = NaN;
		public var excecoes: String;
		public var empresaFisica: EmpresaFisica;
		public var naturezaFornecedor: NaturezaFornecedor;
		public var ativo: Number = NaN;
		public var categoria: CategoriaFiscal;
		public var grupoCliente: GrupoCliente;
		public var caracteristicaProduto: CaracteristicaProduto;
		public var tipoNotaFiscal: TipoNotaFiscal;
		public function AtribuicaoCaractFiscais()
		{
		}
		
		public function get tipoDescricaoCompleta():String
		{
			if (this.tipo == 1)
			{
				return "Tipo de Nota";
			}
			else if(this.tipo == 2)
			{
				return "Grupo de Cliente";
			}
			else if (this.tipo ==3)
			{
				return "Cliente/Produto";
			}
			else if(this.tipo==4)
			{
				return "Cliente/NCM";
			}
			else if (this.tipo==5)
			{
				return "Produto";
			}
			else if (this.tipo == 6)
			{
				return "NCM";
			}
			else if (this.tipo==7)
			{
				return "Cliente";
			}
			else if(this.tipo==8)
			{
				return "Grupo Cliente/Produto";
			}
			else if(this.tipo ==9)
			{
				return "Grupo Cliente/NCM";
			}
			else if(this.tipo==10)
			{
				return "Atividade";
			}
			else if (this.tipo==11)
			{
				return "Atividade/Produto";
			}
			else 	
			{
				return "";
			}
		}
		
	}
}

