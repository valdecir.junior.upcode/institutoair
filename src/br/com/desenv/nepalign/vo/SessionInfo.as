package br.com.desenv.nepalign.vo
{
	import flash.display.DisplayObject;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	import mx.collections.ArrayCollection;
	import mx.core.Application;
	import mx.core.FlexGlobals;
	import mx.managers.PopUpManager;
	import mx.messaging.FlexClient;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import spark.modules.ModuleLoader;
	
	import br.com.desenv.framework.SessionConnectionManager;
	import br.com.desenv.framework.util.DsvParametrosSistema;
	import br.com.desenv.nepalign.view.component.PedidoVendaComponenteCadastroVendedor;
	import br.net.upcode.framework.popup.UPCPopUpManager;

	/** 
	 * @author lucas.leite
	 * @langversion ActionScript 3.0
	 */
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.wsdl.SessionInfo")]	
	public class SessionInfo
	{
		public var usuario:Usuario;
		public var amfChannelUrl:String;
		public var amfChannelPollingUrl:String;

		public var ultimaAtividade:Date;
		
		public var homeTiles:Array = null;
		public var homeController:String = null;
		
		public var mostrarMovimentoBaixado:Boolean = false;
		
		public var sessionTimeOut:int = 10000;
		public var ultimoMovBaixado:String;
		
		public var timezoneOffset:Number;
		
		public var connectedServerName:String = "[Nenhum]";
		public var connectedServerCode:String = "0";
		
		public var connectionManager:SessionConnectionManager;
		
		public var pedidoVendaCadastroVendedor:PedidoVendaComponenteCadastroVendedor;
		
		public var uae:Boolean = false;
		public var isLoja:Boolean = false;
		public var grupo:String = "";
		
		public function SessionInfo()
		{
			connectionManager = new SessionConnectionManager();

			/*new DsvHttpGet().getResponse("http://www.portaldesenv.com.br/nepal/indexnepal.html",
			function(event:Event):void 
			{ 
				FlexGlobals.topLevelApplication.htmlIndex(event.currentTarget.data);
			}, function(event:SecurityErrorEvent):void{}, function(event:IOErrorEvent):void{});*/
		}
		
		public function loadPreferences(data:ByteArray):void
		{
			try
			{
				while(data.bytesAvailable > 0)
				{
					var preferences:String = data.readUTF();
					
					switch (preferences)
					{
						case "[HOME]=":
							if(homeTiles == null)
								homeTiles = new Array();
							
							homeTiles.push({labelField:data.readUTF(), info:new ArrayCollection([data.readUTF(),  getDefinitionByName(data.readUTF())])});
							break;
						case "[HOME.CONTROLLER]=":
							homeController = data.readUTF();
							break;
						case "[SESSION_TIMEOUT]=":
							sessionTimeOut = data.readInt();
							break;
						case "[SHOW_LAST_MOVUPDATE]=":
							mostrarMovimentoBaixado = data.readBoolean();
							break;
						default:
							trace(preferences);
							break;
					}
				}	
			}
			catch(e:Error){}
			finally
			{
				loadHomeScreen();	
			}
		}
		
		public function loadHomeScreen():void
		{
			if(homeController != null)
			{
				FlexGlobals.topLevelApplication.moduleLoader.unloadModule();
				FlexGlobals.topLevelApplication.loadModule(homeController);	
			}
			else
			{
				if(this.homeTiles == null)
					return;
				
				if((FlexGlobals.topLevelApplication.moduleLoader as ModuleLoader).url == null)
				{
					FlexGlobals.topLevelApplication.tileMenu.tileList = homeTiles;
					FlexGlobals.topLevelApplication.tileMenu.refreshDisplayList();	
				}
				else
				{
					FlexGlobals.topLevelApplication.moduleLoader.unloadModule();
					FlexGlobals.topLevelApplication.moduleLoader.url = null;
					loadHomeScreen();
				}
			}
		}
		
		public function checkInitAsSeller():void
		{
			FlexGlobals.topLevelApplication.montarObjetoRemoto("usuarioService", initAsSellerResultHandler).getOperation("iniciarComoVendedor").send(FlexGlobals.topLevelApplication.usuarioLogado);
		}
		
		protected function initAsSellerResultHandler(event:ResultEvent):void
		{
			if(event.result as Boolean)
			{
				if(pedidoVendaCadastroVendedor == null)
					pedidoVendaCadastroVendedor = new PedidoVendaComponenteCadastroVendedor();
				else
				{
					UPCPopUpManager.instance.removePopUp(pedidoVendaCadastroVendedor);
					pedidoVendaCadastroVendedor = new PedidoVendaComponenteCadastroVendedor();
				}
				
				UPCPopUpManager.instance.addPopUp(pedidoVendaCadastroVendedor, FlexGlobals.topLevelApplication as DisplayObject, true);
				UPCPopUpManager.instance.centerWindow(pedidoVendaCadastroVendedor);
			}
			else
				FlexGlobals.topLevelApplication.montarObjetoRemoto("usuarioService", recuperarPreferenciasResultHandler, function(event:FaultEvent):void { trace(event.fault.faultString); }).getOperation("recuperarPreferencias").send(FlexGlobals.topLevelApplication.usuarioLogado);
		}
		
		public function setUae(uae:Boolean):void
		{
			this.uae = uae;
			FlexGlobals.topLevelApplication.montarObjetoRemoto("usuarioService").getOperation("setUae").send(uae);
		}
		
		protected function recuperarPreferenciasResultHandler(event:ResultEvent):void
		{
			loadPreferences(event.result as ByteArray); 
		}
		
		public function unloadSession():void
		{
			sysout("Unloading Session " + usuario.nome + " ID : " + FlexClient.getInstance().id);
			
			try
			{
				connectionManager.destroyAMFKeepAlive();
				
				FlexGlobals.topLevelApplication.moduleLoader.unloadModule();
				FlexGlobals.topLevelApplication.moduleLoader.url = null;
				
				FlexGlobals.topLevelApplication.tileMenu.tileList = new Array();
				FlexGlobals.topLevelApplication.tileMenu.refreshDisplayList();	
				
				(FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).dispose();

				if(pedidoVendaCadastroVendedor != null)
				{
					UPCPopUpManager.instance.removePopUp(pedidoVendaCadastroVendedor);
					pedidoVendaCadastroVendedor = null;
				}
				
				FlexClient.getInstance().id = null;
				
				sysout("Session Unload Complete. Ready");
			}
			catch(e:Error)
			{
				sysout("Cannot unload Session. " + e.message);
				syserr(e);
			}
			finally
			{
				Application.application.callLater(function():void
				{
					(FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).carregarParametrosLocais();
				});
			}
		}
	}	
}