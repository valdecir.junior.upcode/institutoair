package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SituacaoEnvioLV")]
	public class SituacaoEnvioLV 
	{
		public var id: int;
		public var descricao: String;
		public var alerta: String;
		public function SituacaoEnvioLV()
		{
		}
		
		public function get alertaDescricaoCompleta(): String
		{
			if (this.alerta == null)
			{
				return this.alerta;
			}
			else if (this.alerta == "S")
			{
				return "SIM";	
			}
			else if (this.alerta == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

