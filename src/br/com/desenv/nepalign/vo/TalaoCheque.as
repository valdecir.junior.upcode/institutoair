package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TalaoCheque")]
	public class TalaoCheque 
	{
		public var id: Number = NaN;
		public var contaCorrente: ContaCorrente;
		public var numeroInicial: Number = NaN;
		public var numeroFinal: Number = NaN;
		public var identificadorTalao: String;
		public function TalaoCheque()
		{
		}
		
		
	}
}

