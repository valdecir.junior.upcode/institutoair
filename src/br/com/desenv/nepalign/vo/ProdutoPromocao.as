package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ProdutoPromocao")]
	public class ProdutoPromocao 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var produto: Produto;
		public var precoPromocional: Number = NaN;
		public var dataInicial: Date;
		public var dataFinal: Date;
		public function ProdutoPromocao()
		{
		}
		
		
	}
}

