package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Atividade")]
	public class Atividade 
	{
		public var id: int;
		public var descricao: String;
		public var cnae: String;
		public function Atividade()
		{
		}
		
		
	}
}

