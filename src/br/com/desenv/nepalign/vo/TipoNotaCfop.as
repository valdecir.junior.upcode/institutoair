package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoNotaCfop")]
	public class TipoNotaCfop 
	{
		public var id: Number = NaN;
		public var tipoNotaFiscal: TipoNotaFiscal;
		public var cfop: Cfop;
		public var cfopForaEstado: Cfop;
		public function TipoNotaCfop()
		{
		}
		
		
	}
}

