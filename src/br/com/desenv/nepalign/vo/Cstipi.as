package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Cstipi")]
	public class Cstipi 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public var tipoIpi: Number = NaN;
		public function Cstipi()
		{
		}
		
		public function get tipoIpiDescricaoCompleta(): String
		{
			if (this.tipoIpi == 0)
			{
				return "Nao";
			}
			else if (this.tipoIpi == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		
		public function get descricaoCompleta():String
		{
			return codigo + "-" + descricao;
		}
		
	}
}

