package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ItemNotaIcmsNfe")]
	public class ItemNotaIcmsNfe 
	{
		public var id: Number = NaN;
		public var itemNota: ItemNotaFiscal;
		public var codigoCSTICMS: Number = NaN;
		public var origemMercadoria: Number = NaN;
		public var modDetermBaseCalcICMS: Number = NaN;
		public var valBaseCalcICMS: Number = NaN;
		public var valICMS: Number = NaN;
		public var modBaseCalcICMSST: Number = NaN;
		public var valBaseCalcICMSST: Number = NaN;
		public var aliqICMSST: Number = NaN;
		public var valICMSST: Number = NaN;
		public var percMVAICMSST: Number = NaN;
		public var percRedBaseCalcICMSST: Number = NaN;
		public var ufStICMS: String;
		public var pbcopIcms: Number = NaN;
		public var vbcstretIcms: Number = NaN;
		public var motdesicms: Number = NaN;
		public var vbcstdesticms: Number = NaN;
		public var vbcstdest: Number = NaN;
		public var pcredsn: Number = NaN;
		public var vcredicmssn: Number = NaN;
		public var vicmsstret: Number = NaN;
		public var vicmsstdest: Number = NaN;
		public function ItemNotaIcmsNfe()
		{
		}
		
		
	}
}

