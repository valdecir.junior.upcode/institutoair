package br.com.desenv.nepalign.vo
{ 
	
	import mx.collections.ArrayCollection;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.frameworkignorante.Page")]
	public class Page
	{
		public var totalRecords: int;
		public var pageSize: int;
		public var pageNumber: int;
		public var recordList: ArrayCollection;
		
		public function Page()
		{
		}
		
		public function nextPage():int
		{
			if (pageNumber == lastPage())
			{
				return pageNumber;
			}
			return pageNumber + 1; 
		}
		
		public function previousPage():int
		{
			if (pageNumber == 1)
			{
				return pageNumber;
			}
			return pageNumber - 1; 
		}
		
		public function lastPage(): int
		{
			if (totalRecords == 0)
			{
				return 1;
			}
			else if (totalRecords % pageSize == 0)
			{
				return (totalRecords / pageSize);
			}
			else
			{
				return (totalRecords / pageSize) + 1;
			}
			
			
		}
	}
}




