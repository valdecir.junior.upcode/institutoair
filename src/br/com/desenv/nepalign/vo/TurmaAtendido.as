package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TurmaAtendido")]
	public class TurmaAtendido 
	{
		public var id: int;
		public var turma: Turma;
		public var atendido: Atendido;
		public function TurmaAtendido()
		{
		}
		
		
	}
}

