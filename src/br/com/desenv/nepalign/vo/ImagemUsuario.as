package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.ImagemUsuario")]
	public class ImagemUsuario 
	{
		public var id: Number = NaN;
		public var tabelaSistema: TabelaSistema;
		public var descricao: String;
		public var campoChave: String;
		public var infoCampoChave: String;
		public var imagem: ByteArray;
		public function ImagemUsuario()
		{
		}
		
		
	}
}

