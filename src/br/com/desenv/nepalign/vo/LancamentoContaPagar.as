package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	import br.com.desenv.framework.util.DsvObjectUtil;
	
	import flashx.textLayout.formats.Float;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LancamentoContaPagar")]
	public class LancamentoContaPagar 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var fornecedor: Fornecedor;
		public var centroFinanceiro: CentroFinanceiro;
		public var contaGerencial: ContaGerencial;
		public var tipoConta: TipoConta;
		public var contaCorrente: ContaCorrente;
		public var dataLancamento: Date;
		public var dataVencimento: Date;
		public var dataQuitacao: Date;
		public var valorOriginal: Number = NaN;
		public var valorDesconto: Number = NaN;
		public var valorMulta: Number = NaN;
		public var percentualMulta: Number = NaN;
		public var valorPago: Number = NaN;
		public var documentoPagamento: String;
		public var valorJuroDiario: Number = NaN;
		public var percentualJuroDiario: Number = NaN;
		public var favorecido: String;
		public var numeroDocumento: String;
		public var observacao: String;
		public var dataLimiteDesconto: Date;
		public var situacaoEmissaoCheque: Number = NaN;
		public var formaPagamento: FormaPagamento;
		public var lancamentoContaPagarFixo: LancamentoContaPagarFixo;
		public var situacaoLancamento: SituacaoLancamento;
		public var codigoBarra: String;
		public var codigoBarraConvertido: String;
		public var nossoNumero: String;
		public var prazoPagamento: String;
		public var lancamentoAgrupador: Number = NaN;
		public var planoContaAntigo:String;
		public var codigoSeguranca:String;
		public var selecionado:Boolean = false;
		public var pagamentoDocumentoEntradaSaida:PagamentoDocumentoEntradaSaida;
		public var empresasRateio:String;
		public function LancamentoContaPagar()
		{
		}
		
		public function get valorSaldo():Number
		{
			if(this.situacaoLancamento.id != 2)
			{
				if((valorOriginal - valorPago) < 0)
					return 0;
				else
					return DsvObjectUtil.round((valorOriginal - valorPago), 2);	
			}
			else
				return 0;
		}
		
		public function get valorSaldoFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(this.valorSaldo);	
		}
		
		public function get valorOriginalFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(valorOriginal);
		}
		
		public function get valorPagoFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(valorPago);
		}
	}
}

