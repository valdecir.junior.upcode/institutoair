package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.BandeiraCartao")]
	public class BandeiraCartao 
	{
		public var id: Number = NaN;
		public var descricao: String;
		
		public function BandeiraCartao()
		{
		}
		
		[Bindable]
		public function get descricaoCompleta():String
		{
			return id + " - " + descricao;
		}
	}
}