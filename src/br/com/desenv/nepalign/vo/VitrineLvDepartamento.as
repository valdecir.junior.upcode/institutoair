package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.VitrineLvDepartamento")]
	public class VitrineLvDepartamento 
	{
		public var id: Number = NaN;
		public var vitrineLv: VitrineLv;
		public var departamento: Departamento;
		public var situacaoEnvioLv: SituacaoEnvioLV;
		public function VitrineLvDepartamento()
		{
		}
		
		
	}
}

