package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Marca")]
	public class Marca 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var codigo: Number;
		public var ultimaAlteracao: Date;
		public var franquia: Number = NaN;
		public function Marca()
		{
		}

		public function get descricaoCompleta():String
		{
			return "" + id  + " - " + descricao;
		}
		
	}
}

