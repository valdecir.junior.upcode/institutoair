package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.BaixaLancamentoContaReceber")]
	public class BaixaLancamentoContaReceber 
	{
		public var id: Number = NaN;
		public var lancamentoContaReceber: LancamentoContaReceber;
		public var valor:Number = NaN;
		public var dataBaixa:Date;
		public var tipoBaixa:Number = NaN;
		public var observacao:String;
		
		public function BaixaLancamentoContaReceber()
		{
		}
		
		public function get valorFormatado():String
		{
			return Formatador.formatarDouble(valor, 0x02);
		}
	}
}