package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PagamentoNotaFiscal")]
	public class PagamentoNotaFiscal 
	{
		public var id: Number = NaN;
		public var notaFiscal: NotaFiscal;
		public var formaPagamento: FormaPagamento;
		public var lancamentoContaReceber: LancamentoContaReceber;
		public var dataPagamento: Date;
		public var valor: Number = NaN;
		public var numeroDocumento: String;
		public var numeroCheque: String;
		public var cnpjCpf: String;
		public var numeroDuplicata: String;
		public var situacaoLancamento: SituacaoLancamento;
		public function PagamentoNotaFiscal()
		{
		}
		
		public function get cnpjCpfFormatado(): String
		{
			if (this.cnpjCpf == null)
			{
				return this.cnpjCpf;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cnpjCpf);
			}		
		}
		
	}
}

