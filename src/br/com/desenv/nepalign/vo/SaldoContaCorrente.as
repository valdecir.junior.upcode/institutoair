package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SaldoContaCorrente")]
	public class SaldoContaCorrente 
	{
		public var id: Number = NaN;
		public var contaCorrente: ContaCorrente;
		public var anoMes: String;
		public var saldo: Number = NaN;
		public var anoMesAtual:String ;
		public var saldoAtual:Number = NaN;
		public function SaldoContaCorrente()
		{
		}
		
		
	}
}

