package br.com.desenv.nepalign.vo{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Etiqueta")]
	public class Etiqueta
	{
		public var  codigoBarras:String;
		public var  data:String;
		public var  referencia:String;
		public var  idReferencia:String;
		public var  ordem:String;
		public var  idOrdem:String;
		public var  cor:String;
		public var  venda:String;
		public var  produto:String;
		public var  numero:String;
		public var  quantidade:String;
		public var  tamanho:String;
		public var  idTamanho:String;
		public var  empresa:EmpresaFisica;
		public function Etiqueta()
		{
		}
		
		public function serializeMe():Array
		{
			var str:Array = new Array(codigoBarras, data, empresa.id, quantidade);
			return str;
		}
		public function stringAlizeMe():String
		{
			var str:String = codigoBarras+";"+data+";"+empresa.id+";"+quantidade;
			return str;
		}		
		public function get produtoCompleto():String
		{
			var str:String = produto+" "+referencia+" "+ordem+" "+cor+" "+tamanho;
		return 	str;
		}
	}
}