package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;
	import br.com.desenv.framework.util.DsvObjectUtil;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.BaixaLancamentoContaPagar")]
	public class BaixaLancamentoContaPagar 
	{
		public var id: Number = NaN;
		public var lancamentoContaPagar: LancamentoContaPagar;
		public var formaPagamento: FormaPagamento;
		public var contaCorrente: ContaCorrente;
		public var documento: String;
		public var numeroAgrupador:String;
		public var dataPagamento: Date;
		public var valor: Number = NaN;
		public var considerarBaixaIntegral: Number = NaN;
		public var dataLancamento: Date;
		public var observacao: String;
		public var valorDesconto: Number = NaN;
		public var valorMulta: Number = NaN;
		public var valorJuros: Number = NaN;
		public var valorOriginal: Number = NaN;
		
		public function BaixaLancamentoContaPagar()
		{
		}
		
		public function get considerarBaixaIntegralDescricaoCompleta(): String
		{
			if (this.considerarBaixaIntegral == 0)
			{
				return "Não";
			}
			else if (this.considerarBaixaIntegral == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}	
		}	
		
		public function get valorFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(this.valor);
		}
		
		public function get valorDescontoFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(this.valorDesconto);
		}
		
		public function get valorMultaFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(this.valorMulta);
		}
		
		public function get valorJurosFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(this.valorJuros);
		}

		public function get valorOriginalFormatado():String
		{
			return DsvObjectUtil.formatadorDinheiro.formatNumber(this.valorOriginal);
		}
	}
}