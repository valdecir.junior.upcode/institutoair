package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SitucaoEnvioLV")]
	public class SitucaoEnvioLV 
	{
		public var id: int;
		public var descricao: String;
		public var alerta: String;
		public function SitucaoEnvioLV()
		{
		}
		
		public function get alertaDescricaoCompleta(): String
		{
			if (this.alerta == null)
			{
				return this.alerta;
			}
			else if (this.alerta == "S")
			{
				return "SIM";	
			}
			else if (this.alerta == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

