package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoMovimentoCaixa")]
	public class TipoMovimentoCaixa
	{
		public var id: Number = NaN;
		public var codigo:String = null;
		public var descricao:String = null;
		public var creditoDebito:Number = NaN;
		
		public function TipoMovimentoCaixa()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return codigo + " - " + descricao;
		}
	}
}