package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.BiometriaCliente")]
	public class BiometriaCliente 
	{
		public var id: Number = NaN;
		public var cliente: Cliente;
		public var biometria: String;
		public function BiometriaCliente()
		{
		}
		
		
	}
}

