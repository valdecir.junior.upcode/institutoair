package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Pais")]
	public class Pais 
	{
		public var id: int;
		public var nome: String;
		public var nomeSemAcento: String;
		public var codigoIbge: int;
		public var sigla: String;
		public function Pais()
		{
		}
		
		
	}
}

