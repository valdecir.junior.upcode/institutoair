package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Condicional")]
	public class Condicional 
	{
		public var id: Number = NaN;
		public var empresaFisica: EmpresaFisica;
		public var vendedor: Vendedor;
		public var usuario: Usuario;
		public var cliente: Cliente;
		public var dataGeracao: Date;
		public var valorTotal: Number = NaN;
		public var valorTotalProdutos: Number = NaN;
		public var dataExpiracaoCondicional: Date;
		public var situacao: String;
		public var observacao: String;
		
		public function Condicional()
		{
		}
		
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
				return this.situacao;
			else if (this.situacao == "A")
				return "ABERTO";
			else if (this.situacao == "D")
				return "DEVOLVIDO";	
			else if (this.situacao == "F")
				return "FECHADO";	
			else
				return "";
		}
		
		public function get valorCondicionalFormatado():String
		{
			return Formatador.formatarDouble(valorTotalProdutos, 2);
		}
	}
}