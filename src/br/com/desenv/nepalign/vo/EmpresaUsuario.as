package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EmpresaUsuario")]
	public class EmpresaUsuario 
	{
		public var id: int;
		public var empresa: Empresa;
		public var usuario: Usuario;
		public function EmpresaUsuario()
		{
		}
		
		
	}
}

