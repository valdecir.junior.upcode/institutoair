package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass (alias="br.com.desenv.nepalign.model.ContagemBalancoUnificada")]
	public class ContagemBalancoUnificada
	{
		public var id:Number = NaN;
		public var empresaFisica:EmpresaFisica;
		public var sequenciaBase:Number = NaN;
		public var itemContagem:Number = NaN;
		public var estoqueProduto:EstoqueProduto;
		public var quantidade:Number = NaN;
		public var horaFechamento:Date;
		
		public function ContagemBalancoUnificada()
		{
		}
		
	}
}