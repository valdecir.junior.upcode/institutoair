package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoMovimentoCaixaDiaGrupoEmpresa")]
	public class TipoMovimentoCaixaDiaGrupoEmpresa 
	{
		public var id: Number = NaN;
		public var tipoMovimentoCaixaDia: TipoMovimentoCaixaDia;
		public var grupoEmpresa: GrupoEmpresa;
		
		public function TipoMovimentoCaixaDiaGrupoEmpresa()
		{
		}
		
	}
}