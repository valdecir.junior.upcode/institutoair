package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PlanoPagamento")]
	public class PlanoPagamento 
	{
		public var id: Number = NaN;
		public var tipoPagamento: TipoPagamento;
		public var descricao: String;
		public var tipoPercentual: String;
		public var percentual: Number = NaN;
		public var diasCarencia: Number = NaN;
		public var quantParcelas: Number = NaN;
		public var formaPagamento: FormaPagamento;
		public var diasPagamento: String;
		public var empresaFisica:EmpresaFisica;
		public var crediario:String;
		public var grupoPlano:String;
		public var codigoPlano:String;
		
		public function PlanoPagamento()
		{
		}
		public function get descricaoCrediarioCompleta():String
		{
			if(crediario == "S")
			{
				return "SIM";	
			}
			else if(crediario == "N")
			{
				return "NÃO";
			}
			else
			{
				return crediario;
			}
			
		}	
		public function get descricaoCompleta():String
		{
			return "P"+(codigoPlano == null ? '' : codigoPlano)+"-"+descricao;
		}	
		public function get descricaoCompletaPedidoVenda():String
		{
			return ""+(codigoPlano == null ? '' : codigoPlano)+"-"+descricao;
		}
		public function get descricaoCompletaComEmpresa():String
		{
			return this.empresaFisica.idFormatado+"- "+descricaoCompleta;
		}
	}
}