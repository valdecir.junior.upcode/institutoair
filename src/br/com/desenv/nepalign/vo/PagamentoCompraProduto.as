package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PagamentoCompraProduto")]
	public class PagamentoCompraProduto 
	{
		public var id: Number = NaN;
		public var pedidoFornecedor: PedidoCompraVenda;
		public var tipoDocumentoFinanceiro: TipoDocumentoFinanceiro;
		public var documentoEntradaSaida: DocumentoEntradaSaida;
		public var formaPagamento: FormaPagamento;
		public var numeroDocumento: String;
		public var valor: Number = NaN;
		public var dataPagamento: Date;
		public var horaPagamento: Date;
		public var lancamentoContaPagar: LancamentoContaPagar;
		public function PagamentoCompraProduto()
		{
		}
		
		
	}
}

