package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EmpresaFinanceiro")]
	public class EmpresaFinanceiro
	{
		public var id:Number = NaN; 
		public var nomeEmpresa:String;
		public var empresaFisicaReferente:EmpresaFisica;
		public var mesAberto:Number = NaN;
		public var anoAberto:Number = NaN;
		public var tipoEmpresa:Number;
		
		public function EmpresaFinanceiro()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return id.toString() + " - " + nomeEmpresa;
		}
		
		public function get tipoEmpresaFormatado():String
		{
			if(tipoEmpresa == 0x01)
				return "Cheque";
			else if(tipoEmpresa == 0x02)
				return "Cartão";
			else if(tipoEmpresa == 0x03)
				return "Vale";
			else
				return "";
		}
	}	
}