package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoContaContaGer")]
	public class TipoContaContaGer 
	{
		public var id: Number = NaN;
		public var tipoConta: TipoConta;
		public var contaGerencial: ContaGerencial;
		public function TipoContaContaGer()
		{
		}
		
		
	}
}

