package br.com.desenv.nepalign.vo
{
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LancamentoContaReceber")]
	public class LancamentoContaReceber 
	{
		public var id:Number = 0x00;
		public var empresaFinanceiro:EmpresaFinanceiro;
		public var bandeiraCartao:BandeiraCartao;
		public var taxaCartao:TaxaCartao;
		public var formaPagamentoCartao:FormaPagamentoCartao;
		public var dataLancamento:Date;
		public var dataVencimento:Date;
		public var dataQuitacao:Date;
		public var emitente:String;
		public var cpfCnpjEmitente:String;
		public var banco:Banco;
		public var numeroDocumento:String;
		public var valorLancamento:Number;
		public var valorParcelaSemDesconto:Number;
		public var percentualCartao:Number;
		public var quantidadeParcelas:int;
		public var numeroParcela:int;
		public var valorTotal:Number;
		public var situacaoLancamento:SituacaoLancamento;
		public var observacao:String;
		
		public function LancamentoContaReceber()
		{
		}
		
		[Bindable]
		public function get valorLancamentoFormatado():String
		{
			return Formatador.formatarDouble(valorLancamento, 0x02);
		}
		
		public function set valorLancamentoFormatado(value:String):void
		{
			valorLancamento = Number(value.replace(",", "."));
		}
		
		[Bindable]
		public function get valorParcelaSemDescontoFormatado():String
		{
			return Formatador.formatarDouble(valorParcelaSemDesconto, 0x02);
		}
		
		public function set valorParcelaSemDescontoFormatado(value:String):void
		{
			valorParcelaSemDesconto = Number(value.replace(",", "."));
		}
		
		[Bindable]
		public function get numeroDocumentoFormatado():String
		{
			if(!isNaN(quantidadeParcelas) && !isNaN(numeroParcela))
				return numeroDocumento + " " + numeroParcela + "/" + quantidadeParcelas;
			else
				return numeroDocumento;
		}
	}
}