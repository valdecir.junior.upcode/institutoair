package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Representante")]
	public class Representante 
	{
		public var id: int;
		public var nome: String;
		public var foto: ByteArray;
		public var percentualComissao: Number = 0.0;
		public function Representante()
		{
		}
		
		
	}
}

