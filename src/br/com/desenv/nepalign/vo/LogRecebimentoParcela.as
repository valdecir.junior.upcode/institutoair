package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.LogRecebimentoParcela")]
	public class LogRecebimentoParcela 
	{
		public var id: Number = NaN;
		public var motivo: String;
		public var data: Date;
		public var usuario: Usuario;
		public var valorSelecionado: Number = NaN;
		public var valorInformado: Number = NaN;
		public var cliente: Cliente;
		public var empresaFisica: EmpresaFisica;
		public function LogRecebimentoParcela()
		{
		}
		
		
	}
}

