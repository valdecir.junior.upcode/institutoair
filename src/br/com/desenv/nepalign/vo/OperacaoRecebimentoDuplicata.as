package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.OperacaoRecebimentoDuplicata")]
	public class OperacaoRecebimentoDuplicata 
	{
		public var id: Number = NaN;
		public var dataOperacao: Date;
		public var usuario: Usuario;
		public var valorPago: Number = NaN;
		public var valorOperacao: Number = NaN;
		public function OperacaoRecebimentoDuplicata()
		{
		}
		
		public function get valorPagoFormatado():String
		{
			var valor:Number = valorPago;
			return Formatador.formatarDouble(valor,2);
		}
		public function get valorOperacaoFormatado():String
		{
			var valor:Number = valorOperacao;
			return Formatador.formatarDouble(valor,2);			
		}
		
	}
}

