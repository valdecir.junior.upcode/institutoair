package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.MovimentoParcelaDuplicata")]
	public class MovimentoParcelaDuplicata 
	{
		public var id: Number = NaN;
		public var duplicata: Duplicata;
		public var duplicataParcela: DuplicataParcela;
		public var empresaFisica: EmpresaFisica;
		public var cliente: Cliente;
		public var usuario: Usuario;
		public var numeroDuplicata: Number = NaN;
		public var numeroParcela: Number = NaN;
		public var dataMovimento: Date;
		public var tipoPagamento: String;
		public var valorCompraPago: Number = NaN;
		public var valorEntradaJuros: Number = NaN;
		public var valorRecebido: Number = NaN;
		public var dataVencimento: Date;
		public var valorParcela: Number = NaN;
		public var historico: String;
		public var formaPagamento:FormaPagamento;
		public var chequeRecebido:ChequeRecebido;
		public var valorDesconto:Number = NaN;
		public var horaMovimento:Date;
		public var pagamentoCartao:PagamentoCartao;
		public var selecionado:Boolean;
		public var seprocadoReativados:SeprocadoReativados;
		public var duplicataParcelaCobranca:DuplicataParcelaCobranca;
		public var operadoraCartao:OperadoraCartao;
		public function MovimentoParcelaDuplicata()
		{
		}
		public function get valorCompraPagoFormatado():String
		{
			var valor:Number = valorCompraPago;
			return Formatador.formatarDouble(valor,2);
		}		
		public function get valorEntradaJurosFormatado():String
		{
			var valor:Number = valorEntradaJuros;
			return Formatador.formatarDouble(valor,2);
		}
		public function get valorRecebidoFormatado():String
		{
			var valor:Number = valorRecebido;
			return Formatador.formatarDouble(valor,2);
		}		
		public function get valorParcelaFormatado():String
		{
			var valor:Number = valorParcela;
			return Formatador.formatarDouble(valor,2);
		}		
		public function get tipoPagamentoDescricaoCompleta(): String
		{
			if (this.tipoPagamento == null)
			{
				return this.tipoPagamento;
			}
			else if (this.tipoPagamento == "0")
			{
				return "Compra";	
			}
			else if (this.tipoPagamento == "1")
			{
				return "Pagamento Total";	
			}
			else if (this.tipoPagamento == "2")
			{
				return "Pagamento Parcial";	
			}
			else if (this.tipoPagamento == "5")
			{
				return "Acordo";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

