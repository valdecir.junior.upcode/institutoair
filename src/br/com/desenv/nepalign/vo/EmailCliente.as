package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.EmailCliente")]
	
	public class EmailCliente {
		public var id: Number;
		public var cliente: Cliente;
		public var email: String;
		public function EmailCliente() {
		}
	}
}

