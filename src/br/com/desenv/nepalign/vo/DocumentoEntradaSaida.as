package br.com.desenv.nepalign.vo
{
	import mx.collections.ArrayCollection;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.DocumentoEntradaSaida")]
	public class DocumentoEntradaSaida 
	{
		public var id: Number;
		public var tipo: String;
		public var empresaFisica: EmpresaFisica;
		public var fornecedor: Fornecedor;
		public var pedidoCompraVenda: PedidoCompraVenda;
		public var numeroDocumentoEntradaSaida: String;
		public var dataEmissao: Date;
		public var dataEntrada: Date;
		public var numeroNotaFiscal: String;
		public var serie: Number;
		public var valorTotal: Number;
		public var valorTotalNotaFiscal: Number;
		public var quantidadeTotal: Number;
		public var situacao: String;
		public var observacao: String;
		
		public var listaItemDocumentoEntradaSaida: ArrayCollection = new ArrayCollection();
		
		public function DocumentoEntradaSaida()
		{
		}
		
		public function get tipoDescricaoCompleta(): String
		{
			if (this.tipo == null)
			{
				return this.tipo;
			}
			else if (this.tipo == "E")
			{
				return "Entrada";	
			}
			else if (this.tipo == "S")
			{
				return "Saída";	
			}
			else
			{
				return "";
			}
		}
		public function get situacaoDescricaoCompleta(): String
		{
			if (this.situacao == null)
			{
				return this.situacao;
			}
			else if (this.situacao == "1")
			{
				return "Salvo";	
			}
			else if (this.situacao == "2")
			{
				return "Baixado";	
			}
			else if (this.situacao == "3")
			{
				return "Revisado";	
			}
			else if (this.situacao == "4")
			{
				return "Baixa Autorizada";	
			}
			else
			{
				return "";
			}
		}
		
		public function get valorTotalFormatado():String
		{
			
			if (isNaN(this.valorTotal))
			{
				return "";
			}
			else
			{
				return Formatador.formatarDouble(this.valorTotal, 2);
			}	
		}
		
		public function get valorTotalNotaFiscalFormatado():String
		{
			if (isNaN(this.valorTotalNotaFiscal))
			{
				return "";
			}
			else
			{
				return Formatador.formatarDouble(this.valorTotalNotaFiscal, 2);
			}	
		}	
	}
}