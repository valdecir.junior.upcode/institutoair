package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.BoletimUnificado")]
	public class BoletimUnificado 
	{
		public var id: Number = NaN;
		public var origem: String;
		public var dataMovimento: Date;
		public var descricao: String;
		public var valorDebito: Number = NaN;
		public var valorCredito: Number = NaN;
		public function BoletimUnificado()
		{
		}
		
		public function get origemDescricaoCompleta(): String
		{
			if (this.origem == null)
			{
				return this.origem;
			}
			else if (this.origem == "G")
			{
				return "GERADO";	
			}
			else if (this.origem == "I")
			{
				return "IMPORTADO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

