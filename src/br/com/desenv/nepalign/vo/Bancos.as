package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Bancos")]
	public class Bancos
	{
		public var id: Number = NaN;
		public var empresaBancos:EmpresaBancos = null;
		public var bancoRelacionado:Bancos = null;
		public var nomeBanco:String = null;
		public var mesAberto:Number = NaN;
		public var anoAberto:Number = NaN;
		
		public function Bancos()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return this.id + "-" + this.nomeBanco;
		}	
	}
}

