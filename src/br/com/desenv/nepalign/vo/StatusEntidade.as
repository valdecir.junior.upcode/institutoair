package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.StatusEntidade")]
	public class StatusEntidade 
	{
		public var id: int;
		public var descricao: String;
		public var permiteConsignacao: String;
		public var permiteEmprestimo: String;
		public var permitePagamentoParcelado: String;
		public var permitePagamentoCartaoCred: String;
		public var permitePagamentoCheque: String;
		public var bloquearVenda: String;
		public function StatusEntidade()
		{
		}
		
		public function get permiteConsignacaoDescricaoCompleta(): String
		{
			if (this.permiteConsignacao == null)
			{
				return this.permiteConsignacao;
			}
			else if (this.permiteConsignacao == "S")
			{
				return "SIM";	
			}
			else if (this.permiteConsignacao == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get permiteEmprestimoDescricaoCompleta(): String
		{
			if (this.permiteEmprestimo == null)
			{
				return this.permiteEmprestimo;
			}
			else if (this.permiteEmprestimo == "S")
			{
				return "SIM";	
			}
			else if (this.permiteEmprestimo == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get permitePagamentoParceladoDescricaoCompleta(): String
		{
			if (this.permitePagamentoParcelado == null)
			{
				return this.permitePagamentoParcelado;
			}
			else if (this.permitePagamentoParcelado == "S")
			{
				return "SIM";	
			}
			else if (this.permitePagamentoParcelado == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get permitePagamentoCartaoCredDescricaoCompleta(): String
		{
			if (this.permitePagamentoCartaoCred == null)
			{
				return this.permitePagamentoCartaoCred;
			}
			else if (this.permitePagamentoCartaoCred == "S")
			{
				return "SIM";	
			}
			else if (this.permitePagamentoCartaoCred == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get permitePagamentoChequeDescricaoCompleta(): String
		{
			if (this.permitePagamentoCheque == null)
			{
				return this.permitePagamentoCheque;
			}
			else if (this.permitePagamentoCheque == "S")
			{
				return "SIM";	
			}
			else if (this.permitePagamentoCheque == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		public function get bloquearVendaDescricaoCompleta(): String
		{
			if (this.bloquearVenda == null)
			{
				return this.bloquearVenda;
			}
			else if (this.bloquearVenda == "S")
			{
				return "SIM";	
			}
			else if (this.bloquearVenda == "N")
			{
				return "NAO";	
			}
			else
			{
				return "";
			}
		}
		
	}
}

