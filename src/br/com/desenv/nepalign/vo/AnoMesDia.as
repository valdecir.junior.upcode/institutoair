package br.com.desenv.nepalign.vo
{
	public class AnoMesDia
	{
		public var ano: Number = 0;
		public var mes: Number = 0;
		public var dia: Number = 0;

		public function AnoMesDia()
		{
		}
		
		public function imprimir():String
		{
			return this.ano + "a" + this.mes + "m" + this.dia + "d";
		}
		
		
		public static function calcularDiferencaDataAtual(dataPassada:Date):AnoMesDia
		{
			return calcularDiferencaData(dataPassada, new Date());
		}
		
		public static function calcularDiferencaData(dataInicio:Date, dataFim:Date):AnoMesDia
		{
			if (dataInicio == null || dataFim == null) 
			{
				return null;
			}
			
			if (dataInicio > dataFim) 
			{
				return null;
			}
			
			var diaInicio:Number = dataInicio.getDate();
			var mesInicio:Number = dataInicio.getMonth();
			var anoInicio:Number = dataInicio.getFullYear();
			
			var diaFim:Number = dataFim.getDate();
			var mesFim:Number = dataFim.getMonth();
			var anoFim:Number = dataFim.getFullYear();
			
			// Diminuo os anos
			var qtdAno:Number = new Number(anoFim - anoInicio);
			var adicionarMes:Number = new Number(0);
			
			if (mesFim < mesInicio)
			{
				qtdAno--;
			}
			
			if (mesFim == mesInicio && diaFim < diaInicio)
			{
				qtdAno--;
			}
			
			var anoMesDiaQtdAnoTemp:AnoMesDia = new AnoMesDia();
			anoMesDiaQtdAnoTemp.ano = qtdAno;
			
			var dataResultado:Date = somarAnoMesDiaAData(dataInicio, anoMesDiaQtdAnoTemp);
			
			// Verificar se a data &eacute; igual
			if (compararData(dataResultado, dataFim))
			{
				return anoMesDiaQtdAnoTemp;
			}
			
			var qtdMes:Number = new Number(0);
			
			if (mesFim >= mesInicio)
			{
				qtdMes = mesFim - mesInicio;
			}
			else
			{
				qtdMes = mesFim + 12 - mesInicio;
			}
			
			if (diaFim < diaInicio)
			{
				qtdMes--;
			}
			
			if (qtdMes == -1)
			{
				qtdMes = 11;
			}
			
			var anoMesDiaQtdMesTemp:AnoMesDia = new AnoMesDia();
			anoMesDiaQtdMesTemp.mes = qtdMes;
			
			dataResultado = somarAnoMesDiaAData(dataResultado, anoMesDiaQtdMesTemp);
			
			// Verificar se a data &eacute; igual
			if (compararData(dataResultado, dataFim))
			{
				anoMesDiaQtdMesTemp.ano = qtdAno;
				return anoMesDiaQtdMesTemp;
			}
			
			var qtdDia:Number = Math.round((dataFim.getTime() - dataResultado.getTime())/(1000*60*60*24));
			var anoMesDiaQtdDiaTemp:AnoMesDia = new AnoMesDia();
			anoMesDiaQtdDiaTemp.dia = qtdDia;
			dataResultado = somarAnoMesDiaAData(dataResultado, anoMesDiaQtdDiaTemp);
			
			// Verificar se a data &eacute; igual
			if (compararData(dataResultado, dataFim))
			{
				anoMesDiaQtdDiaTemp.ano = qtdAno;
				anoMesDiaQtdDiaTemp.mes = qtdMes;
				return anoMesDiaQtdDiaTemp;
			}
			else
			{

				var anoMesDiaUmDia: AnoMesDia = new AnoMesDia();
				anoMesDiaUmDia.dia = 1;

				//se data calculada maior que a data fim, diminui 1
				if (dataResultado.valueOf() > dataFim.valueOf())
				{
					//tenta 1 a menos
					dataResultado = subtrairAnoMesDiaDaData(dataResultado, anoMesDiaUmDia);

					if (compararData(dataResultado, dataFim))
					{
						anoMesDiaQtdDiaTemp.ano = qtdAno;
						anoMesDiaQtdDiaTemp.mes = qtdMes;
						anoMesDiaQtdDiaTemp.dia--;
						return anoMesDiaQtdDiaTemp;
					}

				}
				else
				{
					dataResultado = somarAnoMesDiaAData(dataResultado, anoMesDiaUmDia);

					if (compararData(dataResultado, dataFim))
					{
						anoMesDiaQtdDiaTemp.ano = qtdAno;
						anoMesDiaQtdDiaTemp.mes = qtdMes;
						anoMesDiaQtdDiaTemp.dia++;
						return anoMesDiaQtdDiaTemp;
					}
					
				}
			}
			
			return new AnoMesDia();
			
			
		}
		
		// Fun&ccedil;&atilde;o somarAnoMesDiaAData
		// Parametros
		//    dataBase - data de base - tipo date
		//    anoMesDia - prazo em anos, meses e dias - tipo anoMesDia
		public static function somarAnoMesDiaAData(dataBase:Date, anoMesDia:AnoMesDia):Date
		{			
			if (dataBase != null &&  anoMesDia==null)
			{
				return dataBase;
			}
			
			if (dataBase == null ||  anoMesDia==null)
			{
				return null;
			}
			var dataFinal:Date = new Date(dataBase);				
			dataFinal.setFullYear(dataFinal.getFullYear() + anoMesDia.ano);
			
			// Pego o dia original, pois depois de somarmos os meses, o dia mudar, indica que o dia correspondente n&atilde;o existe no mes final
			var diaOriginal:Number = dataFinal.getDate();
			
			dataFinal.setMonth(dataFinal.getMonth() + anoMesDia.mes);
			
			// Esta funcao e usada apenas por causa de fevereiro
			// Exemplo:
			// 31/12/2000 + 2M = Pelo Javascript daria 3/03/2001, mas pela lei penal deve ser dia 01/03/2001	
			// 30/12/2000 + 2M = Pelo Javascript daria 2/03/2001, mas pela lei penal deve ser dia 01/03/2001	
			if (diaOriginal != dataFinal.getDate())
			{
				dataFinal.setDate(1);
			}				
			
			dataFinal.setDate(dataFinal.getDate() + anoMesDia.dia);	
			
			return dataFinal;
		}	
		
		
		public static function compararData(dataBase:Date, data:Date):Boolean
		{
			if (dataBase.getFullYear() == data.getFullYear() && dataBase.getMonth() == data.getMonth() && dataBase.getDate() == data.getDate())
			{
				return true;
			}
			return false;
		}
		
		public static function subtrairAnoMesDiaDaData(dataBase:Date, anoMesDia:AnoMesDia):Date
		{
			if (dataBase != null &&  anoMesDia==null)
			{
				return dataBase;
			}
			
			if (dataBase == null ||  anoMesDia==null)
			{
				return null;
			}
			var dataFinal:Date = new Date(dataBase);
			dataFinal.setDate(dataFinal.getDate() - anoMesDia.dia);
			dataFinal.setMonth(dataFinal.getMonth() - anoMesDia.mes);			
			dataFinal.setFullYear(dataFinal.getFullYear() - anoMesDia.ano);
			return dataFinal;
		}	
		
	}
}