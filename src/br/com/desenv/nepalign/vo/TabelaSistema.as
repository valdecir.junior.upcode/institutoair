package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TabelaSistema")]
	public class TabelaSistema 
	{
		public var id: Number = NaN;
		public var descricao: String;
		public var nomeTabela: String;
		public var campoPrincipal: String;
		public var descricaoCampo: String;
		public function TabelaSistema()
		{
		}
	}
}

