package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.TipoMovimentoBancos")]
	public class TipoMovimentoBancos
	{
		public var id: Number = NaN;
		public var codigo:String = null;
		public var descricao:String = null;
		public var creditoDebito:Number = NaN;
		
		public function TipoMovimentoBancos()
		{
		}
		
		public function get nomeConcatenado():String
		{
			return codigo + " - " + descricao;
		}
	}
}