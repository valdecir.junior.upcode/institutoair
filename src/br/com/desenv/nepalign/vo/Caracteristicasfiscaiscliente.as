package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Caracteristicasfiscaiscliente")]
	public class Caracteristicasfiscaiscliente 
	{
		public var id: Number = NaN;
		public var categoriaFiscal: CategoriaFiscal;
		public var cliente: Cliente;
		public var situacao: Number = NaN;
		public var reducaoBaseCalculo: Number = NaN;
		public var observacaoNotaFiscal: String;
		public function Caracteristicasfiscaiscliente()
		{
		}
		
		
	}
}

