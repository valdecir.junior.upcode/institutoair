package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.PreLancamentoContaPagarRateio")]
	public class PreLancamentoContaPagarRateio
	{
		public var id: Number = NaN;
		public var empresaFisica:EmpresaFisica;
		public var preLancamentoContaPagar:PreLancamentoContaPagar;
		public var porcentagemRateio:Number = NaN;
	}
}