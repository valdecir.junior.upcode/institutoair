package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.NotaFiscaComplNfe")]
	public class NotaFiscaComplNfe 
	{
		public var id: Number = NaN;
		public var notaFiscal: NotaFiscal;
		public var flLocalEntregaDif: Number = NaN;
		public var flLocalRetiradaDif: Number = NaN;
		public var flInsideISSQN: Number = NaN;
		public var codigoMunicipioEmpr: String;
		public var nomeMunicipioEmpr: String;
		public var siglaUFEmpr: String;
		public var cepEmpr: String;
		public var codigoPaisEmpr: String;
		public var telefoneEmpr: String;
		public var inscrMunicipalEmpr: String;
		public var cNAEEmpr: String;
		public var statusNFE: Number = NaN;
		public var dataRetornoNFE: Date;
		public var numProtocoloREFAZ: String;
		public var chaveAcessoNFE: String;
		public var motivoStatusNFE: String;
		public var statusCancelamentoNFE: Number = NaN;
		public var motivoStatusCanc: String;
		public var statusInutilizacaoNFE: Number = NaN;
		public var motivoInutilizacaoNFE: String;
		public var statusImpressaoNFE: Number = NaN;
		public var tipoNotaNFE: Number = NaN;
		public var flgLocalEntregaDiferente: Number = NaN;
		public var flgLocalRetiradaDiferente: Number = NaN;
		public var flgIncideISSQN: Number = NaN;
		public var finalidadeNota: Number = NaN;
		public var nfeReferenciada: String;
		public var codigoUFNFeReferenciada: Number = NaN;
		public var anoMesEmissaoNFeRef: String;
		public var cnpjEmitente: String;
		public var modeloNfeRef: String;
		public var serieNFeRef: String;
		public var numeroNFeRef: String;
		public function NotaFiscaComplNfe()
		{
		}
		
		public function get flLocalEntregaDifDescricaoCompleta(): String
		{
			if (this.flLocalEntregaDif == 0)
			{
				return "Nao";
			}
			else if (this.flLocalEntregaDif == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flLocalRetiradaDifDescricaoCompleta(): String
		{
			if (this.flLocalRetiradaDif == 0)
			{
				return "Nao";
			}
			else if (this.flLocalRetiradaDif == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flInsideISSQNDescricaoCompleta(): String
		{
			if (this.flInsideISSQN == 0)
			{
				return "Nao";
			}
			else if (this.flInsideISSQN == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get cepEmprFormatado(): String
		{
			if (this.cepEmpr == null)
			{
				return this.cepEmpr;
			}	
			else
			{
				return Formatador.formatarCep(this.cepEmpr);
			}		
		}
		public function get telefoneEmprFormatado(): String
		{
			if (this.telefoneEmpr == null)
			{
				return this.telefoneEmpr;
			}	
			else
			{
				return Formatador.formatarFone(this.telefoneEmpr);
			}		
		}
		public function get flgLocalEntregaDiferenteDescricaoCompleta(): String
		{
			if (this.flgLocalEntregaDiferente == 0)
			{
				return "Nao";
			}
			else if (this.flgLocalEntregaDiferente == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgLocalRetiradaDiferenteDescricaoCompleta(): String
		{
			if (this.flgLocalRetiradaDiferente == 0)
			{
				return "Nao";
			}
			else if (this.flgLocalRetiradaDiferente == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get flgIncideISSQNDescricaoCompleta(): String
		{
			if (this.flgIncideISSQN == 0)
			{
				return "Nao";
			}
			else if (this.flgIncideISSQN == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get finalidadeNotaDescricaoCompleta(): String
		{
			if (this.finalidadeNota == 0)
			{
				return "Nao";
			}
			else if (this.finalidadeNota == 1)
			{
				return "Sim";
			}
			else
			{
				return "";
			}
			
		}
		public function get cnpjEmitenteFormatado(): String
		{
			if (this.cnpjEmitente == null)
			{
				return this.cnpjEmitente;
			}	
			else
			{
				return Formatador.formatarCnpj(this.cnpjEmitente); 
			}		
		}
		
	}
}

