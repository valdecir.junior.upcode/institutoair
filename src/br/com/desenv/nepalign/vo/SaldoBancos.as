package br.com.desenv.nepalign.vo
{
	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SaldoBancos")]
	public class SaldoBancos
	{
		public var id: Number = NaN;
		public var banco:Bancos = null;
		public var mes:Number = NaN;
		public var ano:Number = NaN;
		public var saldo:Number = NaN;
		
		public function SaldoBancos()
		{
		}
	}
}