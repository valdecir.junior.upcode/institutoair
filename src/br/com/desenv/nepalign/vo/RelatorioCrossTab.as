package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.RelatorioCrossTab")]
	public class RelatorioCrossTab 
	{
		public var id: Number = NaN;
		public var grupo1: String;
		public var grupo2: String;
		public var grupo3: String;
		public var valor: Number = NaN;
		public var indiceCampoColuna: Number = NaN;
		public var campoColuna: String;
		public var total1000: Number = NaN;
		public var total1001: Number = NaN;
		public var total1002: Number = NaN;
		public var total1003: Number = NaN;
		public var total1004: Number = NaN;
		public var total1005: Number = NaN;
		public var idRelatorio: String;
		public function RelatorioCrossTab()
		{
		}
		
		
	}
}

