package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.Transportadora")]
	public class Transportadora 
	{
		public var id: Number = NaN;
		public var dataCadastro: Date;
		public var nomeFantasia: String;
		public var razaoSocial: String;
		public var cnpjCpf: String;
		public var inscricaoEstadualRg: String;
		public var enderecoTransportadora: String;
		public var complementoEndereco: String;
		public var bairroTransportadora: String;
		public var cepTransportadora: String;
		public var cidade: Cidade;
		public var contato: String;
		public var dddTelefoneTransportadora: String;
		public var telefone: String;
		public var dddFaxTransportadora: String;
		public var faxTransportadora: String;
		public var observacao: String;
		public var comentario: String;
		public var emailTransportadora: String;
		public function Transportadora()
		{
		}
		
		public function get cnpjCpfFormatado(): String
		{
			if (this.cnpjCpf == null)
			{
				return this.cnpjCpf;
			}	
			else
			{
				return Formatador.formatarCpfCnpj(this.cnpjCpf);
			}		
		}
		public function get cepTransportadoraFormatado(): String
		{
			if (this.cepTransportadora == null)
			{
				return this.cepTransportadora;
			}	
			else
			{
				return Formatador.formatarCep(this.cepTransportadora);
			}		
		}
		public function get telefoneFormatado(): String
		{
			if (this.telefone == null)
			{
				return this.telefone;
			}	
			else
			{
				return Formatador.formatarFone(this.telefone);
			}		
		}
		public function get faxTransportadoraFormatado(): String
		{
			if (this.faxTransportadora == null)
			{
				return this.faxTransportadora;
			}	
			else
			{
				return Formatador.formatarFone(this.faxTransportadora);
			}		
		}
		
	}
}

