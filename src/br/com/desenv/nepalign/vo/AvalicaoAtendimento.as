package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.AvalicaoAtendimento")]
	public class AvalicaoAtendimento 
	{
		public var id: Number = NaN;
		public var pedidoVenda: PedidoVenda;
		public var nota: Number = NaN;
		public var usuario: Usuario;
		public var duplicata: Duplicata;
		public function AvalicaoAtendimento()
		{
		}
		
		
	}
}

