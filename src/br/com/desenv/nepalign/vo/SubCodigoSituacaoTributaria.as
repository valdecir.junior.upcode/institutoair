package br.com.desenv.nepalign.vo
{
	import flash.utils.ByteArray;
	import br.com.desenv.framework.component.formatter.Formatador;

	[Bindable]
	[RemoteClass(alias="br.com.desenv.nepalign.model.SubCodigoSituacaoTributaria")]
	public class SubCodigoSituacaoTributaria 
	{
		public var id: Number = NaN;
		public var codigo: Number = NaN;
		public var descricao: String;
		public function SubCodigoSituacaoTributaria()
		{
		}
		
		public function get descricaoCompleta():String
		{
			return codigo + "-" + descricao; 
		}
	}
}

