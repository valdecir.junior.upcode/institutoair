package br.com.desenv.framework
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.Security;
	
	import mx.controls.Alert;
	import mx.events.FlexEvent;

	public class DsvHttpGet
	{
		public var resultFunction:Function = null;
		
		public function DsvHttpGet()
		{
			
		}
		
		public function getResponse(url:String, result:Function, securityError:Function = null, ioError:Function = null):void
		{
			this.resultFunction = result;
			try
			{
				Security.loadPolicyFile("xmlsocket://localhost:8080/");
				var requestSender:URLLoader = new URLLoader();
				var urlRequest :URLRequest = new URLRequest(url);
				requestSender.addEventListener(Event.COMPLETE, result);
				requestSender.addEventListener(SecurityErrorEvent.SECURITY_ERROR, (securityError == null ? this.securityErrorHandler : securityError));
				requestSender.addEventListener(IOErrorEvent.IO_ERROR, (ioError == null ? this.ioErrorHandler : ioError));
				
				urlRequest.method = URLRequestMethod.GET;
				urlRequest.idleTimeout = 3000;
			}
			catch(e:Error)
			{
				Alert.show(e.message);
				throw e;
			}
			finally
			{
				requestSender.load(urlRequest);
			}
		}
		
		public function securityErrorHandler(event:SecurityErrorEvent) : void
		{
			Alert.show("Security Error. Error Id : " + event.errorID + " Message : " + event.toString());
			
			if(this.resultFunction != null)
				this.resultFunction();
		}
		
		public function ioErrorHandler(event:IOErrorEvent):void
		{
			Alert.show("IO Error");
		}
		
	}
}