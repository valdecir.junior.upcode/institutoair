package br.com.desenv.framework.parametrossistema
{
	import flash.filesystem.File;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * @author Lucas
	 */
	public class ParametrosSistemaLoader extends URLLoader
	{
		private var grupo:String;
		
		public function ParametrosSistemaLoader(grupo:String) 
		{
			this.grupo = grupo;
		}
		
		public function loadParameters(loja:String):void
		{
			load(new URLRequest("br/com/desenv/framework/parametrossistema/" + grupo + "/ParametrosSistema_" + loja + ".NEPAL"));
		}
		
		public function loadAvaliableGroups():ArrayCollection
		{
			var groupsAvaliable:ArrayCollection = new ArrayCollection();
			var file:File = File.applicationDirectory.resolvePath("br/com/desenv/framework/parametrossistema");
			
			for (var i:uint = 0; i < file.getDirectoryListing().length; i++) 
			{
				var dir:File = (file.getDirectoryListing()[i] as File);
				
				if(dir.isDirectory)
				{
					var grupo:Object = new Object();
					grupo.codigo = dir.name.toLowerCase();
					grupo.label = dir.name.toUpperCase();
					groupsAvaliable.addItem(grupo);
				}
			}
			return groupsAvaliable;
		}
		
		public function loadAvaliable():ArrayCollection
		{
			var paramAvaliables:ArrayCollection = new ArrayCollection();
			var file:File = File.applicationDirectory.resolvePath("br/com/desenv/framework/parametrossistema/" + grupo + "/");
			
			for (var i:uint = 0; i < file.getDirectoryListing().length; i++) 
			{
				var codigoLoja:String = (file.getDirectoryListing()[i] as File).name.split("_")[0x00000001].split(".")[0x0000000];
				var nomeLoja:String = "Loja " + codigoLoja;
				
				if(codigoLoja == "52")
					nomeLoja = "Escritório";
				
				var loja:Object = new Object();
				loja.codigo = codigoLoja;
				loja.label = nomeLoja;
				
				paramAvaliables.addItem(loja);
			}
			
			return paramAvaliables;
		}
	}
}