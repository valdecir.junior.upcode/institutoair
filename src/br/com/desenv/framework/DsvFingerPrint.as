package br.com.desenv.framework
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.sampler.NewObjectSample;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.rpc.events.ResultEvent;
	
	import br.com.desenv.framework.util.DsvObjectUtil;

	/**
	 * @author Lucas
	 */
	[Event(name="DataCaptured", type="mx.rpc.events.ResultEvent")]
	public class DsvFingerPrint extends EventDispatcher
	{
		private var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();  
		private var startUpFileSystem:File = File.applicationDirectory.resolvePath((FlexGlobals.topLevelApplication.parametrosSistema.getParametro("FINGERPRINT_PATH") as String) + "/DsvFingerPrint.exe");
		
		protected var startupVerify:Boolean = false;

		protected var readComplete:Function;
	
		private var receivedByteArrayList:ArrayList = new ArrayList();
		
		public function setReadCompleteFunction(func:Function):void
		{
			this.readComplete = func;
		}
		
		public function DsvFingerPrint()
		{ 
			/**
			 * TODO: Verificar se API se encontra na pasta correta.
			 */
			
			startupVerify = true;
		}
		
		/**
		 * Captura a leitura biométrica.
		 */
		public function capture():void
		{  
			if(startupVerify)
			{ 
				nativeProcessStartupInfo.executable = startUpFileSystem;
				
				var processArgs:Vector.<String> = new Vector.<String>();

				processArgs.push("1");
				//processArgs.push("C:/Desenv/FingerPrint/captura.txt");
				
				nativeProcessStartupInfo.arguments = processArgs; 
				
				var outputDataHandlerFunction:Function = handleOutputData("1");
				
				var process:NativeProcess = new NativeProcess(); 
				process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, outputDataHandlerFunction);
				process.addEventListener(NativeProcessExitEvent.EXIT, onExitNativeProcessEvent);
				process.start(nativeProcessStartupInfo);
			}
			else
				Alert.show("Sistema não carregado. Reinicie o leitor biométrico");
		}
				
		public function verify(storedFingerprint:String):void
		{
			if(startupVerify)
			{
				nativeProcessStartupInfo.executable = startUpFileSystem; 
				 
				var processArgs:Vector.<String> = new Vector.<String>();
				processArgs.push("3");
				processArgs.push(storedFingerprint)
				
				nativeProcessStartupInfo.arguments = processArgs; 
				
				var outputDataHandlerFunction:Function = handleOutputData("3");
				
				var process:NativeProcess = new NativeProcess(); 
				process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, outputDataHandlerFunction);
				process.addEventListener(NativeProcessExitEvent.EXIT, onExitNativeProcessEvent);
				process.start(nativeProcessStartupInfo);
			}
			else
				Alert.show("Sistema não carregado. Reinicie o leitor biométrico");
		}
		
		/*

		*/
		
		private function onExitNativeProcessEvent(event:NativeProcessExitEvent):void 
		{
			var output:String = "";
			
			for(var i:int=0; i < receivedByteArrayList.length; i++)
			{
				var received:String = receivedByteArrayList.getItemAt(i) as String;
				
				output = output + (received as String);
			}

			if(DsvObjectUtil.startWith("RESULT", output))
				dispatchEvent(new ResultEvent("DataCaptured", false, true, output.replace("RESULT:", ""), null, null));	
			
			receivedByteArrayList.removeAll();
		}
		
		private function handleOutputData(operation:String):Function
		{
			return function (event:ProgressEvent):void
			{
				var nativeProcess:NativeProcess = (event.target as NativeProcess);
				var bytesAvailable:int = nativeProcess.standardOutput.bytesAvailable;

				receivedByteArrayList.addItem(nativeProcess.standardOutput.readUTFBytes(bytesAvailable) as String); 
			}
		}
	}
}