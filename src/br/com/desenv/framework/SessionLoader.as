package br.com.desenv.framework
{
	import flash.desktop.NativeApplication;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequestMethod;
	import flash.system.Capabilities;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.mxml.WebService;
	
	import br.com.desenv.framework.security.Credentials;
	import br.com.desenv.framework.updater.NepalignUpdater;
	import br.com.desenv.framework.util.DsvObjectUtil;
	import br.com.desenv.framework.util.DsvParametrosSistema;
	import br.com.desenv.framework.util.JSONEncoder;
	import br.com.desenv.framework.util.XMLUtil;
	import br.com.desenv.framework.webservices.WebServiceProvider;
	import br.com.desenv.nepalign.vo.SessionInfo;
	import br.net.upcode.framework.amf.AMFConnector;
	
	/**
	 * @author Lucas
	 */
	[Event(name="ServidoresCarregados", type="flash.events.Event")]
	public class SessionLoader extends EventDispatcher
	{
		public static const SERVIDORES_CARREGADOS_EVENT:String = "ServidoresCarregados";
		
		private const dsvHttpGet:DsvHttpGet = new DsvHttpGet();
		
		private static const WSDL_REMOTE_PORT:String = "IgnAuthenticationServiceInterface.Remote";
		private static const WSDL_STANDARD_PORT:String = "IgnAuthenticationServiceInterface";

		public var opRunning:Boolean = false;
	
		private var tryingRemote:Boolean = false;
		
		private var webService:WebService;
		
		private var username:String;
		private var password:String;
		private var server:String;
		
		private var connectOnExternalPort:Boolean = false;
		
		private var sessionInfoBuffer:SessionInfo;
		private var currentServer:Object = null;
		
		[Bindable]
		public var listaServidores:ArrayCollection = new ArrayCollection();
		
		public function get WSDL_PORT():String
		{
			var wsdlPort:String = tryingRemote ? WSDL_REMOTE_PORT : WSDL_STANDARD_PORT;
			
			if((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("NOME_GRUPO") != null)
				wsdlPort = wsdlPort + "." + ((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("NOME_GRUPO") as String).toLowerCase();
			if((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("LOJA") != null)
				wsdlPort = wsdlPort + "." + ((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("LOJA") as String).toLowerCase();
			
			return wsdlPort;
		}

		public function SessionLoader()
		{
			
		}
		
		/**
		 * Loads User Session WEB SERVICE
		 * 
		 * @param _username username
		 * @param _password password in md5
		 * @param serverInfo to compatible with 1.9.17 or minor
		 * @langversion ActionScript 3.0
		 * @playerversion Flash 9.0
		 * @tiptext Loads User Session
		 */
		public function loadSession(_username:String, _password:String, serverInfo:Object = null):void
		{
			const credentials:Credentials = new Credentials();
			credentials.username = _username;
			credentials.password = _password;
			
			const encoder:JSONEncoder = new JSONEncoder(credentials);
			
			var WEBSERVICE_URL:String = "";
			
			if((serverInfo.serverIp as String).indexOf("wsdl") > -0x01)
				WEBSERVICE_URL = (serverInfo.serverIp as String).split("/")[0x000002];
			else
				WEBSERVICE_URL = (serverInfo.serverIp as String);
			
			const webServiceProvider:WebServiceProvider = new WebServiceProvider();
			webServiceProvider.addEventListener(WebServiceProvider.WEBSERVICEPROVIDER_RESULT, function(event:DataEvent):void
			{
				completeProcessLogin(new br.com.desenv.framework.util.XMLUtil().deserialize(XML(event.data as String), SessionInfo) as SessionInfo);
			});
			webServiceProvider.call("http://" + WEBSERVICE_URL + "/nepalign-ws/authentication/desktop", encoder.getString(), URLRequestMethod.POST);
		}
		
		public function load(_username:String, _password:String, serverInfo:Object):void
		{
			/*loadSession(_username, _password, serverInfo);
			currentServer = serverInfo;	*/
			try
			{
				if(opRunning)
					return;
				if(FlexGlobals.topLevelApplication.sessionInfo != null)
					return;
				
				sysout("Loading Session - " + _username);
				
				opRunning = true;
				
				if(webService == null) 
					webService = new WebService(); 
				
				CursorManager.setBusyCursor();
				
				webService.showBusyCursor = true;
				
				webService.requestTimeout = 0x0000005; //5 seconds timeout
				webService.addEventListener(LoadEvent.LOAD, webServiceLoadEvent);
				webService.addEventListener(FaultEvent.FAULT, webServiceFaultEvent);
				webService.port = WSDL_PORT;
								
				if(tryingRemote)
				{
					sysout("#### Remote Option is enabled.");
					
					webService.wsdl = serverInfo.serverBackupIp;

					webService.processLoginFromPort.resultFormat = "object";
					webService.processLoginFromPort.addEventListener(ResultEvent.RESULT, resultHandler);
					webService.processLoginFromPort.addEventListener(FaultEvent.FAULT, faultHandler);
				}
				else
				{
					webService.wsdl = serverInfo.serverIp;

					webService.processLogin.resultFormat = "object";
					webService.processLogin.addEventListener(ResultEvent.RESULT, resultHandler);
					webService.processLogin.addEventListener(FaultEvent.FAULT, faultHandler);
				}
				
				sysout("Using WSDL location " + webService.wsdl + " on port " + webService.port);
				
				//webService.wsdl = serverInfo.serverIp;
				//webService.port = serverInfo.connectionType == undefined ? WSDL_STANDARD_PORT : WSDL_STANDARD_PORT;

				username = _username;
				password = _password;
				
				webService.loadWSDL(); 
				
				currentServer = serverInfo;	
			}
			catch(e:Error)
			{
				Alert.show(e.message, "Mensagem");
				opRunning = false;
			}
		}
		
		protected function webServiceLoadEvent(event:LoadEvent):void
		{
			sysout("WSDL signature load complete.");
			
			if(!tryingRemote)
				webService.processLogin.send(username, password);
			else
				webService.processLoginFromPort.send(username, password, WSDL_REMOTE_PORT);
		}
		
		protected function webServiceFaultEvent(event:FaultEvent):void
		{
			if(Capabilities.isDebugger)
			{
				Alert.show("Servidor de login não conseguiu receber a conexão. \n\nDetalhe:\nCodigo:  " + event.fault.errorID + "#" + event.fault.faultCode + "\n"
					+ "FaultString: " + event.fault.faultString + "\n"
					+ "FaultDetail: " + event.fault.faultDetail, "Mensagem do Sistema");
				CursorManager.removeBusyCursor();
			}
			
			syserr(event);
			//Alert.show("Não foi possível se conectar ao Servidor de Login. Tente novamente mais tarde, se o erro persistir entre em contato com o suporte!", "AVISO");
			if(!Capabilities.isDebugger)
			{
				//Alert.show("Não foi possível se conectar ao Servidor de Login. Tente novamente, se o erro persistir entre em contato com o suporte!", "AVISO");
				
				if(tryingRemote)
				{
					Alert.show("Não foi possível se conectar ao Servidor de Login. \n\nTente novamente mais tarde. Se o erro persistir,\n entre em contato com o suporte!", "Mensagem do Sistema");
					tryingRemote = false;
				}
				else if(currentServer.serverBackupIp != null)
					tryBackupWsdlUrl();
				else
					Alert.show("Não foi possível se conectar ao Servidor de Login. \n\nTente novamente mais tarde. Se o erro persistir,\n entre em contato com o suporte!", "Mensagem do Sistema");
				
				CursorManager.removeBusyCursor();
			}
			
			opRunning = false;
		}
		
		private function tryBackupWsdlUrl():void
		{
			tryingRemote = true;
			load(username, password, currentServer);
		}
		
		protected function resultHandler(event:ResultEvent):void
		{
			try
			{
				sysout("Processing login data.");
				
				if(event.result == null)
				{
					sysout("User not found.");
					Alert.show("Usuário não encontrado.", "Mensagem");
					
					opRunning = false;
					CursorManager.removeBusyCursor();
				}
				else
				{
					sysout("User found. Completing login process");
					var sessionInfo:SessionInfo = null;
					try
					{
						sessionInfoBuffer = new br.com.desenv.framework.util.XMLUtil().deserialize(XML(event.result.toString()), SessionInfo) as SessionInfo;	
					}
					finally
					{
						completeProcessLogin(sessionInfoBuffer);
						colocarDataAtual();
					}
				}
			}
			catch(e:Error)
			{
				Alert.show("Não foi processar o seu login. \n\nTente novamente mais tarde. Se o erro persistir,\n entre em contato com o suporte!", "Mensagem do Sistema");
				syserr(e);
				
				opRunning = false;
				CursorManager.removeBusyCursor();
			}
		}
		
		protected function delayTimerCompleted(event:TimerEvent):void
		{
			try
			{
				completeProcessLogin(sessionInfoBuffer);
			}
			catch(e:Error)
			{
				//SUPRESS
			}
		}
		
		/**
		 * Completes the login process using SessionInfo
		 * 
		 * @param sessionInfo SessionInfo generated by LoginServer
		 * @langversion ActionScript 3.0
		 */
		protected function completeProcessLogin(sessionInfo:SessionInfo):void
		{
			sysout("Loading Session Info");
			
			FlexGlobals.topLevelApplication.sessionInfo = sessionInfo;
			(FlexGlobals.topLevelApplication.sessionInfo as SessionInfo).connectedServerName = currentServer.serverName;
			(FlexGlobals.topLevelApplication.sessionInfo as SessionInfo).timezoneOffset = (-new Date().getTimezoneOffset() * 60 * 1000);
			
			sysout("User session logged on " + currentServer.serverName + " using timezoneOffset " + (FlexGlobals.topLevelApplication.sessionInfo as SessionInfo).timezoneOffset.toString());
			sysout("Server AMF " + sessionInfo.amfChannelUrl);
			AMFConnector.instance.registerAMF(sessionInfo.amfChannelUrl);
			sysout("Storing SessionInfo on Server.");
			
			FlexGlobals.topLevelApplication.montarObjetoRemoto("usuarioService", 
			function(event:ResultEvent):void 
			{
				sysout("User SessionInfo stored on server successfully");
				
				(FlexGlobals.topLevelApplication.sessionInfo as SessionInfo).checkInitAsSeller();
				FlexGlobals.topLevelApplication.setFocus();
				CursorManager.removeBusyCursor();
			},
			function(event:FaultEvent):void
			{
				syserr("Não foi possível invocar o metodo colocarDadosFlexSession", event);
				Alert.show("Não foi possível armazenar os dados do usuário no servidor!", "Mensagem"); 
				
				FlexGlobals.topLevelApplication.displayLogin(true);
				opRunning = false;
				CursorManager.removeBusyCursor();
			}).getOperation("colocarDadosFlexSession").send(sessionInfoBuffer);
			
			if(sessionInfo == null)
			{
				syserr("SessionInfo is null. Rollbacking login process.");
				Alert.show("Não foi possível estabelecer conexão com o Servidor de Dados. Tente novamente.", "Mensagem");
				
				FlexGlobals.topLevelApplication.sessionInfo = null;
				opRunning = false;
				CursorManager.removeBusyCursor();
			}
			else
			{	
				(FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).addEventListener("ParametrosCarregados", parametrosCarregadosHandler);
				(FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).init();
				
				PopUpManager.removePopUp(FlexGlobals.topLevelApplication.loginForm);
				FlexGlobals.topLevelApplication.loginForm = null;
				CursorManager.removeBusyCursor();
			}
			
			sessionInfo.connectionManager.startAMFKeepAlive();
			
			NativeApplication.nativeApplication.idleThreshold = (sessionInfo.sessionTimeOut / 1000) * 60;
			NativeApplication.nativeApplication.addEventListener(Event.USER_IDLE, onUserIdle);
			
			sysout("Logged successfully as " + sessionInfo.usuario.nome);
			opRunning = false;
			
			CursorManager.removeBusyCursor();
		}
		
		protected function onUserIdle(event:Event):void
		{
			NativeApplication.nativeApplication.removeEventListener(Event.USER_IDLE, onUserIdle);
			 
			FlexGlobals.topLevelApplication.displayLogin(true);
		}
		
		protected function parametrosCarregadosHandler(event:Event):void
		{			
			(FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).removeEventListener("ParametrosCarregados", parametrosCarregadosHandler);
			
			if(!Capabilities.isDebugger)	
				new NepalignUpdater().checkUpdate();
		}
		
		protected function removerDadosFlexSessionResultHandler(event:ResultEvent):void
		{
			FlexGlobals.topLevelApplication.montarObjetoRemoto("usuarioService", colocarDadosFlexSessionResultHandler).getOperation("colocarDadosFlexSession").send(sessionInfoBuffer);
		}
		
		protected function colocarDadosFlexSessionResultHandler(event:ResultEvent):void { }		 
		
		protected function faultHandler(event:FaultEvent):void
		{
			Alert.show("O Servidor não conseguiu processar o seu login.\n\nMotivo: " + event.fault.faultString + "\n\nTente novamente mais tarde. Se o erro persistir,\n entre em contato com o suporte!", "Mensagem do Sistema");
			syserr("Error on WebService " + event.fault.faultString);
			opRunning = false;
			
			CursorManager.removeBusyCursor();
		}
		
		public function recuperarServidoresLoginLocal():void
		{
			for (var param:String in (FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametros)
			{
				if(param.indexOf("SRV") > -1)
				{
					var serverInfoObj:Object = new Object();
					serverInfoObj.serverName = param.split(".")[0x1];
					serverInfoObj.serverIp = ((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametros[param]);
					
					if(serverInfoObj.serverIp.split(";").length > 1)
					{
						serverInfoObj.serverBackupIp = serverInfoObj.serverIp.split(";")[0x1];
						serverInfoObj.serverIp = serverInfoObj.serverIp.split(";")[0x0];
					}

					listaServidores.addItem(serverInfoObj);
				}
			}
			DsvObjectUtil.arrayCollectionSort(listaServidores, "serverName", false);
			dispatchServidoresCarregadosEvent();
		}
		
		public function recuperarServidoresLoginExterno():void
		{
			var urlPrimaryServer:String = "";
			
			var serverIp:String = (FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("IP_SERVIDOR") as String;
			
			if(serverIp == null)
			{
				if((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDORES_LOGIN") != null &&
					((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDORES_LOGIN") as String) != "")
					serverIp = ((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDORES_LOGIN") as String).split("/")[0x00002];
				else
				{
					recuperarServidoresLoginLocal();
					return;
				}
			}

			urlPrimaryServer = "http://" + serverIp + "/upcode/ParametrosSistema_" + (FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("LOJA") + ".NEPAL";

			new DsvHttpGet().getResponse(urlPrimaryServer, 
				function(event:Event):void 
				{ 
					for each(var info:String in (event.currentTarget.data as String).split("\r\n"))
					{
						if(info.substring(0, 2) == "//")
							continue;
						
						if(info.indexOf("SRV") > -1)
						{
							var serverInfoObj:Object = new Object();
							serverInfoObj.serverName = (info.split("=")[0x0] as String).split(".")[0x1];
							serverInfoObj.serverIp = (info.split("=")[0x1] as String);
							
							if(serverInfoObj.serverIp.split(";").length > 1)
							{
								serverInfoObj.serverBackupIp = serverInfoObj.serverIp.split(";")[0x1];
								serverInfoObj.serverIp = serverInfoObj.serverIp.split(";")[0x0];
							}
							
							listaServidores.addItem(serverInfoObj);
						}
						else
							(FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).addParameter((info.split("=")[0x0] as String), (info.split("=")[0x1] as String));
					}
					DsvObjectUtil.arrayCollectionSort(listaServidores, "serverName", false);
					dispatchServidoresCarregadosEvent();
				}, 
				null, 
				function loadLastUpdateOnSalesFaultHandler(event:IOErrorEvent):void
				{
					recuperarServidoresLoginLocal();
				});
			
		}
		
		protected function dispatchServidoresCarregadosEvent():void
		{
			sysout("Servidores Carregados");
			dispatchEvent(new Event(SERVIDORES_CARREGADOS_EVENT));
		}
		
		private function colocarDataAtual():void
		{
			FlexGlobals.topLevelApplication.montarObjetoRemoto("dsvParametrosSistemaService", colocarDataAtualResultHandler, 
			function(event:FaultEvent):void
			{
			
			}).getOperation("getParametro").send("DataAtual");
		}
		
		private function colocarDataAtualResultHandler(event:ResultEvent):void
		{
			if(event.result != null)
				FlexGlobals.topLevelApplication.setDataAtual( event.result as Date);
			else
				FlexGlobals.topLevelApplication.setDataAtual(new Date());
		}
	}	
}
