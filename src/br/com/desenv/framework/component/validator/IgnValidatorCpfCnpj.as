package br.com.desenv.framework.component.validator
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class IgnValidatorCpfCnpj extends Validator
	{
		private var results:Array;
		public static var CPF:int = 1;
		public static var CNPJ:int = 2;
		static protected const FIRST_VERIFIER_DIGITS:Array=[ 5,4,3,2,9,8,7,6,5,4,3,2 ];
		static protected const SECOND_VERIFIER_DIGITS:Array=[ 6,5,4,3,2,9,8,7,6,5,4,3 ];		
		
		public function IgnValidatorCpfCnpj()
		{
			super();
			//formato padrao: CPF
			
		}
		override protected function doValidation(value:Object):Array 
		{
			results = [];
			if (value.toString()!="" || required==true)
			{
				var err:String = "";
				var tamanho:int;
				var notNumber : RegExp = /[^0-9]/g;

				var tDesformatado:String = value.toString().replace(notNumber, "");
				
				//results = validateNumber(tDesformatado, "text");
				
				if (tDesformatado.length == 11)
					results = doValidationCpf(tDesformatado);
				else
					results = doValidationCnpj(tDesformatado);					
			}
			return results;
		}
		protected function doValidationCnpj(value:Object):Array 
		{
			// Check first name field.
			var cnpj:String=value as String;
			if(cnpj==null||cnpj=="") {
				return results;
			} else if(cnpj.length!=14) {
				results.push(new ValidationResult(true,"","","O CNPJ deve conter 14 dígitos"));
				return results;
			} else if(!verifyDigitsCnpj(cnpj)) {
				results.push(new ValidationResult(true,"","","CNPJ inválido"));
				return results;
			}
			return results;
		}
		private function verifyDigitsCnpj(cnpj:String):Boolean 
		{
			var firstDigit:int=defineDigitValueCnpj(getDigitsPlusOperationCnpj(cnpj,FIRST_VERIFIER_DIGITS)%11);
			if(new Number(firstDigit)!=new Number(cnpj.charAt(12))) {
				return false;
			}
			var secondDigit:int=defineDigitValueCnpj((getDigitsPlusOperationCnpj(cnpj,SECOND_VERIFIER_DIGITS)+firstDigit*2)%11);
			return new Number(secondDigit)==new Number(cnpj.charAt(13));
		}
		
		private function defineDigitValueCnpj(digit:int):int 
		{
			return digit>1?11-digit:0;
		}
		
		private function getDigitsPlusOperationCnpj(cnpj:String,verifierDigitis:Array):int 
		{
			var s:int=0;
			for(var i:int=0;i<12;++i) {
				s+=new Number(cnpj.charAt(i))*verifierDigitis[i];
			}
			return s;
		}
		private function doValidationCpf(value:Object):Array 
		{
			var cpf:String=value as String;
			if(cpf==null||cpf=="") {
				return results;
			} else if(cpf.length!=11) {
				results.push(new ValidationResult(true,"","","O CPF deve conter 11 dígitos"));
				return results;
			} else if(!verifyDigitsCpf(cpf)) {
				results.push(new ValidationResult(true,"","","CPF inválido"));
				return results;
			}
			return results;
		}
		
		private function verifyDigitsCpf(cpf:String):Boolean {
			var firstDigit:int=defineDigitValueCpf(getDigitsPlusOperationCpf(cpf,10)%11);
			if(new Number(firstDigit)!=new Number(cpf.charAt(9))) {
				return false;
			}
			var secondDigit:int=defineDigitValueCpf((getDigitsPlusOperationCpf(cpf,11)+firstDigit*2)%11);
			return new Number(secondDigit)==new Number(cpf.charAt(10));
		}
		
		private function defineDigitValueCpf(digit:int):int 
		{
			return digit>1?11-digit:0;
		}
		
		private function getDigitsPlusOperationCpf(cpf:String,initialFactor:int):int 
		{
			var s:int=0;
			for(var i:int=0;i<9;++i,--initialFactor) {
				s+=new Number(cpf.charAt(i))*initialFactor;
			}
			return s;
		}
	}
}