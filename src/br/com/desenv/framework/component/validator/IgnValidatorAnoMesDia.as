package br.com.desenv.framework.component.validator
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class IgnValidatorAnoMesDia extends Validator
	{
		public function IgnValidatorAnoMesDia()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array 
		{
			var anoMesDia:String = value.toString();
			
			var results:Array = new Array();
			
			//pode ser ano
			//pode ser mes
			//pode ser dia
			
			//pode ser ano/mes
			//pose ser ano/dia
			//mes/dia
			
			
			if (anoMesDia != "")
			{
				var anoMesDiaNumber: RegExp = /([0-9]+[a|A])?([0-9]+[m|M])?([0-9]+[d|D])?/;
				
				
				if (!anoMesDiaNumber.test(anoMesDia))
				{
					results.push(new ValidationResult(true, null, "Erro", "Formato de tempo em anos, meses e dias é inválido. Exemplos: 2a5m10d (dois anos, 5 meses e 10 dias), 5 (5 anos), 5a (5 anos), 5a3m (cinco anos e três meses)"));
				}
			}
			
			return results;
			
		}
	}
}


