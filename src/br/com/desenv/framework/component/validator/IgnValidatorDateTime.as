package br.com.desenv.framework.component.validator
{
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.formatters.DateFormatter;
	import mx.validators.DateValidator;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class IgnValidatorDateTime extends Validator
	{
		public function IgnValidatorDateTime()
		{
			super();
		}
		
		override protected function doValidation(value: Object):Array 
		{
			
			var dateTimeValue:String = value.toString();
			
			 
			
			
			var results:Array = new Array();
			
			if (dateTimeValue.length != 16)
			{	
				return results;
			}	
			
			// tem dois formatos
			// dd/MM/yyyy HH:mm
			// dd/MM/yy HH:mm
			// Faço split pelo espaco
			
			if (dateTimeValue != "")
			{
				var dateSplit: Array = dateTimeValue.split(" ");
				
				if (dateSplit.length != 2)
				{
					results.push(new ValidationResult(true, null, "Erro", "Data inválida!"));
				}
				
				
				// valida a primeira parte
				// dd/MM/yyyy
				var dataTemp: Date = DateField.stringToDate(dateSplit[0], "DD/MM/YYYY");
				
				if (dataTemp == null)
				{
					results.push(new ValidationResult(true, null, "Erro", "Data inválida!"));
				}
				
				//valida a segunda parte
				//HH:MM
				// divide a hora do minuta
				var hourSplit: Array = dateSplit[1].split(":");
				
				if (hourSplit.length != 2)
				{
					results.push(new ValidationResult(true, null, "Erro", "Hora inválida!"));
				}
				
				
				if (parseInt(hourSplit[0]) > 23)
				{
					results.push(new ValidationResult(true, null, "Erro", "Hora inválida. Favor informar horas de 0 à 23!"));
				}

				if (parseInt(hourSplit[1]) > 59)
				{
					results.push(new ValidationResult(true, null, "Erro", "Minuto inválido. Favor informar minutos de 0 à 59!"));
				}
			}
			
			return results;
			
		}
	}
}


