package br.com.desenv.framework.component.validator
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class IgnValidatorCpf extends Validator
	{
		private var results:Array;
		public static var CPF:int = 1;
		static protected const FIRST_VERIFIER_DIGITS:Array=[ 5,4,3,2,9,8,7,6,5,4,3,2 ];
		static protected const SECOND_VERIFIER_DIGITS:Array=[ 6,5,4,3,2,9,8,7,6,5,4,3 ];		
		
		public function IgnValidatorCpf()
		{
			super();
			//formato padrao: CPF
			
		}
		override protected function doValidation(value:Object):Array 
		{
			results = [];
			if (value.toString()!="" || required==true)
			{
				var err:String = "";
				var tamanho:int;
				var notNumber : RegExp = /[^0-9]/g;

				var tDesformatado:String = value.toString().replace(notNumber, "");
				
				//results = validateNumber(tDesformatado, "text");
				
				results = doValidationCpf(tDesformatado);
								
			}
			return results;
		}
		
		private function doValidationCpf(value:Object):Array 
		{
			var cpf:String=value as String;
			if(cpf==null||cpf=="") {
				return results;
			} else if(cpf.length!=11) {
				results.push(new ValidationResult(true,"","","O CPF deve conter 11 dígitos"));
				return results;
			} else if(!verifyDigitsCpf(cpf)) {
				results.push(new ValidationResult(true,"","","CPF inválido"));
				return results;
			}
			return results;
		}
		
		private function verifyDigitsCpf(cpf:String):Boolean {
			var firstDigit:int=defineDigitValueCpf(getDigitsPlusOperationCpf(cpf,10)%11);
			if(new Number(firstDigit)!=new Number(cpf.charAt(9))) {
				return false;
			}
			var secondDigit:int=defineDigitValueCpf((getDigitsPlusOperationCpf(cpf,11)+firstDigit*2)%11);
			return new Number(secondDigit)==new Number(cpf.charAt(10));
		}
		
		private function defineDigitValueCpf(digit:int):int 
		{
			return digit>1?11-digit:0;
		}
		
		private function getDigitsPlusOperationCpf(cpf:String,initialFactor:int):int 
		{
			var s:int=0;
			for(var i:int=0;i<9;++i,--initialFactor) {
				s+=new Number(cpf.charAt(i))*initialFactor;
			}
			return s;
		}
	}
}