package br.com.desenv.framework.component.validator
{
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.formatters.DateFormatter;
	import mx.validators.DateValidator;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;

	
	public class IgnValidatorTime extends Validator
	{
		public function IgnValidatorTime()
		{
			super();
		}
		
		override protected function doValidation(value: Object):Array 
		{
			
			var timeValue:String = value.toString();
			
			
			
			
			var results:Array = new Array();
			
			if (timeValue.length != 8)
			{	
				return results;
			}	
			
			// tem dois formatos
			// dd/MM/yyyy HH:mm
			// dd/MM/yy HH:mm
			// Faço split pelo espaco
			
			if (timeValue != "")
			{		
				//valida a segunda parte
				//HH:MM
				// divide a hora do minuta
				var hourSplit: Array = timeValue.split(":");
				
				if (hourSplit.length != 3)
				{
					results.push(new ValidationResult(true, null, "Erro", "Hora inválida!"));
				}
				
				
				if (parseInt(hourSplit[0]) > 23)
				{
					results.push(new ValidationResult(true, null, "Erro", "Informar horas de 0 à 23!"));
				}
				
				if (parseInt(hourSplit[1]) > 59)
				{
					results.push(new ValidationResult(true, null, "Erro", "Informar minutos de 0 à 59!"));
				}
				if(parseInt(hourSplit[2]) > 59)
				{
					results.push(new ValidationResult(true, null, "Erro", "Informar segundos de 0 à 59"));
				}
			}
			
			return results;
			
		}
	}
}



