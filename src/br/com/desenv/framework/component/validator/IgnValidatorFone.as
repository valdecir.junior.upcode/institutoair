package br.com.desenv.framework.component.validator
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;

	public class IgnValidatorFone extends Validator
	{
		public function IgnValidatorFone()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array 
		{
			var telefone:String = value.toString();
			
			var results:Array = new Array();
			
			if (telefone != "")
			{
				var phoneNumber: RegExp = /([(][0-9]{2}[)][ ])?[0-9]{4,5}[-][0-9]{4}/;
				
				if (!phoneNumber.test(telefone))
				{
					results.push(new ValidationResult(true, null, "Erro", "Número de telefone inválido!"));
				}
			}
			
			return results;
			
		}
	}
}