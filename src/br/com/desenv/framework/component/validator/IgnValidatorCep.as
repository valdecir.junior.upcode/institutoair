package br.com.desenv.framework.component.validator
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;

	public class IgnValidatorCep extends Validator
	{
		public function IgnValidatorCep()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array 
		{
			var cep:String = value.toString();
			
			var results:Array = new Array();
			
			if (cep != "")
			{
				var cepNumber: RegExp = /[0-9]{5}[-][0-9]{3}/;
				
				if (!cepNumber.test(cep))
				{
					results.push(new ValidationResult(true, null, "Erro", "Formato de CEP inválido!"));
				}
			}
			
			return results;
			
		}
	}
}