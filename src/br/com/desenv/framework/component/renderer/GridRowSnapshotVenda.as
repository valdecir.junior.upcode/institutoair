package br.com.desenv.framework.component.renderer
{
	import mx.controls.Label;

	/**
	 * @author Lucas
	 */
	public class GridRowSnapshotVenda extends Label 
	{
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if(data.empresaFisica.id == 0x00)
				setStyle("fontWeight", "bold");
			else
				setStyle("fontWeight", "normal");
		}
	}	
}