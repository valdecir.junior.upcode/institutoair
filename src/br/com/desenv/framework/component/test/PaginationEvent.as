package br.com.desenv.framework.component.test
{
	import flash.events.Event;
	
	import br.com.desenv.framework.component.test.PaginationButton;

	public class PaginationEvent extends Event
	{
		public static const PAGE_NAV:String = "pageNav";
		
		public var pageButton:PaginationButton;
		
		public function PaginationEvent(type:String, page:PaginationButton, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			pageButton = page;
		}		
	}
}