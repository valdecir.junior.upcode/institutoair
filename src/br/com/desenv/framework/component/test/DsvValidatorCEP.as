package br.com.desenv.framework.component.test
{
	import mx.validators.ValidationResult;

	public class DsvValidatorCEP extends DsvNumberValidator
	{
		private var results:Array;
		
		public function DsvValidatorCEP()
		{
			super();
		}
		override protected function doValidation(value:Object):Array 
		{
			results = [];		
			if (value.toString()!="" || required==true)
			{
				var err:String = "";
				var tDesformatado:String = DsvUtilText.removeCaracteresBaseNumerica(value.toString());
	
				if (tDesformatado.length !=8)
				{
					err = "Campo deve possuir oito caracteres numéricos.";
					results.push(new ValidationResult(true, null, "NaN", err));
				}
				results = results.concat(validateNumber(tDesformatado, "number"));			
			}
			
			return results;
		}
	}
}