package br.com.desenv.framework.component.test
{
	import mx.formatters.Formatter;
	import mx.formatters.SwitchSymbolFormatter;

	public class DsvFormatterBaseNumerica extends Formatter
	{
		public var formatString : String = ""; //Formato da String
		public var numCharAc : int = 0; //numero de caracteres aceiros, se não definido não valida
		public var numCharPas : int = 0; //numero de caracteres difitados no componente
		private var _formato:int;
		
		public function DsvFormatterBaseNumerica()
		{
			super();
		}
		protected function formataMe(value:Object):String
		{
			
			if (value.toString() == "")
			{
				return "";
			}
			else
			{	
				//Verifica se a String de formato esta definida
				if (formatString=="")
				{
					error = "Padrão de formatação não foi definido.";
					return "";
				}			
				
				// Conta a quantidade de caracteres de mascara passados no formato
				this.numCharPas = 0;
				for(var i:int = 0; i<formatString.length; i++ ) 
				{
					if( formatString.charAt(i) == "#" ) 
					{
						this.numCharPas++;
					}
				}
				if (value.toString().length != this.numCharPas)
				{
					return value.toString();
				}
				else
				{
					// if the formatString and value are valid, format the number
					var dataFormatter:SwitchSymbolFormatter = new SwitchSymbolFormatter();
					return dataFormatter.formatValue(formatString, value);			
				}
			}
		}
		public function set formato(value:int):void
		{
			_formato = value;
		}
		public function get formato():int
		{
			return _formato;
		}
	}
	
}