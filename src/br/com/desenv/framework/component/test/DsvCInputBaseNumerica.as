package br.com.desenv.framework.component.test;
{
	import flash.events.Event;
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	
	import spark.components.TextInput;
	
	import br.com.desenv.framework.component.test.DsvNumberValidator;
	import br.com.desenv.framework.component.test.DsvUtilText;

	
	[Event(name="valueChange", type="flash.events.Event")]
	
	public class DsvCInputBaseNumerica extends TextInput 
	{
		private var _arVal:Array = null; 
		private var arrayValidators:Array;
		private var _formato:int;
		private var _required:Boolean=false;;
		private var _value:String="";
		
		public function DsvCInputBaseNumerica()
		{
			super();
			arrayValidators = new Array();
			this.addEventListener(Event.CHANGE, this.validaCaracteres, false, 0, true);
			this.addEventListener(FocusEvent.FOCUS_IN, this.removeCaracteres, false, 0, true);
			this.addEventListener(FocusEvent.FOCUS_OUT, this.formataCampo, false, 0, true);
			this.addEventListener(FlexEvent.CREATION_COMPLETE, this.criaValidators, false, 0, true);
			this.addEventListener(FlexEvent.CREATION_COMPLETE, this.adicionaValidators, false, 1, true);
			this.restrict = "0-9";
			this.setStyle("textAlign", "right");
		}
		public function set arVal(value:Array):void
		{
			_arVal = value;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			adicionaValidators(new FlexEvent(FlexEvent.CHANGE_END));
		}	
		protected function validaCaracteres(event:Event):void
		{
		}
		protected function removeCaracteres(event:FocusEvent):void
		{
			this.text = DsvUtilText.removeCaracteresBaseNumerica(this.text);
		}
		protected function trataCampo(event:FocusEvent):Boolean
		{			
			formataCampo(event);
			return true;
		}
		protected function validaCampo(event:FocusEvent):Boolean
		{
			return true;
		}
		protected function formataCampo(event:FocusEvent):void
		{
			
		}
		protected function getArVal():Array
		{
			return _arVal;
		}
		private function adicionaValidators(event:FlexEvent):void
		{
			var validator:DsvNumberValidator;
			for each(validator in arrayValidators)
			{
				validator.formato = this._formato;
				validator.required = this._required;
				if (_arVal!=null)
				{
					getArVal().push(validator);
				}
			}
		}
		protected function pushValidator(validator:DsvNumberValidator):void
		{
			arrayValidators.push(validator);
		}
		public function set formato(value:int):void
		{
			_formato = value;
		}		
		public function get formato():int
		{
			return _formato;
		}		
		public function set required(value:Boolean):void
		{
			_required = value;
		}		
		public function get required():Boolean
		{
			return _required;
		}
		protected function criaValidators(event:FlexEvent):void
		{		
		}
		public function get value():String {
			return DsvUtilText.removeCaracteresBaseNumerica(this.text);
		}
		
		//[Bindable(event="valueChange")]
		public function set value(value:String):void 
		{
			this._value = DsvUtilText.removeCaracteresBaseNumerica(value);
			this.text = this._value;
			this.dispatchEvent(new FocusEvent(FocusEvent.FOCUS_OUT));
		}
		
	}
}