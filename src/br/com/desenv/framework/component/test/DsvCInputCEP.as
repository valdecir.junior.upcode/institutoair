package br.com.desenv.framework.component.test
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	
	[Event(name="valueChange", type="flash.events.Event")]
	
	public class DsvCInputCEP extends DsvCInputBaseNumerica
	{		
		public function DsvCInputCEP()
		{
			super();
			this.addEventListener(FocusEvent.FOCUS_OUT, this.formataCampo, false, 0, true);
		}
		override protected function formataCampo(event:FocusEvent):void
		{
			var formater:DsvFormatterCEP  = new DsvFormatterCEP();
			this.text = formater.format(this.text);
		}
		
		override protected function criaValidators(event:FlexEvent):void
		{
			var validator:DsvValidatorCEP;
			validator = new DsvValidatorCEP();
			validator.source = this;
			validator.property="text"
			//validator.required=true;
			pushValidator(validator);
		}
		
		
	}
}
