package br.com.desenv.framework.component.test
{
	

	public class DsvFormatterCEP extends DsvFormatterBaseNumerica
	{
		public function DsvFormatterCEP()
		{
			super();
			this.formatString="#####-###";
			this.numCharAc = 8;
		}
		override public function format(value:Object):String 
		{
			return formataMe(value);
		}	
	}
}