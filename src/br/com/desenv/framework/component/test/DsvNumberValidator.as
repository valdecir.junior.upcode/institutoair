package br.com.desenv.framework.component.test
{
	import spark.validators.NumberValidator;
	
	
	public class DsvNumberValidator extends NumberValidator
	{
		protected var _formato:int;
		
		public function DsvNumberValidator()
		{
			super();
			invalidCharError = "Somente caracteres numéricos são aceitos.";
			parseError = "Somente caracteres numéricos são aceitos. Erro de conversão.";
		}
		
		public function set formato(value:int):void
		{
			_formato = value;
		}
		public function get formato():int
		{
			return _formato;
		}		
	}
}