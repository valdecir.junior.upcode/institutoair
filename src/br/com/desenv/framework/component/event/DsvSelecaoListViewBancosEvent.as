package br.com.desenv.framework.component.event
{
		import flash.events.Event;
		
		import br.com.desenv.nepalign.vo.Bancos;
		
		public class DsvSelecaoListViewBancosEvent extends Event
		{
			public static const BANCOS_SELECIONADO:String = "BANCOS_SELECIONADO";
			
			private var _selectedBancos:Bancos;
			
			public function get selectedBancos():Bancos
			{
				return _selectedBancos;
			}
			
			public function DsvSelecaoListViewBancosEvent(type:String, _selectedBancos:Bancos = null)
			{
				super(type, bubbles, cancelable);
				
				this._selectedBancos = _selectedBancos;
			}
		}
	}