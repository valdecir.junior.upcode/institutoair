package br.com.desenv.framework.component.event
{
	import flash.events.Event;
	
	public class ConfirmacaoExclusaoEvent extends Event
	{
		public static const RESPOSTA_SIM:String = "sim";
		public static const RESPOSTA_NAO:String = "nao";
		
		public var manage:Object;
		
		public function ConfirmacaoExclusaoEvent(type:String, manage:Object, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			this.manage = manage;
			super(type, bubbles, cancelable);
		}
	}
}