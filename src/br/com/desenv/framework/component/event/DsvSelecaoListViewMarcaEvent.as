package br.com.desenv.framework.component.event
{
		import flash.events.Event;
		
		import br.com.desenv.nepalign.vo.Marca;
		
		public class DsvSelecaoListViewMarcaEvent extends Event
		{
			public static const MARCA_SELECIONADA:String = "MARCA_SELECIONADA";
			
			private var _selectedMarca:Marca;
			
			public function get selectedMarca():Marca
			{
				return _selectedMarca;
			}
			
			public function DsvSelecaoListViewMarcaEvent(type:String, _selectedMarca:Marca = null)
			{
				super(type, bubbles, cancelable);
				
				this._selectedMarca = _selectedMarca;
			}
		}
	}