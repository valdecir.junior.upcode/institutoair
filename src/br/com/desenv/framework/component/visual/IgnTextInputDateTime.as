package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorDateTime;
	
	
	public class IgnTextInputDateTime extends IgnTextInput
	{
		
		
		
		public function IgnTextInputDateTime()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			//super.criar(event);
			this.restrict="\/: 0-9"; // somente nros e virgula
			
			//dd/MM/YYYY HH:MM
			this.maxChars = 16;
			this.width = 123;			
			
			var validadorDateTime: IgnValidatorDateTime = new IgnValidatorDateTime();
			validadorDateTime.source = this;
			validadorDateTime.property = "text";
			this._listaValidadorForm.push(validadorDateTime);
			this._listaValidadorInterno.push(validadorDateTime);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = this.text;
			this._valor = texto;
			var temSinalNegativo: Boolean = false;
			if (texto.charAt(0) == "-")
			{
				temSinalNegativo = true;
			}
			texto = this.text.replace(notNumber, "");
			
			// Formato dd/MM/YYYYY HH:MM
			if (texto.length == 12)
			{
				this.text = texto.substr(0, 2) + "/" + texto.substr(2, 2) + "/" + texto.substr(4, 4) + " " + texto.substr(8, 2) + ":" + texto.substr(10, 2);
			}
			// Formato dd/MM/YY HH:MM
			else if (texto.length == 10)
			{
				this.text = texto.substr(0, 2) + "/" + texto.substr(2, 2) + "/20" + texto.substr(4, 2) + " " + texto.substr(6, 2) + ":" + texto.substr(8, 2);
			}
			// Verifica se soma ou diminui 
			else if (texto.length >= 1 && texto.length < 4)
			{
				var dataAtual: Date = new Date();
				var variacaoDias: int = parseInt(texto);
				if (temSinalNegativo == false)
				{
					dataAtual.setDate(dataAtual.getDate() + variacaoDias);
				}
				else
				{
					dataAtual.setDate(dataAtual.getDate() - variacaoDias);
				}
				
				var dateFormatter: DateFormatter= new DateFormatter();
				dateFormatter.formatString = "DD/MM/YYYY HH:NN";
				this.text = dateFormatter.format(dataAtual);
			}
			
			
			
			Validator.validateAll(this._listaValidadorInterno);
			
			
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
	}
}


