package br.com.desenv.framework.component.visual
{
	import flash.display.Sprite;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.core.FlexGlobals;
	
	import br.com.desenv.nepalign.vo.SaldoEstoqueProduto;
	
	public class IgnDataGridAlerta extends DataGrid
	{
		public var rowBackgroundColorFunction:Function;
		public var corDestaque:uint = 0xD6F3D8;
		public var corPadrao:uint = 0xFFFFFF;
		public var corPadrao2:uint = 0xEFF3FA;
		public var idCorPadrao = 1;
		public var aceitaDestaque:Boolean = true;
		
		public function IgnDataGridAlerta()
		{
			super();
		}
		
		override protected function drawRowBackground(s:Sprite, rowIndex:int, y:Number, height:Number, color:uint, dataIndex:int):void
		{
			if(this.aceitaDestaque)
			{
				if((this.dataProvider as ArrayCollection).length > 0)
				{
					if(((this.dataProvider as ArrayCollection).length - 1) >= dataIndex)
					{
						if(rowBackgroundColorFunction != null && (dataProvider as ArrayCollection).getItemAt(dataIndex) != null)
						{
							var item:Object = (dataProvider as ArrayCollection).getItemAt(dataIndex);
							if(item != null)
							{
								if(item.destaque)
								{
									super.drawRowBackground(s,rowIndex,y,height,this.corDestaque,dataIndex);
								}
								else
								{
									if(idCorPadrao == 1)
									{
										super.drawRowBackground(s,rowIndex,y,height,this.corPadrao,dataIndex);
										this.idCorPadrao = 2;
									}
									else
									{
										super.drawRowBackground(s,rowIndex,y,height,this.corPadrao2,dataIndex);
										this.idCorPadrao = 1;
									}
								}
							}
							else
							{
								if(idCorPadrao == 1)
								{
									super.drawRowBackground(s,rowIndex,y,height,this.corPadrao,dataIndex);
									this.idCorPadrao = 2;
								}
								else
								{
									super.drawRowBackground(s,rowIndex,y,height,this.corPadrao2,dataIndex);
									this.idCorPadrao = 1;
								}
							}
						}
						else
						{
							if(idCorPadrao == 1)
							{
								super.drawRowBackground(s,rowIndex,y,height,this.corPadrao,dataIndex);
								this.idCorPadrao = 2;
							}
							else
							{
								super.drawRowBackground(s,rowIndex,y,height,this.corPadrao2,dataIndex);
								this.idCorPadrao = 1;
							}
						}
					}
					else
					{
						if(idCorPadrao == 1)
						{
							super.drawRowBackground(s,rowIndex,y,height,this.corPadrao,dataIndex);
							this.idCorPadrao = 2;
						}
						else
						{
							super.drawRowBackground(s,rowIndex,y,height,this.corPadrao2,dataIndex);
							this.idCorPadrao = 1;
						}
					}
				}
				else
				{
					if(idCorPadrao == 1)
					{
						super.drawRowBackground(s,rowIndex,y,height,this.corPadrao,dataIndex);
						this.idCorPadrao = 2;
					}
					else
					{
						super.drawRowBackground(s,rowIndex,y,height,this.corPadrao2,dataIndex);
						this.idCorPadrao = 1;
					}
				}
			}
			else
			{
				if(idCorPadrao == 1)
				{
					super.drawRowBackground(s,rowIndex,y,height,this.corPadrao,dataIndex);
					this.idCorPadrao = 2;
				}
				else
				{
					super.drawRowBackground(s,rowIndex,y,height,this.corPadrao2,dataIndex);
					this.idCorPadrao = 1;
				}
			}
		}
	}
}