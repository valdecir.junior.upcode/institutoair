package br.com.desenv.framework.component.visual
{
	import mx.containers.VBox;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	
	public class IgnTituloGrade extends VBox
	{
		
		private var tituloGradeA: TextInput;
		private var tituloGradeB: TextInput;
		private var tituloGradeC: TextInput;
		
		public function IgnTituloGrade()
		{
			super();
			this.setStyle("margin", 0);
			this.setStyle("horizontalGap", 0);
			this.setStyle("verticalGap", 0);
			this.setStyle("paddingBottom", 0);
			this.setStyle("paddingTop", 0);
			
			tituloGradeA = new TextInput();
			tituloGradeA.styleName = "textInputGradeA";
			tituloGradeA.editable = false;
			tituloGradeA.top=0;
			tituloGradeA.left=0;
			tituloGradeA.right=0;
			tituloGradeA.bottom=0;
			
			
			tituloGradeB = new TextInput();
			tituloGradeB.styleName = "textInputGradeB";
			tituloGradeB.editable = false;
			tituloGradeB.top=0;
			tituloGradeB.left=0;
			tituloGradeB.right=0;
			tituloGradeB.bottom=0;

			
			tituloGradeC = new TextInput();
			tituloGradeC.styleName = "textInputGradeC";
			tituloGradeC.editable = false;
			tituloGradeC.top=0;
			tituloGradeC.left=0;
			tituloGradeC.right=0;
			tituloGradeC.bottom=0;
			
			this.addChild(tituloGradeA);
			this.addChild(tituloGradeB);
			this.addChild(tituloGradeC);
			
		}
		
		override public function get data(): Object
		{
			return tituloGradeA.text + ":::" + tituloGradeB.text + ":::" + tituloGradeC.text;
		}
		
		override public function set data(value:Object):void
		{
			
			var titulo:String = (value as DataGridColumn).headerText;
			if(titulo == null || titulo == "") return;
			var results:Array = titulo.split(":::");
			tituloGradeA.text = results[0];
			tituloGradeB.text = results[1];
			tituloGradeC.text = results[2];
		}		
	}
}