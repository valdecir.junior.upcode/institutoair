package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorAnoMesDia;
	import br.com.desenv.nepalign.vo.AnoMesDia;
	
	
	public class IgnTextInputAnoMesDia extends IgnTextInput
	{
		
		private var validadorAnoMesDia: IgnValidatorAnoMesDia = new IgnValidatorAnoMesDia();
		
		private var _dataPassada: Date;
		
		public function IgnTextInputAnoMesDia()
		{
			super();
		}
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			super.criar(event);
			//this.restrict="0-9amdAMD"; // somente nros a m e d
			this.maxChars=15; 	 // maximo chars 80320-050 	
			this.width = 100;
			
			var validadorAnoMesDia: IgnValidatorAnoMesDia = new IgnValidatorAnoMesDia();
			validadorAnoMesDia.source = this;
			validadorAnoMesDia.property="text";
			this._listaValidadorForm.push(validadorAnoMesDia);
			this._listaValidadorInterno.push(validadorAnoMesDia);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			// Formata
			
			validadorAnoMesDia.enabled = true;
			var listaErro: Array = Validator.validateAll(this._listaValidadorInterno);
			
			var anoMesDia:AnoMesDia = new AnoMesDia();
			
			if (listaErro.length == 0 && this.text != "") 
			{
				this._valor = this.text;
				
				var texto:String = this.text;
				var encontrou:Boolean = false;
				
				//testa apenas ano com a ou não
				
				// ano,mes,dia
				var regex:RegExp = /([0-9]+)[a|A]([0-9]+)[m|M]([0-9]+)[d|D]/;
				var match:Array = regex.exec(texto);
					
				if (match != null && match.length == 4)
				{
					anoMesDia.ano = parseInt(match[1]);
					anoMesDia.mes = parseInt(match[2]);
					anoMesDia.dia = parseInt(match[3]);
					encontrou = true;
				}
				
				
				if (encontrou == false)
				{
					// Apenas mes e dia
					regex = /([0-9]+)[m|M]([0-9]+)[d|D]/;
					match = regex.exec(texto);
					
					if (match != null && match.length == 3)
					{
						anoMesDia.mes = parseInt(match[1]);
						anoMesDia.dia = parseInt(match[2]);
						encontrou = true;
					}
				}		
				
				if (encontrou == false)
				{
					// Apenas Ano e dia
					regex = /([0-9]+)[a|A]([0-9]+)[d|D]/;
					match = regex.exec(texto);
					
					if (match != null && match.length == 3)
					{
						anoMesDia.ano = parseInt(match[1]);
						anoMesDia.dia = parseInt(match[2]);
						encontrou = true;
					}
				}
				
				if (encontrou == false)
				{
					// Apenas Ano e Mes
					regex = /([0-9]+)[a|A]([0-9]+)[m|M]/;
					match = regex.exec(texto);
					
					if (match != null && match.length == 3)
					{
						anoMesDia.ano = parseInt(match[1]);
						anoMesDia.mes = parseInt(match[2]);
						encontrou = true;
					}
				}
				
				if (encontrou == false)
				{
					// Apenas Mes
					regex = /([0-9]+)[d|D]/;
					match = regex.exec(texto);
					
					if (match != null && match.length == 2)
					{
						anoMesDia.dia = parseInt(match[1]);
						encontrou = true;
					}
				}
				
				if (encontrou == false)
				{
					// Apenas Mes
					regex = /([0-9]+)[m|M]/;
					match = regex.exec(texto);
					
					if (match != null && match.length == 2)
					{
						anoMesDia.mes = parseInt(match[1]);
						encontrou = true;
					}
				}
				
				if (encontrou == false)
				{
					regex = /([0-9]+)[a|A]/;
					match = regex.exec(texto);
				
					if (match != null && match.length == 2)
					{
						anoMesDia.ano = parseInt(match[1]);
						encontrou = true;
					}
				}	
				
				


				
				
				
				
				

				
				
				
				this._dataPassada = AnoMesDia.subtrairAnoMesDiaDaData(new Date(), anoMesDia);
				
				this.text = anoMesDia.ano + "a" + anoMesDia.mes + "m" + anoMesDia.dia + "d";
			}
			else
			{
				this._valor = null;
				this._dataPassada = null;
				this.text = "";
			}
			
		}
		
		
		
		
		public function set dataPassada(dataPassada: Date): void
		{
			
			this._dataPassada = dataPassada;
			
			
			if (dataPassada != null)
			{
				var anoMesDia:AnoMesDia = AnoMesDia.calcularDiferencaDataAtual(dataPassada);
				this.text = anoMesDia.ano + "a" + anoMesDia.mes + "m" + anoMesDia.dia + "d";
			}	
		}
		
		public function get dataPassada(): Date
		{
			return this._dataPassada;
		}
		
		
		
		
	}
	
	
}


