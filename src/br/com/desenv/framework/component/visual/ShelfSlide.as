package br.com.desenv.framework.component.visual
{	
	import flash.events.KeyboardEvent;
	
	import br.com.desenv.framework.component.renderer.DisplayShelf;

	/**
	 * @author Acer
	 */
	public class ShelfSlide extends DisplayShelf
	{	
		protected static const ANGLE_MAX_VALUE:Number = 90;
		protected static const ANGLE_MINIMUM_VALUE:Number = 5;
		protected static const POP_MAX_VALUE:Number = 1;
		protected static const POP_MINIMUM_VALUE:Number = 0.43;
		
		public function ShelfSlide()
		{
			super();
			super.enableHistory = false;
			this.setWidth = "100%";
		}
		
		public function set imageAngle(value:Number):void
		{
			if(!(value < ANGLE_MINIMUM_VALUE) && !(value > ANGLE_MAX_VALUE))
				super.angle = value;
		}
		
		public function get imageAngle():Number
		{
			return super.angle;
		}
		
		public function set popOut(value:Number):void
		{
			if(!(value < POP_MINIMUM_VALUE) && !(value > POP_MAX_VALUE))
				super.popout = value;
		}
		
		public function get popOut():Number
		{
			return super.popout;
		}
		
		public function updateDataProvider(value:Array):void
		{
			super.dataProvider = value;
		}
		
		public function get getDataProvider():Object
		{
			return super.dataProvider;
		}
		
		public function set setWidth(value:Object):void
		{
			super.width = value as Number;
		}
		
		override protected function keyDownHandler(event:KeyboardEvent):void
		{
			super.keyDownHandler(event);
		}
		
		public function keyDownHelper(event:KeyboardEvent):void
		{
			this.keyDownHandler(event);
		}
	}	
}