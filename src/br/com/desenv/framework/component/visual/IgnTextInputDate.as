package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.controls.DateField;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.validators.DateValidator;
	import mx.validators.Validator;
	

	
	
	public class IgnTextInputDate extends IgnTextInput
	{
		private var validadorData: DateValidator = new DateValidator();
		protected var _mesAno: Boolean;
		
		public function IgnTextInputDate()
		{
			super();
		}
		
		public function get data():Date
		{
			return DateField.stringToDate(text, "DD/MM/YYYY");
		}
		
		public function set mesAno(mesAno:Boolean):void
		{
			this._mesAno = mesAno;
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			//super.criar(event);
			this.restrict="\-0-9"; // somente nros e virgula
			
			validadorData.source = this;
			validadorData.property = "text";

			if(_mesAno)
				this.maxChars = 7;
			else
				this.maxChars = 10;
			
			this.width = 77;
			
			if (super._obrigatorio)
			{
				this.styleName = "textInputObrigatorio";
				validadorData.required = true;
			}
			else
			{
				validadorData.required = false;
			}
			if(_mesAno)
				validadorData.inputFormat = "mm/yyyy";
			else
				validadorData.inputFormat = "dd/mm/yyyy";
			
			this._listaValidadorForm.push(validadorData);
			this._listaValidadorInterno.push(validadorData);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = this.text;
			var temSinalNegativo: Boolean = false;
			if (texto.charAt(0) == "-")
			{
				temSinalNegativo = true;
			}
			texto = this.text.replace(notNumber, "");
			
			// Formato dd/MM/YYYYY
			if(!_mesAno)
			{
				if (texto.length == 8)
				{
					this.text = texto.substr(0, 2) + "/" + texto.substr(2, 2) + "/" + texto.substr(4, 4);
				}
					// Formato dd/MM/YY
				else if (texto.length == 6)
				{
					this.text = texto.substr(0, 2) + "/" + texto.substr(2, 2) + "/20" + texto.substr(4, 2);
				}
				else if (texto.length >= 1 && texto.length < 4)
				{
					var dataAtual: Date = new Date();
					var variacaoDias: int = parseInt(texto);
					if (temSinalNegativo == false)
					{
						dataAtual.setDate(dataAtual.getDate() + variacaoDias);
					}
					else
					{
						dataAtual.setDate(dataAtual.getDate() - variacaoDias);
					}
					
					var dateFormatter: DateFormatter= new DateFormatter();
					dateFormatter.formatString = "DD/MM/YYYY";
					this.text = dateFormatter.format(dataAtual);
				}
				validadorData.enabled = true;
				var listaErro: Array = Validator.validateAll(this._listaValidadorInterno);
				
				if (listaErro.length == 0 && this.text != "") 
				{
					this._valor = this.text;
				}
				else
				{
					this._valor = null;
				}
			}
			else
			{
				if(texto.length == 4)
				{
					this.text = text.substr(0,2) + "/20" + texto.substr(2,4);
				}
				else if (texto.length == 6)
				{
					this.text = texto.substr(0, 2) + "/" + texto.substr(2, 6);
				}
				else if(texto.length == 7)
				{
					this.text = texto.substr(0, 2) + texto.substr(3, 7);
				}
					
					// Formato (41) 9999-9999 (ddd com dois digitos) 
				else if (texto.length >= 1 && texto.length < 4)
				{
					var dataAtual: Date = new Date();
					var variacaoDias: int = parseInt(texto);
					if (temSinalNegativo == false)
					{
						dataAtual.setDate(dataAtual.getDate() + variacaoDias);
					}
					else
					{
						dataAtual.setDate(dataAtual.getDate() - variacaoDias);
					}
					
					var dateFormatter: DateFormatter= new DateFormatter();
					dateFormatter.formatString = "MM/YYYY";
					this.text = dateFormatter.format(dataAtual);
				}
				validadorData.enabled = true;
				var listaErro: Array = Validator.validateAll(this._listaValidadorInterno);
				
				if (listaErro.length == 0 && this.text != "") 
				{
					this._valor = this.text;
				}
				else
				{
					this._valor = null;
				}
			}
			
			
			
			
			
			
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			validadorData.enabled = false;
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
	}
}

