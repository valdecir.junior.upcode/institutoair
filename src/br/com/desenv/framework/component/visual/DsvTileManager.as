package br.com.desenv.framework.component.visual
{
	import flash.events.MouseEvent;
	import flash.net.dns.AAAARecord;
	import flash.text.Font;
	import flash.text.TextFormat;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Grid;
	import mx.containers.GridItem;
	import mx.containers.GridRow;
	import mx.containers.VBox;
	import mx.containers.ViewStack;
	import mx.controls.Text;
	import mx.controls.TextArea;
	import mx.core.FlexGlobals;
	import mx.core.UIComponent;
	import mx.effects.Fade;
	import mx.rpc.events.ResultEvent;
	
	import spark.components.Label;

	/**
	 * @author Lucas
	 */
	public class DsvTileManager extends UIComponent
	{
		protected static const tileMaxTextLength:Number = 230;
		
		private var _tileList:Array;
		private var _tileGrid:Grid;
		private var _tileRowList:Array;
		private var _tileRows:Number = 3;
		private var _stackContent:ViewStack;
		private var _tileListWindowReference:Array;
		private var _horizontalTileCount:Number = 5;
		
		private var _tileContextMenu:ContextMenu;
		
		private var _tileColorList:ArrayCollection;
		
		public function set tileList(value:Array):void
		{
			this._tileList = value;
			this._tileList.length = this.menuListLength;
		}
		
		public function get tileList():Array
		{
			return this._tileList;
		}
		
		public function set tileGrid(value:Grid):void
		{
			this._tileGrid = value;
		}
		
		public function get tileGrid():Grid
		{
			return this._tileGrid;
		}
		
		public function set tileRows(value:Number):void
		{
			this._tileRows = value;
		}
		
		public function get tileRows():Number
		{
			return this._tileRows;
		}
		
		public function set horizontalTileCount(value:Number):void
		{
			this._horizontalTileCount = value;
		}
		
		public function get horizontalTileCount():Number
		{
			return this._horizontalTileCount;
		}
		
		public function set tileRowList(value:Array):void
		{
			this._tileRowList = value;
		}
		
		public function get tileRowList():Array
		{
			return this._tileRowList;
		}
		
		public function set tileListWindowReference(value:Array):void
		{
			this._tileListWindowReference = value;
		}
		
		public function get tileListWindowReference():Array
		{
			return this._tileListWindowReference;
		}
		
		public function set stackContent(value:ViewStack):void
		{
			this._stackContent = value;
		}
		
		public function get stackContent():ViewStack
		{
			return this._stackContent;
		}
		
		public function set tileColorList(value:ArrayCollection):void
		{
			this._tileColorList = value;
		}
		
		public function get tileColorList():ArrayCollection
		{
			return this._tileColorList;
		}
		
		private function get tileContextMenu():ContextMenu
		{
			return this._tileContextMenu;
		}
		
		/**
		 * Get the number of registred tiles.
		 */
		protected function get menuListLength():Number
		{
			var count:Number = 0;
			
			for (var key:* in this.tileList)
			{
				count = count + 1;
			}
			
			return count;
		}
		
		/**
		 * @default
		 */
		public function DsvTileManager()
		{
			this.tileRowList = new Array();
			this.tileListWindowReference = new Array();
			
			this.tileColorList = new ArrayCollection();
			
			//var cmi1:ContextMenuItem = new ContextMenuItem("Desfixar do inínio", true);
			//cmi1.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, contextMenuItem_menuItemSelect);
			
			//this._tileContextMenu = new ContextMenu();
			//this._tileContextMenu.hideBuiltInItems();
			//this._tileContextMenu.customItems = [cmi1];
			//cm1.addEventListener(ContextMenuEvent.MENU_SELECT, contextMenu_menuSelect);
		}
		
		/**
		 * Create a error message in screen.
		 */
		protected function errorMessage(value:String):void
		{
			this.tileGrid.y = 0;
			this.stackContent.y = 0;
			
			this.tileGrid.x = 0;
			this.stackContent.x = 0;
			
			this.stackContent.width = 900;
			this.stackContent.maxWidth = 900;
			this.tileGrid.width = 900;
			this.tileGrid.maxWidth = 900;
			
			this.tileGrid.removeAllElements();
			var gridRow:GridRow = new GridRow();
			gridRow.width = 900;
			gridRow.percentHeight = 100;
			gridRow.setStyle("horizontalAlign", "center");
			gridRow.setStyle("verticalAlign", "middle");
			
			var gridItem:GridItem = new GridItem();
			gridItem.setStyle("horizontalAlign", "center");
			gridItem.setStyle("verticalAlign", "middle");
			
			var labelText:Label = new Label();
			labelText.text = value;
			labelText.setStyle("fontSize", 20);
			
			gridItem.addElement(labelText);
			gridRow.addElement(gridItem);

			this.tileGrid.addElement(gridRow);
		}

		/**
		 * Generate tiles according parameters
		 * Validations in this method is extremally important. Don't change.
		 */
		public function generateTiles():void
		{
			try
			{
				this.tileGrid.removeAllElements();
				
				if(this.tileGrid == null)
					throw new Error("Invalid TileGrid");
				if(this.tileList == null)
					throw new Error("Invalid TileList");
				if(this.stackContent == null)
					throw new Error("View Stack cannot be null");
				
				var index:Number = 0;
				
				if(this._tileRows < 1)
					throw new Error("Número de linhas inválido.");
				
				if(Number(this.menuListLength / this._horizontalTileCount) > this._tileRows)
				{
					throw new Error("Número de menu inválidos");
				}
				else
				{
					for(var rowIndex:int = 1; rowIndex <= this._tileRows; rowIndex++)
					{
						var gridRow:GridRow = new GridRow();
						gridRow.percentWidth = 100;
						gridRow.setStyle("horizontalAlign", "center");
						gridRow.id = "gridRow" + rowIndex.toString();
						this.tileRowList[rowIndex.toString()] = 0;
						
						this.tileGrid.addElement(gridRow);
					}
					
					if(this.tileList.length < 1)
						throw new Error("Lista de menu inválida.");
					
					for (var tileIndex:int = 0; tileIndex < this.tileList.length; tileIndex++)
					{
						var tempText:Text = new Text();
						tempText.regenerateStyleCache(false);
						var tileTextWidth:int = tempText.measureText(tileList[tileIndex].labelField).width;
						
						if(tileTextWidth > (tileMaxTextLength * 3))
							throw new Error("Invalid text for tile");
						
						
						index++;
						
						var listaBuffer:ArrayCollection = tileList[tileIndex].info as ArrayCollection;
						
						var gridItem:GridItem = new GridItem();
						
						gridItem.id = "gridItemTile" + tileIndex.toString();
						gridItem.addEventListener(MouseEvent.MOUSE_OVER, this.itemGrid_mouseOverHandler);
						gridItem.addEventListener(MouseEvent.MOUSE_OUT, this.itemGrid_mouseOutHandler);
						gridItem.addEventListener(MouseEvent.CLICK, this.clickHandler);
						gridItem.setStyle("backgroundColor", listaBuffer.getItemAt(0).toString());
						//gridItem.borderMetrics.left = 4;
						//gridItem.borderMetrics.right = 4;
						gridItem.percentHeight = 100;
						gridItem.width = 200;
						gridItem.maxWidth = 200;
						gridItem.horizontalScrollPolicy = "off";
						gridItem.verticalScrollPolicy = "off";
						
						var vBox:VBox = new VBox();
						vBox.height = 120;
						vBox.width = 200;
						vBox.maxWidth = 200;
						vBox.setStyle("verticalAlign", "middle");
						vBox.horizontalScrollPolicy = "off";
						vBox.verticalScrollPolicy = "off";
						
						var textArea:TextArea = new TextArea();
						textArea.setStyle("contentBackgroundColor", listaBuffer.getItemAt(0).toString());
						textArea.horizontalScrollPolicy = "off";
						textArea.verticalScrollPolicy = "off";
						textArea.setStyle("borderVisible", false);
						textArea.setStyle("color", "0xFFFFFF");
						textArea.setStyle("fontWeight", "bold");
						textArea.setStyle("fontSize", "15");
						textArea.setStyle("textAlign", "center");
						textArea.setStyle("fontFamily", "Segoe UI");
						textArea.setStyle("selectable", false);
						textArea.focusEnabled = false;
						textArea.editable = false;
						textArea.percentWidth = 100;
						textArea.percentHeight = 50;
						textArea.text = tileList[tileIndex].labelField;
						
						vBox.addElement(textArea);
						gridItem.addElement(vBox);
						gridItem.contextMenu = this.tileContextMenu;
						
						for (var rowKey:String in this.tileRowList)
						{
							if(parseInt(this.tileRowList[rowKey]) < this._horizontalTileCount)
							{
								var row:Object = this.tileGrid.getElementAt((parseInt(rowKey.toString()) - 1));
								(row as GridRow).addElementAt(gridItem, parseInt(this.tileRowList[rowKey]));
								this.tileRowList[rowKey] = parseInt(this.tileRowList[rowKey]) + 1;
								break;
							}
						}
						
						this.tileListWindowReference[gridItem.id] = listaBuffer.getItemAt(1);
					}
					
				}
			}
			catch(e:Error)
			{
				//this.errorMessage(e.message);
			}
			finally
			{
				if(this.stackContent != null)
				{
					var usedRowsCount:Number = Math.ceil(this.menuListLength / this.horizontalTileCount);
					
					var usedHeight = usedRowsCount * 100;
					
					var y:Number = (this.stackContent.height - usedHeight) / 2;
					this.tileGrid.y = (y - (usedHeight / usedRowsCount));
				}
			}
		}
		
		protected function itemGrid_mouseOverHandler(event:MouseEvent):void
		{
			event.currentTarget.setStyle("borderColor", "#000000");
			event.currentTarget.setStyle("borderStyle", "solid");
			event.currentTarget.setStyle("borderVisible", true);
		}
		
		protected function itemGrid_mouseOutHandler(event:MouseEvent):void
		{
			event.currentTarget.setStyle("borderVisible", false);
		}
		
		protected function clickHandler(event:MouseEvent):void
		{
			for (var key:* in this.tileListWindowReference)
			{
				if(key == event.currentTarget.id)
				{
					var windowClass:Class = this.tileListWindowReference[key] as Class;
					var window:Object = new windowClass();
					this.dispatchEvent(new ResultEvent("TileClick", false, true, window));
				}
			}
		}
		
		public function randomColor():String
		{
			try
			{
				return this.tileColorList.getItemAt(Math.floor(Math.random() * (this.tileColorList.length - 0 + 1)) + 0) as String;
			}
			catch(e:Error)
			{
				trace(e);
				return null;
			}
			return null;
		}
	}	
}