package br.com.desenv.framework.component.visual
{
	import com.acm.ComboCheck;
	
	import mx.events.ItemClickEvent;
   
	public class IgnComboCheck extends ComboCheck
	{
		public function IgnComboCheck()
		{
			super();
		}
		
		override public function selectAll():void
		{
			super.selectAll();
		}
		
		override public function deselectAll():void
		{
			super.deselectAll();
		}
		
		public function changeItemState(propertie:String, state:Boolean, applyOnlyToSelecteds:Boolean):void
		{ 
			var array:Array = new Array();
			
			for(var i:int = 0; i < super.dataProvider.length; i++)
			{ 
				var obj:Object = super.dataProvider.getItemAt(i);
				
				if(applyOnlyToSelecteds)
				{
					for each(var item:* in super.selectedItems)
					{
						if(obj.value == item.value)
							obj[propertie] = state;
					}	
				}
				else
				{
					obj[propertie] = state;	
				}
				
				array[i] = obj;
			}
			
			for each(var key:* in array)
			{
				var index:int = key as int;
				
				super.dataProvider.removeItemAt(index);
				super.dataProvider.addItemAt(array[index], index);
			}
			super.comboRefreshDropDown();
		}
	}
}

