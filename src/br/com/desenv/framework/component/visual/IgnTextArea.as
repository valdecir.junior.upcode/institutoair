package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import spark.components.TextArea;
	
	public class IgnTextArea extends TextArea
	{
		protected var _obrigatorio: Boolean;
		protected var _valor: String;
		protected var _listaValidadorForm: Array = new Array();
		protected var _listaValidadorInterno: Array = new Array();
		
		public function IgnTextArea()
		{
			super();
			
			// events listener
			this.addEventListener(FlexEvent.CREATION_COMPLETE, criar);
			this.addEventListener(FocusEvent.FOCUS_OUT, sair);
			this.addEventListener(FocusEvent.FOCUS_IN, entrar);
		}
		
		public function set obrigatorio(obrigatorio: Boolean): void
		{
			this._obrigatorio = obrigatorio;
		}
		
		public function get obrigatorio(): Boolean
		{
			return this._obrigatorio;
		}
		
		public function set listaValidadorForm(listaValidadorForm: Array): void
		{
			this._listaValidadorForm = listaValidadorForm;
		}
		
		
		public function get valor(): String
		{
			return this._valor;
		}
		
		// On Create Complete
		protected function criar(event: FlexEvent) : void 
		{
			this.errorString = "";
			
			if (this._obrigatorio == true)
			{
				this.styleName = "textAreaObrigatorio";
				var validadorObrigatorio: Validator = new Validator();
				validadorObrigatorio.source = this;
				validadorObrigatorio.property = "text";
				validadorObrigatorio.required = true;
				this._listaValidadorForm.push(validadorObrigatorio);
				this._listaValidadorInterno.push(validadorObrigatorio);
			}			
		}
		
		// on Focus Out
		protected function sair(event: FocusEvent): void 
		{
			this._valor = this.text;
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		// on Focus In
		protected function entrar(event: FocusEvent): void 
		{
			
		}
	}
}