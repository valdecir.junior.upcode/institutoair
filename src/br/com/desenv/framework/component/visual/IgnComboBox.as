package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import mx.core.mx_internal;
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import spark.components.ComboBox;
	
	public class IgnComboBox extends ComboBox
	{
		protected var _obrigatorio: Boolean;
		protected var _listaValidadorForm: Array = new Array();
		protected var _listaValidadorInterno: Array = new Array();
		
		public function IgnComboBox()
		{
			super();
			
			// events listener
			this.addEventListener(FlexEvent.CREATION_COMPLETE, criar);
			this.addEventListener(FocusEvent.FOCUS_OUT, sair);
		}
		
		public function set obrigatorio(obrigatorio: Boolean): void
		{
			this._obrigatorio = obrigatorio;
		}
		
		public function get obrigatorio(): Boolean
		{
			return this._obrigatorio;
		}
		
		public function set listaValidadorForm(listaValidadorForm: Array): void
		{
			this._listaValidadorForm = listaValidadorForm;
		}
		
		
	
		
		// On Create Complete
		protected function criar(event: FlexEvent) : void 
		{
			this.errorString = "";
			
			if (this._obrigatorio == true)
			{
				this.styleName = "comboBoxObrigatorio";
				var validadorObrigatorio: Validator = new Validator();
				validadorObrigatorio.source = this;
				validadorObrigatorio.property = "selectedItem";
				validadorObrigatorio.required = true;
				this._listaValidadorForm.push(validadorObrigatorio);
				this._listaValidadorInterno.push(validadorObrigatorio);
			}			
		}
		
		// on Focus Out
		protected function sair(event: FocusEvent): void 
		{
		
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		// on Focus In
		protected function entrar(event: FocusEvent): void 
		{
			
		}
	}
}