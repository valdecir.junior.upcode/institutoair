package br.com.desenv.framework.component.visual
{
	import mx.containers.Grid;
	import mx.containers.GridItem;
	import mx.containers.GridRow;
	import mx.containers.VBox;
	import mx.controls.TextInput;
	import mx.core.IVisualElement;
	import mx.events.FlexEvent;
	
	import br.com.desenv.nepalign.vo.EmpresaFisica;
	import br.com.desenv.nepalign.vo.LancamentoContaPagarRateio;
	import br.com.desenv.nepalign.vo.PreLancamentoContaPagarRateio;
	
	/**
	 * @author lleit
	 */
	public class DsvLancamentoContaPagarRateio extends Grid
	{
		private var mainContent:GridRow;
		private var headerContent:GridItem;
		
		private const BOX_HEIGHT:uint 	= 23;
		private const BOX_WIDTH:uint 	= 40;
		
		/**
		 * Indica se o componente é editável
		 */
		[Bindable]
		private var _readOnly:Boolean = false;
		
		public function get readOnly():Boolean
		{
			return _readOnly;
		}
		
		public function set readOnly(value:Boolean):void
		{
			_readOnly = value;
		}
		
		public function DsvLancamentoContaPagarRateio()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteEventHandler);
		}
		
		protected function creationCompleteEventHandler(event:FlexEvent):void
		{
			removeEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteEventHandler);
			
			setStyle("styleName", "gridCompactadoHorizontal");
			setStyle("width", "100%");
			
			mainContent = new GridRow();
			mainContent.id = "contentEmpresasRateio";
			mainContent.setStyle("styleName", "gridCompactadoHorizontal");
			
			addHeader();
		} 

		/**
		 * Cria o Header EMP, %
		 * @langversion ActionScript 3.0
		 */
		protected function addHeader():void
		{
			headerContent = new GridItem();
			headerContent.id 	= "header";
			headerContent.width = BOX_WIDTH;
			
			var vBox:VBox = new VBox();
			vBox.setStyle("width", BOX_HEIGHT.toString());
			vBox.setStyle("styleName", "gridCompactado");
			
			var textInputHeaderEmp:TextInput = new TextInput();
			textInputHeaderEmp.id 			= "textInputColumnHeader";
			textInputHeaderEmp.text 		= "EMP";
			textInputHeaderEmp.height 		= BOX_HEIGHT;
			textInputHeaderEmp.width		= BOX_WIDTH;
			textInputHeaderEmp.editable		= false;
			textInputHeaderEmp.focusEnabled = false;
			textInputHeaderEmp.setStyle("textAlign", "center");
			textInputHeaderEmp.setStyle("fontWeight", "bold");
			
			var textInputHeaderPercent:TextInput = new TextInput();
			textInputHeaderPercent.id 			= "textInputColumnHeader";
			textInputHeaderPercent.text 		= "%";
			textInputHeaderPercent.height 		= BOX_HEIGHT;
			textInputHeaderPercent.width 		= BOX_WIDTH;
			textInputHeaderPercent.editable 	= false;
			textInputHeaderPercent.focusEnabled = false;
			textInputHeaderPercent.setStyle("textAlign", "center");
			
			vBox.addElement(textInputHeaderEmp);
			vBox.addElement(textInputHeaderPercent);
			
			headerContent.addElement(vBox);
			mainContent.addElement(headerContent);
			
			addElement(mainContent);
		}
		
		/**
		 * Verifica se a empresa está no rateio
		 * @langversion ActionScript 3.0
		 */
		public function estaNoRateio(empresaFisica:EmpresaFisica):Boolean
		{
			for (var i:int = 0x00; i < mainContent.numElements; i++)
			{
				if((mainContent.getElementAt(i) as GridItem).id == new String(empresaFisica.id))
					return true;
			}
			
			return false;
		}
		
		/**
		 * Adiciona informações do rateio já realizado
		 * @langversion ActionScript 3.0
		 */
		public function adicionarRateioPreLancamento(preLancamentoContaPagarRateio:PreLancamentoContaPagarRateio):void
		{ 
			if(!mainContent) return; // se o mainContent ainda não estiver construído, cancela
			if(estaNoRateio(preLancamentoContaPagarRateio.empresaFisica)) return;  // se a empresa já estiver no rateio, cancela
			
			var gridItem:GridItem = new GridItem();
			gridItem.id = new String(preLancamentoContaPagarRateio.empresaFisica.id);
			gridItem.width = BOX_WIDTH;
			
			var ignEmpresaRateio:IgnEmpresaRateio = new IgnEmpresaRateio();
			ignEmpresaRateio.id = "ignEmpresaRateio" + new String(preLancamentoContaPagarRateio.empresaFisica.id);
			ignEmpresaRateio.empresaFisica = preLancamentoContaPagarRateio.empresaFisica;
			ignEmpresaRateio.percentual = preLancamentoContaPagarRateio.porcentagemRateio;
			ignEmpresaRateio.readOnly = _readOnly;
			
			gridItem.addChild(ignEmpresaRateio);
			mainContent.addElement(gridItem);
		}
		
		/**
		 * Adiciona informações do rateio já realizado
		 * @langversion ActionScript 3.0
		 */
		public function adicionarRateio(lancamentoContaPagarRateio:LancamentoContaPagarRateio):void
		{ 
			if(!mainContent) return; // se o mainContent ainda não estiver construído, cancela
			if(estaNoRateio(lancamentoContaPagarRateio.empresaFisica)) return;  // se a empresa já estiver no rateio, cancela
			
			var gridItem:GridItem = new GridItem();
			gridItem.id = new String(lancamentoContaPagarRateio.empresaFisica.id);
			gridItem.width = BOX_WIDTH;
			
			var ignEmpresaRateio:IgnEmpresaRateio = new IgnEmpresaRateio();
			ignEmpresaRateio.id = "ignEmpresaRateio" + new String(lancamentoContaPagarRateio.empresaFisica.id);
			ignEmpresaRateio.empresaFisica = lancamentoContaPagarRateio.empresaFisica;
			ignEmpresaRateio.percentual = lancamentoContaPagarRateio.porcentagemRateio;
			ignEmpresaRateio.readOnly = _readOnly;
			
			gridItem.addChild(ignEmpresaRateio);
			mainContent.addElement(gridItem);
		}
		
		/**
		 * Adiciona uma empresa ao Rateio
		 * @langversion ActionScript 3.0
		 */
		public function adicionarRateioEmpresaFisica(empresaFisica:EmpresaFisica):void
		{ 
			if(!mainContent) return; // se o mainContent ainda não estiver construído, cancela
			if(estaNoRateio(empresaFisica)) return; // se a empresa já estiver no rateio, cancela
			
			var gridItem:GridItem = new GridItem();
			gridItem.id = new String(empresaFisica.id);
			gridItem.width = BOX_WIDTH;
			
			var ignEmpresaRateio:IgnEmpresaRateio = new IgnEmpresaRateio();
			ignEmpresaRateio.id = "ignEmpresaRateio" + new String(empresaFisica.id);
			ignEmpresaRateio.empresaFisica = empresaFisica;
			ignEmpresaRateio.readOnly = _readOnly;
			
			gridItem.addChild(ignEmpresaRateio);
			mainContent.addElement(gridItem);
		}
		
		/**
		 * Remove uma empresa do Rateio
		 * @param empresaFisica empresaFisica a ser removida. Caso seja null, remove a ultima empresa inserida no rateio
		 * @param todas true para remover todas as empresas do rateio
		 * @langversion ActionScript 3.0
		 */
		public function removerRateioEmpresaFisica(empresaFisica:EmpresaFisica, todas:Boolean = false):void
		{
			if(!mainContent) return; // se o mainContent ainda não estiver construído, cancela
			if(todas)
			{
				mainContent.removeAllElements();
				mainContent.addElement(headerContent);
				return;
			}
			
			if(empresaFisica == null)
				mainContent.removeElementAt(mainContent.numElements - 0x01);
			else
			{
				if(!estaNoRateio(empresaFisica)) return; // se a empresa para remoção não estiver no rateio, despreza.
				
				var elementToRemove:IVisualElement = null;
				
				for (var i:int = 0x00; i < mainContent.numElements; i++)
				{
					if((mainContent.getElementAt(i) as GridItem).id == new String(empresaFisica.id))
						elementToRemove = mainContent.getElementAt(i)
				}
				
				if(elementToRemove)
					mainContent.removeElement(elementToRemove);
			}
		}
		
		/**
		 * Recupera o percentual da empresaFisica especificada
		 * @langversion
		 */
		public function getPercentual(empresaFisica:EmpresaFisica):Number
		{
			for (var i:int = 0x00; i < mainContent.numElements; i++)
			{
				if((mainContent.getElementAt(i) as GridItem).id == new String(empresaFisica.id))
					return ((mainContent.getElementAt(i) as GridItem).getElementAt(0) as IgnEmpresaRateio).percentual;
			}
			return NaN;
		}
	}	
}