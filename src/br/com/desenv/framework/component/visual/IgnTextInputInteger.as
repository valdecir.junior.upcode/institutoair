package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	
	public class IgnTextInputInteger extends IgnTextInput
	{
		public function IgnTextInputInteger()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			super.criar(event);
			this.restrict="0-9"; // somente nros
		}
		
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			/*
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = this.text.replace(notNumber, "");
			this._valor = texto;
			*/
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		/*
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}*/
	}
}