package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorCep;
	
	
	public class IgnTextInputCep extends IgnTextInput
	{
		public function IgnTextInputCep()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			super.criar(event);
			this.restrict="0-9"; // somente nros
			this.maxChars=9; 	 // maximo chars 80320-050 	
			this.width = 70;
			
			var validadorCep: IgnValidatorCep = new IgnValidatorCep();
			validadorCep.source = this;
			validadorCep.property="text";
			this._listaValidadorForm.push(validadorCep);
			this._listaValidadorInterno.push(validadorCep);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = this.text.replace(notNumber, "");
			this._valor = texto;
			
			// Formato 80320050
			if (texto.length == 8)
			{
				//80320-050
				this.text = texto.substr(0, 5) + "-" + texto.substr(5, 3);
			}
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
		
		
		
	}
}