package br.com.desenv.framework.component.visual
{
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Camera;
	import flash.net.FileFilter;
	
	import mx.controls.VideoDisplay;
	import mx.core.FlexGlobals;
	import mx.events.FlexEvent;
	import mx.graphics.ImageSnapshot;
	import mx.graphics.codec.JPEGEncoder;
	import mx.managers.PopUpManager;
	import mx.rpc.events.ResultEvent;
	
	import spark.components.Button;
	import spark.components.Image;
	import spark.components.supportClasses.SkinnableComponent;
	
	import br.com.desenv.framework.DsvPickFile;

	[SkinState("capturing")]
	public class IgnInputPhoto extends SkinnableComponent
	{
		protected var dsvPickFile:DsvPickFile;
		
		protected var camera:Camera;
		
		protected var isCameraOn:Boolean = false;
		
		[SkinPart(required="true")]
		public var videoDisplay:VideoDisplay;
		
		[SkinPart(required="true")]
		public var image:Image;
		
		[SkinPart(required="false")]
		public var takePictureButton:Button;
		
		[SkinPart(required="false")]
		public var startCameraButton:Button;
		
		[SkinPart(required="false")]
		public var stopCameraButton:Button;
		
		[SkinPart(required="false")]
		public var removePictureButton:Button;
		
		[SkinPart(required="false")]
		public var pickPictureButton:Button;
		
		[Bindable]
		public var imageWidth:Number = 212;
		[Bindable]
		public var imageHeight:Number = 212;
		[Bindable]
		public var popUpCapturarFoto:Boolean = false;
		[Bindable]
		public var startCameraOnLoad:Boolean = false;
		[Bindable]
		public var clickZoom:Boolean = true;
		
		public function set source(source:Object):void
		{
			if (image==null)
				image = new Image();
			
			image.source = source;
		}
		
		public function get source():Object
		{
			return image.source;
		}
		
		public function IgnInputPhoto()
		{
			super(); 
		}
		
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			
			if (instance == startCameraButton)
			{
				startCameraButton.addEventListener(MouseEvent.CLICK, startCameraButton_click);
			}
			if (instance == stopCameraButton)
			{
				stopCameraButton.addEventListener(MouseEvent.CLICK, stopCameraButton_click);
			}
			if (instance == removePictureButton)
			{
				removePictureButton.addEventListener(MouseEvent.CLICK, removePictureButton_click);
			}
			if (instance == pickPictureButton)
			{
				pickPictureButton.addEventListener(MouseEvent.CLICK, pickPictureButton_click);
			}
			if (instance == takePictureButton)
			{
				takePictureButton.addEventListener(MouseEvent.CLICK, takePictureButton_click);
			}
		}
		
		override protected function partRemoved(partName:String, instance:Object):void
		{
			super.partRemoved(partName, instance);
			
			if (instance == startCameraButton)
			{
				startCameraButton.removeEventListener(MouseEvent.CLICK, startCameraButton_click);
			}
			if (instance == stopCameraButton)
			{
				stopCameraButton.removeEventListener(MouseEvent.CLICK, stopCameraButton_click);
			}
			if (instance == removePictureButton)
			{
				removePictureButton.removeEventListener(MouseEvent.CLICK, removePictureButton_click);
			}
			if (instance == pickPictureButton)
			{
				pickPictureButton.removeEventListener(MouseEvent.CLICK, pickPictureButton_click);
			}
			if (instance == takePictureButton)
			{
				takePictureButton.removeEventListener(MouseEvent.CLICK, takePictureButton_click);
			}
		}		
		
		override protected function getCurrentSkinState():String
		{
			if (isCameraOn)
			{
				return "capturing";
			}
			
			return "normal";
		}
		
		protected function startCamera():void
		{
			if(!popUpCapturarFoto)
			{
				isCameraOn = true;
				invalidateSkinState();
				callLater(attachCamera);	
			}
			else
			{
				var capturaFoto:DsvCapturaFoto = new DsvCapturaFoto();
				capturaFoto.addEventListener("FotoCapturada",fotoCapturadaResultEvent);
			
				PopUpManager.addPopUp(capturaFoto, this, true);

				capturaFoto.x = FlexGlobals.topLevelApplication.width/2 - capturaFoto.width/2;
				capturaFoto.y = FlexGlobals.topLevelApplication.height/2 - capturaFoto.height/2;
			}
		}
		protected function fotoCapturadaResultEvent(event:ResultEvent):void
		{
			if(event.result != null)
			{
				image.source = event.result;
				this.dispatchEvent(new ResultEvent("Foto",false, true, image.source, null, null));
			}
		}
		protected function attachCamera():void
		{
			camera = Camera.getCamera();
			camera.setQuality(32768, 100);
			camera.setMode(800,600,30,false); 
			
			videoDisplay.attachCamera(camera);
		}
		
		protected function stopCamera():void
		{
			isCameraOn = false;
			invalidateSkinState();
			videoDisplay.attachCamera(null);
		}
		
		public function clearPicture():void
		{
			if(this.videoDisplay != null)
				this.videoDisplay.attachCamera(null);
			this.source = null;
		}
		
		protected function takePicture():void
		{
			var bd:BitmapData = ImageSnapshot.captureBitmapData(videoDisplay);
			var encoder:JPEGEncoder = new JPEGEncoder(100);
			image.source = encoder.encode(bd);	
			
			dispatchEvent(new Event("PictureTaken"));
		}		
		
		protected function startCameraButton_click(event:MouseEvent):void
		{
			startCamera();
		}
		
		protected function stopCameraButton_click(event:MouseEvent):void
		{
			stopCamera();
		}
		
		protected function removePictureButton_click(event:MouseEvent):void
		{
			clearPicture();
		}
		
		protected function takePictureButton_click(event:MouseEvent):void
		{
			takePicture();
		}
		
		protected function pickPictureButton_click(event:MouseEvent):void
		{
			if(this.dsvPickFile == null)
				this.dsvPickFile = new DsvPickFile();
			
			this.dsvPickFile.addEventListener(Event.COMPLETE, this.pickPictureCompleteHandler);
			this.dsvPickFile.fileTypeFilter = new FileFilter("Images", ".gif;*.jpeg;*.jpg;*.png");
			this.dsvPickFile.openDialog();
		}
		
		protected function pickPictureCompleteHandler(event:Event):void
		{
			this.image.source = this.dsvPickFile.source;
			this.dispatchEvent(new ResultEvent("Foto",false, true, this.image.source, null, null));
		}
	}
}