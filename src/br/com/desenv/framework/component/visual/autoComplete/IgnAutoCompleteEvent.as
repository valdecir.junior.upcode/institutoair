package br.com.desenv.framework.component.visual.autoComplete 
{
	
	import flash.events.Event;
	
	public class IgnAutoCompleteEvent extends Event {
		
		public static const ITEM_SELECIONADO_EVENT:String = "itemSelecionadoEvent";
		public static const FILTRO_FOCUS_OUT_EVENT:String = "filtroFocusOutEvent";
		public static const FILTRO_FOCUS_IN_EVENT:String = "filtroFocusInEvent";
		public static const FILTRO_ENTER_EVENT:String = "filtroEnterEvent";
		
		public function IgnAutoCompleteEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
			
			super(type, bubbles, cancelable);
		}
	}
}