package br.com.desenv.framework.component.visual.autoComplete 
	
{
	
	import flash.display.InteractiveObject;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.ui.Keyboard;
	import flash.utils.getDefinitionByName;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.messaging.Channel;
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.Operation;
	import mx.rpc.remoting.RemoteObject;
	import mx.states.State;
	import mx.validators.Validator;
	
	import spark.components.List;
	import spark.components.PopUpAnchor;
	import spark.components.TextInput;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.events.TextOperationEvent;
	
	
	[SkinState("Normal")]
	[SkinState("Disable")] 
	[Event(name="itemSelecionadoEvent", type="autoComplete.IgnAutoCompleteEvent")]
	[Event(name="filtroFocusOutEvent", type="autoComplete.IgnAutoCompleteEvent")]
	[Event(name="filtroEnterEvent", type="autoComplete.IgnAutoCompleteEvent")]
	[Event(name="childWindowsSelectedItem", type="com.desenv.nepal.domain.DsvCChildWindowsEvents")]
	public class IgnAutoComplete extends SkinnableComponent {
		
		/* ============================================================== 
		* INICIO: SkinPart obrigaorios 
		* ============================================================== */
		[SkinPart(required="true")]
		public var listDados:List;
		
		[SkinPart(required="true")]
		public var txtFiltro:TextInput;
		
		[SkinPart(required="true")]
		public var puaListaDados:PopUpAnchor;
		
		private var ultimaConsulta:String;
		protected var _obrigatorio: Boolean;
		protected var _valor: String;
		protected var _listaValidadorForm: Array = new Array();
		protected var _listaValidadorInterno: Array = new Array();
		
		/* ============================================================== 
		* INICIO: SkinPart obrigaorios 
		* ============================================================== */
		
		
		/* ============================================================== 
		* INICIO: Variaveis auxiliares para tratar o CORE do componente 
		* ============================================================== */
		/*
		
		*/
		//private var _objetoFiltro:Object;
		
		/**
		 * @private
		 * Ultimo texto escrito no componente
		 */
		
		private var _ultimoTexto:String;
		
		//Teste PABLO
		private var _nomeClasseObjetoFiltro: String;
		
		/**
		 * @private
		 * Indica se a lista das opções do AutoComplete está visivel para o usuário ou não 
		 */
		private var _isListaVisivel:Boolean = false;
		
		/**
		 * @private
		 * Armazena a listagem de TODOS dos DADOS que estarão disponiveis para serem filtrados. 
		 */
		private var _dataProvider:ArrayCollection;
		
		/**
		 * @private
		 * Armazena a listagem dos DADOS apos a aplicação do filtro com base no digitado no componente TXT_FILTRO.
		 * Estes dados filtrados serão exibidos para o usuário. 
		 */
		private var _dataProviderFiltered:ArrayCollection;
		
		/**
		 * @private
		 * Indica quando os DADOS da listagem foram alterados. Sempre que esta propriedade for alterada o componente 
		 * irá invalidar as propriedades do AutoComplete. 
		 */
		private var _dataProviderChanged:Boolean = false;
		
		/**
		 * @private
		 * Define qual item da LISTA está selecionado. 
		 */
		private var _itemSelecionado:Object; 
		
		/**
		 * @private
		 * Define se aparece o codigo junto com a descricao. 
		 */
		private var _mostrarCodigoConcatenado:Boolean = true; 
		
		
		/**
		 * @private
		 * Indica quando um Item da lista foi Selecionado. Tambem serve para indicar se o usuário deseja selecionar algum 
		 * algum item que está contido na lista. 
		 */
		private var _itemSelecionadoAlterado:Boolean = false; 
		
		
		
		private var _buscarTambemPorChave:Boolean = true;
		
		
		/**
		 * @private
		 * Equivalente ao labelField do LIST, DROPDOWN, ETC...
		 */
		private var _palavraChave:String = "label";
		
		/**
		 * @private
		 * @override
		 * Substitui oque será escrito no AutoComplete
		 */
		private var _overrideLabelField:String = null;
		
		/**
		 * @private
		 * Armazena qual PROPRIEDADE do Objeto contido no data provider servirá de base para a ordenção dos DADOS .
		 */
		private var _palavraOrdenacao:String;
		
		/**
		 * @private
		 * Indica se o usuário alterou a propriedade BASE de ordenação nos dados do DataProvider. 
		 */
		private var _palavraOrdenacaoAlterado:Boolean = false;
		
		/**
		 * @private
		 * Indica qual o STATE atual em que o componente se encontra. 
		 */
		private var _stateAtual:String = "Normal";
		
		/**
		 * @private
		 * Reponsavel por armazenar um SKIN para o componente TXT FILTRO. 
		 */
		private var _textInputSkin:Class;
		
		private var resultado:ArrayCollection;
		
		//adicionado por Anderson Paradelas
		private var ro:RemoteObject=null;
		private var _destino:String;
		private var _funcaoChamada:String;
		private var _enviarArgumentos:Boolean = false;
		
		//Recebe o nome do campo ID do objeto que sera consultado
		private var _idKeyName:String;
		
		//Definie o len para iniciar a consulta.
		//Importante para tabelas muito grandes pois ele s'o consulta o servico quando a String alcança um tamanho padrao
		private var _lenStartConsulta:int;
		
		/* ============================================================== 
		* FIM: Variaveis auxiliares para tratar o CORE do componente 
		* ============================================================== */
		
		public function tratarFocusIn(event:FocusEvent):void
		{
			this.txtFiltro.setFocus();
		}
		
		public function IgnAutoComplete() 
		{
			super();
			
			this.setStyle("skinClass", IgnAutoCompleteSkin);

			this.addEventListener(FocusEvent.FOCUS_IN, this.tratarFocusIn);
			this.addEventListener(FlexEvent.CREATION_COMPLETE, trateCreationComplete);
		
			this.states = this.states.concat(
				new State({name: "Normal"}),
				new State({name: "Disable"})
			);			
		}
		
		
		/** 
		 * Generic catch all fault handler to the RemoteObject 
		 * to pop up an Alert and notify the user of the issue.
		 */
		private function remoteObjectFault(event:FaultEvent):void
		{
			Alert.show(event.fault.faultString);
		}		
		
		/* ============================================================== 
		* INICIO: Sobrescrevendo metodos internos 
		* ============================================================== */
		
		override public function set enabled(value:Boolean):void
		{
			if(value)
			{
				currentState = "Normal";
			}
			else 
			{
				currentState = "Disable";
			}
			
			super.enabled = value;
		}
		
		override public function set currentState(value:String):void {
			super.currentState = value;
			_stateAtual = value;
		}
		
		override protected function getCurrentSkinState():String {
			return _stateAtual;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if(_textInputSkin != null && this.txtFiltro.getStyle("skinClass") != _textInputSkin){
				this.txtFiltro.setStyle("skinClass", _textInputSkin);
				this.txtFiltro.validateNow();
			}
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			//seta RemoteObject
			if (ro==null)
			{
				//Alert.show(_destino);
				ro = FlexGlobals.topLevelApplication.montarObjetoRemoto(this._destino);
				ro.addEventListener(FaultEvent.FAULT, remoteObjectFault);
			}
			
			/* Verifica se a propriedade 'labelField' é diferente da definida pelo usuário.
			* Se Sim: Altera a mesma*/
			if(_palavraChave != listDados.labelField)
			{
				if(this._overrideLabelField == null)
					listDados.labelField = _palavraChave;
				else
					listDados.labelField = _overrideLabelField;
			}			
			
			/* Verifica se o dataprovider do componente foi alterado.
			* Se Sim: Faz o vinculo do novo dataProvider e realiza o refresh do filter function */
			if(_dataProviderChanged && _dataProvider){
				_dataProviderFiltered = new ArrayCollection(_dataProvider.source as Array);
				_dataProviderFiltered.filterFunction = functionFiltroDados;
				
				listDados.dataProvider = _dataProviderFiltered;
				
				_dataProviderChanged = false;
			}
			
			if(_palavraOrdenacaoAlterado && _palavraOrdenacao != null && _dataProviderFiltered){
				_dataProviderFiltered = sort(_dataProviderFiltered, palavraOrdenacao);
			}
			
			/* Verifica se o Item selecionado foi alterado. Isso acontece quando o usuario 
			* informa um valor/objeto que contenha no DataProvider. Se existir o item selecionado irá 
			* ser exibido no componente textinput */
			if(_itemSelecionadoAlterado && _dataProviderFiltered){
				if(itemSelecionado is String){
					this.txtFiltro.text = itemSelecionado as String;
				} else if(itemSelecionado is Object){
					this.txtFiltro.text = itemSelecionado[palavraChave];
					
					for each(var obj:Object in _dataProvider){
						if(itemSelecionado[palavraChave] == obj[palavraChave]){
							_itemSelecionado = obj;
							break;
						}
					}
				}
				
				this.txtFiltro.selectRange(this.txtFiltro.text.length, this.txtFiltro.text.length);
				
				_itemSelecionadoAlterado = false;
				_dataProviderFiltered.refresh();
			}
		}
		
		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);
			
			/* Verifica o tipo da instancia do PartAdicionado */
			if (instance == txtFiltro) {
				/* Adiciona os eventos ao componente Filtro para pesquisa */
				txtFiltro.addEventListener(TextOperationEvent.CHANGE, trateTxtFiltroTextAlterado);
				txtFiltro.addEventListener(KeyboardEvent.KEY_DOWN, trateTxtFiltroKeyDown);
				txtFiltro.addEventListener(MouseEvent.CLICK, trateTxtFiltroClicked);
				txtFiltro.addEventListener(FocusEvent.FOCUS_OUT, trateTxtFiltroFocusOut);
				txtFiltro.addEventListener(FocusEvent.FOCUS_IN, trateTxtFiltroFocusIn);
			} else if (instance == listDados) {
				/* Adiciona os eventos ao componente Filtro para pesquisa */
				listDados.addEventListener(KeyboardEvent.KEY_UP, trateListDadosKeyUp);
				listDados.addEventListener(MouseEvent.CLICK, trateListDadosClicked, false, 10);
			}
		}
		
		override protected function partRemoved(partName:String, instance:Object):void 
		{
			super.partRemoved(partName, instance);
			
			/* Verifica o tipo da instancia do PartAdicionado */
			if (instance == txtFiltro) {
				/* Remove os eventos */
				txtFiltro.removeEventListener(TextOperationEvent.CHANGE, trateTxtFiltroTextAlterado);
				txtFiltro.removeEventListener(KeyboardEvent.KEY_DOWN, trateTxtFiltroKeyDown);
				txtFiltro.removeEventListener(MouseEvent.CLICK, trateTxtFiltroClicked);
				txtFiltro.removeEventListener(FocusEvent.FOCUS_OUT, trateTxtFiltroFocusOut);
				txtFiltro.removeEventListener(FocusEvent.FOCUS_IN, trateTxtFiltroFocusIn);
			} else if (instance == listDados) {
				/* Adiciona os eventos ao componente Filtro para pesquisa */
				listDados.removeEventListener(KeyboardEvent.KEY_UP, trateListDadosKeyUp);
				listDados.removeEventListener(MouseEvent.CLICK, trateListDadosClicked);
			}
		}
		
		override protected function childrenCreated():void {
			super.childrenCreated();
		}
		/* ============================================================== 
		* FIM: Sobrescrevendo metodos internos 
		* ============================================================== */
		
		
		/* ============================================================== 
		* INICIO: Tratando os eventos dos EventListeners dos skin part 
		* ============================================================== */
		/**
		 * @private
		 * Fica escutando quando o componente foi criado por complete. Sempre chamado no final do seu ciclo de criação. 
		 * @param evt
		 */
		private function trateCreationComplete(evt:FlexEvent):void
		{
			/* Adiciona um Evento Diretamente no System Manager */
			systemManager.getSandboxRoot().addEventListener(MouseEvent.MOUSE_UP, trateSandrboxDown);
			
			this.errorString = "";
			
			if (this._obrigatorio == true)
			{
				this.styleName = "textInputObrigatorio";
				var validadorObrigatorio: Validator = new Validator();
				validadorObrigatorio.source = this;
				validadorObrigatorio.property = "itemSelecionado";
				validadorObrigatorio.required = true;
				this._listaValidadorForm.push(validadorObrigatorio);
				this._listaValidadorInterno.push(validadorObrigatorio);
			}
		}
		
		/**
		 * @private
		 * Fica escutando quando o usuario clicou em algum lugar da aplicação. 
		 * @param evt
		 */
		private function trateSandrboxDown(evt:MouseEvent):void
		{
			fecharListaDados();
		}
		
		/**
		 * Fica escutando quando o usuario clicou em algum item do dataProvider.
		 * Este evento faz com que o item selecionado seja armazenado na propriedade itemSelecionado
		 * e a lista de opções do dataProvider seja fechada.
		 * 
		 * @param evt
		 */
		private function trateListDadosClicked(evt:MouseEvent):void{
			this.itemSelecionado = this.listDados.selectedItem;
			fecharListaDados();
			
			this.dispatchEvent(new IgnAutoCompleteEvent(IgnAutoCompleteEvent.ITEM_SELECIONADO_EVENT));
			informaAoChamador();
		}
		
		/**
		 * Fica escutando quando o usuario pressionou a tecla ENTER com focus no componente LIST DADOS
		 * Este evento faz com que o primeiro item da lista de dados seja selecionado para movimentação do teclado.
		 *  
		 * @param evt
		 */
		private function trateListDadosKeyUp(evt:KeyboardEvent):void{
			/* Verifica se o usuario pressionou a tecla enter 
			* SE SIM: Recupera o Objeto e fecha o componente 
			* SE NAO: Passa direto */
			if(evt.keyCode == Keyboard.ENTER){
				itemSelecionado = this.listDados.selectedItem;
				fecharListaDados();
				informaAoChamador();
				this.dispatchEvent(new IgnAutoCompleteEvent(IgnAutoCompleteEvent.ITEM_SELECIONADO_EVENT));
			} else if(evt.keyCode == Keyboard.ESCAPE){
				this.fecharListaDados();
				this.txtFiltro.setFocus();
				
				evt.stopImmediatePropagation();	
			}
		}
		
		private function tratarTextEvent(event:TextEvent):void
		{
			this.txtFiltro.text += 			event.text;
			this.txtFiltro.selectRange(this.txtFiltro.text.length, this.txtFiltro.text.length);
			this.trateTxtFiltroTextAlterado(new TextOperationEvent(""));
			this.txtFiltro.dispatchEvent(event);
		}
		
		private function tratarKeyPress(event:KeyboardEvent):void
		{
			this.txtFiltro.dispatchEvent(event);
		}
		
		private function tratarKeyDown(event:KeyboardEvent):void
		{
			var txtEscrito:String = String.fromCharCode(event.charCode);
			var notNumber : RegExp = /[^a-z]/g;
			txtEscrito = txtEscrito.replace(notNumber, "");
			
			this.txtFiltro.text += txtEscrito;
			this.txtFiltro.selectRange(this.txtFiltro.text.length, this.txtFiltro.text.length);
			this.trateTxtFiltroTextAlterado(new TextOperationEvent(""));
			/*
			if(event.keyCode != Keyboard.ENTER)
			{
				if(event.keyCode == Keyboard.BACKSPACE)
					this.txtFiltro.text = this.txtFiltro.text.substring(0, this.txtFiltro.text.length);
				else if(event.keyCode == Keyboard.RIGHT)
					this.txtFiltro.selectRange(this.txtFiltro.selectionActivePosition - 1, this.txtFiltro.selectionActivePosition - 1);
				else if(event.keyCode == Keyboard.DOWN)
					return;
				else
				{
					this.txtFiltro.text += String.fromCharCode(event.charCode);
					this.txtFiltro.selectRange(this.txtFiltro.text.length, this.txtFiltro.text.length);
					this.trateTxtFiltroTextAlterado(new TextOperationEvent(""));
				}
			}*/
			//this.txtFiltro.dispatchEvent(event);
		}
		
		/**
		 * Fica escutando quando o usuario clicou no componente filtro.
		 * Aqui é definido o FOCUS no componente e depois definido o range na posição final do FILTRO. 
		 * @param evt
		 */
		private function trateTxtFiltroClicked(evt:MouseEvent):void{
			this.txtFiltro.setFocus();
			if(this.txtFiltro.text != null){
				this.txtFiltro.selectRange(0,this.txtFiltro.text.length);
			} 
		}
		
		/**
		 * Fica escutando quando o usuario pressionou a tecla DOWN com focus no componente TXT FILTRO
		 * Este evento faz com que o primeiro item da lista de dados seja selecionado para movimentação do teclado.
		 *  
		 * @param evt
		 */
		private function trateTxtFiltroKeyDown(evt:KeyboardEvent):void{
			if(evt.keyCode == Keyboard.DOWN){
				abrirListaDados();
				
				this.listDados.setFocus();
				this.listDados.selectedIndex = 0;
			} else if(evt.keyCode == Keyboard.ESCAPE){
				this.fecharListaDados();
				this.txtFiltro.setFocus();
				
				if (_itemSelecionado != null)
				{
					if (_mostrarCodigoConcatenado == false)
					{
						if(_overrideLabelField == null)
							this.txtFiltro.text = _itemSelecionado[_palavraChave];
						else
							this.txtFiltro.text = _itemSelecionado[_overrideLabelField];
					}
					else
					{
						var codigo:* = _itemSelecionado[_idKeyName];
						var texto: String = _overrideLabelField == null ? _itemSelecionado[_palavraChave] : _itemSelecionado[_overrideLabelField];
						this.txtFiltro.text =  codigo.toString() + " - " + texto;
					}
				}
				
			}
			else if(evt.keyCode == Keyboard.ENTER)
			{
				this.ultimoTexto = this.txtFiltro.text;
				this.fecharListaDados();
				this.txtFiltro.setFocus();
				
				this.dispatchEvent(new IgnAutoCompleteEvent(IgnAutoCompleteEvent.FILTRO_ENTER_EVENT));
			}
			
			
			if (this.txtFiltro.text == "")
			{
				_itemSelecionado = null;
				_itemSelecionadoAlterado = true;
			}
		}
		
		/**
		 * Responsavel por escutar e tratar quando o componente perder o Focus.
		 * 
		 * @param evt
		 */
		protected function trateTxtFiltroFocusOut(evt:FocusEvent):void
		{
			if (evt.target != txtFiltro.textDisplay && evt.target != txtFiltro && evt.target != listDados){
				fecharListaDados();
				
				this.dispatchEvent(new IgnAutoCompleteEvent(IgnAutoCompleteEvent.FILTRO_FOCUS_OUT_EVENT));
			}
			
			
			if (_mostrarCodigoConcatenado == true)
			{
				if (_itemSelecionado != null)
				{
					
					var codigo:* = itemSelecionado[_idKeyName];
					var texto: String = _overrideLabelField == null ? itemSelecionado[_palavraChave] : itemSelecionado[_overrideLabelField];
					
					this.txtFiltro.text = codigo.toString() + " - " + texto;
				}
			}
			
			if(this.txtFiltro.text == "")
				this.dispatchEvent(new IgnChildWindowsEvents(IgnChildWindowsEvents.CHILDWINDOW_SELECTEDITEM, null));
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		/**
		 * Responsavel por escutar e tratar quando o componente perder o Focus.
		 * 
		 * @param evt
		 */
		
		protected function trateTxtFiltroFocusIn(evt:FocusEvent):void{
			
			if (_mostrarCodigoConcatenado == true)
			{
				if (this.txtFiltro.text.length > 0)
				{
					var listaParte: Array = this.txtFiltro.text.split(" - ");
					
					this.txtFiltro.text = listaParte[1];
				}
			}
			
			this.dispatchEvent(new IgnAutoCompleteEvent(IgnAutoCompleteEvent.FILTRO_FOCUS_IN_EVENT));
		}
		
		
		/**
		 * Responsavel por escutar e tratar cada troca de DATA do TextInput.
		 * Aqui da inicio ao processo de filtragem dos dados no componente LIST
		 *  
		 * @param evt
		 */
		/*protected function trateTxtFiltroTextAlterado(evt:TextOperationEvent):void {
		if (txtFiltro.text != '') {
		abrirListaDados();
		}
		if(_dataProviderFiltered){
		_dataProviderFiltered.refresh();
		}
		}*/
		protected function trateTxtFiltroTextAlterado(evt:TextOperationEvent):void 
		{
			if (txtFiltro.text != '') 
			{
				var apenasNumeros: Boolean = false;
				
				if (_buscarTambemPorChave == true)
				{
					var notNumber : RegExp = /[^0-9]/g;
					var textoSomenteNumero : String = txtFiltro.text.replace(notNumber, "");
					
					//indica apenas a existencia de numeros
					if (textoSomenteNumero.length > 0 && textoSomenteNumero.length == txtFiltro.text.length)
					{
						apenasNumeros = true;
					}
				}
				
				if (txtFiltro.text.length>(this._lenStartConsulta-1) || apenasNumeros == true)
				{
					//					if (txtFiltro.text.length==this._lenStartConsulta || apenasNumeros == true)
					//					{
					if (ultimaConsulta != txtFiltro.text || apenasNumeros == true)
					{
						var myClass:Class = getDefinitionByName(_nomeClasseObjetoFiltro) as Class;
						var objetoFiltro: Object = new myClass();
						
						if (_buscarTambemPorChave == false)
						{
							objetoFiltro[_palavraChave]="%"+txtFiltro.text+"%";
							
							recuperaDados(_funcaoChamada,objetoFiltro);
						}
						else
						{
							
							
							//indica apenas a existencia de numeros
							if (apenasNumeros == true)
							{
								var chave:Number = parseInt(txtFiltro.text) as Number;
								objetoFiltro[_idKeyName]= chave;
								recuperaDados(_funcaoChamada,objetoFiltro);
							}
							else
							{
								objetoFiltro[_palavraChave]="%"+txtFiltro.text+"%";
								
								recuperaDados(_funcaoChamada,objetoFiltro);
							}
							
						}
						//						}	
					}
					abrirListaDados();
				}
				else
				{
					fecharListaDados();
				}
			}
			if(_dataProviderFiltered)
			{
				_dataProviderFiltered.refresh();
			}
		}		
		/* ============================================================== 
		* FIm: Tratando os eventos dos EventListeners dos skin part 
		* ============================================================== */
		
		
		/* ============================================================== 
		* INICIO: Tratando os metodos de core do sistema 
		* ============================================================== */
		
		/**
		 * Criado por: Anderson Paradelas
		 *  Define o id do objeto que esta sendo consultado
		 * @param value
		 */
		public function set idKeyName(value:String):void
		{			
			_idKeyName = value;
		}		
		
		public function set nomeClasseObjetoFiltro(value:String):void
		{
			_nomeClasseObjetoFiltro = value;
			
			
		}
		
		
		//public function set objetoFiltro(value:Object):void
		//{
		//_objetoFiltro = value;
		//}
		/**
		 * Criado por: Anderson Paradelas
		 *  Define se o texto deve ser mandado como parametro para consulta no serviço
		 * @param value
		 */
		public function set enviarArgumentos(value:String):void
		{			
			_enviarArgumentos = (value=="true"?true:value=="TRUE"?true:false);
		}		
		
		/**
		 * Criado por: Anderson Paradelas
		 *  Define o servico destino que ira disponibilizar a consulta
		 * @param value
		 */
		public function set destino(value:String):void
		{
			_destino = value;
		}	
		
		/**
		 * Criado por: Anderson Paradelas
		 *  Define a funcao 
		 * @param value
		 */
		public function set funcaoChamada(value:String):void
		{
			_funcaoChamada = value;
		}	
		
		
		
		/**
		 * Criado por: Anderson Paradelas
		 * Definie o len para iniciar a consulta.
		 * Importante para tabelas muito grandes pois ele s'o consulta o servico quando a String alcança um tamanho padrao 
		 * @param value
		 */
		public function set lenStartConsulta(value:int):void
		{
			_lenStartConsulta = value;
		}		
		
		/**
		 * Realiza o Filtro na lista dos dados do componente.
		 * 
		 * @param item a ser comparado no filtro.
		 * @return 
		 */
		private function functionFiltroDados(item:Object):Boolean {
			return functionFiltroPadrao(txtFiltro.text, item);
		}
		
		/**
		 *  
		 * @param palavraFiltro String que será a base do da busca do filtro.
		 * @param item Item que deve ser comparado com a palavraFiltro
		 * @return 
		 */
		private function functionFiltroPadrao(palavraFiltro:String, item:Object):Boolean {
			var match:Array = String(item[palavraChave]).match(new RegExp(palavraFiltro, 'i'));
			return (match && match.length > 0);
		}
		
		/**
		 * Responsavel por definir a lista de dados que deverão ser exibidos no componente 
		 * @param value
		 */
		public function set dataProvider(value:ArrayCollection):void{
			_dataProvider = value;
			_dataProviderChanged = true;
			
			invalidateProperties();
		}
		
		/**
		 * Retorna a lista dos dados que foi definida para se exibida no componente 
		 * @return Lista dos dados. 
		 */
		public function get dataProvider():ArrayCollection{
			return _dataProvider;
		}
		
		/**
		 * Metodo responsavel por abrir a LISTA que exibe todos os dados filtrados. 
		 */
		public function abrirListaDados():void{
			if (!_isListaVisivel) {
				_isListaVisivel = true;
				puaListaDados.displayPopUp = true;
			}
		}
		
		/**
		 * Metodo responsavel por fechar a LISTA que exibe todos os dados filtrados. 
		 */
		public function fecharListaDados():void{
			if (_isListaVisivel) {
				_isListaVisivel = false;
				puaListaDados.displayPopUp = false;
			}
		}
		
		public function set itemSelecionado(value:Object):void{
			if(value){
				_itemSelecionado = value;
				_itemSelecionadoAlterado = true;
				
				
				if (_mostrarCodigoConcatenado == false)
				{
					this.txtFiltro.text = _overrideLabelField == null ? _itemSelecionado[_palavraChave] : _itemSelecionado[_overrideLabelField];
				}
				else
				{
					var codigo: * = _itemSelecionado[_idKeyName];
					var texto: String = _overrideLabelField == null ? _itemSelecionado[_palavraChave] : _itemSelecionado[_overrideLabelField];
					this.txtFiltro.text =  codigo.toString() + " - " + texto;
				}
				
				invalidateProperties();
			} 
			else
			{
				limpar();
			}
		}
		
		public function get itemSelecionado():Object{
			return _itemSelecionado;
		}
		
		public function set palavraChave(value:String):void{
			_palavraChave =  value;
			
			invalidateProperties();
		}		
		
		
		public function get mostrarCodigoConcatenado():Boolean{
			return _mostrarCodigoConcatenado;
		}
		
		public function set mostrarCodigoConcatenado(value:Boolean):void{
			_mostrarCodigoConcatenado =  value;
		}		
		
		public function get palavraChave():String{
			return _palavraChave;
		}
		
		public function set palavraOrdenacao(value:String):void{
			_palavraOrdenacao =  value;
			_palavraOrdenacaoAlterado = true;
			
			invalidateProperties();
		}
		
		public function get palavraOrdenacao():String{
			return _palavraOrdenacao;
		}
		
		public function limpar():void
		{
			this.ultimoTexto = this.txtFiltro.text;
			_itemSelecionado = null;
			txtFiltro.text = "";
		}
		
		
		public function set buscarTambemPorChave(value:Boolean):void
		{
			_buscarTambemPorChave = value;
		}
		
		
		/**
		 * Define um SKIN para o campo de filtro; 
		 * @param value
		 */
		public function set txtFiltroSkin(value:Class):void{
			_textInputSkin = value;
			
			invalidateDisplayList();
		}
		
		public function get txtFiltroSkin():Class{
			return _textInputSkin;
		}
		/* ============================================================== 
		* FIM: Tratando os metodos de core do sistema 
		* ============================================================== */
		
		/**
		 * Método responsavél por ordenar um ArrayCollection
		 *
		 * @param ArrayCollection arrayCollection
		 * @param String sortBy - nome do campo que o filtro irá ser aplicado
		 * @param String order - Crescente = false (default)  ou Decrescente = true
		 * @param Boolean numericSort
		 *
		 * @return ArrayCollection
		 */
		public function sort(arrayCollection:ArrayCollection, sortBy:String, order:Boolean=false, numericSort:Boolean=false):ArrayCollection 
		{
			if (!arrayCollection) {
				return null;
			}
			
			var dataSortField:SortField = new SortField();
			dataSortField.name = sortBy;
			dataSortField.caseInsensitive = true;
			dataSortField.numeric = numericSort;
			
			if (order) {
				dataSortField.descending = true;
			}
			
			var dataSort:Sort = new Sort();
			dataSort.fields = [dataSortField];
			
			arrayCollection.sort = dataSort;
			arrayCollection.refresh();
			
			return arrayCollection;
		}		
		
		
		private function recuperaDados(strFuncaoPesquisa:String, objetoFiltro:Object):void 
		{
			var loadPesquisaOperation:Operation = new Operation();
			loadPesquisaOperation.name = strFuncaoPesquisa;
			//Alert.show(strFuncaoPesquisa);
			if (_enviarArgumentos==true)
			{
				loadPesquisaOperation.arguments = [objetoFiltro]
			}
			loadPesquisaOperation.addEventListener(ResultEvent.RESULT, recuperaDadosResultHandler);
			loadPesquisaOperation.addEventListener(FaultEvent.FAULT, recuperaDadosFaultHandler);
			ro.operations = [loadPesquisaOperation];
			loadPesquisaOperation.send();
		}
		private function recuperaDadosResultHandler(event:ResultEvent):void
		{
			resultado = event.result as ArrayCollection;
			_dataProvider = resultado;
			_dataProviderFiltered = new ArrayCollection(_dataProvider.source as Array);
			dataProvider = resultado;
			_dataProviderChanged = true;
		}		
		private function recuperaDadosFaultHandler(event:FaultEvent):void
		{
			Alert.show(event.fault.message,"Erro na conexao do componente de dados do IgnAutoComplete.");
		}
		
		private function informaAoChamador():void
		{
			if (itemSelecionado!=null)
			{
				var obj:IgnChildWindowsObj = new IgnChildWindowsObj();
				obj.id = itemSelecionado[_idKeyName];
				obj.descricao = itemSelecionado[_palavraChave];
				obj.objeto = itemSelecionado;
				this.dispatchEvent(new IgnChildWindowsEvents(IgnChildWindowsEvents.CHILDWINDOW_SELECTEDITEM, obj));
			}
		}
		
		public function set obrigatorio(obrigatorio: Boolean): void
		{
			this._obrigatorio = obrigatorio;
		}
		
		public function get obrigatorio(): Boolean
		{
			return this._obrigatorio;
		}
		
		public function set listaValidadorForm(listaValidadorForm: Array): void
		{
			this._listaValidadorForm = listaValidadorForm;
		}
		
		
		public function get valor(): String
		{
			return this._valor;
		}
		
		protected function sair(event: FocusEvent): void 
		{
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		public function set ultimoTexto(valor:String):void
		{
			this._ultimoTexto = valor;
		}
		
		public function get ultimoTexto():String
		{
			return this._ultimoTexto;
		}
		
		public function set overrideLabelField(value:String):void
		{
			this._overrideLabelField = value;
		}
		
		public function get overrideLabelField():String
		{
			return this._overrideLabelField;
		}
	}	
}