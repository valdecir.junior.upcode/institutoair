
package br.com.desenv.framework.component.visual.autoComplete
{
	import flash.events.Event;

	public class IgnChildWindowsEvents extends Event
	{
		public static const CHILDWINDOW_DBCLICKED:String = "childWindowDbClicked";
		public static const CHILDWINDOW_POPUPCLOSED:String = "childWindowPopUpClosed";
		public static const CHILDWINDOW_POPUPOPENED:String = "childWindowPopUpOpened";
		public static const CHILDWINDOW_SELECTEDITEM:String = "childWindowsSelectedItem";
	
		public var childWindows:IgnChildWindowsObj;
		
		public function IgnChildWindowsEvents(type:String, childWindows:IgnChildWindowsObj, bubbles:Boolean = true, cancelable:Boolean = false)
   		{
   			this.childWindows = childWindows;
			super(type, bubbles, cancelable);
		}
	}
}
