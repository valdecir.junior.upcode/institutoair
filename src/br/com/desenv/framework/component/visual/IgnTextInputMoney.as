package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import spark.events.TextOperationEvent;
	
	import flashx.textLayout.formats.Float;
	import flashx.textLayout.operations.FlowTextOperation;
	import flashx.textLayout.operations.InsertTextOperation;
	import flashx.textLayout.operations.PasteOperation;
	
	
	public class IgnTextInputMoney extends IgnTextInput
	{ 
		public function IgnTextInputMoney() 
		{ 
			updateRegex(); 
		} 
		
		private var regex:RegExp; 
		private var _permiteNegativo: Boolean = false;
		private var _numeroCasaDecimal:int = -1;

		public function set permiteNegativo(value:Boolean):void 
		{ 
			_permiteNegativo = value; 
			updateRegex(); 
		} 
		
		public function set numeroCasaDecimal(value:int):void 
		{ 
			_numeroCasaDecimal = value; 
			updateRegex(); 
		} 
		
		
		private var _useMin:Boolean = false; 
		private var _useMax:Boolean = false; 
		private var _min:Number = 0; 
		private var _max:Number = 0; 
		
		public function set minNumber(num:Number):void 
		{ 
			_useMin = true; 
			_min = num; 
		} 
		
		public function set maxNumber(num:Number):void 
		{ 
			_useMax = true; 
			_max = num; 
		} 
		
		/** 
		 *  @private 
		 */ 
		override public function set restrict(value:String):void 
		{ 
			throw(new Error("You are not allowed to change the restrict property of this class.  It is read-only.")); 
		} 
		
		override protected function childrenCreated():void 
		{ 
			super.childrenCreated(); 
			
			//listen for the text change event 
			addEventListener(TextOperationEvent.CHANGING, onTextChange); 
			addEventListener(FocusEvent.FOCUS_OUT, onFocusLost); 
		} 
		
		public function onTextChange(event:TextOperationEvent):void 
		{ 
			if (regex) 
			{ 
				var textToBe:String = ""; 
				var selectionStart:int = 0; 
				var selectionEnd:int = 0; 
				if (event.operation is InsertTextOperation) { 
					selectionStart = Math.min(selectionActivePosition, selectionAnchorPosition); 
					selectionEnd = Math.max(selectionActivePosition, selectionAnchorPosition); 
					textToBe = text.substr(0,selectionStart) + (event.operation as InsertTextOperation).text + text.substr(selectionEnd); 
				} else if (event.operation is PasteOperation) { 
					textToBe = text.substr(0,(event.operation as PasteOperation).absoluteStart) + (event.operation as PasteOperation).textScrap.textFlow.getText() + text.substr((event.operation as PasteOperation).absoluteEnd); 
				} else if (event.operation is FlowTextOperation) { //Handles DeleteOperation and CutOperation 
					textToBe = text.substr(0,(event.operation as FlowTextOperation).absoluteStart) + text.substr((event.operation as FlowTextOperation).absoluteEnd); 
				} 
				
				var match:Object = regex.exec(textToBe); 
				if (!match || match[0] != textToBe) 
				{ 
					// The textToBe didn't match the expression... stop the event 
					event.preventDefault(); 
				} 
				
				if (_useMin) 
					if (parseFloat(textToBe) < _min) 
						event.preventDefault(); 
				
				if (_useMax) 
					if (parseFloat(textToBe) > _max) 
						event.preventDefault(); 
			} 
		} 
		
		private function onFocusLost(event:FocusEvent):void 
		{ 
			//if (_fractionalDigits) 
			//	text = parseFloat(text.replace(",", ".")).toString(); 
			//else 
			//	text = parseInt(text.replace(",", ".")).toString(); 
		} 
		
		private function updateRegex():void 
		{ 
			var regexString:String = "^"; 
			
			if(_permiteNegativo) 
				regexString += "\\-?"; 
			
			regexString += "\\d*"; 
			
			if(_numeroCasaDecimal > 0 ) 
				regexString += "\\,?\\d{0," + _numeroCasaDecimal.toString() + "}"; 
			else if (_numeroCasaDecimal == -1) 
				regexString += "\\,?\\d*"; 
			
			regexString += "$"; 
			regex = new RegExp(regexString); 
		}
		public function get numberValue():Number 
		{
			var novoValor:String = this._valor;
			novoValor = novoValor.replace(".","").replace(",",".");
			return parseFloat(novoValor);
			
		}
		
	} 
}