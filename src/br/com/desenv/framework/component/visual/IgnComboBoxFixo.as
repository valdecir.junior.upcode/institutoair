package br.com.desenv.framework.component.visual
{
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	public class IgnComboBoxFixo extends IgnComboBox
	{
		
		public function IgnComboBoxFixo()
		{
			super();
			this.labelFunction = mostrarDescricaoItem;
			
			this.addEventListener(KeyboardEvent.KEY_DOWN, ignKeyDownHandler);
		}
		
		public function ignKeyDownHandler(event:KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.ENTER)
			{
				try
				{
					if(super.dataProvider == null)
						return;
					
					for(var objectIndex:int = 0x00; objectIndex < super.dataProvider.length; objectIndex++)
					{
						var obj:Object = super.dataProvider.getItemAt(objectIndex);
						
						if(obj == null)
							continue;
						
						if(obj.codigo != null)
						{
							if(obj.codigo == super.textInput.text)
							{
								selectedIndex = objectIndex;
								selectedItem = obj;
								commitProperties();
								return;
							}	
						}	
					}
				}
				catch(e:Error)
				{
					//SUPRESS
				}
			}
		}
		
		public function mostrarDescricaoItem(item: Object): String 
		{
			try
			{
				if (item  == null)
				{
					return "";	
				}
				else
				{
					return item.valor;
				}		
			}
			catch(e:Error)
			{
				//SUPRESS
			}
			
			return "";
		}
	}
}