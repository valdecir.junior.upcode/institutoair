package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorCpfCnpj;
	
	
	public class IgnTextInputCnpj extends IgnTextInput
	{
		public function IgnTextInputCnpj()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			super.criar(event);
			this.restrict="0-9"; // somente nros
			
			
			
			this.maxChars=18; 	 // 03.847.655/0001-98
			this.width = 140;
			
			var validadorCpfCnpj: IgnValidatorCpfCnpj = new IgnValidatorCpfCnpj();
			validadorCpfCnpj.source = this;
			validadorCpfCnpj.property="text";
			this._listaValidadorForm.push(validadorCpfCnpj);
			this._listaValidadorInterno.push(validadorCpfCnpj);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = this.text.replace(notNumber, "");
			this._valor = texto;
			
			// Formato 03.847.655/0001-98
			if (texto.length == 14)
			{
				this.text = texto.substr(0, 2) + "." + texto.substr(2, 3) + "." + texto.substr(5, 3) + "/" + texto.substr(8, 4) + "-" + texto.substr(12, 2);
			}
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
		
		
		
	}
}