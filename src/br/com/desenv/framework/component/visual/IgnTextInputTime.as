package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorTime;
	
	
	public class IgnTextInputTime extends IgnTextInput
	{
		
		
		
		public function IgnTextInputTime()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			//super.criar(event);
			this.restrict=": 0-9"; // somente nros e virgula
			
			//dd/MM/YYYY HH:MM
			this.maxChars = 8;
			this.width = 77;			
			
			var validadorTime: IgnValidatorTime = new IgnValidatorTime();
			validadorTime.source = this;
			validadorTime.property = "text";
			this._listaValidadorForm.push(validadorTime);
			this._listaValidadorInterno.push(validadorTime);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = this.text;
			this._valor = texto;
			var temSinalNegativo: Boolean = false;
			if (texto.charAt(0) == "-")
			{
				temSinalNegativo = true;
			}
			texto = this.text.replace(notNumber, "");
			
			// Formato dd/MM/YYYYY HH:MM
			if(texto.length == 2)
			{
				this.text = texto.substr(0, 2) + ":00:00";
			}
			if (texto.length == 4)
			{
				this.text = texto.substr(0, 2) + ":" + texto.substr(2, 2) + ":00";
			}
				// Formato dd/MM/YY HH:MM
			else if (texto.length == 6)
			{
				this.text = texto.substr(0, 2) + ":" + texto.substr(2, 2) + ":" + texto.substr(4, 2);
			}
			else if (texto.length == 1 && texto == "0")
			{
				var horaAtual: Date = new Date();
			
				horaAtual.setDate(horaAtual.getDate());
				
			
				
				var dateFormatter: DateFormatter= new DateFormatter();
				dateFormatter.formatString = "HH:NN:SS";
				this.text = dateFormatter.format(horaAtual);
			}

			Validator.validateAll(this._listaValidadorInterno);
			
			
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
	}
}



