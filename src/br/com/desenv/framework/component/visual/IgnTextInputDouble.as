package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.NumberValidator;
	import mx.validators.Validator;
	
	import spark.events.TextOperationEvent;
	
	
	public class IgnTextInputDouble extends IgnTextInput
	{
		protected var _permiteNegativo: Boolean = false;
		
		public function IgnTextInputDouble()
		{
			super();
		}
		
		public function set permiteNegativo(permiteNegativo: Boolean): void
		{
			this._permiteNegativo = permiteNegativo;
		}
		
		public function get permiteNegativo(): Boolean
		{
			return this._permiteNegativo;
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
//			super.criar(event);
			//this.restrict="\-0-9"; // somente nros e virgula
			
			super.addEventListener(TextOperationEvent.CHANGE, this.onChange);
			
			var validadorDouble: NumberValidator = new NumberValidator();
			validadorDouble.source = this;
			validadorDouble.property = "text";
			validadorDouble.decimalSeparator = ",";
			validadorDouble.thousandsSeparator = ".";
			validadorDouble.allowNegative = _permiteNegativo;
			validadorDouble.precision
			if (super._obrigatorio)
			{
				this.styleName = "textInputObrigatorio";
				validadorDouble.required = true;
			}
			else
			{
				validadorDouble.required = false;
			}
			this._listaValidadorForm.push(validadorDouble);
			this._listaValidadorInterno.push(validadorDouble);
			
		}
		
		private function onChange(event:TextOperationEvent):void
		{
			//var notNumber : RegExp = /[^0-9,]/g;
			//this.text = this.text.replace(notNumber, "");
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			/*var notNumber : RegExp = /[^0-9,]/g;
			var texto : String = '';
			texto = this.text.replace(notNumber, "");
			this._valor = texto;*/
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		/*
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9,]/g;
			this.text = this.text.replace(notNumber, "");
		}
		*/
		
		public function get numberValue():Number 
		{
			var novoValor:String = this.text;
			novoValor = novoValor.replace(".","").replace(",",".");
			return parseFloat(novoValor);
			
		}		
	}
}

