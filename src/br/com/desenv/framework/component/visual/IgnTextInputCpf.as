package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorCpf;
	
	
	public class IgnTextInputCpf extends IgnTextInput
	{
		public function IgnTextInputCpf()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			super.criar(event);
			this.restrict="0-9"; // somente nros
			this.maxChars=14; 	 //035.053.797-60
			this.width=108; 	 //035.053.797-60
			
			var validadorCpf: IgnValidatorCpf = new IgnValidatorCpf();
			validadorCpf.source = this;
			validadorCpf.property="text";
			this._listaValidadorForm.push(validadorCpf);
			this._listaValidadorInterno.push(validadorCpf);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = this.text.replace(notNumber, "");
			this._valor = texto;
			
			// Formato 999.999.999-70
			if (texto.length == 11)
			{
				this.text = texto.substr(0, 3) + "." + texto.substr(3, 3) + "." + texto.substr(6, 3) + "-" + texto.substr(9, 2);
			}
			
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
		
		
		
	}
}