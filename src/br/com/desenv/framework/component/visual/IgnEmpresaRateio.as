package br.com.desenv.framework.component.visual
{
	import mx.containers.VBox;
	import mx.controls.TextInput;
	import mx.core.FlexGlobals;
	
	import br.com.desenv.framework.util.DsvParametrosSistema;
	import br.com.desenv.nepalign.vo.EmpresaFisica;
	
	
	public class IgnEmpresaRateio extends VBox
	{
		private const BOX_HEIGHT:uint 	= 23;
		private const BOX_WIDTH:uint 	= 40;
		
		/* NOME DO PARAMETRO QUE CONTÉM A PORCENTAGEM PADRÃO DA EMPRESA */
		private const PARAMETRO_PERCENTUAL_RATEIO:String = "PERCENTUAL_RATEIO";
		
		/**
		 * Indica se o componente é editável
		 */
		public function set readOnly(value:Boolean):void
		{
			porcentagemEmpresa.editable = !value;
		}

		private var tituloEmpresa		: TextInput;
		private var porcentagemEmpresa  : TextInput;
		
		public function IgnEmpresaRateio()
		{
			super();
			
			applyStyles();
			createElements();
		}

		/**
		 * Aplica os estilos para o componente principal
		 * @langversion ActionScript 3.0
		 */
		protected function applyStyles():void
		{
			setStyle("margin", 			0x00);
			setStyle("horizontalGap",	0x00);
			setStyle("verticalGap",		0x00);
			setStyle("paddingBottom",   0x00);
			setStyle("paddingTop",      0x00);
		}
		
		/**
		 * Cria o título e o campo de informação de porcentagem da Empresa
		 * @langversion ActionScript 3.0
		 */
		protected function createElements():void
		{
			tituloEmpresa = new TextInput();
			tituloEmpresa.selectable	= false;
			tituloEmpresa.editable		= false;
			tituloEmpresa.focusEnabled	= false;
			tituloEmpresa.top	 		= 0;
			tituloEmpresa.left			= 0;
			tituloEmpresa.right			= 0;
			tituloEmpresa.bottom 		= 0;
			tituloEmpresa.width			= BOX_WIDTH;
			tituloEmpresa.height		= BOX_HEIGHT;
			tituloEmpresa.setStyle("textAlign", "center");
			tituloEmpresa.setStyle("fontWeight", "bold");
			
			porcentagemEmpresa = new TextInput();
			porcentagemEmpresa.top	 		= 0;
			porcentagemEmpresa.left			= 0;
			porcentagemEmpresa.right		= 0;
			porcentagemEmpresa.bottom 		= 0;
			porcentagemEmpresa.width		= BOX_WIDTH;
			porcentagemEmpresa.height		= BOX_HEIGHT;
			porcentagemEmpresa.setStyle("textAlign", "center");
			
			addChild(tituloEmpresa);
			addChild(porcentagemEmpresa);
		}
		
		/**
		 * Atualiza o texto com o título e o percentual padrão da empresa
		 * @langversion ActionScript 3.0
		 */
		public function set empresaFisica(empresaFisica:EmpresaFisica):void
		{
			tituloEmpresa.text = empresaFisica.idFormatado;
			
			if(porcentagemEmpresa.text == "")
				porcentagemEmpresa.text = (FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametroPorEmpresa(empresaFisica.id, PARAMETRO_PERCENTUAL_RATEIO) as String;
		}
		
		/**
		 * Recupera o percentual informado da Empresa
		 * @langversion ActionScript 3.0
		 */
		public function get percentual():Number
		{
			return Number(porcentagemEmpresa.text);
		}
		
		/**
		 * Atualiza o percentual da Empresa
		 * @langversion ActionScript 3.0
		 */
		public function set percentual(percentual:Number):void
		{
			porcentagemEmpresa.text = percentual.toString();
		}
	}
}