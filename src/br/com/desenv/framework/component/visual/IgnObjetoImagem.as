package br.com.desenv.framework.component.visual
{
	public class IgnObjetoImagem extends Object
	{
		[Bindable]
		public var label:String;
		[Bindable]
		public var thumbnailImage:String;
		[Bindable]
		public var fullImage:String;
		
		public function IgnObjetoImagem() { }
	}
}