package br.com.desenv.framework.component.visual
{
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridListData;
	
	import br.com.desenv.nepalign.vo.ItemDocumentoEntradaSaida;
	import br.com.desenv.nepalign.vo.ItemPedidoCompraVenda;
	
	
	public class IgnValorGrade extends TextInput
	{
		private var letraGrade: String;
		
		public function IgnValorGrade()
		{
			super();
			this.setStyle("margin", 0);
			this.setStyle("horizontalGap", 0);
			this.setStyle("verticalGap", 0);
			this.setStyle("paddingBottom", 0);
			this.setStyle("paddingTop", 0);
			
			
			this.styleName = "textInputGradeA";
			this.editable = false;
			this.top=0;
			this.left=0;
			this.right=0;
			this.bottom=0;
		}
		
		
		
		override public function set data(value:Object):void {
			if (value is ItemPedidoCompraVenda)
			{
				var itemPedidoCompraVenda: ItemPedidoCompraVenda = value as ItemPedidoCompraVenda;
			

				if (itemPedidoCompraVenda.letraGrade == 'A')
				{
					this.styleName = "textInputGradeAInvertida";
				}
			
				if (itemPedidoCompraVenda.letraGrade == 'B')
				{
					this.styleName = "textInputGradeBInvertida";
				}
					
				if (itemPedidoCompraVenda.letraGrade == 'C')
				{
					this.styleName = "textInputGradeCInvertida";
				}
			}
			else if (value is ItemDocumentoEntradaSaida)
			{
				var itemDocumentoEntradaSaida: ItemDocumentoEntradaSaida = value as ItemDocumentoEntradaSaida;
				
				
				if (itemDocumentoEntradaSaida.letraGrade == 'A')
				{
					this.styleName = "textInputGradeAInvertida";
				}
				
				if (itemDocumentoEntradaSaida.letraGrade == 'B')
				{
					this.styleName = "textInputGradeBInvertida";
				}
				
				if (itemDocumentoEntradaSaida.letraGrade == 'C')
				{
					this.styleName = "textInputGradeCInvertida";
				}

			}
				
				
			text = value[DataGridListData(listData).dataField];
			
			super.invalidateDisplayList();
		}
		
	}
}


