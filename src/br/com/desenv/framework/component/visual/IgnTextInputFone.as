package br.com.desenv.framework.component.visual
{
	import flash.events.FocusEvent;
	
	import mx.events.FlexEvent;
	import mx.validators.Validator;
	
	import br.com.desenv.framework.component.validator.IgnValidatorFone;
	
	
	public class IgnTextInputFone extends IgnTextInput
	{
		public function IgnTextInputFone()
		{
			super();
		}
		
		
		// on Create Complete
		override protected function criar(event: FlexEvent) : void 
		{
			super.criar(event);
			this.restrict="0-9"; // somente nros
			this.maxChars=15; 	 // maximo chars 15		(41) 09148-9900  (11) 99999-9999
			this.width 115;
			
			var validadorTelefone: IgnValidatorFone = new IgnValidatorFone();
			validadorTelefone.source = this;
			validadorTelefone.property="text";
			this._listaValidadorForm.push(validadorTelefone);
			this._listaValidadorInterno.push(validadorTelefone);
		}
		
		// on Focus Out
		override protected function sair(event: FocusEvent): void 
		{
			// Formata
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = this.text.replace(notNumber, "");
			this._valor = texto;
			
			// Formato (41) 99999-9999
			if (texto.length == 8)
			{
				this.text = texto.substr(0, 4) + "-" + texto.substr(4, 4);
			}
			// Formato Sao Paulo 99999-9999
			else if (texto.length == 9)
			{
				this.text = texto.substr(0, 5) + "-" + texto.substr(5, 4);
			}
			// Formato (41) 9999-9999 (ddd com dois digitos) 
			else if (texto.length == 10)
			{
				this.text = "(" + texto.substr(0, 2) + ") " + texto.substr(2, 4) + "-" + texto.substr(6, 4);
			}
			// Formato (041) 9999-9999 (ddd com dois digitos e zero na frente) 
			else if (texto.length == 11 && texto.charAt(0) == "0")
			{
				this.text = "(" + texto.substr(1, 2) + ") " + texto.substr(3, 4) + "-" + texto.substr(7, 4);
			}
			// Formato Sao Paulo (41) 99999-9999 (ddd com dois digitos) 
			else if (texto.length == 11 && texto.charAt(0) != "0")
			{
				this.text = "(" + texto.substr(0, 2) + ") " + texto.substr(2, 5) + "-" + texto.substr(7, 4);
			}
			// Formato Sao Paulo (041) 99999-9999 (ddd com dois digitos e zero na frente) 
			else if (texto.length == 12 && texto.charAt(0) == "0")
			{
				this.text = "(" + texto.substr(1, 2) + ") " + texto.substr(3, 5) + "-" + texto.substr(8, 4);
			}
			
			Validator.validateAll(this._listaValidadorInterno);
		}
		
		
		// on Focus In
		override protected function entrar(event: FocusEvent): void 
		{
			super.entrar(event);
			var notNumber : RegExp = /[^0-9]/g;
			this.text = this.text.replace(notNumber, "");
		}
		
		
		
	}
}