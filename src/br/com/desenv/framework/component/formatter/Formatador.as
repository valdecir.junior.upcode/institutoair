package br.com.desenv.framework.component.formatter
{
	
	
	import mx.formatters.DateFormatter;
	import mx.formatters.NumberFormatter;
	
	public class Formatador
	{
		public function Formatador()
		{
		}
		
		public static function formatarCep(valor: String): String
		{
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = valor.replace(notNumber, "");
			
			// Formato 80320050
			if (texto.length == 8)
			{
				//80320-050
				texto = texto.substr(0, 5) + "-" + texto.substr(5, 3);
			}
			
			return texto;
		}
		
		public static function formatarCpf(valor: String): String
		{
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = valor.replace(notNumber, "");
			
			
			// Formato 999.999.999-70
			if (texto.length == 11)
			{
				texto = texto.substr(0, 3) + "." + texto.substr(3, 3) + "." + texto.substr(6, 3) + "-" + texto.substr(9, 2);
			}
			
			return texto;
		}
		
		public static function formatarCpfCnpj(valor: String): String
		{
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = valor.replace(notNumber, "");
			
			
			// Formato 999.999.999-70
			if (texto.length == 11)
			{
				texto = texto.substr(0, 3) + "." + texto.substr(3, 3) + "." + texto.substr(6, 3) + "-" + texto.substr(9, 2);
			}
				//99.999.999/9999-01
			else if (texto.length == 14)
			{
				texto = texto.substr(0, 2) + "." + texto.substr(2, 3) + "." + texto.substr(5, 3) + "/" + texto.substr(8, 4) + "-" + texto.substr(12, 2);
			}
			
			
			return texto;
		}
		
		public static function formatarFone(valor: String): String
		{
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = valor.replace(notNumber, "");
			
			
			// Formato 9999-9999
			if (texto.length == 8)
			{
				texto = texto.substr(0, 4) + "-" + texto.substr(4, 4);
			}
				// Formato Sao Paulo 99999-9999
			else if (texto.length == 9)
			{
				texto = texto.substr(0, 5) + "-" + texto.substr(5, 4);
			}
				// Formato (41) 9999-9999 (ddd com dois digitos) 
			else if (texto.length == 10)
			{
				texto = "(" + texto.substr(0, 2) + ") " + texto.substr(2, 4) + "-" + texto.substr(6, 4);
			}
				// Formato (041) 9999-9999 (ddd com dois digitos e zero na frente) 
			else if (texto.length == 11 && texto.charAt(0) == "0")
			{
				texto = "(" + texto.substr(1, 2) + ") " + texto.substr(3, 4) + "-" + texto.substr(7, 4);
			}
				// Formato Sao Paulo (41) 99999-9999 (ddd com dois digitos) 
			else if (texto.length == 11 && texto.charAt(0) != "0")
			{
				texto = "(" + texto.substr(0, 2) + ") " + texto.substr(2, 5) + "-" + texto.substr(7, 4);
			}
				// Formato Sao Paulo (041) 99999-9999 (ddd com dois digitos e zero na frente) 
			else if (texto.length == 12 && texto.charAt(0) == "0")
			{
				texto = "(" + texto.substr(1, 2) + ") " + texto.substr(3, 5) + "-" + texto.substr(8, 4);
			}
			
			return texto;
		}
		
		
		public static function formatarDate(valor:Date): String 
		{
			var texto : String = "";
			var dateFormatter: DateFormatter= new DateFormatter();
			dateFormatter.formatString = "DD/MM/YYYY";
			texto = dateFormatter.format(valor);
			
			return texto;
			
		}
		public static function formatarDateTime(valor:Date): String 
		{
			var texto : String = "";
			var dateTimeFormatter: DateFormatter= new DateFormatter();
			dateTimeFormatter.formatString = "DD/MM/YYYY HH:NN";
			texto = dateTimeFormatter.format(valor);
			
			return texto;
			
		}
		public static function formatarCnpj(valor: String): String
		{
			var notNumber : RegExp = /[^0-9]/g;
			var texto : String = '';
			texto = valor.replace(notNumber, "");
			
			
			if (texto.length == 14)
			{
				texto = texto.substr(0, 2) + "." + texto.substr(2, 3) + "." + texto.substr(5, 3) + "/" + texto.substr(8, 4) + "-" + texto.substr(12, 2);
			}
			
			
			return texto;
		}
		
		public static function formatarDouble(valor:Number, numeroCasasDecimais: int): String 
		{
			return numberFormat(valor, numeroCasasDecimais, true, true);
		}
		
		private static function numberFormat(number:*, maxDecimals:int = 2, forceDecimals:Boolean = false, siStyle:Boolean = true):String
		{
			if(isNaN(number as Number))
				return "0,00";
			
			var i:int = 0;
			var inc:Number = Math.pow(10, maxDecimals);
			var str:String = String(Math.round(inc * Number(number))/inc);
			var hasSep:Boolean = str.indexOf(".") == -1, sep:int = hasSep ? str.length : str.indexOf(".");
			var ret:String = (hasSep && !forceDecimals ? "" : (siStyle ? "," : ".")) + str.substr(sep+1);
			if (forceDecimals) 
			{
				for (var j:int = 0; j <= maxDecimals - (str.length - (hasSep ? sep-1 : sep)); j++) ret += "0";
			}
			while (i + 3 < (str.substr(0, 1) == "-" ? sep-1 : sep)) ret = str.substr(sep - (i += 3), 3) + ret;
			return str.substr(0, sep - i) + ret;
		}
	}
}