package br.com.desenv.framework.security
{
	import br.com.desenv.nepalign.vo.Usuario;

	/**
	 * Model utilizado para fazer comunicação com o WS de Login
	 * @author lucas.leite
	 * @langversion ActionScript 3.0 / Serializable on JSON
	 */
	public class Credentials
	{
		public var username:String;
		public var password:String;
		public var token:String;
		public var usuario:Usuario;
		
		public function Credentials()
		{
			
		}
	}	
}