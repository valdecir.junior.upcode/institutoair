/*---------------------------------------------------------------------------------------------

[AS3] NepalignUpdater
=======================================================================================

Copyright (c) 2015 UpCode Tecnologia

e   contato@upcode.net.br
w   http://upcode.net.br

---------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------

ERROR CODES

16800 Occurs during validating the downloaded update file. The subErrorID property may contain additional information. 
16801 Invalid Adobe AIR file (missing application.xml). 
16802 Invalid Adobe AIR file (missing MIME type). 
16803 Invalid Adobe AIR file (format). 
16804 Invalid Adobe AIR file (invalid flags). 
16805 Invalid Adobe AIR file (unknown compression). 
16806 Invalid Adobe AIR file (invalid filename). 
16807 Invalid Adobe AIR file (corrupt). 
16808 Configuration file does not exist. 
16809 The updateURL property is not set. 
16810 Reserved. 
16811 Invalid configuration file (unknown configuration version). 
16812 Invalid configuration file (URL missing). 
16813 Invalid configuration file (delay format). 
16814 Invalid configuration file (invalid defaultUI values). 
16815 Invalid update descriptor (unknown descriptor version). 
16816 Invalid update descriptor (missing update version). 
16817 Invalid update descriptor (invalid description). 
16818 IO error while saving data to disk. The subErrorID property may provide more information. 
16819 Security error while downloading. The subErrorID property may provide more information. 
16820 Invalid HTTP status code. The subErrorID property may contain the invalid status code. 
16821 Reserved. 
16822 I/O error while downloading. The subErrorID property may provide more information. 
16823 End-of-file error while saving data to disk. The subErrorID property may provide more information. 
16824 Invalid update descriptor. The subErrorID property may provide more information. 
16825 The update file contains an application with a different application ID. 
16826 The update file does not contain a newer version of the application. 
16827 The version contained in the update file does not match the version from the update descriptor. 
16828 Cannot update application, usually because the application is running in the AIR Debug Launcher (ADL). 
16829 Missing update file at install time. 

---------------------------------------------------------------------------------------------*/

package br.com.desenv.framework.updater
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.display.DisplayObject;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	
	import mx.core.FlexGlobals;
	import mx.managers.PopUpManager;
	
	import air.update.ApplicationUpdaterUI;
	import air.update.events.DownloadErrorEvent;
	import air.update.events.StatusUpdateErrorEvent;
	import air.update.events.UpdateEvent;
	
	import br.com.desenv.framework.component.visual.NepalignUpdateView;
	import br.com.desenv.framework.util.DsvObjectUtil;

	/**
	 * @author Lucas
	 */
	public class NepalignUpdater
	{
		private var appUpdater:ApplicationUpdaterUI = new ApplicationUpdaterUI();
		private var nepalignUpdateView:NepalignUpdateView;

		public function NepalignUpdater()
		{
			//.ctor	
		}
		
		public function checkUpdate():void
		{
			if(DsvObjectUtil.getSupportedProfiles() == "desktop")
				checkUpdateDesktop();
			else if(DsvObjectUtil.getSupportedProfiles() == "extendedDesktop")
				checkUpdateExtendedDesktop();
		}

		/**
		 * Atualiza o sistema a partir da API ApplicationUpdaterUI da Adobe.
		 * 
		 * SOMENTE PARA APLICAÇÕES supportedProfiles = desktop
		 */
		private function checkUpdateDesktop():void 
		{
			//Se o parâmetro da URL do XML do atualizador não for encontrado, o processo é abortado.
			if(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES") == null)
				return;

			//Carrega o XML do atualizador
			var nepalignUpdateXmlLoader:URLLoader = new URLLoader();
			nepalignUpdateXmlLoader.addEventListener(Event.COMPLETE, processXML);
			nepalignUpdateXmlLoader.load(new URLRequest(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES") as String));
			
			function processXML(e:Event):void 
			{
				nepalignUpdateXmlLoader.removeEventListener(Event.COMPLETE, processXML);
				
				//Passa o valor do XML obtido para o método checkNow
				openApplicationUpdaterUI(new XML(e.target.data));
			}
		}
		
		private function openApplicationUpdaterUI(nepalignUpdateXml:XML):void
		{
			//versão minima para funcionamento estável do sistema definido pela desenvolvedor
			var versaoMinima:String = nepalignUpdateXml.children().length() == 0x05 ? nepalignUpdateXml.children()[0x04].text() : null;
			//versao atual do sistema 
			var currentVersion:String = DsvObjectUtil.getAppVersion();
			
			appUpdater.updateURL = FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES") as String;
			//Despachado após a inicialização ter sido concluída.
			appUpdater.addEventListener(UpdateEvent.INITIALIZED, onUpdate);
			//Despachado quando tiver ocorrido um erro durante a inicialização ou durante o processo de atualização (se ocorrer algo inesperado).
			appUpdater.addEventListener(ErrorEvent.ERROR, onError);
			//Despachado se houver um erro durante a conexão ou o download do arquivo de atualização.
			appUpdater.addEventListener(DownloadErrorEvent.DOWNLOAD_ERROR, onDownloadEror);
			//	Despachado caso ocorra um erro ao tentar baixar ou analisar o arquivo de descritor de atualização.
			appUpdater.addEventListener(StatusUpdateErrorEvent.UPDATE_ERROR, onUpdateError);
			//Permite a visibilidade das caixas de diálogo Verificar atualizações, Nenhuma atualização e Erro de atualização.
			appUpdater.isCheckForUpdateVisible = false;
			//Permite a visibilidade das caixas de diálogo Atualização de arquivo, Nenhuma atualização de arquivo e Erro de arquivo.
			appUpdater.isFileUpdateVisible = false;
			//Permite a visibilidade da caixa de diálogo Instalar atualização.
			appUpdater.isInstallUpdateVisible = false;
			//Permite a visibilidade da caixa de diálogo Baixar atualização.
			//Parâmetro criado para esconder a atualização do sistema, caso seja necessário 
			appUpdater.isDownloadUpdateVisible = !(versaoMinima != null && parseFloat(currentVersion.replace("\.", "")) < parseFloat(versaoMinima.replace("\.", "")));
			
			//Inicializa o processo de atualização
			appUpdater.initialize();   
		}
		
		private function onUpdate(event:UpdateEvent):void 
		{
			var nepalignUpdateXmlLoader:URLLoader = new URLLoader();
			nepalignUpdateXmlLoader.addEventListener(Event.COMPLETE, processXML);
			nepalignUpdateXmlLoader.load(new URLRequest(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES") as String));
			
			function processXML(e:Event):void 
			{
				nepalignUpdateXmlLoader.removeEventListener(Event.COMPLETE, processXML);
				
				var applicationProfile:String = DsvObjectUtil.getSupportedProfiles();
				var updateProfile:String = new XML(e.target.data).children()[3].text();

				//Verifica se os profiles da aplicação e da atualização são iguais
				if(applicationProfile != updateProfile)
					return; 
				
				appUpdater.checkNow();
			}
		}
		
		private function onDownloadEror(event:DownloadErrorEvent):void
		{
			syserr("Download Error", event.subErrorID, "URL_DESCRITOR_ATUALIZACOES > " + FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES"));
		}
		
		private function onUpdateError(event:StatusUpdateErrorEvent):void
		{
			syserr("Update Error", event.subErrorID, "URL_DESCRITOR_ATUALIZACOES > " + FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES"));
		}
		
		private function onError(event:ErrorEvent):void 
		{
			syserr("Error on Update", event.toString(), "URL_DESCRITOR_ATUALIZACOES > " + FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES"));
		}
		
		/**
		 * Verifica se o XML do atualizador está disponível, se sim, carrega o XML e continua o processo.
		 * 
		 * SOMENTE PARA APLICAÇÕES supportedProfiles = extendedDesktop
		 */
		private function checkUpdateExtendedDesktop():void
		{
			//Se o parâmetro da URL do XML do atualizador não for encontrado, o processo é abortado.
			if(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES") == null)
				return;
			
			//Carrega o XML do atualizador
			var nepalignUpdateXmlLoader:URLLoader = new URLLoader();
			nepalignUpdateXmlLoader.addEventListener(Event.COMPLETE, processXML);
			nepalignUpdateXmlLoader.load(new URLRequest(FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_DESCRITOR_ATUALIZACOES") as String));
			
			function processXML(e:Event):void 
			{
				nepalignUpdateXmlLoader.removeEventListener(Event.COMPLETE, processXML);
				
				//Passa o valor do XML obtido para o método checkNow
				checkNow(new XML(e.target.data));
			}
		}
		
		/**
		 * Verifica as condições necessárias para fazer a atualização. 
		 */
		private function checkNow(nepalignUpdateXml:XML):void
		{
			//Verifica se os profiles da aplicação e da atualização são iguais
			var applicationProfile:String = DsvObjectUtil.getSupportedProfiles();
			var updateProfile:String = nepalignUpdateXml.children()[3].text();
			//versão minima para funcionamento estável do sistema definido pela desenvolvedor
			var versaoMinima:String = nepalignUpdateXml.children().length() == 0x05 ? nepalignUpdateXml.children()[0x04].text() : null;
			
			if(applicationProfile != updateProfile)
				return; 
			
			//Recupera os valores da versão atual e da última versão
			var currentVersion:String = DsvObjectUtil.getAppVersion();
			var lastVersion:String = nepalignUpdateXml.children()[0].text();
			
			//Se as versões foram iguais, aborta o processo.
			if(currentVersion == lastVersion)
				return;
			
			var currentVersionDetails:Array = currentVersion.split("\.");
			var lastVersionDetails:Array = lastVersion.split("\.");
			
			//Recupera a quantidade de casas do número da versão (X.X.X) = 3 casas.
			var currentVersionCases:int = currentVersionDetails.length;
			var lastVersionCases:int = lastVersionDetails.length;	
			
			//Flag para fazer a atualização.
			var update:Boolean = false;
			//Flag para forçar a atualização
			var force:Boolean = false;
			
			//verifica se a versão atual é menor que a versão minima. se for já pede a atualização forçada do sistema
			if(versaoMinima != null && parseFloat(currentVersion.replace("\.", "")) < parseFloat(versaoMinima.replace("\.", "")))
				update = force = true;
			//Verifica se o número de casas da versão atual é menor do que a da última versão.
			if(currentVersionCases < lastVersionCases)
				update = true;
			else if(currentVersionCases == lastVersionCases)
			{
				//Percorre todas as casas do numero da versao atual.
				for (var caseIndex:int = 0; caseIndex < currentVersion.length; caseIndex++)
				{
					// Verifica se o numero da casa atual é menor do que a da ultima versao.
					// 1.5.9 < 1.6.0
					if(Number(currentVersionDetails[caseIndex]) < Number(lastVersionDetails[caseIndex]))
					{
						update = true;
						break;
					}	
				}
			}
			
			//Se a atualização realmente existir, abre a tela pedindo a confirmação da atualização para o usuário.
			if(update)
			{
				nepalignUpdateView = new NepalignUpdateView();
				nepalignUpdateView.currentVersionNumber = currentVersion;
				nepalignUpdateView.lastVersionNumber = lastVersion;
				nepalignUpdateView.minimumVersion = versaoMinima;
				nepalignUpdateView.notesUrl = (nepalignUpdateXml.children()[1].text());
				nepalignUpdateView.notesUrl = nepalignUpdateView.notesUrl.substring(0, nepalignUpdateView.notesUrl.lastIndexOf("/"));
				nepalignUpdateView.force = force;
				
				nepalignUpdateView.addEventListener("UpdateNow", function(event:Event):void { makeUpdate(nepalignUpdateXml.children()[1].text()) });
				nepalignUpdateView.addEventListener("UpdateLater", function(event:Event):void {  PopUpManager.removePopUp(nepalignUpdateView); nepalignUpdateView = null; });
				
				PopUpManager.addPopUp(nepalignUpdateView, FlexGlobals.topLevelApplication as DisplayObject, force);
				FlexGlobals.topLevelApplication.centralizarJanela(nepalignUpdateView);
			}
		}
		
		/**
		 * Faz o download do pacote de atualização a partir da URL updatePackageURL passada como parâmetro
		 */
		private function makeUpdate(updatePackageURL:String):URLStream
		{
			if(nepalignUpdateView != null)
				nepalignUpdateView.changeToUpdating();
			
			var updatePackageDownloadStream:URLStream = new URLStream();
			updatePackageDownloadStream.addEventListener(Event.COMPLETE, updatePackageDownloadComplete);
			updatePackageDownloadStream.addEventListener(ProgressEvent.PROGRESS, function(event:ProgressEvent):void { nepalignUpdateView.setCurrentProgress(event.bytesLoaded, event.bytesTotal); });
			updatePackageDownloadStream.load(new URLRequest(updatePackageURL));
			
			return updatePackageDownloadStream;
		}
		
		/**
		 * Cria e executa pacote de atualização na máquina do cliente na pasta Local Store localizada na %appdata%/br.com.desenv.nepalign/
		 * 
		 * Logo após a execução a aplicação é fechada para prosseguir com a instação
		 */
		private function updatePackageDownloadComplete(event:Event):void 
		{ 
			var updatePackageBytes:ByteArray = new ByteArray();
			(event.target as URLStream).readBytes(updatePackageBytes);
			
			//Cria a referência do arquivo do pacote de atualização na pasta %appdata%/br.com.desenv.nepalign/Local Store/
			var updatePackage:File = File.documentsDirectory; 
			updatePackage = File.applicationStorageDirectory.resolvePath("Nepal.exe");  
			
			//Se já existir um pacote de instalação, excluir para escrever o novo pacote.
			if(updatePackage.exists)
				updatePackage.deleteFile();
			
			//Escreve os bytes recebidos do download do pacote em um arquivo (updatePackage)
			var updatePackageFileStream:FileStream = new FileStream();
			updatePackageFileStream.open(updatePackage, FileMode.WRITE);
			updatePackageFileStream.writeBytes(updatePackageBytes);
			updatePackageFileStream.close(); 
			
			var updatePackageProcessInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			updatePackageProcessInfo.executable = updatePackage;

			//Inicia o pacote de instação
			var updatePackageProcess:NativeProcess = new NativeProcess();
			updatePackageProcess.start(updatePackageProcessInfo);
			
			//Fecha a aplicação para continuar com a atualização
			FlexGlobals.topLevelApplication.closeApplication();
		}
	}	
}