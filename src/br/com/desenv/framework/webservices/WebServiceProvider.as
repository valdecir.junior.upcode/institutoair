package br.com.desenv.framework.webservices
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;

	/**
	 * Usada para fazer a conexão com o WS de Login
	 * @author lucas.leite
	 * @langversion ActionScript 3.0
	 */
	public class WebServiceProvider extends EventDispatcher
	{
		public static const WEBSERVICEPROVIDER_RESULT:String = "WEBSERVICEPROVIDER_RESULT";
		
		public function WebServiceProvider()
		{
			
		}
		
		/**
		 * Realiza a chamada para a Resource do WS
		 * @param body Body da mensagem (conteúdo JSON ou text/plain)
		 * @param requestMethod Método de requisição (POST/GET/PUT/DELETE)
		 * @param url URL da Resource da API
		 * @author lucas.leite
		 * @langversion ActionScript 3.0
		 */
		public function call(url:*, body:* = undefined, requestMethod:String = URLRequestMethod.GET):void
		{
			var request:URLRequest; 
			
			if(url is String)
				request = new URLRequest(url as String);
			else if(url is URLRequest)
				request = url as URLRequest;
	
			if(request.requestHeaders == null || request.requestHeaders.length == 0x00000)
				request.requestHeaders=[new URLRequestHeader("Content-Type", "application/json")];

			request.method = requestMethod;
			
			if(body != undefined)
				request.data = body;
			
			var loader:URLLoader=new URLLoader();
			loader.addEventListener(Event.COMPLETE, receive);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, notAllowed);
			loader.addEventListener(IOErrorEvent.IO_ERROR, notFound);
			loader.load(request);
		}
		
		protected function receive(event:Event):void
		{
			dispatchEvent(new DataEvent(WEBSERVICEPROVIDER_RESULT, false, false, event.target.data));
		}
		
		protected function notAllowed(event:SecurityErrorEvent):void
		{
			dispatchEvent(event);
		}
		
		protected function notFound(event:IOErrorEvent):void
		{
			dispatchEvent(event);
		}
	}	
}