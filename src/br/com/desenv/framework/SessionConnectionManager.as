package br.com.desenv.framework
{
	import flash.display.Sprite;
	import flash.events.StatusEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.events.CloseEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import air.net.URLMonitor;
	
	import br.com.desenv.framework.util.DsvParametrosSistema;

	/**
	 * Classe de gerenciamento de conectividade com o servidor
	 * @author lucas.leite
	 * @langversion ActionScript 3.0
	 */
	public class SessionConnectionManager
	{
		private var serverMonitor:URLMonitor;
		private var connectionTimer:Timer;
		private var amfKeepAliveTimer:Timer;
		
		[Bindable]
		private var alertOnNetworkDown:Alert;
		
		/**
		 * Verifica conectividade com o MessageBroker de 5 em 5 segundos.
		 * 
		 * *** Inutilizado
		 * @langversion ActionScript 3.0
		 */
		public function runConnectionMonitor():void
		{
			connectionTimer = new Timer(5000);
			connectionTimer.addEventListener(TimerEvent.TIMER, connectionTimerTick);
		}
		
		protected function connectionTimerTick(event:TimerEvent):void
		{
			serverMonitor = new URLMonitor(new URLRequest(FlexGlobals.topLevelApplication.sessionInfo.amfChannelUrl));
			serverMonitor.addEventListener(StatusEvent.STATUS, netConnectivityServerMonitor);
			serverMonitor.start();
		}	
		
		public function startAMFKeepAlive():void
		{
			amfKeepAliveTimer = new Timer(5000);
			amfKeepAliveTimer.addEventListener(TimerEvent.TIMER, amfKeepAliveTick);
		}
		
		protected function amfKeepAliveTick(event:TimerEvent):void
		{
			//ping
			FlexGlobals.topLevelApplication.montarObjetoRemoto("pongService", function(event:ResultEvent):void {}, function(event:FaultEvent):void {}).getOperation("pong").send();			
		}
		
		public function destroyAMFKeepAlive():void
		{
			if(amfKeepAliveTimer != null)
				amfKeepAliveTimer.stop();
		}
		
		protected function netConnectivityServerMonitor(e:StatusEvent):void 
		{
			serverMonitor.stop();
			
			if(!(e.target as URLMonitor).available)
			{
				if(FlexGlobals.topLevelApplication.sessionInfo != null)
				{
					Alert.show("Sua conexão com o servidor foi perdida! Não feche o sistema para não perder os dados não salvos!", "Mensagem", Alert.OK, FlexGlobals.topLevelApplication as Sprite, closeEventNetworkChangedAlert);
					
					connectionTimer.stop();	
				}
			}
		}
		
		/**
		 * Executado a cada mudança de network detectada pelo AdobeAir
		 * 
		 * @langversion ActionScript 3.0
		 */
		public function networkDownHook():void
		{
			//Despreza o hook caso não esteja logado
			if(FlexGlobals.topLevelApplication.sessionInfo == null)
				return;
			
			var serverIp:String = (FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("IP_SERVIDOR") as String;
			
			if(serverIp == null)
			{
				if((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDORES_LOGIN") != null &&
					((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDORES_LOGIN") as String) != "")
					serverIp = ((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDORES_LOGIN") as String).split("/")[0x00002];
			}
			
			if(serverIp != null)
			{
				var monitor:URLMonitor = new URLMonitor(new URLRequest("http://" + serverIp + "/nepalign/messagebroker/amf/"));
				monitor.addEventListener(StatusEvent.STATUS, netConnectivity);
				monitor.start();	
			}
		}
		
		protected function netConnectivity(e:StatusEvent):void 
		{
			if(!(e.target as URLMonitor).available && FlexGlobals.topLevelApplication.sessionInfo != null && FlexGlobals.topLevelApplication.loginForm == null && alertOnNetworkDown == null)
				alertOnNetworkDown = Alert.show("Sua conexão com a internet foi perdida! Não feche o sistema para não perder os dados não salvos!", "Mensagem", Alert.OK, FlexGlobals.topLevelApplication as Sprite, closeEventNetworkChangedAlert);	
			
			(e.target as URLMonitor).stop();
		}
		
		private function closeEventNetworkChangedAlert(event:mx.events.CloseEvent):void
		{
			alertOnNetworkDown = null; 
			
			if(FlexGlobals.topLevelApplication.sessionInfo != null && FlexGlobals.topLevelApplication.loginForm == null)
				FlexGlobals.topLevelApplication.displayLogin(true);
		}		
	}
}