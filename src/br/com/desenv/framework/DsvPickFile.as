package br.com.desenv.framework
{
	import flash.events.Event;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.core.UIComponent;

	/**
	 * @author Lucas
	 */
	public class DsvPickFile extends UIComponent
	{
		/**
		 * File Byte Source.
		 * Can be used to pick byteArray of choosen file.
		 * If choosen file is null (Why the component is never used by user or another possibilits) this variable returns null;
		 */
		[Bindable]
		protected var _source:ByteArray = null;
		
		/**
		 * Used by component core.
		 * Is used to reference an chosen file. 
		 */
		protected var _fileReference:FileReference = new FileReference();
		
		/**
		 * Used by FileReference
		 * Can be used to choose a specific or default file types.
		 */
		protected var _fileTypeFilter:FileFilter = new FileFilter("Files", "*"); // Default File Filter (Can be modified);
		
		/**
		 * @default Default Getter and Setters
		 */
		public function get source():ByteArray
		{
			return this._source;
		}
		
		public function set source(value:ByteArray):void
		{
			this._source = value;
		}
		
		public function get fileReference():FileReference
		{
			return this._fileReference;
		}
		
		public function get fileTypeFilter():FileFilter
		{
			return this._fileTypeFilter;
		}
		
		public function set fileTypeFilter(value:FileFilter):void
		{
			this._fileTypeFilter = value;
		}
		
		/**
		 * @default Default Class Constructor.
		 * Declares File Reference Events.
		 */
		public function DsvPickFile()
		{
			this.fileReference.addEventListener(Event.SELECT, this.fileReferenceSelectHandler);
			this.fileReference.addEventListener(Event.COMPLETE, this.fileReferenceCompleteHandler);
		}
		
		/**
		 * Event Dispatched when file is selected.
		 */
		protected function fileReferenceSelectHandler(event:Event):void
		{
			this.fileReference.load();
		}
		
		/**
		 * Event Dispatched when file loading is complete.
		 */
		protected function fileReferenceCompleteHandler(event:Event):void
		{
			this.source = this.fileReference.data;
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		/**
		 * Used to open "Choose File Dialog"
		 */
		public function openDialog():void
		{
			var typeFileArrBuffer:Array = [];
			typeFileArrBuffer.push(this.fileTypeFilter);
			this.fileReference.browse(typeFileArrBuffer);
		}
	}	
}