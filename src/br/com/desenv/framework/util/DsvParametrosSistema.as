package br.com.desenv.framework.util
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;
	
	import mx.messaging.FlexClient;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import br.net.upcode.framework.amf.AMFConnector;
	
	[Event(name="ParametrosLocaisAtualizados", type="flash.events.Event")]
	[Event(name="LocalParametersNotFound", type="flash.events.Event")]
	[Event(name="LocalParametersLoaded", type="flash.events.Event")]
	public class DsvParametrosSistema extends EventDispatcher
	{
		private var parametros:Array = null;
		public var priorizarParametrosLocais:Boolean = true;
	
		public function DsvParametrosSistema(autoInit:Boolean = true)
		{
			if(autoInit)
			{
				sysout("Auto init is true. Initializing");
				init();	
			}
		}
		
		/**
		 * Inicializa a classe, carregando os parâmetros
		 */
		public function init():void
		{
			carregaParametros();
		}
		
		/**
		 * Recupera o Array dos parâmetros
		 */
		public function get getParametros():Array
		{
			return this.parametros;
		}
		
		/**
		 * Recupera um parâmetro por Nome
		 */
		public function getParametro(value:String):Object
		{
			try
			{
				if(this.parametros == null)
					return null;	
				
				return this.parametros[value.toUpperCase()];		
			}
			catch(e:Error)
			{
				return null;
			}
			return null;
		}
		
		/**
		 * Recupera um parâmetro por Nome, Empresa
		 * Id da Empresa, Nome do Parâmetro
		 */
		public function getParametroPorEmpresa(idEmpresa:Number, value:String):Object
		{
			if(this.parametros == null)
				return null;
			
			if((idEmpresa.toString() + "_" + value).toUpperCase() in this.parametros)
				return this.parametros[(idEmpresa.toString() + "_" + value).toUpperCase()];
			else
				return null;
		}
		
		/**
		 * Adiciona um novo parâmetro à lista de parâmetros
		 */
		public function addParameter(parameterName:String, parameterValue:String):void
		{
			if(this.parametros == null)
				this.parametros = new Array();
			
			this.parametros[parameterName] = parameterValue;
		}
		
		/**
		 * Carrega os parâmetros fornecido pelo servidor
		 */
		private function carregaParametros():void
		{
			sysout("Loading Server Parameters");
			try
			{
				AMFConnector.instance.prepararObjetoRemoto("dsvParametrosSistemaService", carregaParametrosResultHandler, carregarParametrosFaultHandler).getOperation("listaParametrosCarregados").send();

				FlexClient.getInstance().id = null;
			}
			catch(e:Error)
			{
				syserr("Erro ao carregar os parametros!", e);
			}
		}

		private function carregaParametrosResultHandler(event:ResultEvent):void
		{
			try
			{
				if(this.parametros == null)
					this.parametros = new Array();
				
				for (var p:String in event.result)
				{
					if(priorizarParametrosLocais)
					{
						if(this.parametros[p] == null)
							this.parametros[p] = event.result[p];	
					}
					else
						this.parametros[p] = event.result[p];
				}
				
				dispatchEvent(new Event("ParametrosCarregados"));	
			}
			catch(e:Error)
			{
				syserr("Error on Handing Server Parameters.", e);
			}
			finally
			{
			}
			sysout("Server Parameters Loaded.");
		}
		
		private function carregarParametrosFaultHandler(event:FaultEvent):void
		{
			try
			{
				syserr("Error on load Server Parameters : " + event.fault.faultDetail);	
			}
			finally
			{
			}
		}

		/**
		 * Carrega os parâmetros do arquivo local c:/desenv/ParametrosSistema.NEPAL
		 * 
		 * 
		 * @langversion ActionScript 3.0
		 */
		public function carregarParametrosLocais(_params:String = null):void
		{
			sysout("Loading Local Parameters");
			try
			{
				if(this.parametros == null)
					this.parametros = new Array();
				
				var params:String = null;
				
				//mac /Users/<usuario>/Library/Preferences/br.com.desenv.nepalign/Local Store/
				var paramFile:File = File.applicationStorageDirectory.resolvePath("ParametrosSistema.UPCODE");
				
				// copy old content from %appdata%/{package}.{appid} to c:/desenv/ if windows
				if(paramFile.exists && Capabilities.os.indexOf("Windows") >= 0x00)
				{
					var newParamFile:File = new File("c:/upcode/ParametrosSistema.UPCODE");
					
					if(!newParamFile.exists)
					{
						var fileStream:FileStream = new FileStream();
						fileStream.open(newParamFile, FileMode.WRITE);
						fileStream.writeUTFBytes(DsvObjectUtil.readTextFile(paramFile.nativePath));
						fileStream.close();	
					}
					
					params = DsvObjectUtil.readTextFile("c:/upcode/ParametrosSistema.UPCODE");
				}
				else if(!paramFile.exists && Capabilities.os.indexOf("Windows") >= 0x00 && new File("c:/upcode/ParametrosSistema.UPCODE").exists)
					params = DsvObjectUtil.readTextFile("c:/upcode/ParametrosSistema.UPCODE");
				else if(_params != null)
				{
					var newParamFileLocale:File;
					
					if(Capabilities.os.indexOf("Windows") >= 0x00)
						newParamFileLocale = new File("c:/upcode/ParametrosSistema.UPCODE");
					else
						newParamFileLocale = File.applicationStorageDirectory.resolvePath("ParametrosSistema.UPCODE");
					
					var fileStreamLocale:FileStream = new FileStream();
					fileStreamLocale.open(newParamFileLocale, FileMode.WRITE);
					fileStreamLocale.writeUTFBytes(_params);
					fileStreamLocale.close();	
					
					params = _params;
				}
				else
				{
					if(Capabilities.os.indexOf("Windows") >= 0x00)
						paramFile = new File("c:/upcode/ParametrosSistema.UPCODE");
					else
						paramFile = File.applicationStorageDirectory.resolvePath("ParametrosSistema.UPCODE");
					
					params = DsvObjectUtil.readTextFile(paramFile.nativePath);
				}

				if(params != null)
				{
					for each(var param:String in params.split("\r\n"))
					{
						if(param.substring(0, 2) == "//")
							continue;
						
						this.parametros[param.split("=")[0x0]] = param.split("=")[0x1];
					}
					dispatchEvent(new Event("LocalParametersLoaded"));
				}
				else
					dispatchEvent(new Event("LocalParametersNotFound"));
			}
			catch(e:Error)
			{
				syserr("Error on Load Local Parameters\n", e);
			}
		}
		
		/**
		 * Finaliza a classe
		 */
		public function dispose():void
		{
			sysout("Disposing DsvParametrosSistema");
			try
			{
				parametros.length = 0;
				parametros = null;
				
				sysout("Disposed!");
			}
			catch(e:Error)
			{
				sysout("Error on Dispose DsvParametrosSistema");
			}
		}
	}	
}