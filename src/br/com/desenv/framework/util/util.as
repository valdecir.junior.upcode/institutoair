import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.controls.dataGridClasses.DataGridColumn;
import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.formatters.DateFormatter;
import mx.managers.ToolTipManager;
import mx.messaging.Channel;
import mx.messaging.events.ChannelEvent;
import mx.messaging.ChannelSet;
import mx.messaging.channels.AMFChannel;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.rpc.remoting.mxml.RemoteObject;
import mx.utils.ObjectUtil;

import spark.events.TitleWindowBoundsEvent;

import br.com.desenv.framework.util.DsvObjectUtil;

import br.com.desenv.framework.util.IgnReportUtil;

[Bindable]
private var channelSet:ChannelSet = new ChannelSet();
private var customChannel:Channel;
[Bindable]
private var amfPollingChannel:Channel;

[Bindable]
public var dsvObjectUtil:DsvObjectUtil = new DsvObjectUtil();
public var ignReportUtil:IgnReportUtil = new IgnReportUtil();

public function getCurrentChannelSet():ChannelSet
{
	return this.channelSet;
}

public function showErrorImmediately(target:UIComponent):void
{
	// we have to callLater this to avoid other fields that send events
	// that reset the timers and prevent the errorTip ever showing up.
	target.callLater(showDeferred, [target]);
}

private function showDeferred(target:UIComponent):void
{
	var oldShowDelay:Number = ToolTipManager.showDelay;
	ToolTipManager.showDelay = 0;
	if (target.visible)
	{
		// try popping the resulting error flag via the hack 
		// courtesy Adobe bug tracking system
		target.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OUT));
		target.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OVER));
	}
	ToolTipManager.showDelay = oldShowDelay;
}

public function clearErrorImmediately(target:UIComponent):void
{
	target.callLater(clearDeferred, [target]);
}

private function clearDeferred(target:UIComponent):void
{
	var oldDelay:Number = ToolTipManager.hideDelay;
	ToolTipManager.hideDelay = 0;
	if (target.visible)
	{
		// clear the errorTip
		try
		{
			target.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OVER));
			target.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OUT));
		}
		catch (e:Error)
		{
			// sometimes things aren't initialized fully when we try that trick
		}
	}
	ToolTipManager.hideDelay = oldDelay;
}

private function prepararConsumer(service: String, messageFunction: Function = null, faultFunction:Function = null):mx.messaging.Consumer
{
	if(customChannel == null)
	{
		amfPollingChannel = new AMFChannel("my-polling-amf", FlexGlobals.topLevelApplication.sessionInfo.amfChannelPollingUrl);
		trace("[NPL] [" + new Date().toUTCString() + "] Montando Polling AMFChannel Global com URL " + FlexGlobals.topLevelApplication.sessionInfo.amfChannelPollingUrl);
		amfPollingChannel.requestTimeout  = 60000;
		amfPollingChannel.addEventListener(ChannelEvent.DISCONNECT, channelDisconnect);
		channelSet.addChannel(amfPollingChannel);
	}

	var consumer:mx.messaging.Consumer = new mx.messaging.Consumer();
	consumer.channelSet = channelSet;
	consumer.destination = service + ".polling";
	consumer.addEventListener(mx.messaging.events.MessageEvent.MESSAGE, messageFunction);
	consumer.addEventListener(mx.messaging.events.MessageFaultEvent.FAULT, faultFunction);

	return consumer;
}

private function prepararObjetoRemoto(service: String, funcaoOk: Function = null, funcaoFalha:Function = null, showBusyCursor:Boolean = true): mx.rpc.remoting.mxml.RemoteObject
{
	if(customChannel == null)
	{
		customChannel = new AMFChannel("my-amf", FlexGlobals.topLevelApplication.sessionInfo.amfChannelUrl);
		trace("[NPL] [" + new Date().toUTCString() + "] Montando AMFChannel Global com URL " + FlexGlobals.topLevelApplication.sessionInfo.amfChannelUrl);
		customChannel.requestTimeout  = 60000;
		customChannel.addEventListener(ChannelEvent.DISCONNECT, channelDisconnect);
		channelSet.addChannel(customChannel);
	}
		
	var remoteObject: RemoteObject = new RemoteObject(service);
	remoteObject.channelSet = channelSet;
	
	remoteObject.showBusyCursor = showBusyCursor;
	
	if(funcaoFalha != null)
		remoteObject.addEventListener(FaultEvent.FAULT, funcaoFalha);
	else
		remoteObject.addEventListener(FaultEvent.FAULT, falhar);
	
	if (funcaoOk != null)
	{
		remoteObject.addEventListener(ResultEvent.RESULT, funcaoOk);
	}

	FlexGlobals.topLevelApplication.sessionInfo.ultimaAtividade = new Date();
	
	return remoteObject;
		
}

private function channelDisconnect(event:ChannelEvent):void
{
		
}

private function falhar(event: FaultEvent): void 
{
	Alert.show(event.fault.faultString.replace("java.lang.Exception : ", "").replace("br.com.desenv.frameworkignorante.DsvException : ", ""), "AVISO");

	syserr(event.fault.faultString);
}

private function faultHandler( event: FaultEvent ): void 
{	
	Alert.show("FALHA 2" + event.fault.faultString);
	
	syserr(event.fault.faultString);
}

private function formatDate(item:Object,column:DataGridColumn):String
{
	var dateformatter:DateFormatter = new DateFormatter();

	dateformatter.formatString = "DD/MM/YYYY";
	return dateformatter.format(item[column.dataField]);
}

private function formatDateTime(item:Object,column:DataGridColumn):String
{
	var dateformatter:DateFormatter = new DateFormatter();
	
	dateformatter.formatString = "DD/MM/YYYY HH:NN";
	return dateformatter.format(item[column.dataField]);
}

private function formatTime(item:Object,column:DataGridColumn):String
{
	var dateformatter:DateFormatter = new DateFormatter();
	
	dateformatter.formatString = "HH:NN:SS";
	return dateformatter.format(item[column.dataField]);
}

public function centerWindow(janela:*):void
{
	janela.x = FlexGlobals.topLevelApplication.width/2 - janela.width/2; //popup x coord
	janela.y = FlexGlobals.topLevelApplication.height/2 - janela.height/2; //popup y coord
}
public function centerAlert(janela: Alert):void
{
	janela.x = FlexGlobals.topLevelApplication.width/2 - janela.width/2; //popup x coord
	janela.y = FlexGlobals.topLevelApplication.height/2 - janela.height/2; //popup y coord
}
public function moveWindow(evt:TitleWindowBoundsEvent):void 
{
	if (evt.afterBounds.left < 0) 
	{
		evt.afterBounds.left = 0;	
	} 
	else if (evt.afterBounds.right > FlexGlobals.topLevelApplication.systemManager.getVisibleApplicationRect().width) 
	{
		evt.afterBounds.left = FlexGlobals.topLevelApplication.systemManager.getVisibleApplicationRect().width - evt.afterBounds.width;
	}
	
	if (evt.afterBounds.top < 0) 
	{
		evt.afterBounds.top = 0;
	} 
	else if (evt.afterBounds.bottom > FlexGlobals.topLevelApplication.systemManager.getVisibleApplicationRect().height) 
	{
		evt.afterBounds.top = FlexGlobals.topLevelApplication.systemManager.getVisibleApplicationRect().height - evt.afterBounds.height;
	}
}

public function trimStart(value:String, char:String):String
{
	while(value.charAt(0) == char)
	{
		value = value.substring(1, value.length);
	}
	
	return value;
}

public function desserizalizarObjeto(obj:Object, allowNull:Boolean = false):ArrayCollection
{
	var arrayData:ArrayCollection = new ArrayCollection();
	
	try
	{	
		if(obj == null)
			throw new Error("NullPointerError");
		
		var objBuffer:Object = ObjectUtil.getClassInfo(obj);
		
		arrayData.addItem("classalias=" + objBuffer.alias);
		
		for each(var field:QName in objBuffer.properties)
		{
			if(obj[field] is Number)
			{
				if (!isNaN(obj[field]))
				{
					arrayData.addItem(field.localName + "=" + obj[field]);
				}
				else if(isNaN(obj[field]) && allowNull)
				{
					arrayData.addItem(field.localName + "=" + obj[field]);
				}
			}
			else if (obj[field] is String)
			{
				if ((obj[field] as String) != "")
				{
					arrayData.addItem(field.localName + "=" + obj[field]);
				}
				else if((obj[field] as String) == "" && allowNull)
				{
					arrayData.addItem(field.localName + "=" + obj[field]);
				}
			}
			else
			{
				if(obj[field] != null && obj[field].id != null)
				{
					arrayData.addItem(field.localName + "=" + obj[field].id);
				}
			}
		}
	}
	catch(e:Error)
	{
		throw e;
	}
	return arrayData;
}