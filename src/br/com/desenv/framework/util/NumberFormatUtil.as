package br.com.desenv.framework.util
{
			
	import spark.formatters.CurrencyFormatter;

	public class NumberFormatUtil
	{
		public static function arredondar(valor:Number, precisao:int):Number
		{
			var potencia:int = Math.pow(10, precisao);
			return Math.round(valor * potencia)/potencia;
		}
		public static function truncar(valor:Number, precisao:int):Number
		{
			return Number(valor.toFixed(2));
		}
		
		public static function formatarDecimal(valor:Number):String
		{
			var formatter:spark.formatters.CurrencyFormatter = new CurrencyFormatter();
			formatter.decimalSeparator = ",";
			//formatter.useCurrencySymbol = true;
			return formatter.format(valor);
		}
	}
}