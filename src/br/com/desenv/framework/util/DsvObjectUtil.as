package br.com.desenv.framework.util
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SampleDataEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.globalization.NumberFormatter;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.FlexGlobals;
	import mx.formatters.DateFormatter;
	import mx.managers.SystemManager;

	/**
	 * @author Lucas
	 */
	[Event(name="checkNetworkError", type="flash.events.Event")]
	[Event(name="checkNetworkSuccess", type="flash.events.Event")]
	public class DsvObjectUtil extends EventDispatcher
	{
		public static const CHECK_NETWORK_EVENT_SUCCESS:String = "checkNetworkSuccess";
		public static const CHECK_NETWORK_EVENT_ERROR:String = "checkNetworkError";
		
		public var networkActive:Boolean = false;
		
		public static function setSelectedItemById(id:Number, combo:*):void
		{
			for(var i:int = 0; i < combo.dataProvider.length; i++)
			{
				var obj:* = combo.dataProvider.getItemAt(i);
				
				if(obj.id == id)
					combo.selectedIndex = i;
			}
		}
		
		public static function setSelectedItemByCode(code:String, combo:*):void
		{
			for(var i:int = 0; i < combo.dataProvider.length; i++)
			{
				var obj:* = combo.dataProvider.getItemAt(i);
				
				if(obj.codigo == code)
					combo.selectedIndex = i;
			}
		}
		
		public static function contains(array:*, obj:*):Boolean
		{
			for(var i:int = 0; i < array.length; i++)
			{
				var arrayObj:* = array.getItemAt(i);
				
				if(arrayObj.id == obj.id)
					return true;
			}
			
			return false;
		}
		public static function containsNumber(array:*, obj:Number):Boolean
		{
			for(var i:int = 0; i < array.length; i++)
			{
				var arrayObj:Number = array.getItemAt(i) as Number;
				
				if(arrayObj == obj)
					return true;
			}
			
			return false;
		}
		public static function removeById(array:*, obj:*):void
		{
			for(var i:int = 0; i < array.length; i++)
			{
				var arrayObj:* = array.getItemAt(i);
				
				if(arrayObj.id == obj.id)
					array.removeItemAt(i);
			}
		}
		
		public static function removeIfContains(array:*, obj:*):void
		{
			if(contains(array, obj))
				removeById(array, obj);
		}
		
		public static function updateItemOnArray(array:*, obj:*):void
		{
			removeIfContains(array, obj);
			array.addItem(obj);
		}
		
		public static function getById(array:*, obj:*):*
		{
			for(var i:int = 0; i < array.length; i++)
			{
				var arrayObj:* = array.getItemAt(i);
				
				if(arrayObj.id == obj)
					return arrayObj;
			}
			
			return null;
		}
		
		public static function tratarMensagemError(value:String):String
		{
			return value.replace("java.lang.Exception :", "").replace("br.com.desenv.nepalign.framework.DsvException :", "");
		}
		
		public static function get formatadorDinheiro():NumberFormatter
		{
			var moneyFormat:NumberFormatter = new NumberFormatter("pt-br");
			
			return moneyFormat;
		}
		
		public static function limitByStringSize(text:String, size:Number):String
		{
			if(text == null)
				return "";
			
			var field:TextField = new TextField();
			field.text = text;
			
			if(field.textWidth > size)
			{
				while(field.textWidth > size)
				{
					text = text.substr(0, text.length - 1);
					field.text = text;
				}
				
				text += "...";
			}
			
			field.text = text;
			
			return field.text;
		}
		
		public static function getStringWidth(text:String):Number
		{
			if(text == null)
				return 0;
			
			var field:TextField = new TextField();
			field.text = text;
			
			return field.textWidth;
		}
		
		public static function cloneArray(array1:ArrayCollection, array2:ArrayCollection):ArrayCollection
		{
			array2.removeAll();
			
			for each(var obj:* in array1)
			{
				array2.addItem(obj);
			}
			
			return array2;
		}
		
		public static function round(number:Number, precision:int):Number 
		{
			precision = Math.pow(10, precision);
			return Math.round(number * precision)/precision;
		}
		
		public static function beep(lengthInMilliSeconds:Number):void
		{
			var mySound:Sound = new Sound();
			
			mySound.addEventListener(SampleDataEvent.SAMPLE_DATA, function (event:SampleDataEvent):void 
			{
				for (var c:int = 0; c < 8192; c++) 
				{
					event.data.writeFloat(Math.sin((Number(c+event.position)/Math.PI/2))*0.25);
					event.data.writeFloat(Math.sin((Number(c+event.position)/Math.PI/2))*0.25);
				}
			});
			 
			var soundChannel:SoundChannel = mySound.play();
			
			var timer:Timer = new Timer(lengthInMilliSeconds);
			
			timer.addEventListener(TimerEvent.TIMER, function(event:TimerEvent):void
			{
				soundChannel.stop();	
				timer.stop();
			});
			
			timer.start();
		}
		
		public static function readTextFile(path:String):String
		{
			try
			{
				var value:String = "";
				
				var file:File = File.documentsDirectory; 
				file = file.resolvePath(path); 
				
				var fileStream:FileStream = new FileStream(); 
				fileStream.open(file, FileMode.READ);
				
				value = fileStream.readUTFBytes(fileStream.bytesAvailable);
				
				fileStream.close();
				return value;
			}
			catch(e:Error)
			{
				trace(e.toString());
				return null;
			}
			return "";
		}
		
		public static function writeTextFile(file:File, data:String):Boolean
		{
			try
			{
				var ___b:ByteArray = new ByteArray();
				// Include the byte order mark for UTF-8
				___b.writeByte(0x000000EF);
				___b.writeByte(0x000000BB);
				___b.writeByte(0x000000BF);
				// end byte order mark for UTF-8
				___b.writeUTFBytes(data);
				
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.APPEND);
				stream.writeBytes(___b, 0x0000000, ___b.length);
				stream.close();
				
				return true;
			}
			catch(e:Error)
			{
				trace(e.toString());
			}
			
			return false;
		}
		
		public static function getScreenWidth():Number
		{
			return new SystemManager().screen.width;
		}
		
		public static function getAppVersion():String 
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0];
			return appVersion;
		}
		
		public static function getSupportedProfiles():String 
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var profiles:String = appXml.ns::supportedProfiles[0];
			return profiles;
		}
		
		public static function startWith(startStr:String, str:String):Boolean
		{
			return str.substring(0, startStr.length) == startStr;
		}
		
		public static function arrayCollectionSort(ar:ArrayCollection, fieldName:String, isNumeric:Boolean):void 
		{
			var dataSortField:SortField = new SortField();
			dataSortField.name = fieldName;
			dataSortField.numeric = isNumeric;
			var numericDataSort:Sort = new Sort();
			numericDataSort.fields = [dataSortField];
			ar.sort = numericDataSort;
			ar.refresh();
		}
		
		public function checkNetworkActive():void
		{
			var urlServerCheckInternet:String = "";
			var serverIp:String = (FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("IP_SERVIDOR") as String;
			
			if(serverIp == null)
			{
				if((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDOR_VERIFICACAO_INTERNET") != null &&
					((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDOR_VERIFICACAO_INTERNET") as String) != "")
					serverIp = ((FlexGlobals.topLevelApplication.parametrosSistema as DsvParametrosSistema).getParametro("URL_SERVIDOR_VERIFICACAO_INTERNET") as String).split("/")[0x00002];
				else
				{
					networkActive = false;
					dispatchCheckNetworkErrorEvent();
					return;
				}
			}
			
			urlServerCheckInternet = "http://" + serverIp + "/smartsig/messagebroker/amf/";
			
			if(FlexGlobals.topLevelApplication.verbose)
				sysout("Checking Network Status... URL [" + urlServerCheckInternet + "]");
			
			var urlRequest:URLRequest = new URLRequest(urlServerCheckInternet);
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, function(event:HTTPStatusEvent):void
			{
				if(event.status == 200)
				{
					networkActive = true;
					dispatchCheckNetworkSuccessEvent();
				}
				else
				{
					networkActive = false;
					dispatchCheckNetworkErrorEvent();
				}
				
				event.currentTarget.removeEventListener(Event.COMPLETE, arguments.callee);
				event.currentTarget.removeEventListener(IOErrorEvent.IO_ERROR, arguments.callee);
				event.currentTarget.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, arguments.callee);
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, loader_error);
			
			try
			{
				loader.load(urlRequest);
			}
			catch (e:Error)
			{
				networkActive = false;
				dispatchCheckNetworkErrorEvent();
			}
		}
		
		private function loader_error(event:IOErrorEvent):void
		{
			try
			{
				var loader:URLLoader = URLLoader( event.target );
				loader.close();
			}
			catch(e:Error)
			{
				
			}
			
			loader.removeEventListener(IOErrorEvent.IO_ERROR, loader_error);
			
			networkActive = false;
			dispatchCheckNetworkErrorEvent();
		}
		
		private function dispatchCheckNetworkSuccessEvent():void
		{
			dispatchEvent(new Event(CHECK_NETWORK_EVENT_SUCCESS));
		}
		
		private function dispatchCheckNetworkErrorEvent():void
		{
			dispatchEvent(new Event(CHECK_NETWORK_EVENT_ERROR));
		}
		
		public static function convertToASCII(data:String):String
		{
			var output:String= "";
			
			var arry:Array = data.split("");
			
			for(var i:String in arry)
				output += ("%" + arry[i].charCodeAt(0x0).toString(0x10));
			
			return output;
		}
		
		public static function formatDate(item:Object,column:DataGridColumn):String
		{
			var dateformatter:DateFormatter = new DateFormatter();
			
			dateformatter.formatString = "DD/MM/YYYY";
			return dateformatter.format(item[column.dataField]);
		}
	}	
}