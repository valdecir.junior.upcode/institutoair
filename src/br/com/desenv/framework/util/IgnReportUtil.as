package br.com.desenv.framework.util
{
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.net.sendToURL;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.formatters.DateFormatter;
	import mx.utils.Base64Encoder;

	public class IgnReportUtil
	{
		public function gerarRelatorioSeguranca(url:String, window:String, param:URLVariables):void
		{
			try
			{
				var strUserSession:String;
				var idEncoder:Base64Encoder = new Base64Encoder();
				
				var dtFPadrao:DateFormatter = new DateFormatter();
				dtFPadrao.formatString = "YYYYMMDDHHNN";
				
				strUserSession = FlexGlobals.topLevelApplication.sessionInfo.usuario.id + "|$|" + FlexGlobals.topLevelApplication.sessionInfo.usuario.login + "|$|" + FlexGlobals.topLevelApplication.sessionInfo.uae;
				
				idEncoder.encode(dtFPadrao.format(new Date()) + "=" + strUserSession + "|$|");
				
				var urlReq:URLRequest = new URLRequest();
				
				if (url==null)
					urlReq.url = FlexGlobals.topLevelApplication.parametrosSistema.getParametro("URL_SERVLET_RELATORIOSEGURO");
				else
					urlReq.url = url;
				
				param.idreportvalue = idEncoder.toString();
				urlReq.data = param;
				navigateToURL(urlReq);	
			}
			catch(e:Error)
			{
				Alert.show("Não foi possível abrir o relatório!", "Mensagem");
				syserr(e);
			}
		}
	}
}